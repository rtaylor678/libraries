﻿using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.X509Certificates;

namespace Solomon.Common.Cryptography
{
	/// <summary>
	/// Question:	How do you know where the data came from?
	/// Objective:	Verify the message is sent from a specific sender.
	/// Implies:	Signing with sender's private key, validating with sender's public key.
	/// </summary>
	public static class Authenticity
	{
		/// <summary>
		/// Signs a message using a sender's private key.
		/// </summary>
		/// <param name="plainMessage"></param>
		/// <param name="privateCertificate"></param>
		/// <param name="signedMessage"></param>
		/// <returns>Success or failure of the signing; Zero byte array is created if signing fails.</returns>
		/// <remarks>The private key's certificate chain must be valid. For self-signed certificates, the issuing authority public certificate must be installed.</remarks>
		public static bool Sign(byte[] plainMessage, X509Certificate2 privateCertificate, out byte[] signedMessage)
		{
			try
			{
				if (privateCertificate.HasPrivateKey)
				{
					ContentInfo contentInfo = new ContentInfo(plainMessage);
					SignedCms signedCms = new SignedCms(contentInfo, false);
					CmsSigner cmsSigner = new CmsSigner(privateCertificate);
					signedCms.ComputeSignature(cmsSigner);
					signedMessage = signedCms.Encode();
					return true;
				}
				else
				{
					signedMessage = new byte[0];
					return false;
				}
			}
			catch
			{
				signedMessage = new byte[0];
				return false;
			}
		}

		/// <summary>
		/// Verifies a signed message using the sender's public certificate; the certificate can be validated for revocation
		/// </summary>
		/// <param name="signedMessage"></param>
		/// <param name="publicCertificate"></param>
		/// <param name="checkSignatureOnly">If true, checks only signature; if false, checks revocation.</param>
		/// <param name="plainMessage"></param>
		/// <returns>Success or failure of the signature verification; Zero byte array is created if verification fails.</returns>
		public static bool Verify(byte[] signedMessage, X509Certificate2 publicCertificate, bool checkSignatureOnly, out byte[] plainMessage)
		{
			try
			{
				SignedCms signedCms = new SignedCms();
				signedCms.Decode(signedMessage);

				if (signedCms.Certificates.Contains(publicCertificate))
				{
					signedCms.CheckSignature(checkSignatureOnly);
					plainMessage = signedCms.ContentInfo.Content;
					return true;
				}
				else
				{
					plainMessage = new byte[0];
					return false;
				}
			}
			catch
			{
				plainMessage = new byte[0];
				return false;
			}
		}
	}
}