﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Solomon.Common.Cryptography")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("HSB Solomon Associates LLC")]
[assembly: AssemblyProduct("Solomon.Common.Cryptography")]
[assembly: AssemblyCopyright("© 2015 HSB Solomon Associates LLC")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("1357a8b4-1d83-45f5-a58e-4f0baf7fd2b7")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]

[assembly: System.Runtime.CompilerServices.InternalsVisibleToAttribute("Solomon.Common.CryptographyTest, PublicKey=0024000004800000140200000602000000240000525341310010000001000100e7ea1c7dba996971aada9b7f6cb395f2ecdfbfbefc9d02ed75d7dba0bc49f5e4de4814e0b8341d260f2869d3b238cfa69fee7eff137a52267e3d9169a3ea1eeb3e9c5a129997be431946691abc1779fbde1f6e1b13835331a9cd865a89988c197841ca8b66f975860d3423b128a920b79c7be55f0590c9606e6b626f29b7c3dc21c23bdd14e115f17c2d25f46492db69850b0f73e1289384df60b503cd347e290f575f5e5f35cc9e0cb0848074b5c73a1750f95230778de5810ca1b8dd1eb90d69b0d6b00acca026758d0df0c96d467bfedbc6517220659da5549e795e6bf74ed723c18aca8c5bdf87f3c468fdeb9ee6ceff69947e32725685ada4b7d4f24aae81a0842fafa559a439e90eb820e70b95abbea62fc4069916d56fad95a2cd65435d949db4ba47f33df83e62bebbfd5f62e990786fa4dbd61aed6af4cd7a68f3e390335ff13ee0b1c3273f7a1ee86178fa466dc36c92f68c481e2dbe1c35e0a88165bcf191b3556baae4419a9cd952aec7a76ae0f4b2bd438e29c6c6fa06448172b07c62180c477d435c6bd0bec2ff82d69532bebf8e3340b0a5e4f9d0f8d9ecab34f03510f2ffb110e0cf300102cdcce94f6120710ae00474673a6a23dd4d05a8611c121fbc90c6fdbe04649cf4d5354ddd212fb5994df4fd9c8aa65ce8210aa110999e80e25ae251f0768a5416d994acbb953b53091d4b285df5c923e5d18ad3")]