﻿using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.X509Certificates;

namespace Solomon.Common.Cryptography
{
	/// <summary>
	/// Question:	Who should have access to the data?
	/// Objective:	Verify the message is readable by a specific receiver.
	/// Implies:	Encoding with receiver's public key; decoding with receiver's private key.
	/// </summary>
	public static class Confidentiality
	{
		/// <summary>
		/// Encodes a message using recipient's public key.
		/// </summary>
		/// <param name="plainMessage"></param>
		/// <param name="publicCertificate"></param>
		/// <param name="encodedMessage"></param>
		/// <returns>Success or failure of the encoding; Zero byte array is created if encoding fails.</returns>
		public static bool Encode(byte[] plainMessage, X509Certificate2 publicCertificate, out byte[] encodedMessage)
		{
			try
			{
				ContentInfo contentInfo = new ContentInfo(plainMessage);
				EnvelopedCms envelopedCms = new EnvelopedCms(contentInfo);
				CmsRecipient recipient = new CmsRecipient(SubjectIdentifierType.IssuerAndSerialNumber, publicCertificate);
				envelopedCms.Encrypt(recipient);
				encodedMessage = envelopedCms.Encode();
				return true;
			}
			catch
			{
				encodedMessage = new byte[0];
				return false;
			}
		}

		/// <summary>
		/// Encodes a message using multiple recipient's public keys.
		/// </summary>
		/// <param name="plainMessage"></param>
		/// <param name="publicCertificates"></param>
		/// <param name="encodedMessage"></param>
		/// <returns>Success or failure of the encoding; Zero byte array is created if encoding fails.</returns>
		public static bool Encode(byte[] plainMessage, X509Certificate2Collection publicCertificates, out byte[] encodedMessage)
		{
			try
			{
				ContentInfo contentInfo = new ContentInfo(plainMessage);
				EnvelopedCms envelopedCms = new EnvelopedCms(contentInfo);
				CmsRecipientCollection recipient = new CmsRecipientCollection(SubjectIdentifierType.IssuerAndSerialNumber, publicCertificates);
				envelopedCms.Encrypt(recipient);
				encodedMessage = envelopedCms.Encode();
				return true;
			}
			catch
			{
				encodedMessage = new byte[0];
				return false;
			}
		}

		/// <summary>
		/// Decodes a message using a recipient's private key installed in the Personal Store.
		/// </summary>
		/// <param name="encodedMessage"></param>
		/// <param name="plainMessage"></param>
		/// <returns>Success or failure of the decoding; Zero byte array is created if decoding fails.</returns>
		/// <remarks>The private key must installed in the Personal Store on the receiving computer.</remarks>
		public static bool Decode(byte[] encodedMessage, out byte[] plainMessage)
		{
			try
			{
				EnvelopedCms envelopedCms = new EnvelopedCms();
				envelopedCms.Decode(encodedMessage);
				envelopedCms.Decrypt();
				plainMessage = envelopedCms.Encode();
				return true;
			}
			catch
			{
				plainMessage = new byte[0];
				return false;
			}
		}
	
		//public static bool DecodeMultiple(byte[] envelope, out byte[] message)
		//{
		//	try
		//	{
		//		EnvelopedCms envelopedCms = new EnvelopedCms();
		//		envelopedCms.Decode(envelope);

		//		foreach (RecipientInfo ri in envelopedCms.RecipientInfos)
		//		{
		//			envelopedCms.Decrypt(ri);
		//			message = envelopedCms.Encode();
		//			return true;
		//		}

		//		message = new byte[0];
		//		return false;
		//	}
		//	catch
		//	{
		//		message = new byte[0];
		//		return false;
		//	}
		//}
	}
}