﻿using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;

namespace Solomon.Common.Cryptography
{
	/// <summary>
	/// Objective:	Install and find certificates in the approiate Certificate Store (Defaults to Local Machine Store Location).
	/// </summary>
	public static class Certificate
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="storeName"></param>
		/// <param name="certificate"></param>
		/// <returns>Success or failure of installing a certificate.</returns>
		internal static bool Install(StoreName storeName, X509Certificate2 certificate)
		{
			try
			{
				using (X509Store store = new X509Store(storeName, StoreLocation.LocalMachine))
				{
					store.Open(OpenFlags.ReadWrite);
					store.Add(certificate);
					store.Close();
				}
				return true;
			}
			catch
			{
				return false;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="storeName"></param>
		/// <param name="commonName">CN portion of the certificate identification.</param>
		/// <param name="certificate"></param>
		/// <returns>Success or failure of finding a certificate.</returns>
		internal static bool Find(StoreName storeName, string commonName, out X509Certificate2 certificate)
		{
			try
			{
				using (X509Store store = new X509Store(storeName, StoreLocation.LocalMachine))
				{
					store.Open(OpenFlags.ReadOnly);
					X509Certificate2Collection certColl = store.Certificates.Find(X509FindType.FindBySubjectName, commonName, true);
					store.Close();

					if (certColl.Count == 1)
					{
						certificate = certColl[0];
						certColl.Clear();
						return true;
					}
					else
					{
						certificate = new X509Certificate2();
						certColl.Clear();
						return false;
					}
				}
			}
			catch
			{
				certificate = new X509Certificate2();
				return false;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="storeName"></param>
		/// <param name="certificate"></param>
		/// <returns>Success or failure of removing a certificate.</returns>
		internal static bool Remove(StoreName storeName, X509Certificate2 certificate)
		{
			try
			{
				using (X509Store store = new X509Store(storeName, StoreLocation.LocalMachine))
				{
					store.Open(OpenFlags.ReadWrite);
					store.Remove(certificate);
					store.Close();
				}
				return true;
			}
			catch
			{
				return false;
			}
		}

		/// <summary>
		/// Installs a public key to the third-party certificate authority store on the local machine.
		/// </summary>
		/// <param name="path"></param>
		/// <returns>Success or failure of installing a certificate.</returns>
		public static bool InstallToAuthRoot(string path)
		{
			return Certificate.Install(StoreName.AuthRoot, new X509Certificate2(path));
		}

		/// <summary>
		/// Installs a private key to the personal store on the local machine.
		/// </summary>
		/// <param name="path"></param>
		/// <param name="password"></param>
		/// <returns>Success or failure of installing a certificate.</returns>
		public static bool InstallToPersonal(string path, string password)
		{
			return Certificate.Install(StoreName.My, new X509Certificate2(path, password));
		}

		/// <summary>
		/// Installs a public key to the trusted people store on the local machine.
		/// </summary>
		/// <param name="path"></param>
		/// <returns>Success or failure of installing a certificate.</returns>
		public static bool InstallToTrusted(string path)
		{
			return Certificate.Install(StoreName.TrustedPeople, new X509Certificate2(path));
		}

		/// <summary>
		/// Finds a certificate in the personal store on the local machine.
		/// </summary>
		/// <param name="commonName">CN portion of the certificate identification.</param>
		/// <param name="certificate"></param>
		/// <returns>Success or failure of finding a certificate.</returns>
		public static bool FindInPersonal(string commonName, out X509Certificate2 certificate)
		{
			return Certificate.Find(StoreName.My, commonName, out certificate);
		}

		/// <summary>
		/// Finds a certificate in the trusted people store on the local machine.
		/// </summary>
		/// <param name="commonName">CN portion of the certificate identification.</param>
		/// <param name="certificate"></param>
		/// <returns>Success or failure of finding a certificate.</returns>
		public static bool FindInTrusted(string commonName, out X509Certificate2 certificate)
		{
			return Certificate.Find(StoreName.TrustedPeople, commonName, out certificate);
		}


		internal struct RSAAlgorithmParameters
		{
			internal int signatureLength;
			internal HashAlgorithmName hashAlgorithmName;
			internal RSASignaturePadding RSASignaturePadding;
			internal string serialNumber;
			internal string thumbprint;

			public RSAAlgorithmParameters(X509Certificate2 certificate)
			{
				this.signatureLength = 256;
				this.RSASignaturePadding = RSASignaturePadding.Pkcs1;
				this.serialNumber = certificate.SerialNumber;
				this.thumbprint = certificate.Thumbprint;

				switch (certificate.SignatureAlgorithm.FriendlyName)
				{
					case "sha512RSA":
						this.hashAlgorithmName = HashAlgorithmName.SHA512;
						break;
					case "sha256RSA":
						this.hashAlgorithmName = HashAlgorithmName.SHA256;
						break;
					case "sha1RSA":
						this.hashAlgorithmName = HashAlgorithmName.SHA256;
						break;
					default:
						this.hashAlgorithmName = HashAlgorithmName.SHA512;
						break;
				}
			}
		}
	}
}