﻿using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;

namespace Solomon.Common.Cryptography
{
	/// <summary>
	/// Question:	How do you know if the data has been altered?
	/// Objective:	Verify the message has not been altered during transmission.
	/// </summary>
	public static class Integrity
	{
		/// <summary>
		/// Objective:	Sign and verify a message to be sent from one party to another using a shared secret integrity key.
		/// Implies:	Two parties share the same secret integrity key to sign and verify the message.
		/// </summary>
		/// sharedIntegrityKey SHOULD be asymmetrically encrypted; the key will be encrypted, shared, then decrypted.
		/// sharedIntegrityKey MUST shared 'out-of' band.
		/// <remarks>Utilizes FIPS compliant one-way keyed HMAC algorithm.</remarks>
		public static class Symmetric
		{
			/// <summary>
			/// Adds a signature to the data using a shared integrity key to prevent altering.
			/// </summary>
			/// <param name="unsignedMessage"></param>
			/// <param name="sharedIntegrityKey">Must be at least 8 bytes</param>
			/// <param name="signedMessage"></param>
			/// <returns>Success or failure of the signing; Zero byte array is created if signing fails.</returns>
			public static bool Sign(byte[] unsignedMessage, byte[] sharedIntegrityKey, out byte[] signedMessage)
			{
				try
				{
					if (sharedIntegrityKey.Length >= 8)
					{
						using (HMACSHA512 hmac = new HMACSHA512(sharedIntegrityKey))
						{
							byte[] signature = hmac.ComputeHash(unsignedMessage);
							signedMessage = signature.Concat(unsignedMessage).ToArray();
							hmac.Clear();
							return true;
						}
					}
					else
					{
						signedMessage = new byte[0];
						return false;
					}
				}
				catch
				{
					signedMessage = new byte[0];
					return false;
				}
			}

			/// <summary>
			/// Verifies the raw data signed by the shared integrity key matches the sent raw data.
			/// </summary>
			/// <param name="signedMessage"></param>
			/// <param name="sharedIntegrityKey">Must be at least 8 bytes</param>
			/// <param name="unsignedMessage"></param>
			/// <returns>Success or failure of the verification; Zero byte array is created if verification fails.</returns>
			public static bool Verify(byte[] signedMessage, byte[] sharedIntegrityKey, out byte[] unsignedMessage)
			{
				try
				{
					const int hashLength = 64;

					byte[] sentMessage;
					Split.Left(signedMessage, hashLength, out sentMessage);

					byte[] testMessage;
					if (Symmetric.Sign(sentMessage, sharedIntegrityKey, out testMessage) && signedMessage.SequenceEqual(testMessage))
					{
						unsignedMessage = sentMessage;
						return true;
					}
					else
					{
						unsignedMessage = new byte[0]; ;
						return false;
					}
				}
				catch
				{
					unsignedMessage = new byte[0]; ;
					return false;
				}
			}
		}

		/// <summary>
		/// Objective:	Sign and verify a message to be sent from one party to another using public-private key pairs.
		/// Implies:	Sign with sender's private key; verify with sender's public key.
		/// </summary>
		/// <remarks>Signs the message and signed message hash.</remarks>
		public static class Asymmetric
		{
			/// <summary>
			/// Includes a message and hash signature using private key to the data to prevent altering.
			/// </summary>
			/// <param name="unsignedMessage"></param>
			/// <param name="privateCertificate"></param>
			/// <param name="signedHashedMessage"></param>
			/// <returns>Success or failure of the signing; Zero byte array is created if signing fails.</returns>
			public static bool Sign(byte[] unsignedMessage, X509Certificate2 privateCertificate, out byte[] signedHashedMessage)
			{
				try
				{
					if (privateCertificate.HasPrivateKey)
					{
						using (RSACryptoServiceProvider rsa = (RSACryptoServiceProvider)privateCertificate.PrivateKey)
						{
							using (SHA512Managed sha = new SHA512Managed())
							{
								byte[] messageSignature = rsa.SignData(unsignedMessage, HashAlgorithmName.SHA512, RSASignaturePadding.Pkcs1);
								byte[] signedMessage = messageSignature.Concat(unsignedMessage).ToArray();

								byte[] hash = sha.ComputeHash(signedMessage);
								byte[] hashSignature = rsa.SignHash(hash, HashAlgorithmName.SHA512, RSASignaturePadding.Pkcs1);

								signedHashedMessage = hashSignature.Concat(signedMessage).ToArray();
								sha.Clear();
								rsa.Clear();
								return true;
							}
						}
					}
					else
					{
						signedHashedMessage = new byte[0];
						return false;
					}
				}
				catch
				{
					signedHashedMessage = new byte[0];
					return false;
				}
			}

			/// <summary>
			/// Verifies the signed and hashed message has not been altered.
			/// </summary>
			/// <param name="signedHashedMessage"></param>
			/// <param name="publicCertificate"></param>
			/// <param name="unsignedMessage"></param>
			/// <returns>Success or failure of the verification; Zero byte array is created if verification fails.</returns>
			public static bool Verify(byte[] signedHashedMessage, X509Certificate2 publicCertificate, out byte[] unsignedMessage)
			{
				try
				{
					using (RSACryptoServiceProvider rsa = (RSACryptoServiceProvider)publicCertificate.PublicKey.Key)
					{
						using (SHA512Managed sha = new SHA512Managed())
						{
							const int signatureLength = 256;

							byte[] signedMessage;
							byte[] hashSignature = Split.Left(signedHashedMessage, signatureLength, out signedMessage);

							byte[] hash = sha.ComputeHash(signedMessage);

							byte[] sentMessage;
							byte[] messageSignature = Split.Left(signedMessage, signatureLength, out sentMessage);

							sha.Clear();

							if (rsa.VerifyHash(hash, hashSignature, HashAlgorithmName.SHA512, RSASignaturePadding.Pkcs1)
								&& rsa.VerifyData(sentMessage, messageSignature, HashAlgorithmName.SHA512, RSASignaturePadding.Pkcs1))
							{
								unsignedMessage = sentMessage;
								rsa.Clear();
								return true;
							}
							else
							{
								unsignedMessage = new byte[0];
								rsa.Clear();
								return false;
							}
						}
					}
				}
				catch
				{
					unsignedMessage = new byte[0];
					return false;
				}
			}
		}
	}
}