﻿using System.Linq;
using System.Security.Cryptography.X509Certificates;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Solomon.Common.Cryptography
{
	public partial class PrivacyTest
	{
		[TestClass]
		public class SymmetricTest
		{
			const string messageText = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
			byte[] messageOriginal = System.Text.Encoding.ASCII.GetBytes(messageText);

			byte[] encPassword = System.IO.File.ReadAllBytes("Rfc\\PrivacyKey.txt");
			byte[] encSalt = System.IO.File.ReadAllBytes("Rfc\\PrivacySalt.txt");

			[TestMethod]
			public void PrivacySymmetricChain32ByteKey()
			{
				byte[] encKey;
				Assert.IsTrue(Password.Derive(encPassword, encSalt, 32, out encKey), "Creating encryption key failed.");

				byte[] messageEncrypted;
				Assert.IsTrue(Privacy.Symmetric.Encrypt(messageOriginal, encKey, out messageEncrypted), "Encrypting message failed.");

				byte[] messageDecrypted;
				Assert.IsTrue(Privacy.Symmetric.Decrypt(messageEncrypted, encKey, out messageDecrypted), "Decrypting message failed.");

				Assert.IsTrue(messageOriginal.SequenceEqual(messageDecrypted), "Messages are not equal.");
			}

			[TestMethod]
			public void PrivacySymmetricChain24ByteKey()
			{
				byte[] encKey;
				Assert.IsTrue(Password.Derive(encPassword, encSalt, 24, out encKey), "Creating encryption key failed.");

				byte[] messageEncrypted;
				Assert.IsTrue(Privacy.Symmetric.Encrypt(messageOriginal, encKey, out messageEncrypted), "Encrypting message failed.");

				byte[] messageDecrypted;
				Assert.IsTrue(Privacy.Symmetric.Decrypt(messageEncrypted, encKey, out messageDecrypted), "Decrypting message failed.");

				Assert.IsTrue(messageOriginal.SequenceEqual(messageDecrypted), "Messages are not equal.");
			}

			[TestMethod]
			public void PrivacySymmetricChain16ByteKey()
			{
				byte[] encKey;
				Assert.IsTrue(Password.Derive(encPassword, encSalt, 16, out encKey), "Creating encryption key failed.");

				byte[] messageEncrypted;
				Assert.IsTrue(Privacy.Symmetric.Encrypt(messageOriginal, encKey, out messageEncrypted), "Encrypting message failed.");

				byte[] messageDecrypted;
				Assert.IsTrue(Privacy.Symmetric.Decrypt(messageEncrypted, encKey, out messageDecrypted), "Decrypting message failed.");

				Assert.IsTrue(messageOriginal.SequenceEqual(messageDecrypted), "Messages are not equal.");
			}

			[TestMethod]
			public void PrivacySymmetricInvalidDecryption()
			{
				byte[] encKey;
				Assert.IsTrue(Password.Derive(encPassword, encSalt, 16, out encKey), "Creating encryption key failed.");

				byte[] messageEncrypted;
				Assert.IsTrue(Privacy.Symmetric.Encrypt(messageOriginal, encKey, out messageEncrypted), "Encrypting message failed.");

				byte[] decKey;
				Assert.IsTrue(Password.Derive(encPassword, encSalt, 24, out decKey), "Creating decryption key failed.");

				byte[] messageDecrypted;
				Assert.IsFalse(Privacy.Symmetric.Decrypt(messageEncrypted, decKey, out messageDecrypted), "Decrypting message failed.");

				Assert.IsFalse(messageOriginal.SequenceEqual(messageDecrypted), "Messages shuold not be equal.");
			}

			[TestMethod]
			public void PrivacySymmetricInvalidKey()
			{
				byte[] encKey;
				Assert.IsTrue(Password.Derive(encPassword, encSalt, 64, out encKey), "Creating password failed.");

				byte[] messageEncrypted;
				Assert.IsFalse(Privacy.Symmetric.Encrypt(messageOriginal, encKey, out messageEncrypted), "Encrypting message did not fail.");

				Assert.IsTrue(messageEncrypted.Length == 0, "Encrypted message length is not 0.");
			}
		}

		[TestClass]
		public class AsymmetricTest
		{
			const string messageText = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea";// commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
			byte[] messageOriginal = System.Text.Encoding.ASCII.GetBytes(messageText);

			[TestMethod]
			public void PrivacyAsymmetricChain_SHA512()
			{
				byte[] messageEncrypted;
				using (X509Certificate2 certPublic = new X509Certificate2(@"SHA_2\512 Sha Privacy.cer"))
				{
					Assert.IsTrue(Privacy.Asymmetric.Encrypt(messageOriginal, certPublic, out messageEncrypted), "Encrypting message failed.");
				}

				byte[] messageDecrypted;
				using (X509Certificate2 certPrivate = new X509Certificate2(@"SHA_2\512 Sha Privacy.pfx", "A6CF91CB-58C6-4E92-A34C-0DF0584B"))
				{
					Assert.IsTrue(Privacy.Asymmetric.Decrypt(messageEncrypted, certPrivate, out messageDecrypted), "Decrypting message failed.");
				}

				Assert.IsTrue(messageOriginal.SequenceEqual(messageDecrypted), "Messages are not equal.");
			}

			[TestMethod]
			public void PrivacyAsymmetricChainPrivatePair_SHA512()
			{
				byte[] messageEncrypted;
				using (X509Certificate2 certPublic = new X509Certificate2(@"SHA_2\512 Sha Privacy.pfx", "A6CF91CB-58C6-4E92-A34C-0DF0584B"))
				{
					Assert.IsTrue(Privacy.Asymmetric.Encrypt(messageOriginal, certPublic, out messageEncrypted), "Encrypting message failed.");
				}

				byte[] messageDecrypted;
				using (X509Certificate2 certPrivate = new X509Certificate2(@"SHA_2\512 Sha Privacy.pfx", "A6CF91CB-58C6-4E92-A34C-0DF0584B"))
				{
					Assert.IsTrue(Privacy.Asymmetric.Decrypt(messageEncrypted, certPrivate, out messageDecrypted), "Decrypting message failed.");
				}

				Assert.IsTrue(messageOriginal.SequenceEqual(messageDecrypted), "Messages are not equal.");
			}

			[TestMethod]
			public void PrivacyAsymmetricChainPublicDecryption_SHA512()
			{
				byte[] messageEncrypted;
				using (X509Certificate2 certPublic = new X509Certificate2(@"SHA_2\512 Sha Privacy.cer"))
				{
					Assert.IsTrue(Privacy.Asymmetric.Encrypt(messageOriginal, certPublic, out messageEncrypted), "Encrypting message failed.");
				}

				byte[] messageDecrypted;
				using (X509Certificate2 certPrivate = new X509Certificate2(@"SHA_2\512 Sha Privacy.cer"))
				{
					Assert.IsFalse(Privacy.Asymmetric.Decrypt(messageEncrypted, certPrivate, out messageDecrypted), "Decrypting message failed.");
				}

				Assert.IsTrue(messageDecrypted.Length == 0, "Decryption should have failed.");
				Assert.IsFalse(messageOriginal.SequenceEqual(messageDecrypted), "Messages should not equal.");
			}

			[TestMethod]
			public void PrivacyAsymmetricInvalidDecryption_SHA512()
			{
				byte[] messageEncrypted;
				using (X509Certificate2 certPublic = new X509Certificate2(@"SHA_2\512 Sha Privacy.cer"))
				{
				Assert.IsTrue(Privacy.Asymmetric.Encrypt(messageOriginal, certPublic, out messageEncrypted), "Encrypting message failed.");
				}

				byte[] messageDecrypted;
				using (X509Certificate2 certPrivate = new X509Certificate2(@"SHA_2\512 Sha Integrity.pfx", "3C0760F1-12B8-41AB-A231-86596C9E"))
				{
					Assert.IsFalse(Privacy.Asymmetric.Decrypt(messageEncrypted, certPrivate, out messageDecrypted), "Decrypting should have failed.");
				}

				Assert.IsTrue(messageDecrypted.Length == 0, "Privacy should have failed");
				Assert.IsFalse(messageOriginal.SequenceEqual(messageDecrypted), "Messages should not be equal.");
			}
			
			[TestMethod]
			public void PrivacyAsymmetricTooMuchData_SHA512()
			{
				byte[] messageToLong = new byte[216];
				byte[] messageEncrypted;

				using (X509Certificate2 certPublic = new X509Certificate2(@"SHA_2\512 Sha Privacy.cer"))
				{
					Assert.IsFalse(Privacy.Asymmetric.Encrypt(messageToLong, certPublic, out messageEncrypted), "Encrypting message should have failed.");
				}

				Assert.IsTrue(messageEncrypted.Length == 0, "Encrypted message length is not 0");
			}
		}
	}
}