﻿using System.Linq;
using System.Security.Cryptography.X509Certificates;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Solomon.Common.Cryptography
{
	public class IntegrityTest
	{
		[TestClass]
		public class SymmetricTest
		{
			const string messageText = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
			byte[] messageOriginal = System.Text.Encoding.ASCII.GetBytes(messageText);

			[TestMethod]
			public void IntegritySymmetricChain()
			{
				// Encrypt message
				byte[] hmacPassword = System.IO.File.ReadAllBytes("Rfc\\IntegrityKey.txt");
				byte[] hmacSalt = System.IO.File.ReadAllBytes("Rfc\\IntegritySalt.txt");

				byte[] hmacKey;
				Assert.IsTrue(Password.Derive(hmacPassword, hmacSalt, 64, out hmacKey), "Creating password failed.");

				byte[] messageSigned;
				Assert.IsTrue(Integrity.Symmetric.Sign(messageOriginal, hmacKey, out messageSigned), "Signing message failed.");

				byte[] messageVerified;
				Assert.IsTrue(Integrity.Symmetric.Verify(messageSigned, hmacKey, out messageVerified), "Verifying message failed.");

				Assert.IsTrue(messageOriginal.SequenceEqual(messageVerified), "Messages are not equal.");
			}

			[TestMethod]
			public void IntegritySymmetricInvalidVerification()
			{
				// Encrypt message
				byte[] hmacPassword = System.IO.File.ReadAllBytes("Rfc\\IntegrityKey.txt");
				byte[] hmacSalt = System.IO.File.ReadAllBytes("Rfc\\IntegritySalt.txt");

				byte[] hmacKeySign;
				Assert.IsTrue(Password.Derive(hmacPassword, hmacSalt, 64, out hmacKeySign), "Creating signing password failed.");

				byte[] messageSigned;
				Assert.IsTrue(Integrity.Symmetric.Sign(messageOriginal, hmacKeySign, out messageSigned), "Signing message failed.");

				byte[] hmacKeyVerify;
				Assert.IsTrue(Password.Derive(hmacPassword, hmacSalt, 32, out hmacKeyVerify), "Creating verification password failed.");

				byte[] messageVerified;
				Assert.IsFalse(Integrity.Symmetric.Verify(messageSigned, hmacKeyVerify, out messageVerified), "Message should fail verification.");

				Assert.IsFalse(messageOriginal.SequenceEqual(messageVerified), "Messages should not be equal");
			}

			[TestMethod]
			public void IntegritySymmetricInvalidKey_0Bytes()
			{
				byte[] hmacKey = new byte[0];

				byte[] messageSigned;

				Assert.IsFalse(Integrity.Symmetric.Sign(messageOriginal, hmacKey, out messageSigned), "Signing message did not fail.");

				Assert.IsTrue(messageSigned.Length == 0, "Verified message length was not 0");
			}
		}

		[TestClass]
		public class AsymmetricTest
		{
			const string messageText = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
			byte[] messageOriginal = System.Text.Encoding.ASCII.GetBytes(messageText);

			[TestMethod]
			public void IntegrityAsymmetricChain_Sha1()
			{
				byte[] messageSigned;
				using (X509Certificate2 certPrivate = new X509Certificate2(@"SHA_2\1 Sha Authenticity.pfx", "7829ED80-E843-4CE2-8AFB-67EB5BC3"))
				{
					Assert.IsTrue(Integrity.Asymmetric.Sign(messageOriginal, certPrivate, out messageSigned), "Signing message failed.");
				}

				byte[] messageUnsigned;
				using (X509Certificate2 certPublic = new X509Certificate2(@"SHA_2\1 Sha Authenticity.cer"))
				{
					Assert.IsTrue(Integrity.Asymmetric.Verify(messageSigned, certPublic, out messageUnsigned), "Verifying message signature failed.");
				}

				Assert.IsTrue(messageUnsigned.Length > 0, "Integrity chain failed");
				Assert.IsTrue(messageOriginal.SequenceEqual(messageUnsigned), "Messages are not equal.");
			}

			[TestMethod]
			public void IntegrityAsymmetricChain_Sha256()
			{
				byte[] messageSigned;
				using (X509Certificate2 certPrivate = new X509Certificate2(@"SHA_2\256 Sha Integrity.pfx", "3C0760F1-12B8-41AB-A231-86596C9E"))
				{
					Assert.IsTrue(Integrity.Asymmetric.Sign(messageOriginal, certPrivate, out messageSigned), "Signing message failed.");
				}

				byte[] messageUnsigned;
				using (X509Certificate2 certPublic = new X509Certificate2(@"SHA_2\256 Sha Integrity.cer"))
				{
					Assert.IsTrue(Integrity.Asymmetric.Verify(messageSigned, certPublic, out messageUnsigned), "Verifying message signature failed.");
				}

				Assert.IsTrue(messageUnsigned.Length > 0, "Integrity chain failed");
				Assert.IsTrue(messageOriginal.SequenceEqual(messageUnsigned), "Messages are not equal.");
			}

			[TestMethod]
			public void IntegrityAsymmetricChain_Sha512()
			{
				byte[] messageSigned;
				using (X509Certificate2 certPrivate = new X509Certificate2(@"SHA_2\512 Sha Integrity.pfx", "3C0760F1-12B8-41AB-A231-86596C9E"))
				{
					Assert.IsTrue(Integrity.Asymmetric.Sign(messageOriginal, certPrivate, out messageSigned), "Signing message failed.");
				}

				byte[] messageUnsigned;
				using (X509Certificate2 certPublic = new X509Certificate2(@"SHA_2\512 Sha Integrity.cer"))
				{
					Assert.IsTrue(Integrity.Asymmetric.Verify(messageSigned, certPublic, out messageUnsigned), "Verifying message signature failed.");
				}

				Assert.IsTrue(messageUnsigned.Length > 0, "Integrity chain failed");
				Assert.IsTrue(messageOriginal.SequenceEqual(messageUnsigned), "Messages are not equal.");
			}

			[TestMethod]
			public void IntegrityAsymmetricChainPrivatePair_Sha512()
			{
				byte[] messageSigned;
				using (X509Certificate2 certPrivate = new X509Certificate2(@"SHA_2\512 Sha Integrity.pfx", "3C0760F1-12B8-41AB-A231-86596C9E"))
				{
					Assert.IsTrue(Integrity.Asymmetric.Sign(messageOriginal, certPrivate, out messageSigned), "Signing message failed.");
				}

				byte[] messageUnsigned;
				using (X509Certificate2 certPublic = new X509Certificate2(@"SHA_2\512 Sha Integrity.pfx", "3C0760F1-12B8-41AB-A231-86596C9E"))
				{
					Assert.IsTrue(Integrity.Asymmetric.Verify(messageSigned, certPublic, out messageUnsigned), "Verifying message signature failed.");
				}

				Assert.IsTrue(messageUnsigned.Length > 0, "Integrity chain failed");
				Assert.IsTrue(messageOriginal.SequenceEqual(messageUnsigned), "Messages are not equal.");
			}

			[TestMethod]
			public void IntegrityAsymmetricChainPublicSign_Sha512()
			{
				byte[] messageSigned;
				using (X509Certificate2 certPublic = new X509Certificate2(@"SHA_2\512 Sha Integrity.cer"))
				{
					Assert.IsFalse(Integrity.Asymmetric.Sign(messageOriginal, certPublic, out messageSigned), "Signing message failed.");
				}

				Assert.IsTrue(messageSigned.Length == 0, "Signing should have failed.");
			}

			[TestMethod]
			public void IntegrityAsymmetricInvalidVerification_Sha512()
			{
				byte[] messageSigned;
				using (X509Certificate2 certPrivate = new X509Certificate2(@"SHA_2\512 Sha Integrity.pfx", "3C0760F1-12B8-41AB-A231-86596C9E"))
				{
					Assert.IsTrue(Integrity.Asymmetric.Sign(messageOriginal, certPrivate, out messageSigned), "Signing message failed.");
				}

				byte[] messageUnsigned;
				using (X509Certificate2 certPublic = new X509Certificate2(@"SHA_2\512 Sha Privacy.cer"))
				{
					Assert.IsFalse(Integrity.Asymmetric.Verify(messageSigned, certPublic, out messageUnsigned), "Verifying message signature failed.");
				}

				Assert.IsTrue(messageUnsigned.Length == 0, "Integrity should have failed");
				Assert.IsFalse(messageOriginal.SequenceEqual(messageUnsigned), "Messages are not equal.");
			}
		}
	}
}