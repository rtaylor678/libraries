Makecert:	https://msdn.microsoft.com/library/bfsktky3(v=vs.100).aspx
Store name:	https://msdn.microsoft.com/en-us/library/system.security.cryptography.x509certificates.storename.aspx
Providers:	https://msdn.microsoft.com/en-us/library/windows/desktop/bb931357(v=vs.85).aspx

OID:		https://support.microsoft.com/en-us/kb/287547
			https://msdn.microsoft.com/en-us/library/windows/desktop/aa378155.aspx

Public Key: https://msdn.microsoft.com/en-us/library/system.security.cryptography.x509certificates.publickey.oid(v=vs.110).aspx



The private keys are created then exported from their stores.

Certificate validity should be no more than 39 months.
-m 39

Should also tie the serial number to the client key.
-# 1234567890


Authority			5DDD226A-8431-4D9B-8FEB-466F73A3							OID_PKIX_KP_CLIENT_AUTH			1.3.6.1.5.5.7.3.2
																				OID_PKIX_KP_CODE_SIGNING		1.3.6.1.5.5.7.3.3

(Pkcs)				DEFAULT PASSWORD					SENDER	-> RECEIVER
Authenticity:		7829ED80-E843-4CE2-8AFB-67EB5BC3	Private -> Public 		OID_PKIX_KP_CLIENT_AUTH			1.3.6.1.5.5.7.3.2
Confidentiality:	E3BD99F7-84B2-4E4E-ABF9-083D1C47	Public	-> Private		OID_PKIX_KP_SERVER_AUTH			1.3.6.1.5.5.7.3.1	

(RSA)				DEFAULT PASSWORD					SENDER	-> RECEIVER
Integrity:			3C0760F1-12B8-41AB-A231-86596C9E	Private -> Public		OID_PKIX_KP_CODE_SIGNING		1.3.6.1.5.5.7.3.3
Privacy:			A6CF91CB-58C6-4E92-A34C-0DF0584B	Public	-> Private		OID_PKIX_KP_EMAIL_PROTECTION	1.3.6.1.5.5.7.3.4


OID_ANY_APPLICATION_POLICY		1.3.6.1.4.1.311.10.12.1
OID_LICENSES					1.3.6.1.4.1.311.10.6.1
OID_DRM							1.3.6.1.4.1.311.10.5.1


