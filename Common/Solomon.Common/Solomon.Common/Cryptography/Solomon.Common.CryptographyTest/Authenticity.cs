﻿using System.Linq;
using System.Security.Cryptography.X509Certificates;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Solomon.Common.Cryptography
{
	[TestClass]
	public class AuthenticityTest
	{
		const string messageText = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
		byte[] messageOriginal = System.Text.Encoding.ASCII.GetBytes(messageText);

		X509Certificate2 authCert = new X509Certificate2(@"SHA_2\512 Sha Authority.cer");
		X509Certificate2 senderPrivate = new X509Certificate2(@"SHA_2\512 Sha Authenticity.pfx", "7829ED80-E843-4CE2-8AFB-67EB5BC3");
		X509Certificate2 senderPublic = new X509Certificate2(@"SHA_2\512 Sha Authenticity.cer");

		[TestMethod]
		public void AuthenticityChain()
		{
			byte[] messageSigned;

			Assert.IsTrue(Certificate.Install(StoreName.AuthRoot, authCert));

			// Sign message
			Assert.IsTrue(Solomon.Common.Cryptography.Authenticity.Sign(messageOriginal, senderPrivate, out messageSigned), "Signing message failed.");

			Assert.IsTrue(Certificate.Remove(StoreName.AuthRoot, authCert));

			// Verify signature
			byte[] messageAuthenticated;
			Assert.IsTrue(Solomon.Common.Cryptography.Authenticity.Verify(messageSigned, senderPublic, true, out messageAuthenticated), "Veryfing signed messge failed.");

			//	Assert orignal message and authenticated message are the same.
			Assert.IsTrue(messageOriginal.SequenceEqual(messageAuthenticated), "Messages are not equal.");
		}

		[TestMethod]
		public void AuthenticityInvalidRecipient()
		{
			X509Certificate2 senderPublicInvalid = new X509Certificate2(@"SHA_2\512 Sha Privacy.cer");

			byte[] messageSigned;

			Assert.IsTrue(Certificate.Install(StoreName.AuthRoot, authCert));

			// Sign message
			Assert.IsTrue(Solomon.Common.Cryptography.Authenticity.Sign(messageOriginal, senderPrivate, out messageSigned), "Signing message failed.");

			Assert.IsTrue(Certificate.Remove(StoreName.AuthRoot, authCert));

			// Verify signature (invalid receiver)
			byte[] messageFail;
			Assert.IsFalse(Solomon.Common.Cryptography.Authenticity.Verify(messageSigned, senderPublicInvalid, true, out messageFail), "Veryfing signed messge did not fail.");

			Assert.IsFalse(messageOriginal.SequenceEqual(messageFail), "Messages should not be equal.");
		}

		[TestMethod]
		public void AuthenticitySignerValid()
		{
			Assert.IsTrue(Certificate.Install(StoreName.AuthRoot, authCert));

			// Sign message
			byte[] messageSigned;
			Assert.IsTrue(Solomon.Common.Cryptography.Authenticity.Sign(messageOriginal, senderPrivate, out messageSigned), "Signing message failed.");
				
			Assert.IsTrue(Certificate.Remove(StoreName.AuthRoot, authCert));

			Assert.IsTrue(messageSigned.Length > 0, "Message should be zero length.");
		}

		[TestMethod]
		public void AuthenticitySignerInvalid()
		{
			// Sign message (invalid signer; no authority certificate installed)
			byte[] messageSigned;
			Assert.IsFalse(Solomon.Common.Cryptography.Authenticity.Sign(messageOriginal, senderPrivate, out messageSigned), "Signing message did not fail.");

			Assert.IsTrue(messageSigned.Length == 0, "Message should be zero length.");
		}
	}
}