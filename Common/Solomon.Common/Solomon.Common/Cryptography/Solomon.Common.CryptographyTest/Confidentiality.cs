﻿using System.Linq;
using System.Security.Cryptography.X509Certificates;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Solomon.Common.Cryptography
{
	[TestClass]
	public class ConfidentialityTest
	{
		const string messageText = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
		byte[] messageOriginal = System.Text.Encoding.ASCII.GetBytes(messageText);

		X509Certificate2 receiverPublic = new X509Certificate2(@"SHA_2\512 Sha Confidentiality.cer");
		X509Certificate2 receiverPrivate = new X509Certificate2(@"SHA_2\512 Sha Confidentiality.pfx", "E3BD99F7-84B2-4E4E-ABF9-083D1C47");

		[TestMethod]
		public void ConfidentialityChain()
		{
			// Encrypt message
			byte[] messageEncrypted;
			Assert.IsTrue(Confidentiality.Encode(messageOriginal, receiverPublic, out messageEncrypted), "Encoding message failed.");

			// Decrypt message
			Assert.IsTrue(Certificate.Install(StoreName.My, receiverPrivate));

			byte[] messageDecrypted;
			Assert.IsTrue(Confidentiality.Decode(messageEncrypted, out messageDecrypted), "Decoding message failed.");

			Assert.IsTrue(Certificate.Remove(StoreName.My, receiverPrivate));

			//	Assert orignal message and decrypted message are the same.
			Assert.IsTrue(messageOriginal.SequenceEqual(messageDecrypted), "Messages are not equal.");
		}

		[TestMethod]
		public void ConfidentialityChainMultiple()
		{
			X509Certificate2Collection receiverMultiple = new X509Certificate2Collection();
			receiverMultiple.Add(new X509Certificate2(@"SHA_2\512 Sha Authenticity.cer"));
			receiverMultiple.Add(new X509Certificate2(@"SHA_2\512 Sha Confidentiality.cer"));
			receiverMultiple.Add(new X509Certificate2(@"SHA_2\512 Sha Integrity.cer"));
			receiverMultiple.Add(new X509Certificate2(@"SHA_2\512 Sha Privacy.cer"));

			byte[] messageEncrypted;
			Assert.IsTrue(Confidentiality.Encode(messageOriginal, receiverMultiple, out messageEncrypted), "Encoding message failed.");

			// Decrypt message
			Assert.IsTrue(Certificate.Install(StoreName.My, receiverPrivate));

			byte[] messageDecrypted;
			Assert.IsTrue(Confidentiality.Decode(messageEncrypted, out messageDecrypted), "Decoding message failed.");

			Assert.IsTrue(Certificate.Remove(StoreName.My, receiverPrivate));

			//	Assert orignal message and decrypted message are the same.
			Assert.IsTrue(messageOriginal.SequenceEqual(messageDecrypted), "Messages are not equal.");
		}

		[TestMethod]
		public void ConfidentialityChainInvalidRecipient()
		{
			X509Certificate2 receiverPrivateInvalid = new X509Certificate2(@"SHA_2\512 Sha Authenticity.pfx", "7829ED80-E843-4CE2-8AFB-67EB5BC3");

			// Encrypt message
			byte[] messageEncrypted;
			Assert.IsTrue(Confidentiality.Encode(messageOriginal, receiverPublic, out messageEncrypted), "Encoding message failed.");

			Assert.IsTrue(Certificate.Install(StoreName.My, receiverPrivateInvalid));

			byte[] messageDecrypted;
			Assert.IsFalse(Confidentiality.Decode(messageEncrypted, out messageDecrypted), "Decoding message should have failed.");

			Assert.IsTrue(Certificate.Remove(StoreName.My, receiverPrivateInvalid));

			//	Assert orignal message and decrypted message are not the same.
			Assert.IsFalse(messageOriginal.SequenceEqual(messageDecrypted), "Messages should not be equal.");
		}
	}
}