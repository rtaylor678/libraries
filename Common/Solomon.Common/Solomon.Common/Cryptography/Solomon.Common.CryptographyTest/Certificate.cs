﻿using System.Linq;
using System.Security.Cryptography.X509Certificates;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Solomon.Common.Cryptography
{
	[TestClass]
	public class CertificateTest
	{
		[TestMethod]
		public void CertificateValidChain()
		{
			X509Certificate2 certificateInstall = new X509Certificate2(@"SHA_2\512 Sha Privacy.pfx", "A6CF91CB-58C6-4E92-A34C-0DF0584B");
			X509Certificate2 certAuthority = new X509Certificate2(@"SHA_2\512 Sha Authority.cer");

			X509Certificate2 certificatePrivate;

			Assert.IsTrue(Certificate.Install(StoreName.AuthRoot, certAuthority), "Could not install authority certificate.");
			Assert.IsTrue(Certificate.Install(StoreName.My, certificateInstall), "Could not install certificate.");
			Assert.IsTrue(Certificate.FindInPersonal("512 Sha Privacy", out certificatePrivate), "Could not find valid certificate.");

			byte[] a = certificateInstall.GetPublicKey();
			byte[] b = certificatePrivate.GetPublicKey();

			Assert.IsTrue(a.SequenceEqual(b));

			Assert.IsTrue(Certificate.Remove(StoreName.AuthRoot, certAuthority));
			Assert.IsTrue(Certificate.Remove(StoreName.My, certificateInstall));
		}

		[TestMethod]
		public void CertificateInvalidChain()
		{
			X509Certificate2 certificateInstall = new X509Certificate2(@"SHA_2\512 Sha Privacy.pfx", "A6CF91CB-58C6-4E92-A34C-0DF0584B");

			X509Certificate2 certificatePrivate;

			Assert.IsTrue(Certificate.Install(StoreName.My, certificateInstall), "Could not install certificate.");

			Assert.IsFalse(Certificate.FindInPersonal("1 Privacy (HSB)", out certificatePrivate), "Valid certificate should not have been found.");

			Assert.IsTrue(Certificate.Remove(StoreName.My, certificateInstall));
		}
	}
}