﻿using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

using Microsoft.VisualStudio.TestTools.UnitTesting;

//	http://world.std.com/~dtd/sign_encrypt/sign_encrypt7.html

namespace Solomon.Common.Cryptography
{
	[TestClass]
	public partial class EndToEndSecurityTest
	{
		[TestMethod]
		public void ManualCertSetup()
		{
			//////////////////////////////////////////////////////////////////////////////
			//																			//
			//				Share Encrypted Secret Keys - Out of Band					//
			//																			//
			//////////////////////////////////////////////////////////////////////////////

			byte[] integrityPassword = File.ReadAllBytes("Rfc\\IntegrityKey.txt");
			byte[] integritySalt = File.ReadAllBytes("Rfc\\IntegritySalt.txt");

			byte[] privacyPassword = File.ReadAllBytes("Rfc\\PrivacyKey.txt");
			byte[] privacySalt = File.ReadAllBytes("Rfc\\PrivacySalt.txt");

			//	Original message to send
			const string originalMessage = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
			byte[] originalBytes = System.Text.Encoding.ASCII.GetBytes(originalMessage);

			//////////////////////////////////////////////////////////////////////////////
			//																			//
			//				Sending Computer											//
			//																			//
			//////////////////////////////////////////////////////////////////////////////

			#region Client Computer (Sending)

			#region Add Certificates

			//	Authority.cer (Public Key) creates a valid certificate chain for the sender's private key.
			X509Certificate2 sndAuthority = new X509Certificate2(@"SHA_2\512 Sha Authority.cer");

			//	Authenticity.pfx (Private Key) represents the sender's identity.
			//	Objective: Verify message came from a specific sender
			X509Certificate2 sndSenderPrivate = new X509Certificate2(@"SHA_2\512 Sha Authenticity.pfx", "7829ED80-E843-4CE2-8AFB-67EB5BC3");

			//	Confidentiality.cer (Public Key) represents the receivers's identity (available to all senders).
			//	Objective: Verify the message is readbale by a spcific recipient
			X509Certificate2 sndRecipientPublic = new X509Certificate2(@"SHA_2\512 Sha Confidentiality.cer");

			Assert.IsTrue(Certificate.Install(StoreName.AuthRoot, sndAuthority));
			Assert.IsTrue(Certificate.Install(StoreName.My, sndSenderPrivate));
			Assert.IsTrue(Certificate.Install(StoreName.TrustedPeople, sndRecipientPublic));

			#endregion

			//	Prevent message tampering

			byte[] integrityKey = new byte[128];
			Assert.IsTrue(Password.Derive(integrityPassword, integritySalt, integrityKey.Length, out integrityKey));

			byte[] integrityBytes;
			Assert.IsTrue(Integrity.Symmetric.Sign(originalBytes, integrityKey, out integrityBytes));

			//	Encrypt plain-text (How do you prevent encryption tampering?)

			byte[] privacyKey = new byte[32];
			Assert.IsTrue(Password.Derive(privacyPassword, privacySalt, privacyKey.Length, out privacyKey));

			byte[] privacyBytes;
			Assert.IsTrue(Privacy.Symmetric.Encrypt(integrityBytes, privacyKey, out privacyBytes));

			//	Address message to one recipient

			X509Certificate2 recipientPublic;
			Assert.IsTrue(Certificate.FindInTrusted("512 Sha Confidentiality", out recipientPublic));

			byte[] confidentialityBytes;
			Assert.IsTrue(Confidentiality.Encode(privacyBytes, recipientPublic, out confidentialityBytes));

			//	Sign the message (How do you prevent signature tampering?)

			X509Certificate2 senderPrivate;
			Assert.IsTrue(Certificate.FindInPersonal("512 Sha Authenticity", out senderPrivate));

			byte[] authenticityBytes;
			Assert.IsTrue(Authenticity.Sign(confidentialityBytes, senderPrivate, out authenticityBytes));

			#region Remove Certificates

			Assert.IsTrue(Certificate.Remove(StoreName.TrustedPeople, sndRecipientPublic));
			Assert.IsTrue(Certificate.Remove(StoreName.My, sndSenderPrivate));
			Assert.IsTrue(Certificate.Remove(StoreName.AuthRoot, sndAuthority));

			#endregion

			#endregion

			//////////////////////////////////////////////////////////////////////////////
			//																			//
			//				Send														//
			//																			//
			//////////////////////////////////////////////////////////////////////////////

			byte[] transmissionBytes = authenticityBytes;

			//////////////////////////////////////////////////////////////////////////////
			//																			//
			//				Receive														//
			//																			//
			//////////////////////////////////////////////////////////////////////////////

			#region Receiving Computer

			#region Add Certificates

			//	Authenticity.cer (Public Key) represents the sender's identity (available to all receivers)
			X509Certificate2 recSenderPublic = new X509Certificate2(@"SHA_2\512 Sha Authenticity.cer");

			//	Confidentiality.pfx (Private Key) represents the reveivers's identity
			X509Certificate2 recRecipientPrivate = new X509Certificate2(@"SHA_2\512 Sha Confidentiality.pfx", "E3BD99F7-84B2-4E4E-ABF9-083D1C47");

			Assert.IsTrue(Certificate.Install(StoreName.TrustedPeople, recSenderPublic));
			Assert.IsTrue(Certificate.Install(StoreName.My, recRecipientPrivate));

			#endregion

			//	Verify sender

			X509Certificate2 senderPublic;
			Assert.IsTrue(Certificate.FindInTrusted("512 Sha Authenticity", out senderPublic));

			byte[] authenticatedBytes;
			Assert.IsTrue(Authenticity.Verify(transmissionBytes, senderPublic, true, out authenticatedBytes));

			//	Verify recipient (Private Certificate (*.pfx) must be in the "My" Store)

			byte[] confidentialBytes;
			Assert.IsTrue(Confidentiality.Decode(authenticatedBytes, out confidentialBytes));

			//	Decrypt plain-text

			byte[] decryptKey = new byte[32];
			Assert.IsTrue(Password.Derive(privacyPassword, privacySalt, decryptKey.Length, out decryptKey));

			byte[] decryptBytes;
			Assert.IsTrue(Privacy.Symmetric.Decrypt(confidentialBytes, decryptKey, out decryptBytes));

			//	Verify message integrity

			byte[] verifyKey = new byte[128];
			Assert.IsTrue(Password.Derive(integrityPassword, integritySalt, verifyKey.Length, out verifyKey));

			byte[] messageBytes;
			Assert.IsTrue(Integrity.Symmetric.Verify(decryptBytes, verifyKey, out messageBytes));

			//	Verify received message and original message are the same.

			Assert.IsTrue(messageBytes.SequenceEqual(originalBytes));

			#region Remove Certificates

			Assert.IsTrue(Certificate.Remove(StoreName.TrustedPeople, recSenderPublic));
			Assert.IsTrue(Certificate.Remove(StoreName.My, recRecipientPrivate));

			#endregion

			#endregion
		}
	}
}