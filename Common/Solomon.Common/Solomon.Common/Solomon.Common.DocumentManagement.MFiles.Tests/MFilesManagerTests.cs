﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Solomon.Common.DocumentManagement.MFiles.WebAccess;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;

namespace Solomon.Common.DocumentManagement.MFiles.Tests
{
    [TestClass]
    public class MFilesManagerTests
    {
        private MFilesManager _library = null;
        private int _classDoc = 0;
        private int _docId = 589;
        private int _docVersion = 6;

        [TestInitialize]
        public void startup()
        {
            string vaultGuid = ConfigurationManager.AppSettings["VaultGuid"].ToString();
            string password = DevConfigHelper.ReadConfig("MFilesPw");
            string url = ConfigurationManager.AppSettings["Url"].ToString();
            string id = DevConfigHelper.ReadConfig("MFilesId");
            _library = new MFilesManager(url, vaultGuid, id, password);
        }

        [TestMethod]
        public void GetObjectsUnfilteredTest()
        {
            Results<ObjectVersion> results = _library.GetObjectsUnfiltered();
            Assert.AreEqual(results.Items.Count(), 500);
        }

        [TestMethod]
        public void GetSpecificObjectExcludeTest()
        {
            ExtendedObjectVersion result = _library.GetSpecificObjectExclude(15998);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GetSpecificObjectIncludeTest()
        {
            ExtendedObjectVersion result = _library.GetSpecificObjectInclude(3452,true);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GetObjectHistoryTest()
        {
            ObjectVersion[] result = _library.GetObjectHistory(15998);
            Assert.IsTrue(result.Length > 1);
        }

        [TestMethod]
        public void GetCheckedOutStatusTest()
        {
            MFCheckOutStatus result = _library.GetCheckedOutStatus(15998, 2);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GetObjectTitleTest()
        {
            string title = _library.GetObjectTitle(15998, 2);
            Assert.IsTrue(title.Length > 1);
        }

        [TestMethod]
        public void GetObjectPropertiesTest()
        {
            IList<PropertyValue> results = _library.GetObjectProperties(_docId);
            Assert.IsTrue(results.Count > 0);
        }

        [TestMethod]
        public void GetPropertyDefsTest()
        {
            //just writing this list out so can use it.
            int count = 0;
            IList<PropertyDef> results = _library.GetPropertyDefs();
            System.IO.StreamWriter sw = new System.IO.StreamWriter("C:\\temp\\MfilesPropertyDefs.txt", false);
            //results.ToList().ForEach(x => Console.WriteLine(x.Name));
            foreach (PropertyDef item in results.ToList())
            {
                sw.WriteLine(item.Name + "^^^" + item.ID.ToString());
                count++;
            }
            //results.ToList().ForEach(x => sw.WriteLine(x.Name));
            sw.Close();
            Assert.IsTrue(count > 0);
        }

        //[TestMethod]
        //public void GetPropertyDefsByIdTest()
        //{
        //    //just writing this list out so can use it.
        //    int count = 0;
        //    IList<PropertyDef> results = _library.GetPropertyDefs(6702);
        //    System.IO.StreamWriter sw = new System.IO.StreamWriter("C:\\MfilesPropertyDefs.txt", false);
        //    //results.ToList().ForEach(x => Console.WriteLine(x.Name));
        //    foreach (PropertyDef item in results.ToList())
        //    {
        //        sw.WriteLine(item.Name + "^^^" + item.ID.ToString());
        //        count++;
        //    }
        //    //results.ToList().ForEach(x => sw.WriteLine(x.Name));
        //    sw.Close();
        //    Assert.IsTrue(count > 0);
        //}

        [TestMethod]
        public void GetObjectsByPropertyValueTest()
        {   //searchOperator  "*="  is 'contains'            
            Results<ObjectVersion> results = _library.GetObjectsByPropertyValue(0, "*=", "test");
            Assert.IsTrue(results.Items.Count() > 1);
            bool testResult = false;
            Results<ObjectVersion> results2 = _library.GetObjectsByPropertyValue(0, "*=", "VF1 DOW ");
            foreach (ObjectVersion ov in results2.Items)
            {
                if (ov.Class == 52)
                {
                    testResult = true; break;
                }
            }
            Assert.IsTrue(testResult);

            testResult = false;
            Results<ObjectVersion> results3a = _library.GetObjectsByPropertyValue(23, "=", "27"); //7 is Penny, I am 27.
            foreach (ObjectVersion ov in results3a.Items)
            {
                if (ov.DisplayID == "2861")
                {
                    testResult = true; break;
                }
            }
            Assert.IsTrue(testResult);

            testResult = false;
            //Results<ObjectVersion> results3 = _library.GetObjectsByPropertyValue(0, "=", "3049");
            Results<ObjectVersion> results4 = _library.GetObjectsByPropertyValue(22, "=", "true");
            foreach (ObjectVersion ov in results3a.Items)
            {
                if (ov.SingleFile == true)
                {
                    testResult = true; break;
                }
            }
            Assert.IsTrue(testResult);

            //List<PropertyValue> prop1911 = _library.GetObjectProperties(1911).ToList<PropertyValue>();
            //foreach (PropertyValue pv in prop1911)
            //{
            //    string testpv= pv.PropertyDef.ToString();
            //    //Console.WriteLine(pv.PropertyDef + "/" + pv.TypedValue);
            //}
        }

        [TestMethod]
        public void GetSomeValueListItemsTest()
        {
            bool testResult = false;
            //Results<ValueListItem> classes = _library.GetLookups(0); //classes
            //var allObjTypeItems = _library.GetAllObjTypeItems(); 

            Results<ValueListItem> result1 = _library.GetSomeValueListItems(1); //deliverable type
            foreach (ValueListItem valueItem in result1.Items)
            {
                if (valueItem.Name == "Benchmarking" && valueItem.DisplayID == "56" && valueItem.ID == 56)
                {
                    testResult = true;
                    break;
                }
            }
            Assert.IsTrue(testResult);

            testResult = false;
            Results<ValueListItem> result2 = _library.GetSomeValueListItems(6); //USERS
            foreach (ValueListItem valueItem in result2.Items)
            {
                if (valueItem.ID == 123)
                {
                    if (valueItem.Name == "Stuart F. Bard"  && valueItem.ID == 123 && valueItem.DisplayID == "123")
                    {
                        testResult = true;
                        break;
                    }
                }
            }
            Assert.IsTrue(testResult);

            testResult = false;
            Results<ValueListItem> result3 = _library.GetSomeValueListItems(163); //bENCHMARKING
            foreach (ValueListItem valueItem in result3.Items)
            {
                if (valueItem.DisplayID == "102EUR12" && valueItem.ID == 546)
                {
                    testResult = true;
                    break;
                }
            }
            Assert.IsTrue(testResult);

            testResult = false;
            Results<ValueListItem> result4 = _library.GetSomeValueListItems(157); //DeliverabelType
            foreach (ValueListItem valueItem in result4.Items)
            {
                if (valueItem.DisplayID == "9" && valueItem.ID == 12 && valueItem.Name == "Input Data Review (IDR)")
                {
                    testResult = true;
                    break;
                }
            }

            Results<ValueListItem> resultGeneral = _library.GetSomeValueListItems(198); //General Refinery
            foreach (ValueListItem valueItem in resultGeneral.Items)
            {
                if (valueItem.DisplayID.StartsWith("102EUR")) // == "102EUR12" && valueItem.ID == 546)
                {
                    if (valueItem.Name.StartsWith("102EUR"))
                    {
                        string stophere = "";
                    }
                }
            }

            Results<ValueListItem> resultDrawings = _library.GetSomeValueListItems(199); //Drawings
            foreach (ValueListItem valueItem in resultDrawings.Items)
            {
                if (valueItem.DisplayID.StartsWith("D")) // == "102EUR12" && valueItem.ID == 546)
                {
                    if (valueItem.Name.StartsWith("D"))
                    {
                        string stophere = "";
                    }
                }
            }

            Assert.IsTrue(testResult);

            //55 1142  fails here
            Results<ValueListItem> prjs = _library.GetSomeValueListItems(162); //Projects
            
            System.IO.StreamWriter sw = new System.IO.StreamWriter("C:\\temp\\MFilesProjects.txt", false);
            //results.ToList().ForEach(x => Console.WriteLine(x.Name));
            int count = 0;
            foreach (ValueListItem item in prjs.Items)
            {
                sw.WriteLine(item.Name + "^^^" + item.ID.ToString());
                count++;
            }
            //results.ToList().ForEach(x => sw.WriteLine(x.Name));
            sw.Close();

        }

        [TestMethod]
        public void GetSpecificValueListItem()
        {
            int id = -1;
            string name = "6PM006S - 2015 Worldwide Power CPA";
            Results<ValueListItem> prjs = _library.GetSomeValueListItems(162,null,null,null,null,name,null,null); //Drawings
            foreach(ValueListItem itm in prjs.Items)
            {
                if (itm.Name.StartsWith("6PM006S"))
                {
                    id = itm.ID;
                    break;
                }
            }
            Assert.IsTrue(id>-1);
        }

        [TestMethod]
        public void GetExtendedObjectVersionById()
        {
            ExtendedObjectVersion extendedObjectVersion = _library.GetSpecificObjectInclude(34392, true);
            Assert.IsTrue(extendedObjectVersion != null);
        }

        [TestMethod]
        public void GetObjectById()
        {
            object objectVersion = _library.GetObjectById(32418);
            Assert.IsTrue(objectVersion != null);
        }

        [TestMethod]
        public void DownloadFileToTempFolderById()
        {
            string fullPath = @"C:\\Temp\\SbTestUpload2016-02.txt";
            if (System.IO.File.Exists(fullPath))
                System.IO.File.Delete(fullPath);
            Assert.IsTrue(_library.DownloadFileToTempFolder(2861));
        }

        //OBSOLETE, use UploadNewDocWithProjectNumberTest() instead.
        //[TestMethod]
        //public void UploadNewDocTest()
        //{
        //    //don't want to do this each time . . . 
        //    Assert.Inconclusive("SKIPPING");
        //    int docClass = 52;
        //    int deliverableTYpe =12;
        //    int benchmarkingParticpatant = 5323;

        //    var objVer = _library.UploadNewDoc(@"C:\temp\SbTestUpload2016-03-17.txt", 
        //        docClass, deliverableTYpe, benchmarkingParticpatant,null);
        //    Assert.IsTrue(objVer.ObjVer.ID > 0);
        //}

        [TestMethod]
        public void UploadNewDocWithProjectNumberTest()
        {
            //don't want to do this each time . . . 
            Assert.Inconclusive("SKIPPING");
            int docClass = 52;
            int deliverableTYpe = 12;
            int benchmarkingParticpatant = 5323;
            int projectNumber = 2017;

            var objVer = _library.UploadNewDoc(@"C:\temp\SbTestUpload2017-05-22.txt",
                docClass, deliverableTYpe, benchmarkingParticpatant,null, projectNumber);
            Assert.IsTrue(objVer.ObjVer.ID > 0);
        }

        [TestMethod]
        public void UploadNewGeneralRefineryItemTest()
        {
            //don't want to do this each time . . . 
            //Assert.Inconclusive("SKIPPING");
            int docClassGeneralRefinery = 99;
            //int deliverableTYpe = 12;
            //int benchmarkingParticpatant = 5323;
            int projectNumber = 2017;

            var objVer = _library.UploadNewGeneralRefineryItem(@"C:\Temp\TestBlank2.jpg",
                "TestBlank2", "777EUR", DateTime.Today, 2, projectNumber);
            Assert.IsTrue(objVer.ObjVer.ID > 0);
        }

        [TestMethod]
        public void UploadNewIDRTest()
        {
            //don't want to do this each time . . . 
            Assert.Inconclusive("SKIPPING");
            int docClassGeneralRefinery = 99;
            //int deliverableTYpe = 12;
            //int benchmarkingParticpatant = 5323;
            int projectNumber = 2017;

            
            var objVer = _library.UploadNewIDR(@"C:\Temp\VF1.6 SOLOMON REFINING - PARIS.docx",
                "777EUR16A",null,projectNumber);
            
            var objVer2 = _library.UploadNewIDR(@"C:\Temp\VF1.6 SOLOMON REFINING - PARIS.docx",
                "777EUR16A", null, projectNumber,"VF1.7 SOLOMON REFINING - PARIS");
            Assert.IsTrue(objVer.ObjVer.ID > 0);
        }

        [TestMethod]
        public void UploadDocAsNewVersionTest()
        {
            //don't want to do this each time . . . 
            Assert.Inconclusive("SKIPPING");
            int docId  = 39001;
            var result = _library.UploadNewVersionOfDoc(@"C:\Temp\TestBlank.jpg", docId);
            Assert.IsTrue(result.DisplayID == docId.ToString());
            //ObjectVersion objectVersion = Api.Get.Object(0, 3431); //, -1);
            //ObjectFile file = Api.Get.Files(objectVersion.ObjVer).SingleOrDefault();
            //string requestURL = "objects/" + objectVersion.ObjVer.Type.ToString() + "/" + objectVersion.ObjVer.ID.ToString() + "/" + "latest" + "/files/" + file.ID.ToString() + "/content";
            //string filepath =  @"C:\Temp\TestUploadBench3.txt";
            //FileStream filestream = new FileStream(filepath, FileMode.Open);
            //Api.CheckOut(objectVersion.ObjVer);
            //Api.Client.Put<ObjectVersion>(requestURL, filestream);
            //Api.CheckIn(objectVersion.ObjVer);            
        }

        [TestMethod]
        public void UploadDocAsNewVersionTest2()
        {
            //don't want to do this each time . . . 
            Assert.Inconclusive("SKIPPING");

            //for existing docs in M-files that don't have a 
            //project property populated, this works (for now).
            int docId = 53584;
            //var result = _library.UploadNewVersionOfDoc(@"C:\Temp\SbTestUpload2017-05-22.txt", docId);

            //docId = 16193;
            //var result = _library.UploadNewVersionOfDoc(@"C:\Temp\SbTestUpload2016-09-21.txt", docId);

            //refining general: new doc, and update existing doc
            //docId = 53624;
            //var result = _library.UploadNewVersionOfDoc(@"C:\Temp\TestBlank2.jpg", docId);
            docId = 39119;
            var result = _library.UploadNewVersionOfDoc(@"C:\Temp\TestBlank.jpg", docId);




            Assert.IsTrue(result.DisplayID == docId.ToString());         
        }

        [TestMethod]
        public void GetBenchmarkingParticipantId()
        {            
            Assert.IsTrue(_library.GetBenchmarkingParticipantId("2013PCH998") == 5322);
            Assert.IsTrue(_library.GetBenchmarkingParticipantId("2015PCH134") == 8849);
            Assert.IsTrue(_library.GetBenchmarkingParticipantIdByUniqueNameOrTitle("Power16Co-NATURKRAFT - NATURKRAFT (Company)") ==11845);
        }

        [TestMethod]
        public void CheckoutStatusTest()
        {
            ObjectVersion objectVersion = _library.SetCheckedoutStatus(3452, MFCheckOutStatus.CheckedOutToMe);
            MFCheckOutStatus status = _library.GetCheckedoutStatus(3452);
            Assert.IsTrue(status == MFCheckOutStatus.CheckedOutToMe);
            //objectVersion = _library.SetCheckedoutStatus(objectVersion.ObjVer, MFCheckOutStatus.CheckedIn);
            objectVersion = _library.SetCheckedoutStatus(3452, MFCheckOutStatus.CheckedIn);
            status = _library.GetCheckedoutStatus(objectVersion.ObjVer.ID);
            Assert.IsTrue(status == MFCheckOutStatus.CheckedIn);
        }

        [TestMethod]
        public void OnlyNondeletedResultsObjectsTest()
        {
            string vaultGuid = ConfigurationManager.AppSettings["VaultGuid"].ToString();
            string password = DevConfigHelper.ReadConfig("MFilesPw");
            string url = ConfigurationManager.AppSettings["Url"].ToString();
            string id = DevConfigHelper.ReadConfig("MFilesId");
            object[] ctor = new object[]
            {
                url, vaultGuid, id, password,false
            };
            PrivateObject privateObject = new PrivateObject(typeof(MFilesManager), ctor);
            Results<ObjectVersion> start = new Results<ObjectVersion>();
            Solomon.Common.DocumentManagement.MFiles.WebAccess.ObjectVersion[] tempItems = new Solomon.Common.DocumentManagement.MFiles.WebAccess.ObjectVersion[2];
            ObjectVersion t = new ObjectVersion();
            t.Deleted = true;
            ObjectVersion f = new ObjectVersion();
            f.Deleted = false;
            tempItems[0] = t;
            tempItems[1] = f;
            start.Items = tempItems;

            object[] parms = new object[1];
            parms[0] = start;

            var testResult = privateObject.Invoke("OnlyNondeletedObjects", parms);
            Results<ObjectVersion> convertedTestResult = (Results<ObjectVersion>)testResult;
            Assert.IsTrue(convertedTestResult.Items.Count() == 1);

            Results<ObjectVersion> testEmptyOne = new Results<ObjectVersion>();
            testEmptyOne.Items = null;
            parms[0] = testEmptyOne;
            var testResult3 = privateObject.Invoke("OnlyNondeletedObjects", parms);
            Assert.IsTrue(testResult3 == testEmptyOne);
        }

        [TestMethod]
        public void OnlyNondeletedArrayObjectsTest()
        {
            string vaultGuid = ConfigurationManager.AppSettings["VaultGuid"].ToString();
            string password = DevConfigHelper.ReadConfig("MFilesPw");
            string url = ConfigurationManager.AppSettings["Url"].ToString();
            string id = DevConfigHelper.ReadConfig("MFilesId");
            object[] ctor = new object[]
            {
                url, vaultGuid, id, password,false
            };
            PrivateObject privateObject = new PrivateObject(typeof(MFilesManager), ctor);
            //ObjectVersion[] start = new ObjectVersion[1] ;
            Solomon.Common.DocumentManagement.MFiles.WebAccess.ObjectVersion[] tempItems = new Solomon.Common.DocumentManagement.MFiles.WebAccess.ObjectVersion[2];
            ObjectVersion t = new ObjectVersion();
            t.Deleted = true;
            ObjectVersion f = new ObjectVersion();
            f.Deleted = false;
            tempItems[0] = t;
            tempItems[1] = f;
            //start.Items = tempItems;

            object[] parms = new object[1];
            parms[0] = tempItems;

            var testResult = privateObject.Invoke("OnlyNondeletedObjects", parms);
            ObjectVersion[] convertedTestResult = (ObjectVersion[])testResult;
            Assert.IsTrue(convertedTestResult.Count() == 1);

            //ObjectVersion[] emptyOne = null;// = new Results<ObjectVersion>();
            //parms[0] = emptyOne;
            //var testResult2 = privateObject.Invoke("OnlyNondeletedObjects", parms);
            //Assert.IsTrue(testResult2 == emptyOne);

            ObjectVersion[] testEmptyOne = new ObjectVersion[0];
            parms[0] = testEmptyOne;
            ObjectVersion[] testResult3 = (ObjectVersion[])privateObject.Invoke("OnlyNondeletedObjects", parms);
            Assert.IsTrue(testResult3.Length == 0);
        }

        [TestMethod]
        public void GetGeneralObjects()
        {
            int generalID = -1;
            string generalId = "102EUR";
            Results<ValueListItem> resultGeneral = _library.GetSomeValueListItems(198);
            foreach (ValueListItem itm in resultGeneral.Items)
            {
                if (itm.DisplayID == generalId)
                {
                    generalID = itm.ID;
                    break;
                }
            }
            int propertyDefinitionId = 1327; // "Class " = 100
            string searchOperator = "="; // where Name Contains property value. Refer to http://www.m-files.com/mfws/syntax.html#sect:search-encoding for more operators.
            string propertyValue = generalID.ToString(); // "198"; // 
            Results<ObjectVersion> results1 =
                _library.GetObjectsByPropertyValue(propertyDefinitionId, searchOperator, propertyValue);

            //int count1 = results1.Items.ToList().Count();
            //var z = results1.Items[0];

            Assert.IsTrue(results1.Items.Count() >0);
        }


        [TestMethod]
        public void GetObjectsByMultiplePropertyValues()
        {
            int count1 = 0;
            /*
            int propertyDefinitionId =100 ; // "Class " = 100
            string searchOperator = "="; // where Name Contains property value. Refer to http://www.m-files.com/mfws/syntax.html#sect:search-encoding for more operators.
            string propertyValue = "99"; //  ID of the desired property definition ('BencmarcihgGeneral.)
            Results<ObjectVersion> results1 = 
                _library.GetObjectsByPropertyValue(propertyDefinitionId,searchOperator,propertyValue);            
            */
            int generalID=0;
            
            Results<ValueListItem> resultGeneral = _library.GetSomeValueListItems(198);
            foreach (ValueListItem itm in resultGeneral.Items)
            {
                if (itm.DisplayID == "102EUR")
                {
                    generalID = itm.ID;
                }
            }
            
            //Results<ObjectVersion> test198 = _library.GetObjectsByPropertyValue(, "*=", "6");  //0 is title
            //string y = test198.Items[0].DisplayID;


            int propertyDefinitionId = 1327; // "Class " = 100
            string searchOperator = "="; // where Name Contains property value. Refer to http://www.m-files.com/mfws/syntax.html#sect:search-encoding for more operators.
            string propertyValue = generalID.ToString(); // "198"; // 
            Results<ObjectVersion> results1 =
                _library.GetObjectsByPropertyValue(propertyDefinitionId, searchOperator, propertyValue);

            count1 = results1.Items.ToList().Count();
            var z = results1.Items[0];
            int count2 = 0;

            /*
            int count1 = 0;
            int propertyDefinitionId = 0; // ID of the desired property definition ('Name or title' in this case.)
            string searchOperator = "*="; // where Name Contains property value. Refer to http://www.m-files.com/mfws/syntax.html#sect:search-encoding for more operators.
            string propertyValue = "test"; // Search for "test" as the property value.
            Results<ObjectVersion> results1 = 
                _library.GetObjectsByPropertyValue(propertyDefinitionId,searchOperator,propertyValue);
            count1 = results1.Items.Count();

            int count2 = 0;
            int propertyDefinitionId2 = 1144;
            string searchOperator2 = "="; 
            string propertyValue2 = "5322";
            string searchQuery = propertyDefinitionId2.ToString() + searchOperator2 + propertyValue2;
            Results<ObjectVersion> results2 =
                _library.GetObjectsByPropertyValue(propertyDefinitionId2, searchOperator2, propertyValue2);
            count2 = results2.Items.Count();

            int count3 = 0;
            List<string> args = new List<string>();
            char delim = '^';
            args.Add(propertyDefinitionId.ToString() + delim + searchOperator + delim + propertyValue);
            args.Add(propertyDefinitionId2.ToString() + delim + searchOperator2 + delim + propertyValue2);
            Results<ObjectVersion> results3 = _library.GetObjectsByMultiplePropertyValues(delim,args);
            count3 = results3.Items.Count();
            Assert.IsTrue(count1 != count2 && count2 != count3);
            */
        }

        [TestMethod]
        public void GetMultipleRefnumsDocs()
        {
            List<string> args = new List<string>();
            char delim =  (char)14; // '^'; //DONT USE ANY OF THE CHARS THAT GO INTO THE SEARCH FILTER
            args = AddNewBenchmarkingParticipantArg(args, 1144,delim, "=", "5323"); //41. all
            args = AddNewBenchmarkingParticipantArg(args, 0, delim, "^=", "VF");  //15
            args = AddNewBenchmarkingParticipantArg(args, 1144, delim, "=", "8849"); //2015PCH134 - shoud be 1 and it is VF
            //-yes this adds 1
        
            Results<ObjectVersion> results = _library.GetObjectsByMultiplePropertyValues(delim, args);
            int count = results.Items.Count();
            
            //then: try adding in filter for docname starts with VF
            //(0, "*=", "VF"); - this only works if its the only filter.
            // ‘ˆ=’ ; Starts With
            Assert.IsTrue(count>1);
        }

        private List<string> AddNewBenchmarkingParticipantArg(List<string> list, int propertyDefId, char delim, string searchOperator, string propertyValue)
        {
            if (list == null)
            {
                list = new List<string>();
            }
            list.Add(propertyDefId.ToString() + delim + searchOperator + delim + propertyValue);
            return list;
        }


        [TestMethod]
        public void GetUserIdTest()
        {
            int id = _library.GetUserId();
            Assert.IsTrue(id == 123);
        }

        [TestCleanup]
        public void shutdown()
        {
            try
            {
                _library.Logout();
            }
            catch (Exception ex)
            { }
            _library = null;
        }

    }
}
