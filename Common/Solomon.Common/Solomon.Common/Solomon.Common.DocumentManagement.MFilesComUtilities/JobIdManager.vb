﻿'Imports System.Runtime.InteropServices

'Namespace Solomon.Common.DocumentManagement.MFilesComUtilities
Public Class JobIdManager
    Const MfilesVault As String = "solomon"
    Const _vaultConnection = "dallas-dms"
    Dim _windowHandle As Long
    Dim oClientApp As Object ' New MFilesAPI.MFilesClientApplication
    Dim oVaultConnection As Object '  As MFilesAPI.VaultConnection
    Dim oVault As Object ' As MFilesAPI.Vault

    ' --------- MFiles ProperyDefsID   ---------------
    Const MFpdClassProperty = 100
    Const MFpdParticipant = 1144
    Const MFpdProject = 1142
    Const MFpdDocDate = 1179
    Const MFpdDeliverableType = 1211

    ' --------- MFiles ProperyDefs Common Values   ---------------
    Const MFpdvIDRDeliverable = 12                  ' for use with MFpdDeliverableType
    Const MFpdvClassBenchmarkingParticipant = 52    ' for use with MFpdClassProperty
    Const MFpdvRefNums = 163                        ' Value List for Refnums
    Const MFpdvProjects = 162                       ' Value List for Projects

    Private Function Startup(ByRef errors As String) As Boolean
        'late bind COM dependencies
        Dim location As String = "MFilesAPI.MFilesClientApplication"
        Try
            oClientApp = CreateObject("MFilesAPI.MFilesClientApplication")
            location = "MFilesAPI.VaultConnection"
            oVaultConnection = CreateObject("MFilesAPI.VaultConnection")
            location = "MFilesAPI.Vault"
            oVault = CreateObject("MFilesAPI.Vault")
            location = String.Empty
            Return True
        Catch ex As Exception
            errors = "Error in MFilesComUtilities.Startup(), at " & location & ": " & ex.Message
        End Try
        Return False
    End Function

    Private Function Set_oVault(ByRef errors As String) As Boolean
        Try

            'If oVault Is Nothing Then

            'windowHandle = Application.Hwnd

            ' Initialize the MFilesClientApplication object.
            oClientApp = CreateObject("MFilesAPI.MFilesClientApplication")

            'On Error Resume Next
            Try
                oVaultConnection = oClientApp.GetVaultConnection(MfilesVault)
            Catch
            End Try
            'On Error GoTo 0

            If oVaultConnection Is Nothing Then oVaultConnection = oClientApp.GetVaultConnection(_vaultConnection)
            oVault = oVaultConnection.BindToVault(_windowHandle, True, False)
            'End If
            Return True
        Catch ex As Exception
            errors = "Error in MFilesComUtilities.Set_oVault(): " & ex.Message
        End Try
        Return False
    End Function

    '---------------------------------------------------------------------------------------
    ' Method : MFSearchIDRDeliverables
    ' Author : swp
    ' Date   : 1/2/2016
    ' Purpose: Find files for a particular Refnum - Return as MFilesAPI.ObjectSearchResults
    '---------------------------------------------------------------------------------------
    Private Function MFSearchProjectObj(ByVal sJobNumber As String) As Object ' MFilesAPI.ObjectSearchResults

        'On Error GoTo DL_Error
        Dim returnValue As Object ' MFilesAPI.ObjectSearchResults
        Dim errors As String = String.Empty
        If Not Set_oVault(errors) Then
            Throw New Exception(errors)
        End If

        Const I_OBJTYPE = 162  ' Project Object Type

        Dim oSc As Object = CreateObject("MFilesAPI.SearchCondition")
        Dim oScs As Object = CreateObject("MFilesAPI.SearchConditions")

        ' Project only
        oSc.ConditionType = 1 ' MFConditionTypeEqual
        oSc.Expression.DataStatusValueType = 6 ' MFStatusTypeObjectTypeID
        oSc.TypedValue.SetValue(9, I_OBJTYPE) 'MFDatatypeLookup, I_OBJTYPE)
        oScs.Add(-1, oSc)

        ' Not deleted
        oSc.ConditionType = 1 'MFConditionTypeEqual
        oSc.Expression.DataStatusValueType = 5 ' MFStatusTypeDeleted
        oSc.TypedValue.SetValue(8, False) 'MFDatatypeBoolean, False)
        oScs.Add(-1, oSc)

        ' Search Project
        oSc.ConditionType = 9 ' MFConditionTypeStartsWith
        oSc.Expression.DataPropertyValuePropertyDef = 0 ' MFBuiltInPropertyDefNameOrTitle
        oSc.TypedValue.SetValue(1, sJobNumber) 'MFDatatypeText, sJobNumber)
        oScs.Add(-1, oSc)

        returnValue = oVault.ObjectSearchOperations.SearchForObjectsByConditions(oScs, 2, True) 'MFSearchFlagReturnLatestVisibleVersion, True)

        oSc = Nothing
        oScs = Nothing
        Return returnValue
        'Exit Function

        'DL_Error:
        '        Debug.Print "Error " & Err.Number & " (" & Err.Description & ") Could not open files (MFiles)"
        '        Resume Next
    End Function

    Public Function MFGetJobNoID(sJobNumber As String, windowHandle As Long) As String
        Dim startupErrors As String = String.Empty
        If Not Startup(startupErrors) Then
            Throw New Exception(startupErrors)
        End If

        Try
            _windowHandle = windowHandle

            'Get list of files for Refnum
            Dim oSearchResults As Object = Nothing ' = CreateObject("MFilesAPI.ObjectSearchResults")
            oSearchResults = MFSearchProjectObj(sJobNumber)

            Dim oOneObject As Object = CreateObject("MFilesAPI.ObjectVersion")
            If oSearchResults.Count = 1 Then
                Return oSearchResults.Item(1).ObjVer.ID
            ElseIf oSearchResults.Count = 0 Then
                Return "NO MATCH"
            ElseIf oSearchResults.Count > 1 Then
                Return "MORE THAN ONE"
            End If

            oOneObject = Nothing
            oSearchResults = Nothing
            Return "MFilesComUtilities.MFGetJobNoID(): COUNT < 0"
        Catch ex As Exception
            Throw New Exception("Error in MFilesComUtilities.MFGetJobNoID(): " & ex.Message)
        End Try


    End Function



End Class
'End Namespace