﻿using System;
using System.Data.SqlTypes;

using Microsoft.SqlServer.Server;
using Solomon.Common.Meta.Numerics.Statistics;
using Solomon.Common.Meta.Numerics.Statistics.Distributions;

public partial class UserDefinedFunctions
{
	[SqlFunction(IsDeterministic = true, IsPrecise = true)]
	public static SqlDouble Student_Statistic(SqlDouble DegreesOfFreedom, SqlDouble Probability, SqlByte Tails)
	{
		Probability = (1.0 - Probability) / (double)Tails;
		StudentDistribution d = new StudentDistribution((double)DegreesOfFreedom);
		return -d.InverseLeftProbability((double)Probability);
	}
}