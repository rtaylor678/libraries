﻿using System;
using System.Data.SqlTypes;

using Microsoft.SqlServer.Server;
using Solomon.Common.Meta.Numerics.Statistics;
using Solomon.Common.Meta.Numerics.Statistics.Distributions;

public partial class UserDefinedFunctions
{
	[SqlFunction]
	public static SqlDouble ChiSquared_Probability(SqlInt32 DegreesOfFreedom, SqlDouble CStatistic)
	{
		ChiSquaredDistribution d = new ChiSquaredDistribution((int)DegreesOfFreedom);
		return (1.0 - d.LeftProbability((double)CStatistic));
	}
}