﻿using System;
using System.Data.SqlTypes;

using Microsoft.SqlServer.Server;
using Solomon.Common.Meta.Numerics.Statistics;
using Solomon.Common.Meta.Numerics.Statistics.Distributions;

public partial class UserDefinedFunctions
{
	[SqlFunction]
	public static SqlDouble Fisher_Probability(SqlDouble DegreesOfFreedomNumerator, SqlDouble DegreesOfFreedomDenominator, SqlDouble FStatistic)
	{
		FisherDistribution d = new FisherDistribution((double)DegreesOfFreedomNumerator, (double)DegreesOfFreedomDenominator);
		return d.LeftProbability((double)FStatistic);
	}
}