﻿using System;
using System.Data.SqlTypes;
using System.IO;

using Microsoft.SqlServer.Server;
using Solomon.Common.Meta.Numerics.Statistics;
using Solomon.Common.Meta.Numerics.Statistics.Distributions;

[Serializable]
[SqlUserDefinedAggregate
	(
		Format.UserDefined,
		IsInvariantToNulls = true,
		IsInvariantToDuplicates = true,
		IsInvariantToOrder = true,
		IsNullIfEmpty = true,
		MaxByteSize = -1
	)
]
public class IsNormalDistribution_Probability : MetaNumericsSample
{
	new public SqlDouble Terminate()
	{
		double mu = base.sampleDataSet.Mean;
		double s = base.sampleDataSet.StandardDeviation;

		TestResult tr = base.sampleDataSet.KolmogorovSmirnovTest(new NormalDistribution(mu, s));
		return tr.Probability;
	}

	new public void Init()
	{
		base.Init();
	}

	new public void Accumulate(SqlDouble Observation)
	{
		base.Accumulate(Observation);
	}

	public void Merge(IsNormalDistribution_Probability Group)
	{
		base.Merge(Group);
	}
}

/// <summary>
///  A high value of the test statistic, corresponding to a low right tail probability,
///  indicates that the sample distribution disagrees with the given distribution
///  to a degree unlikely to arise from statistical fluctuations.
///  
/// Note: Low value = true
/// </summary>
[Serializable]
[SqlUserDefinedAggregate
	(
		Format.UserDefined,
		IsInvariantToNulls = true,
		IsInvariantToDuplicates = true,
		IsInvariantToOrder = true,
		IsNullIfEmpty = true,
		MaxByteSize = -1
	)
]
public class IsNormalDistribution_Statistic : MetaNumericsSample
{
	new public SqlDouble Terminate()
	{
		double mu = base.sampleDataSet.Mean;
		double s = base.sampleDataSet.StandardDeviation;

		TestResult tr = base.sampleDataSet.KolmogorovSmirnovTest(new NormalDistribution(mu, s));
		return tr.Statistic;
	}

	new public void Init()
	{
		base.Init();
	}

	new public void Accumulate(SqlDouble Observation)
	{
		base.Accumulate(Observation);
	}

	public void Merge(IsNormalDistribution_Statistic Group)
	{
		base.Merge(Group);
	}
}