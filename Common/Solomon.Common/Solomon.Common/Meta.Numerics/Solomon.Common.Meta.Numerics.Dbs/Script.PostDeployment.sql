﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

SET NOCOUNT ON;

DECLARE @i		INT	=   0;
DECLARE @Items	INT	= 100;

WHILE (@i <= @Items)
BEGIN
	INSERT [stat].[sysSequence] DEFAULT VALUES;
	SET @i = @i + 1;
END;