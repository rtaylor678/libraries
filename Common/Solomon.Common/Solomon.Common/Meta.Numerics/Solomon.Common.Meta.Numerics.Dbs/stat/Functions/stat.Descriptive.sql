﻿CREATE FUNCTION [stat].[Descriptive]
(
	@Sample					[stat].[SampleRandomVar]	READONLY							--	Data points to examine
)
RETURNS TABLE
WITH SCHEMABINDING
AS RETURN
(
SELECT
	[d].[qTile],
	[d].[SampleType],

	[d].[xCount],
	[d].[xMin],
	[d].[xMax],
	[d].[xRange],

	[d].[BreakPrevious],
	[d].[BreakNext],

	[d].[xMean],
	[d].[xStDev],
	[d].[xVar],

	[d].[ErrorStandard],
	[d].[VariationCoefficient],
	[d].[DispersionIndex],

	[d].[xSum],
	[d].[SSE],
	[d].[MSE],

	[d].[xwMean],
	[d].[xwDiv],

	[d].[LimitLower],
	[d].[LimitUpper],
	[d].[LimitRange],

	[d].[StudentTTest],
	[d].[StudentTConfidence],
	[d].[StudentTAlpha],
	[d].[StudentTStatistic],
	[d].[StudentTHypothesis],

	[d].[IsNormalStatistic],
	[d].[IsNormalProbability],
	[d].[IsUniformStatistic],
	[d].[IsUniformProbability],

	[d].[ConfidenceInterval],
	[d].[PredictionInterval],
	[d].[PredictionLower],
	[d].[ConfidenceLower],
	[d].[ConfidenceUpper],
	[d].[PredictionUpper],

	[d].[BoxLowerExtreme],
	[d].[BoxLowerWhisker],
	[d].[BoxLower],
	[d].[BoxMedian],
	[d].[BoxUpper],
	[d].[BoxUpperWhisker],
	[d].[BoxUpperExtreme],
	[d].[BoxIMR],
	[d].[BoxWidth],
	[d].[BoxNotch],

	[d].[Kurtosis],
	[d].[Skewness]

FROM
	[stat].[DescriptiveArithmetic](@Sample, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT) [d]
);