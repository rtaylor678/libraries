﻿CREATE FUNCTION [stat].[TwoSampleBiVariateObservations]
(
	@SampleBasis			[stat].[SampleBiVariate]	READONLY,									--	Data points to examine
	@SampleAudit			[stat].[SampleBiVariate]	READONLY									--	Data points to examine
)
RETURNS TABLE
WITH SCHEMABINDING
AS RETURN
(
SELECT
	[o].[Relationship],

	[o].[Id],
	[o].[RowId],
	[o].[x],
	[o].[y],

	[o].[yQuartile],
	[o].[yPercentile],
	[o].[yPercentRank],
	[o].[yCumulative],
	[o].[yRank],
	[o].[yRankDense],

	[o].[yEstimate],

	[o].[zScore],
	[o].[zQuartile],
	[o].[zPercentile],
	[o].[zHypothesis],

	[o].[ErrorAbsolute],
	[o].[ErrorRelative],

	[o].[ResidualStandard],
	[o].[ResidualAbsolute],
	[o].[ResidualRelative],

	[o].[PlotY1],
	[o].[PlotY2],
	[o].[PlotY3],
	[o].[PlotY4],

	[o].[PlotZ1],
	[o].[PlotZ2],
	[o].[PlotZ3],
	[o].[PlotZ4],

	[o].[ResidualThreshold],
	[o].[ResidualStdIn],
	[o].[ResidualStdOut],

	[o].[ConfidenceInterval],
	[o].[PredictionInterval],

	[o].[PredictionLower],
	[o].[ConfidenceLower],
	[o].[ConfidenceUpper],
	[o].[PredictionUpper],

	[o].[ConfidenceIn],
	[o].[ConfidenceOut],
	[o].[PredictionIn],
	[o].[PredictionOut],

	[o].[PearsonThreshold],
	[o].[PearsonIn],
	[o].[PearsonOut],

	[o].[xAsCeiling],
	[o].[CeilingBreach],

	[o].[SuspectWarning],
	[o].[SuspectCritical],

	[o].[b0],
	[o].[b1],
	[o].[Confidence_Pcnt]

FROM
	[stat].[TwoSampleBiVariateObservationsAnalysis](@SampleBasis, @SampleAudit, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT) [o]
);