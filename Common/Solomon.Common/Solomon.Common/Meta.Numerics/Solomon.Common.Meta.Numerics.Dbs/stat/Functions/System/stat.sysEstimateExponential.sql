﻿CREATE FUNCTION [stat].[sysEstimateExponential]
(
	@b0				FLOAT,
	@b1				FLOAT,
	@x				FLOAT
)
RETURNS FLOAT
WITH SCHEMABINDING
AS
BEGIN

	RETURN EXP(LOG(@b0) + @b1 * @x)

END;
GO