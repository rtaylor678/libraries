﻿CREATE FUNCTION [stat].[sysEstimate]
(
	@Relationship	CHAR(1),
	@b0				FLOAT,
	@b1				FLOAT,
	@x				FLOAT
)
RETURNS FLOAT
WITH SCHEMABINDING
AS
BEGIN

	RETURN CASE @Relationship
		WHEN 'M' THEN [stat].[sysEstimateMean](@b0, @b1, @x)
		WHEN 'L' THEN [stat].[sysEstimateLinear](@b0, @b1, @x)
		WHEN 'E' THEN [stat].[sysEstimateExponential](@b0, @b1, @x)
		WHEN 'P' THEN [stat].[sysEstimatePower](@b0, @b1, @x)
		WHEN 'S' THEN [stat].[sysEstimateSaturation](@b0, @b1, @x)
		END

END;
GO