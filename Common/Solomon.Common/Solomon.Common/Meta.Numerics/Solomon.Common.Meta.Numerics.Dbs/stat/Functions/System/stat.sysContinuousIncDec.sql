﻿CREATE FUNCTION [stat].[sysContinuousIncDec]
(
	@sysResults				[stat].[sysResultsTempA]		READONLY,		--	Data points to examine
	@SortOrder				SMALLINT									--	Sort order
)
RETURNS @ReturnTable TABLE
(
	[xPlot]					FLOAT				NOT	NULL,
	[yInterpolated]			FLOAT				NOT	NULL,

	[yPlot]					FLOAT					NULL,

	PRIMARY KEY CLUSTERED([xPlot] ASC)
)
WITH SCHEMABINDING
AS
BEGIN

	DECLARE	@Order			FLOAT	= CONVERT(FLOAT, ABS(@SortOrder) / @SortOrder);

	INSERT INTO @ReturnTable
	(
		[xPlot],
		[yInterpolated]
	)
	SELECT
		[r].[xPlot],
		[r].[yInterpolated]
	FROM
		@sysResults [r];

	DECLARE @Limit		FLOAT;

	DECLARE @Min		FLOAT;
	DECLARE @Max		FLOAT;
	DECLARE @MinId		INT;
	DECLARE @MaxId		INT;

	SELECT
		@Min	= MIN([t].[yInterpolated]),
		@Max	= MAX([t].[yInterpolated])
	FROM
		@ReturnTable	[t];

	SELECT
		@MinId	= [t].[xPlot]
	FROM
		@ReturnTable	[t]
	WHERE
		[t].[yInterpolated]	= @Min
	ORDER BY
		[t].[xPlot]	* @Order;

	SELECT
		@MaxId	= [t].[xPlot]
	FROM
		@ReturnTable	[t]
	WHERE
		[t].[yInterpolated]	= @Max
	ORDER BY
		[t].[xPlot]	* @Order;

	IF (@Order > 0)
	BEGIN

		SET @Limit = @Min;

		UPDATE @ReturnTable
		SET
			[yPlot] = @Min
		WHERE
			[xPlot] <= @MinId;

		UPDATE @ReturnTable
		SET
			[yPlot] = @Max
		WHERE
			[xPlot] >= @MaxId;

		UPDATE @ReturnTable
		SET
			@Limit = [yPlot] = CASE	WHEN [yInterpolated] < @Limit THEN @Limit ELSE [yInterpolated] END
		WHERE
			[yPlot]	IS NULL;

	END;
	ELSE
	BEGIN

		SET @Limit = @Max;

		UPDATE @ReturnTable
		SET
			[yPlot] = @Min
		WHERE
			[xPlot] >= @MinId;

		UPDATE @ReturnTable
		SET
			[yPlot] = @Max
		WHERE
			[xPlot] <= @MaxId;

		UPDATE @ReturnTable
		SET
			@Limit = [yPlot] = CASE WHEN [yInterpolated] > @Limit THEN @Limit ELSE [yInterpolated] END
		WHERE
			[yPlot]	IS NULL;

	END;

	RETURN;

END;
GO
