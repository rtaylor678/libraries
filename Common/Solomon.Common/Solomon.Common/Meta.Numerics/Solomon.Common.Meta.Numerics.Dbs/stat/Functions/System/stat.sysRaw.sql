﻿CREATE FUNCTION [stat].[sysRaw]
(
	@Sample					[stat].[SampleRandomVar]	READONLY,									--	Data points to examine
	@SortOrder				TINYINT,
	@EndPointMean			TINYINT
)
RETURNS @ReturnTable TABLE
(
	[Id]					INT					NOT	NULL,
	[RowId]					INT					NOT	NULL,

	[yActual]				FLOAT				NOT	NULL,
	[y]						FLOAT				NOT	NULL,
	[w]						FLOAT					NULL,
	[wPercentile]			FLOAT					NULL	CHECK([wPercentile] BETWEEN 0.0 AND 100.0),

	PRIMARY KEY CLUSTERED([y] ASC, [Id] ASC)
)
WITH SCHEMABINDING
AS
BEGIN

	DECLARE	@Order					FLOAT	= CONVERT(FLOAT, ABS(@SortOrder) / @SortOrder);
	DECLARE @wTile					FLOAT	= CASE WHEN @Order = 1.0 THEN 0.0 ELSE 100.0 END;

	DECLARE	@wSum					FLOAT;

	SELECT
		@wSum		= SUM(COALESCE([s].[w], 1.0))
	FROM
		@Sample	[s];

	INSERT INTO @ReturnTable
	(
		[Id],
		[RowId],
		[yActual],
		[y],
		[w]
	)
	SELECT
		[r].[Id],
		[r].[RowId],
		[r].[y],
			[y]	= COALESCE(CASE
					WHEN [r].[aRow] <= @EndPointMean THEN 
						SUM(CASE WHEN [r].[aRow] <= @EndPointMean THEN [r].[y] * [r].[w] END) OVER()
						/
						SUM(CASE WHEN [r].[aRow] <= @EndPointMean THEN [r].[w] END) OVER()
					WHEN [r].[dRow] <= @EndPointMean THEN 
						SUM(CASE WHEN [r].[dRow] <= @EndPointMean THEN [r].[y] * [r].[w] END) OVER()
						/
						SUM(CASE WHEN [r].[dRow] <= @EndPointMean THEN [r].[w] END) OVER()
					END,
					[r].[y]),
			[w]	= COALESCE(CASE
					WHEN [r].[aRow] <= @EndPointMean THEN
						AVG(CASE WHEN [r].[aRow] <= @EndPointMean THEN [r].[w] END) OVER()
					WHEN [r].[dRow] <= @EndPointMean THEN
						AVG(CASE WHEN [r].[dRow] <= @EndPointMean THEN [r].[w] END) OVER()
					END,
					[r].[w])
	FROM (
		SELECT
			[s].[Id],
			[s].[RowId],
				[y]		= [s].[x],
				[w]		= COALESCE([s].[w], 1.0),
				[aRow]	= ROW_NUMBER() OVER(ORDER BY [s].[x] * @Order ASC,	[s].[RowId] ASC),
				[dRow]	= ROW_NUMBER() OVER(ORDER BY [s].[x] * @Order DESC,	[s].[RowId] ASC)
		FROM
			@Sample	[s]
		) [r];

	UPDATE [r]
	SET
			@wTile			= @wTile + [r].[w] / @wSum * 100.0			* @Order,
		[r].[wPercentile]	= @wTile - [r].[w] / @wSum * 100.0 / 2.0	* @Order
	FROM
		@ReturnTable	[r];
	
	RETURN;

END;
GO