﻿CREATE FUNCTION [stat].[sysEstimateLinear]
(
	@b0				FLOAT,
	@b1				FLOAT,
	@x				FLOAT
)
RETURNS FLOAT
WITH SCHEMABINDING
AS
BEGIN

	RETURN  @b0 + (@b1 * @x)

END;
GO