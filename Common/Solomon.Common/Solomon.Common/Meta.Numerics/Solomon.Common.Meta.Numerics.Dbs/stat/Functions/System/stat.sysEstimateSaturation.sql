﻿CREATE FUNCTION [stat].[sysEstimateSaturation]
(
	@b0				FLOAT,
	@b1				FLOAT,
	@x				FLOAT
)
RETURNS FLOAT
WITH SCHEMABINDING
AS
BEGIN

	RETURN 1.0/(1.0/@b0 + @b1 * (1.0/@x))

END;
GO