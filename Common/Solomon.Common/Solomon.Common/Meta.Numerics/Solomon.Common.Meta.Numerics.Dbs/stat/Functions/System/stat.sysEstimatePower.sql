﻿CREATE FUNCTION [stat].[sysEstimatePower]
(
	@b0				FLOAT,
	@b1				FLOAT,
	@x				FLOAT
)
RETURNS FLOAT
WITH SCHEMABINDING
AS
BEGIN

	RETURN POWER(10.0, LOG10(@b0) + @b1 * LOG10(@x))

END;
GO