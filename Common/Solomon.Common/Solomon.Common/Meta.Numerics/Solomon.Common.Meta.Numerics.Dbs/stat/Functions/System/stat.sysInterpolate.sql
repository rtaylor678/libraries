﻿CREATE FUNCTION [stat].[sysInterpolate]
(
	@x		FLOAT,
	@x0		FLOAT,
	@y0		FLOAT,
	@x1		FLOAT,
	@y1		FLOAT,

	@Min	FLOAT	 = NULL,
	@Max	FLOAT	 = NULL
)
RETURNS FLOAT
WITH SCHEMABINDING
AS
BEGIN

	DECLARE @Value	FLOAT =  @y0
		+ CASE WHEN (@x1 <> @x0)
			THEN
				(((@y1 - @y0) / (@x1 - @x0)) * (@x - @x0))
			ELSE
				(@y1 - @y0) / 2.0
			END;

	SET @Value = CASE WHEN @Value < @Min THEN @Min ELSE @Value END;
	SET @Value = CASE WHEN @Value > @Max THEN @Max ELSE @Value END;

	RETURN @Value;

END;
GO