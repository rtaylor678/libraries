﻿CREATE FUNCTION [stat].[sysInterpolationPoints]
(
	@Sample					[stat].[SampleRandomVar]	READONLY,									--	Data points to examine

	@SortOrder				TINYINT,
	@EndPointMean			TINYINT,
	@InterpolationPoints	TINYINT
)
RETURNS @ReturnTable TABLE
(
	[Id]					INT					NOT	NULL,
	[Item]					INT					NOT	NULL,
	[RowId]					INT					NOT	NULL,

	[y]						FLOAT				NOT	NULL,
	[w]						FLOAT					NULL,

	[wPercentile]			FLOAT					NULL	CHECK([wPercentile] BETWEEN 0.0 AND 100.0),

	PRIMARY KEY CLUSTERED([Id] ASC, [y] ASC, [Item] ASC)
)
WITH SCHEMABINDING
AS
BEGIN

	DECLARE	@SmoothingItems			INT		= 100;

	INSERT INTO @ReturnTable
	(
		[Id],
		[Item],
		[RowId],
		[y],
		[w],
		[wPercentile]
	)
	SELECT
		[t].[Id],
		[t].[Item],
		[t].[RowId],
		[t].[y],
		[t].[w],
		[t].[wPercentile]
	FROM (
		SELECT
			[s].[Id],
			[r].[RowId],
			[r].[y],
			[r].[w],
			[r].[wPercentile],

				[Item] = ROW_NUMBER() OVER(PARTITION BY [s].[Id] ORDER BY ABS([r].[wPercentile] - [s].[Id]) ASC, [r].[wPercentile] ASC)
		FROM
			[stat].[sysSequence]									[s]
		CROSS JOIN
			[stat].[sysRaw](@Sample, @SortOrder, @EndPointMean)		[r]
		WHERE
			[s].[Id]	<= @SmoothingItems
		) [t]
	WHERE
		[t].[Item]	<= @InterpolationPoints;

	RETURN;

END;
GO