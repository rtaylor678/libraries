﻿CREATE FUNCTION [stat].[DescriptiveHistogram]
(
	--	http://en.wikipedia.org/wiki/Portal:Statistics
	--	http://en.wikipedia.org/wiki/Histogram

	@Sample					[stat].[SampleRandomVar]	READONLY,								--	Data points to examine
	@Observation			FLOAT				= NULL,
	@zWidth					FLOAT				= 0.5											--	Histogram bucket width
)
RETURNS @ReturnTable TABLE 
(
	[zTestLower]			FLOAT				NOT NULL,										--	Minimum Z-Score of bucket
	[zTestUpper]			FLOAT				NOT NULL,										--	Maximum Z-Score of bucket
	[zRange]				AS CONVERT(VARCHAR(14), '[' + CONVERT(VARCHAR(5), ROUND([zTestLower], 2)) + ', ' + CONVERT(VARCHAR(5), ROUND([zTestUpper], 2)) + ')'),

	[LimitLower]			FLOAT				NOT	NULL,										--	Minimum value of bucket
	[LimitUpper]			FLOAT				NOT	NULL,										--	Maximum value of bucket
	[LimitRange]			AS '[' + CONVERT(VARCHAR(32), ROUND([LimitLower], 2)) + ', ' + CONVERT(VARCHAR(32), ROUND([LimitUpper], 2)) + ')',

	[xCount]				INT					NOT	NULL	CHECK([xCount]	>= 0),				--	Item Count

	[xHistogramObs]			AS	CASE WHEN [Observation] IS NOT NULL
								THEN	REPLICATE(CASE WHEN (ABS([zTestLower]) = [zTestUpper]) THEN N'▓' ELSE N'░' END,				FLOOR(([Observation] - [LimitLower]) / ([LimitUpper] - [LimitLower]) * [xCount])) + N'█' + 
										REPLICATE(CASE WHEN (ABS([zTestLower]) = [zTestUpper]) THEN N'▓' ELSE N'░' END, [xCount] -	FLOOR(([Observation] - [LimitLower]) / ([LimitUpper] - [LimitLower]) * [xCount]) - 1)
								ELSE	REPLICATE(CASE WHEN (ABS([zTestLower]) = [zTestUpper]) THEN N'▓' ELSE N'░' END, [xCount])
								END,

	[Observation]			FLOAT					NULL,

	[xMin]					FLOAT					NULL,										--	Minimum
	[xMax]					FLOAT					NULL,										--	Maximum
	[xRange]				AS [xMax] - [xMin]													--	Range
							PERSISTED					,
	[xMean]					FLOAT					NULL,										--	Mean

	CHECK([zTestLower] <= [zTestUpper]),
	CHECK([LimitLower] <= [LimitUpper]),

	CHECK([xMin]	<= [xMax]),
	CHECK([xMin]	BETWEEN [LimitLower] AND [LimitUpper]),
	CHECK([xMax]	BETWEEN [LimitLower] AND [LimitUpper]),
	CHECK([xMean]	BETWEEN [xMin] AND [xMax]),

	PRIMARY KEY CLUSTERED([zTestLower] ASC)
)
WITH SCHEMABINDING
AS
BEGIN

	DECLARE @xMean		FLOAT;
	DECLARE @xStDevP	FLOAT;
	DECLARE @zMin		FLOAT;
	DECLARE @zMax		FLOAT;

	DECLARE @zTestLower	FLOAT;
	DECLARE @zTestUpper	FLOAT;
	DECLARE @LimitLower	FLOAT;
	DECLARE @LimitUpper	FLOAT;

	SELECT
		@xMean		= AVG([s].[x]),
		@xStDevP	= STDEVP([s].[x]),
		@zMin		= (MIN([s].[x]) - AVG([s].[x])) / STDEVP([s].[x]),
		@zMax		= (MAX([s].[x]) - AVG([s].[x])) / STDEVP([s].[x])
	FROM
		@Sample [s];
		
	SET @zTestLower = FLOOR(@zMin) - (@zWidth / 2.0);

	WHILE @zTestLower <= CEILING(@zMax) + (@zWidth / 2.0)
	BEGIN
	
		SET @zTestUpper = @zTestLower + @zWidth;
		SET @LimitLower = @xMean + @zTestLower * @xStDevP
		SET @LimitUpper = @xMean + (@zTestLower + @zWidth) * @xStDevP;

		INSERT INTO @ReturnTable
		(
			[zTestLower],
			[zTestUpper],
			[LimitLower],
			[LimitUpper],
			[xCount],
			[xMin],
			[xMax],
			[xMean],
			[Observation]
		)
		SELECT
			[zTestLower]	= @zTestLower,
			[zTestUpper]	= @zTestUpper,
			[LimitLower]	= @LimitLower,
			[LimitUpper]	= @LimitUpper,
			[xCount]		= COUNT([s].[x]),
			[xMin]			= MIN([s].[x]),
			[xMax]			= MAX([s].[x]),
			[xMean]			= AVG([s].[x]),
			[Observation]	= CASE WHEN @Observation >= @LimitLower AND @Observation < @LimitUpper THEN @Observation END
		FROM
			@Sample	[s]
		WHERE
				[s].[x] >=	@LimitLower 
			AND [s].[x] <	@LimitUpper;
			
		SET @zTestLower = @zTestLower + @zWidth;
		
	END;
	
	RETURN;

END;
GO