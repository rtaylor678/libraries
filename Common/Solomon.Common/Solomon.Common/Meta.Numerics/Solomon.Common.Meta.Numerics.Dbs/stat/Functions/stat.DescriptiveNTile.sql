﻿CREATE FUNCTION [stat].[DescriptiveNTile]
(
	@Sample					[stat].[SampleRandomVar]	READONLY,									--	Data points to examine

	@SortOrder				SMALLINT			= 1,												--	Sort order of data 1 (Ascending), -1 Descending
	@Tiles					TINYINT				= 4													--	Number of nTiles
)
RETURNS TABLE
WITH SCHEMABINDING
AS RETURN
(
SELECT
	[d].[qTile],
	[d].[SampleType],

	[d].[xCount],
	[d].[xMin],
	[d].[xMax],
	[d].[xRange],

	[d].[BreakPrevious],
	[d].[BreakNext],

	[d].[xMean],
	[d].[xStDev],
	[d].[xVar],

	[d].[LimitLower],
	[d].[LimitUpper],
	[d].[LimitRange]

FROM
	[stat].[DescriptiveArithmetic](@Sample, @SortOrder, @Tiles, DEFAULT, DEFAULT, DEFAULT, DEFAULT) [d]
);