﻿CREATE FUNCTION [stat].[TwoSampleObservationsAnalysis]
(
	--	http://en.wikipedia.org/wiki/Portal:Statistics
	--	http://en.wikipedia.org/wiki/Summary_statistic
	--	http://en.wikipedia.org/wiki/Descriptive_statistics

	@SampleBasis			[stat].[SampleRandomVar]	READONLY,									--	Data points to examine
	@SampleAudit			[stat].[SampleRandomVar]	READONLY,									--	Data points to examine

	@SortOrder				SMALLINT			= 1,												--	Sort order of data 1 (Ascending), -1 Descending
	@Tiles					TINYINT				= 4,												--	Number of nTiles
	@Confidence_Pcnt		FLOAT				= 0.90,												--	Prediction/confidence interval level	[0.0, 1.0]
	@PearsonThreshold		FLOAT				= 0.90,												--	Pearson threshold						[0.0, 1.0]
	@ResidualThreshold		FLOAT				= 2.00												--	Residual identification threshold
)
RETURNS @ReturnTable TABLE 
(
	[Id]					INT					NOT	NULL,
	[RowId]					INT					NOT	NULL,
	[x]						FLOAT				NOT	NULL,											--	Raw value
	[w]						FLOAT					NULL,											--	Raw value

	[xQuartile]				TINYINT				NOT	NULL	CHECK([xQuartile] >= 1),				--	Quartile value						http://en.wikipedia.org/wiki/Quantile
	[xPercentile]			FLOAT				NOT	NULL	CHECK(ROUND([xPercentile], 5)	BETWEEN 0.0 AND 100.0),	--	Item-tile			http://en.wikipedia.org/wiki/Percentile
	[xPercentRank]			FLOAT				NOT	NULL	CHECK(ROUND([xPercentRank], 5)	BETWEEN 0.0 AND 100.0),	--	Percentile			https://en.wikipedia.org/wiki/Percentile_rank
	[xCumulative]			FLOAT				NOT	NULL	CHECK(ROUND([xCumulative], 5)	BETWEEN 0.0 AND 100.0),	--	Cumulative			https://en.wikipedia.org/wiki/Cumulative_distribution_function
	[xRank]					INT					NOT	NULL	CHECK([xRank]		>= 1),				--	Value Ranking						http://msdn.microsoft.com/en-us/library/ms176102.aspx
	[xRankDense]			INT					NOT	NULL	CHECK([xRankDense]	>= 1),				--	Value Ranking (Dense)				http://msdn.microsoft.com/en-us/library/ms173825.aspx

	[wPercentile]			FLOAT					NULL	CHECK(ROUND([wPercentile], 5)	BETWEEN 0.0 AND 100.0),	--	Item-tile			http://en.wikipedia.org/wiki/Percentile

	[zScore]				FLOAT				NOT	NULL,											--	Distance Away From Mean				http://en.wikipedia.org/wiki/Z-test
	[zQuartile]				TINYINT					NULL	CHECK([zQuartile] >= 1),				--	Quartile for Z-Test
	[zPercentile]			FLOAT				NOT	NULL	CHECK(ROUND([zPercentile], 5)	BETWEEN 0.0 AND 100.0),	--	Percentile			http://en.wikipedia.org/wiki/Percentile
	[zHypothesis]			INT					NOT	NULL,											--	Null Hypothesis 0 (Rejection), 1 (Acceptance)

	[ErrorAbsolute]			FLOAT				NOT	NULL,											--	Absolute approximation error		http://en.wikipedia.org/wiki/Approximation_error
	[ErrorRelative]			FLOAT					NULL,											--	Realitve approximation error		http://en.wikipedia.org/wiki/Approximation_error

	[ResidualStandard]		FLOAT					NULL,											--	Standard residual error				http://en.wikipedia.org/wiki/Errors_and_residuals_in_statistics
	[ResidualAbsolute]		FLOAT				NOT	NULL,
	[ResidualRelative]		FLOAT				NOT	NULL,

	[PlotQ1]				AS CONVERT(FLOAT, CASE WHEN [xQuartile] = 1 THEN [x] END)				--	Green
							PERSISTED					,
	[PlotQ2]				AS CONVERT(FLOAT, CASE WHEN [xQuartile] = 2 THEN [x] END)				--	Blue
							PERSISTED					,
	[PlotQ3]				AS CONVERT(FLOAT, CASE WHEN [xQuartile] = 3 THEN [x] END)				--	Yellow
							PERSISTED					,
	[PlotQ4]				AS CONVERT(FLOAT, CASE WHEN [xQuartile] = 4 THEN [x] END)				--	Red
							PERSISTED					,

	[PlotZ1]				AS CONVERT(FLOAT, CASE WHEN [zQuartile] = 1 THEN [x] END)				--	Green
							PERSISTED					,
	[PlotZ2]				AS CONVERT(FLOAT, CASE WHEN [zQuartile] = 2 THEN [x] END)				--	Blue
							PERSISTED					,
	[PlotZ3]				AS CONVERT(FLOAT, CASE WHEN [zQuartile] = 3 THEN [x] END)				--	Yellow
							PERSISTED					,
	[PlotZ4]				AS CONVERT(FLOAT, CASE WHEN [zQuartile] = 4 THEN [x] END)				--	Red
							PERSISTED					,

	[ResidualThreshold]		FLOAT				NOT	NULL	CHECK([ResidualThreshold] > 0.0) ,
	[ResidualStdIn]			AS CONVERT(FLOAT, CASE WHEN ABS([ResidualStandard]) <  [ResidualThreshold] THEN [ResidualStandard] END)
							PERSISTED				,
	[ResidualStdOut]		AS CONVERT(FLOAT, CASE WHEN ABS([ResidualStandard]) >= [ResidualThreshold] THEN [ResidualStandard] END)
							PERSISTED				,

	[xMean]					FLOAT				NOT	NULL,											--	Mean								http://en.wikipedia.org/wiki/Mean

	[ConfidenceInterval]	FLOAT				NOT	NULL	CHECK([ConfidenceInterval]	>= 0.0),	--	Confidence Interval					http://en.wikipedia.org/wiki/Confidence_interval
	[PredictionInterval]	FLOAT				NOT	NULL	CHECK([PredictionInterval]	>= 0.0),	--	Prediction Interval					http://en.wikipedia.org/wiki/Prediction_interval

	[PredictionLower]		AS CONVERT(FLOAT, [xMean] - [PredictionInterval])						--	Lower Prediction Limit
							PERSISTED			NOT	NULL,
	[ConfidenceLower]		AS CONVERT(FLOAT, [xMean] - [ConfidenceInterval])						--	Lower Confidence Limit
							PERSISTED			NOT	NULL,
	[ConfidenceUpper]		AS CONVERT(FLOAT, [xMean] + [ConfidenceInterval])						--	Upper Confidence Limit
							PERSISTED			NOT	NULL,
	[PredictionUpper]		AS CONVERT(FLOAT, [xMean] + [PredictionInterval])						--	Upper Prediction Limit
							PERSISTED			NOT	NULL,

	[ConfidenceIn]			AS CONVERT(FLOAT, CASE WHEN ([x]	 BETWEEN [xMean] - [ConfidenceInterval] AND [xMean] + [ConfidenceInterval]) THEN [x] END)
							PERSISTED					,
	[ConfidenceOut]			AS CONVERT(FLOAT, CASE WHEN ([x] NOT BETWEEN [xMean] - [ConfidenceInterval] AND [xMean] + [ConfidenceInterval]) THEN [x] END)
							PERSISTED					,
	[PredictionIn]			AS CONVERT(FLOAT, CASE WHEN ([x]	 BETWEEN [xMean] - [PredictionInterval] AND [xMean] + [PredictionInterval]) THEN [x] END)
							PERSISTED					,
	[PredictionOut]			AS CONVERT(FLOAT, CASE WHEN ([x] NOT BETWEEN [xMean] - [PredictionInterval] AND [xMean] + [PredictionInterval]) THEN [x] END)
							PERSISTED					,

	[PearsonThreshold]		FLOAT				NOT	NULL	CHECK(ROUND([PearsonThreshold], 5) BETWEEN 0.0 AND 1.0),

	[PearsonIn]				AS CONVERT(FLOAT, CASE WHEN ([ResidualAbsolute] <= [PearsonThreshold]	AND [ResidualRelative] <= [PearsonThreshold]) THEN [x] END)
							PERSISTED					,	--	Ring (Black)
	[PearsonOut]			AS CONVERT(FLOAT, CASE WHEN ([ResidualAbsolute] > [PearsonThreshold]	OR	[ResidualRelative] > [PearsonThreshold]) THEN [x] END)
							PERSISTED					,	--	Ring (Black)

	CHECK ([ConfidenceInterval] <= [PredictionInterval]),
	PRIMARY KEY CLUSTERED ([x] ASC, [Id] ASC)
)
WITH SCHEMABINDING
AS
BEGIN

	DECLARE	@Order					FLOAT	= CONVERT(FLOAT, ABS(@SortOrder) / @SortOrder);

	DECLARE @Observations	[stat].[ResultsObservations]

	INSERT INTO @Observations
	(
		[Id],
		[RowId],
		[x],
		[w],

		[xQuartile],
		[xPercentile],
		[xPercentRank],
		[xCumulative],
		[xRank],
		[xRankDense],

		[wPercentile],

		[zScore],
		[zQuartile],
		[zPercentile],
		[zHypothesis],

		[ErrorAbsolute],
		[ErrorRelative],

		[ResidualStandard],
		[ResidualAbsolute],
		[ResidualRelative],

		[PlotQ1],
		[PlotQ2],
		[PlotQ3],
		[PlotQ4],

		[PlotZ1],
		[PlotZ2],
		[PlotZ3],
		[PlotZ4],

		[ResidualThreshold],
		[ResidualStdIn],
		[ResidualStdOut],

		[xMean],

		[ConfidenceInterval],
		[PredictionInterval],

		[PredictionLower],
		[ConfidenceLower],
		[ConfidenceUpper],
		[PredictionUpper],

		[ConfidenceIn],
		[ConfidenceOut],
		[PredictionIn],
		[PredictionOut],

		[PearsonThreshold],
		[PearsonIn],
		[PearsonOut]
	)
	SELECT
		[o].[Id],
		[o].[RowId],
		[o].[x],
		[o].[w],

		[o].[xQuartile],
		[o].[xPercentile],
		[o].[xPercentRank],
		[o].[xCumulative],
		[o].[xRank],
		[o].[xRankDense],

		[o].[wPercentile],

		[o].[zScore],
		[o].[zQuartile],
		[o].[zPercentile],
		[o].[zHypothesis],

		[o].[ErrorAbsolute],
		[o].[ErrorRelative],

		[o].[ResidualStandard],
		[o].[ResidualAbsolute],
		[o].[ResidualRelative],

		[o].[PlotQ1],
		[o].[PlotQ2],
		[o].[PlotQ3],
		[o].[PlotQ4],

		[o].[PlotZ1],
		[o].[PlotZ2],
		[o].[PlotZ3],
		[o].[PlotZ4],

		[o].[ResidualThreshold],
		[o].[ResidualStdIn],
		[o].[ResidualStdOut],

		[o].[xMean],

		[o].[ConfidenceInterval],
		[o].[PredictionInterval],

		[o].[PredictionLower],
		[o].[ConfidenceLower],
		[o].[ConfidenceUpper],
		[o].[PredictionUpper],

		[o].[ConfidenceIn],
		[o].[ConfidenceOut],
		[o].[PredictionIn],
		[o].[PredictionOut],

		[o].[PearsonThreshold],
		[o].[PearsonIn],
		[o].[PearsonOut]

	FROM
		[stat].[ObservationsAnalysis](@SampleBasis, @SortOrder, @Tiles, @Confidence_Pcnt, @PearsonThreshold, @ResidualThreshold) [o];

	INSERT INTO @ReturnTable
	(
		[Id], [RowId],
		[x], [w],
		[xQuartile], [xPercentile], [xPercentRank], [xCumulative], [xRank], [xRankDense], [wPercentile],
		[zScore], [zQuartile], [zPercentile], [zHypothesis],
		[ErrorAbsolute], [ErrorRelative],
		[ResidualStandard], [ResidualAbsolute], [ResidualRelative],
		[ResidualThreshold],
		[xMean],
		[ConfidenceInterval],
		[PredictionInterval],
		[PearsonThreshold]
	)
	SELECT
		[a].[Id],
		[a].[RowId],
		[a].[x],
		[a].[w],

			[xQuartile]			= ROUND([stat].[sysInterpolate]([a].[x], [p].[x], [p].[xQuartile],		[n].[x], [n].[xQuartile],		1, DEFAULT),	0),
			[xPercentile]		=		[stat].[sysInterpolate]([a].[x], [p].[x], [p].[xPercentile],	[n].[x], [n].[xPercentile],		0.0, 100.0),
			[xPercentRank]		=		[stat].[sysInterpolate]([a].[x], [p].[x], [p].[xPercentRank],	[n].[x], [n].[xPercentRank],	0.0, 100.0),
			[xCumulative]		=		[stat].[sysInterpolate]([a].[x], [p].[x], [p].[xCumulative],	[n].[x], [n].[xCumulative],		0.0, 100.0),
			[xRank]				= ROUND([stat].[sysInterpolate]([a].[x], [p].[x], [p].[xRank],			[n].[x], [n].[xRank],			1, DEFAULT),	0),
			[xRankDense]		= ROUND([stat].[sysInterpolate]([a].[x], [p].[x], [p].[xRankDense],		[n].[x], [n].[xRankdense],		1, DEFAULT),	0),
			[wPercentile]		=		[stat].[sysInterpolate]([a].[w], [p].[w], [p].[wPercentile],	[n].[w], [n].[wPercentile],		0.0, 100.0),

			[zScore]			= CASE WHEN ([d].[xStDev] <> 0.0) THEN ([a].[x] - [d].[xMean]) / [d].[xStDev] ELSE 0.0 END,

			[zQuartile]			= ROUND([stat].[sysInterpolate]([a].[x], [p].[x], [p].[zQuartile],		[n].[x], [n].[zQuartile],		1, DEFAULT),	0),

			[zPercentile]		= [stat].[Student_Probability](CONVERT(FLOAT, [d].[xCount]), CASE WHEN ([d].[xStDev] <> 0.0) THEN ([a].[x] - [d].[xMean]) / [d].[xStDev] ELSE 0.0 END, 1),
			[zHypothesis]		= CASE WHEN ((CASE WHEN ([d].[xStDev] <> 0.0) THEN ABS([a].[x] - [d].[xMean]) / [d].[xStDev] ELSE 0.0 END) < [d].[StudentTStatistic])
									THEN 1
									ELSE 0
									END,

			[ErrorAbsolute]		= ([a].[x] - [d].[xMean]),
			[ErrorRelative]		= CASE WHEN [d].[xMean] <> 0.0 THEN ([a].[x] - [d].[xMean]) / [d].[xMean] END,

			[ResidualStandard]	= CASE WHEN [d].[SSE] <> 0.0 AND SQRT(SQUARE([d].[xStDev]) * (1.0 - 1.0/[d].[xCount] - SQUARE([a].[x] - [d].[xMean]) / [d].[SSE])) <> 0.0
					THEN
						([a].[x] - [d].[xMean]) /
						SQRT(SQUARE([d].[xStDev]) * (1.0 - 1.0/[d].[xCount] - SQUARE([a].[x] - [d].[xMean]) / [d].[SSE]))
					END,

			[ResidualAbsolute]	= [stat].[sysInterpolate]([a].[x], [p].[x], [p].[ResidualRelative],	[n].[x], [n].[ResidualRelative], DEFAULT, DEFAULT),
			[ResidualRelative]	= [stat].[sysInterpolate]([a].[x], [p].[x], [p].[ResidualRelative],	[n].[x], [n].[ResidualRelative], DEFAULT, DEFAULT),

			[ResidualThreshold]		= @ResidualThreshold,
		[d].[xMean],
		[d].[ConfidenceInterval],
		[d].[PredictionInterval],
			[PearsonThreshold]		= @PearsonThreshold
	FROM
		@SampleAudit	[a]

	CROSS APPLY
		(SELECT TOP 1
			[b].[Id],
			[b].[RowId],
			[b].[x],
			[b].[w],
			[b].[xQuartile],
			[b].[xPercentile],
			[b].[xPercentRank],
			[b].[xCumulative],
			[b].[xRank],
			[b].[xRankDense],
			[b].[wPercentile],
			[b].[zQuartile],
			[b].[ResidualRelative],
			[b].[ResidualAbsolute]
		 FROM
			@Observations [b]
		WHERE  ([b].[x] <= [a].[x])
			OR ([b].[x] <  [a].[x] AND [b].[Id] = 1)
		ORDER BY
			[b].[x] * @Order DESC,
			[b].[Id] ASC
		) [p]

	CROSS APPLY
		(SELECT TOP 1
			[b].[Id],
			[b].[RowId],
			[b].[x],
			[b].[w],
			[b].[xQuartile],
			[b].[xPercentile],
			[b].[xPercentRank],
			[b].[xCumulative],
			[b].[xRank],
			[b].[xRankDense],
			[b].[wPercentile],
			[b].[zQuartile],
			[b].[ResidualRelative],
			[b].[ResidualAbsolute]
		 FROM
			@Observations [b]
		WHERE  ([b].[x] >= [a].[x])
			OR ([b].[x] >  [a].[x] AND [b].[Id] = (SELECT TOP 1 [m].[Id] FROM @Observations [m] ORDER BY [b].[x] * @Order ASC, [b].[Id] ASC))
		ORDER BY
			[b].[x] * @Order ASC,
			[b].[Id] ASC
		) [n]

	CROSS JOIN
		[stat].[DescriptiveArithmetic](@SampleBasis, @SortOrder, 1, @Confidence_Pcnt, DEFAULT, DEFAULT, DEFAULT)	[d]

	RETURN;

END;
GO