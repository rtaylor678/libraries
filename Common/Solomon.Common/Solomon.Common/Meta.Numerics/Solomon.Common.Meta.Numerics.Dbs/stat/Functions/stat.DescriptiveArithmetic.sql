﻿CREATE FUNCTION [stat].[DescriptiveArithmetic]
(
	--	http://en.wikipedia.org/wiki/Portal:Statistics
	--	http://en.wikipedia.org/wiki/Summary_statistic
	--	http://en.wikipedia.org/wiki/Descriptive_statistics

	@Sample					[stat].[SampleRandomVar]	READONLY,									--	Data points to examine

	@SortOrder				SMALLINT			= 1,												--	Sort order of data 1 (Ascending), -1 Descending
	@Tiles					TINYINT				= 1,												--	Number of nTiles
	@Confidence_Pcnt		FLOAT				= 0.90,												--	Prediction/confidence interval level	[0.0, 1.0]

	@CapMin					FLOAT				= NULL,												--	Minimum reporting values to average
	@CapMax					FLOAT				= NULL,												--	Maximum reporting values to average
	@EndPointMean			TINYINT				= 2													--	Number of endpoints to average
)
RETURNS @ReturnTable TABLE
(
	[qTile]					TINYINT				NOT	NULL	CHECK([qTile]	>= 1),					--	Quartile value
	[SampleType]			VARCHAR(10)			NOT	NULL,

	[xCount]				INT					NOT	NULL	CHECK([xCount]	>= 1),					--	Item Count
	[xMin]					FLOAT				NOT	NULL,											--	Minimum
	[xMax]					FLOAT				NOT	NULL,											--	Maximum
	[xRange]				AS CONVERT(FLOAT, [xMax] - [xMin])
							PERSISTED			NOT	NULL,

	[BreakPrevious]			FLOAT					NULL,
	[BreakNext]				FLOAT					NULL,

	[xMean]					FLOAT				NOT	NULL,											--	Mean								http://en.wikipedia.org/wiki/Mean
	[xStDev]				FLOAT				NOT	NULL	CHECK([xStDev]	>= 0.0),				--	Standard Deviation			σ		http://en.wikipedia.org/wiki/Standard_deviation
	[xVar]					FLOAT				NOT	NULL	CHECK([xVar]	>= 0.0),				--	Variance					σ²		http://en.wikipedia.org/wiki/Variance

	[ErrorStandard]			FLOAT				NOT	NULL,											--	Standard Error						http://en.wikipedia.org/wiki/Z-test
	[VariationCoefficient]	FLOAT					NULL,											--  Coefficient of Variation			http://en.wikipedia.org/wiki/Coefficient_of_variation
	[DispersionIndex]		FLOAT					NULL,											--  Index of Dispersion					http://en.wikipedia.org/wiki/Variance-to-mean_ratio

	[xSum]					FLOAT				NOT	NULL,											--	Sum	of x					Σ(x)
	[SSE]					FLOAT				NOT	NULL,											--	Sum Square Error			Σ(xi-AVG(x))
	[MSE]					FLOAT				NOT	NULL,											--	Mean Square Error			Σ(xi-AVG(x)) / (n-2)

	[xwMean]				FLOAT					NULL,											--	Weighted Mean
	[xwDiv]					FLOAT					NULL,											--	SUM(x) / SUM(w)

	[LimitLower]			FLOAT				NOT	NULL,											--	Minimum (Mean of smallest two items, >= @CapMin)
	[LimitUpper]			FLOAT				NOT	NULL,											--	Minimum (Mean of largest two items, <= @CapMax)
	[LimitRange]			AS CONVERT(FLOAT, [LimitUpper] - [LimitLower])
							PERSISTED			NOT	NULL,

	[StudentTTest]			FLOAT				NOT	NULL	CHECK([StudentTTest]		>= 0.0),	--	Calculated T Test
	[StudentTConfidence]	FLOAT				NOT	NULL	CHECK([StudentTConfidence]	>= 0.0),	--	Calculated Confidence
	[StudentTAlpha]			FLOAT				NOT	NULL	CHECK([StudentTAlpha]		>= 0.0),	--	Alpha' value						http://en.wikipedia.org/wiki/Student's_t-test
	[StudentTStatistic]		FLOAT				NOT	NULL	CHECK([StudentTStatistic]	>= 0.0),	--	Student T Statistic
	[StudentTHypothesis]	TINYINT				NOT	NULL,

	[IsNormalStatistic]		FLOAT				NOT	NULL,
	[IsNormalProbability]	FLOAT				NOT	NULL,
	[IsUniformStatistic]	FLOAT				NOT	NULL,
	[IsUniformProbability]	FLOAT				NOT	NULL,

	[ConfidenceInterval]	FLOAT				NOT	NULL	CHECK([ConfidenceInterval]	>= 0.0),	--	Confidence Interval					http://en.wikipedia.org/wiki/Confidence_interval
	[PredictionInterval]	FLOAT				NOT	NULL	CHECK([PredictionInterval]	>= 0.0),	--	Prediction Interval					http://en.wikipedia.org/wiki/Prediction_interval
	
	[PredictionLower]		AS CONVERT(FLOAT, [xMean] - [PredictionInterval])						--	Lower Prediction Limit
							PERSISTED			NOT	NULL,
	[ConfidenceLower]		AS CONVERT(FLOAT, [xMean] - [ConfidenceInterval])						--	Lower Confidence Limit
							PERSISTED			NOT	NULL,
	[ConfidenceUpper]		AS CONVERT(FLOAT, [xMean] + [ConfidenceInterval])						--	Upper Confidence Limit
							PERSISTED			NOT	NULL,
	[PredictionUpper]		AS CONVERT(FLOAT, [xMean] + [PredictionInterval])						--	Upper Prediction Limit
							PERSISTED			NOT	NULL,

	[BoxLowerExtreme]		AS CONVERT(FLOAT, [BoxLower] - ([BoxUpper] - [BoxLower]) * 3.0)
							PERSISTED			NOT	NULL,
	[BoxLowerWhisker]		AS CONVERT(FLOAT, [BoxLower] - ([BoxUpper] - [BoxLower]) * 1.5)
							PERSISTED			NOT	NULL,
	[BoxLower]				FLOAT				NOT	NULL,
	[BoxMedian]				FLOAT				NOT	NULL,
	[BoxUpper]				FLOAT				NOT	NULL,
	[BoxUpperWhisker]		AS CONVERT(FLOAT, [BoxUpper] + ([BoxUpper] - [BoxLower]) * 1.5)
							PERSISTED			NOT	NULL,
	[BoxUpperExtreme]		AS CONVERT(FLOAT, [BoxUpper] + ([BoxUpper] - [BoxLower]) * 3.0)
							PERSISTED			NOT	NULL,
	[BoxIMR]				AS CONVERT(FLOAT, [BoxUpper] - [BoxLower])
							PERSISTED			NOT	NULL,
	[BoxWidth]				AS CONVERT(FLOAT, SQRT([xCount]))
							PERSISTED			NOT	NULL,
	[BoxNotch]				AS CONVERT(FLOAT, ([BoxUpper] - [BoxLower]) / SQRT([xCount]))
							PERSISTED			NOT	NULL,

	[Kurtosis]				FLOAT					NULL,											--	Pearson's sample excess Kurtosis	http://en.wikipedia.org/wiki/Kurtosis
	[Skewness]				FLOAT					NULL,											--	Skewness (negative leans right)		http://en.wikipedia.org/wiki/Skewness

	CHECK([xMin]				<= [xMean]),
	CHECK([xMean]				<= [xMax]),
	CHECK([xMin]				<= [LimitLower]),
	CHECK([xMax]				>= [LimitUpper]),
	CHECK([ConfidenceInterval]	<= [PredictionInterval]),

	CHECK([BoxLowerExtreme]		<= [BoxLowerWhisker]),
	CHECK([BoxLowerWhisker]		<= [BoxLower]),
	CHECK([BoxLower]			<= [BoxMedian]),
	CHECK([BoxMedian]			<= [BoxUpper]),
	CHECK([BoxUpper]			<= [BoxUpperWhisker]),
	CHECK([BoxUpperWhisker]		<= [BoxUpperExtreme]),

	PRIMARY KEY CLUSTERED([qTile] ASC)
)
WITH SCHEMABINDING
AS
BEGIN

	DECLARE	@Order				FLOAT	= CONVERT(FLOAT, ABS(@SortOrder) / @SortOrder);
	DECLARE	@nTiles				TINYINT	= COALESCE(@Tiles, 4);

	DECLARE @Confidence_Max		FLOAT = 0.999999999999999;
	SET @Confidence_Pcnt		= CASE WHEN @Confidence_Pcnt > @Confidence_Max THEN @Confidence_Max ELSE @Confidence_Pcnt END;

	DECLARE @StudentTAlpha		FLOAT	= @Confidence_Pcnt;

	DECLARE @xCount				INT;
	SELECT	@xCount				= COUNT(1) FROM @Sample;

	IF (@xCount / 2.0 < @Tiles) BEGIN SET @Tiles = @xCount / 2.0;	END;
	IF (@EndPointMean <= 1)		BEGIN SET @EndPointMean = 1;		END;

	DECLARE @nTile TABLE
	(
		[Id]				INT					NOT	NULL,
		[RowId]				INT					NOT	NULL,
		[x]					FLOAT				NOT	NULL,
		[w]					FLOAT					NULL,
		[qTile]				TINYINT				NOT	NULL	CHECK([qTile] >= 1),			--	Quartile value
		[rnMin]				INT						NULL	CHECK([rnMin] >= 1),			--	Row Number
		[rnMax]				INT						NULL	CHECK([rnMax] >= 1),			--	Row Number
		
		PRIMARY KEY CLUSTERED ([x] ASC, [Id] ASC)
	);

	INSERT INTO @nTile
	(
		[Id],
		[RowId],
		[x],
		[w],
		[qTile],
		[rnMin],
		[rnMax]
	)
	SELECT
		[o].[Id],
		[o].[RowId],
		[o].[x],
		[o].[w],
		[o].[qTile],
			[rnMin]	= CASE WHEN [o].[x] >= COALESCE(@CapMin, [o].[x])
						THEN
							ROW_NUMBER() OVER(PARTITION BY CASE WHEN [o].[x] BETWEEN COALESCE(@CapMin, [o].[x]) AND COALESCE(@CapMax, [o].[x]) THEN [o].[qTile] END ORDER BY [o].[x] * @Order ASC, [o].[RowId] ASC)
						END,
			[rnMax]	= CASE WHEN [o].[x] <= COALESCE(@CapMax, [o].[x])
						THEN
							ROW_NUMBER() OVER(PARTITION BY CASE WHEN [o].[x] BETWEEN COALESCE(@CapMin, [o].[x]) AND COALESCE(@CapMax, [o].[x]) THEN [o].[qTile] END ORDER BY [o].[x] * @Order DESC, [o].[RowId] ASC)
						END	
	FROM (
		SELECT
			[o].[Id],
			[o].[RowId],
			[o].[x],
			[o].[w],
				[qTile] = NTILE(@nTiles) OVER(ORDER BY [o].[x] * @Order ASC, [o].[RowId] ASC)	
		FROM @Sample [o]
		) [o];

	DECLARE @Descriptive TABLE
	(
		[qTile]					TINYINT				NOT	NULL	CHECK([qTile]	>= 1),						--	Quartile value
		[xCount]				FLOAT				NOT	NULL,												--	Minimum
		[xMin]					FLOAT				NOT	NULL,												--	Minimum
		[xMax]					FLOAT				NOT	NULL,												--	Maximum
		[_xRange]				AS CAST([xMax] - [xMin]			AS FLOAT)									--	Range
								PERSISTED			NOT	NULL,
		[xSum]					FLOAT				NOT	NULL,												--	Sum	of x					Σ(x)
		[xMean]					FLOAT				NOT	NULL,												--	Mean								http://en.wikipedia.org/wiki/Mean
		[xStDev]				FLOAT				NOT	NULL	CHECK([xStDev]	>= 0.0),					--	Standard Deviation			σ		http://en.wikipedia.org/wiki/Standard_deviation
		[xVar]					FLOAT				NOT	NULL	CHECK([xVar]	>= 0.0),					--	Variance					σ²		http://en.wikipedia.org/wiki/Variance
		
		[_ErrorStandard]		AS [xStDev] / SQRT([xCount])												--	Standard Error						http://en.wikipedia.org/wiki/Z-test
								PERSISTED			NOT	NULL,
		[_xCoeffVarP]			AS CASE WHEN [xMean] <> 0.0 THEN [xStDev]	/ [xMean] END					--  Coefficient of Variation			http://en.wikipedia.org/wiki/Coefficient_of_variation
								PERSISTED					,
		[_xIdxDispP]			AS CASE WHEN [xMean] <> 0.0 THEN [xVar]		/ [xMean] END					--  Index of Dispersion					http://en.wikipedia.org/wiki/Variance-to-mean_ratio
								PERSISTED					,
		
		[xwMean]				FLOAT					NULL,												--	Weighted Mean
		[xwDiv]					FLOAT					NULL,
		[xLimitLower]			FLOAT				NOT	NULL,
		[xLimitUpper]			FLOAT				NOT	NULL,
		[_xLimitRange]			AS ([xLimitUpper] - [xLimitLower])											--	Range
								PERSISTED			NOT	NULL,

		[_StudentTTest]			AS CASE WHEN [xStDev] <> 0.0 THEN ABS([xMean]) / [xStDev] * SQRT([xCount]) ELSE 0.0 END		--	Calcualted T Test
								PERSISTED			NOT	NULL,

		[IsNormalStatistic]		FLOAT				NOT	NULL,
		[IsNormalProbability]	FLOAT				NOT	NULL,
		[IsUniformStatistic]	FLOAT				NOT	NULL,
		[IsUniformProbability]	FLOAT				NOT	NULL,
		
		[StudentTStatistic]		FLOAT				NOT	NULL,
		[ConfidenceInterval]	FLOAT				NOT	NULL,
		[PredictionInterval]	FLOAT				NOT	NULL,

		PRIMARY KEY CLUSTERED ([qTile] ASC)
	);

	INSERT INTO @Descriptive
	(
		[qTile],
		[xCount],
		[xMin],
		[xMax],
		[xSum],
		[xMean],
		[xStDev],
		[xVar],
		[xwMean],
		[xwDiv],
		[xLimitLower],
		[xLimitUpper],
		[IsNormalStatistic],
		[IsNormalProbability],
		[IsUniformStatistic],
		[IsUniformProbability],
		[StudentTStatistic],
		[ConfidenceInterval],
		[PredictionInterval]
	)
	SELECT
		[n].[qTile],
			[xCount]			= COUNT([n].[x]),
			[xMin]				= MIN([n].[x]),
			[xMax]				= MAX([n].[x]),
			[xSum]				= SUM([n].[x]),
			[xMean]				= AVG([n].[x]),
			[xStDev]			= CASE WHEN @nTiles	= 1 THEN STDEVP([n].[x])	ELSE STDEV([n].[x])	END,
			[xVar]				= CASE WHEN @nTiles	= 1 THEN VARP([n].[x])		ELSE VAR([n].[x])	END,
			[xwMean]			= CASE WHEN SUM([n].[w]) <> 0.0 THEN SUM([n].[x] * [n].[w])	/ SUM([n].[w]) END,
			[xwDiv]				= CASE WHEN SUM([n].[w]) <> 0.0 THEN SUM([n].[x])			/ SUM([n].[w]) END,
			[xMeanLower]		= AVG(CASE WHEN [n].[rnMin] <= @EndPointMean THEN [n].[x] END),
			[xMeanUpper]		= AVG(CASE WHEN [n].[rnMax] <= @EndPointMean THEN [n].[x] END),

			[IsNormalStatistic]		= [stat].[IsNormalDistribution_Statistic]([n].[x]),
			[IsNormalProbability]	= ROUND(100.0 * [stat].[IsNormalDistribution_Probability]([n].[x]), 5),

			[IsUniformStatistic]	= [stat].[IsUniformDistribution_Statistic]([n].[x]),
			[IsUniformProbability]	= ROUND(100.0 * [stat].[IsUniformDistribution_Probability]([n].[x]), 5),

			[StudentTStatistic]		= [stat].[Student_Statistic](COUNT(1) - 1, @StudentTAlpha, 2),
			[ConfidenceInterval]	= [stat].[ConfidenceInterval]([n].[x], @StudentTAlpha),
			[PredictionInterval]	= [stat].[ConfidenceInterval]([n].[x], @StudentTAlpha) * SQRT(COUNT(1)) / SQRT(1.0 + 1.0 / COUNT(1))

	FROM
		@nTile	[n]
	GROUP BY
		[n].[qTile];

	DECLARE @ComboStats TABLE
	(
		[qTile]					TINYINT				NOT	NULL	CHECK([qTile]	>= 1),				--	Quartile value
		[SSE]					FLOAT				NOT	NULL,										--	Sum Square Error		Σ(xi-AVG(x))
		[MSE]					FLOAT				NOT	NULL,										--	Mean Square Error		Σ(xi-AVG(x))
		[Kurtosis]				FLOAT					NULL,										--	Pearson's sample excess Kurtosis	http://en.wikipedia.org/wiki/Kurtosis
		[Skewness]				FLOAT					NULL,										--	Skewness (negative leans right)		http://en.wikipedia.org/wiki/Skewness
		[StudentTConfidence]	FLOAT				NOT	NULL,										--	Calculated Confidence
		
		PRIMARY KEY CLUSTERED ([qTile] ASC)
	);

	INSERT INTO @ComboStats
	(
		[qTile],
		[SSE],
		[MSE],
		[Kurtosis],
		[Skewness],
		[StudentTConfidence]
	)
	SELECT
		[d].[qTile],
			[SSE]					= SUM(SQUARE([n].[x] - [d].[xMean])),
			[MSE]					= SUM(SQUARE([n].[x] - [d].[xMean])) / ([d].[xCount] - 2.0),

			[Kurtosis]				= CASE WHEN SUM(SQUARE([n].[x] - [d].[xMean])) <> 0.0
										THEN
											(1.0 / [d].[xCount]) * SUM(POWER([n].[x] - [d].[xMean], 4)) / SQUARE((1.0 / [d].[xCount]) * SUM(SQUARE([n].[x] - [d].[xMean]))) - 3.0
										END,

			[Skewness]				= CASE WHEN SUM(SQUARE([n].[x] - [d].[xMean])) <> 0.0 AND ([d].[xCount] - 2.0) <> 0.0
										THEN 
											(1.0 / [d].[xCount]) * SUM(POWER([n].[x] - [d].[xMean], 3)) / POWER((1.0 / [d].[xCount]) * SUM(SQUARE([n].[x] - [d].[xMean])), 3.0 / 2.0)
											* SQRT([d].[xCount] * ([d].[xCount] - 1.0)) / ([d].[xCount] - 2.0)
										END,

			[StudentTConfidence]	= CASE WHEN [d].[_StudentTTest] <> 0.0 THEN [stat].[Student_Probability]([d].[xCount] - 1, [d].[_StudentTTest], 2) END

	FROM
		@Descriptive		[d]
	INNER JOIN 
		@nTile				[n]
			ON	[n].[qTile]	= [d].[qTile]
	GROUP BY
		[d].[qTile],
		[d].[xCount],
		[d].[_StudentTTest],
		[d].[xStDev];

	DECLARE @BoxIMR TABLE
	(
		[qTile]				TINYINT				NOT	NULL	CHECK([qTile]	>= 1),		--	Quartile value

		[BoxLower]			FLOAT				NOT	NULL,
		[BoxMedian]			FLOAT				NOT	NULL,
		[BoxUpper]			FLOAT				NOT	NULL,

		CHECK([BoxLower]		<= [BoxMedian]),
		CHECK([BoxMedian]		<= [BoxUpper]),

		PRIMARY KEY CLUSTERED([qTile] ASC)
	);

	INSERT INTO @BoxIMR
	(
		[qTile],
		[BoxLower],
		[BoxMedian],
		[BoxUpper]
	)
	SELECT
		[n].[qTile],
			[BoxLower]	= AVG(
				CASE WHEN  [n].[rnMin] = CEILING([d].[xCount] / 4)
						OR([n].[rnMin] = CEILING([d].[xCount] / 4) - 1					AND
							(
								(CONVERT(INT, [d].[xCount]) % 2 = 0 AND CONVERT(INT, [d].[xCount]) % 4 = 2)
							OR	(CONVERT(INT, [d].[xCount]) % 2 = 1 AND CONVERT(INT, [d].[xCount]) % 4 = 1)
							)
						)
					THEN
						[n].[x]
					END
					),
			[BoxMedian] = AVG(
				CASE WHEN  [n].[rnMin] = CEILING([d].[xCount] / 2)
						OR([n].[rnMin] = CEILING([d].[xCount] / 2) + 1					AND CONVERT(INT, [d].[xCount]) % 2 = 0)
					THEN
						[n].[x]
					END
					),
			[BoxUpper]	= AVG(
				CASE WHEN  [n].[rnMin] = [d].[xCount] - CEILING([d].[xCount] / 4) + 1
						OR([n].[rnMin] = [d].[xCount] - CEILING([d].[xCount] / 4) + 2	AND
							(
								(CONVERT(INT, [d].[xCount]) % 2 = 0 AND CONVERT(INT, [d].[xCount]) % 4 = 2)
							OR	(CONVERT(INT, [d].[xCount]) % 2 = 1 AND CONVERT(INT, [d].[xCount]) % 4 = 1)
							)
						)
					THEN
						[n].[x]
					END
					)
	FROM
		@Descriptive		[d]
	INNER JOIN
		@nTile				[n]
			ON	[n].[qTile]	= [d].[qTile]
	GROUP BY
		[n].[qTile],
		[d].[xCount];

	INSERT INTO @ReturnTable
	(
		[qTile],
		[SampleType],
		[xCount],
		[xMin],
		[xMax],

		[BreakPrevious],
		[BreakNext],

		[xMean],
		[xStDev],
		[xVar],

		[ErrorStandard],
		[VariationCoefficient],
		[DispersionIndex],

		[xSum],
		[SSE],
		[MSE],

		[xwMean],
		[xwDiv],

		[LimitLower],
		[LimitUpper],

		[StudentTTest],
		[StudentTConfidence],
		[StudentTAlpha],
		[StudentTStatistic],
		[StudentTHypothesis],

		[IsNormalStatistic],
		[IsNormalProbability],
		[IsUniformStatistic],
		[IsUniformProbability],

		[ConfidenceInterval],
		[PredictionInterval],

		[BoxLower],
		[BoxMedian],
		[BoxUpper],

		[Kurtosis],
		[Skewness]
	)
	SELECT
		[d].[qTile],
			[SampleType]		= CASE WHEN @nTiles = 1 THEN 'Population' ELSE 'Sample' END,
			[xCount]			= CONVERT(INT, [d].[xCount]),
		[d].[xMin],
		[d].[xMax],

			[BreakPrevious]		= ([d].[xMin] + [p].[xMax]) / 2.0,
			[BreakNext]			= ([d].[xMax] + [n].[xMin]) / 2.0,

		[d].[xMean],
		[d].[xStDev],
		[d].[xVar],

		[d].[_ErrorStandard],
		[d].[_xCoeffVarP],
		[d].[_xIdxDispP],

		[d].[xSum],
		[c].[SSE],
		[c].[MSE],
	
		[d].[xwMean],
		[d].[xwDiv],

		[d].[xLimitLower],
		[d].[xLimitUpper],
	
		[d].[_StudentTTest],
		[c].[StudentTConfidence],
			[StudentTAlpha]			= @StudentTAlpha,
		[d].[StudentTStatistic],
			[StudentTHypothesis]	= CASE WHEN [d].[StudentTStatistic] > [d].[_StudentTTest] THEN 1 ELSE 0 END,	--	Null hypothesis test 1 (Accept) or 0 (Reject)

		[d].[IsNormalStatistic],
		[d].[IsNormalProbability],
		[d].[IsUniformStatistic],
		[d].[IsUniformProbability],

		[d].[ConfidenceInterval],
		[d].[PredictionInterval],

		[b].[BoxLower],
		[b].[BoxMedian],
		[b].[BoxUpper],

		[c].[Kurtosis],
		[c].[Skewness]

	FROM
		@Descriptive			[d]

	LEFT OUTER JOIN
		@Descriptive			[p]
			ON	[p].[qTile]		= [d].[qTile] - 1
	LEFT OUTER JOIN
		@Descriptive			[n]
			ON	[n].[qTile]		= [d].[qTile] + 1
	INNER JOIN
		@ComboStats				[c]
			ON	[c].[qTile]		= [d].[qTile]
	INNER JOIN
		@BoxIMR					[b]
			ON	[b].[qTile]		= [d].[qTile];

	RETURN;

END;
GO