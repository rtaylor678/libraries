﻿CREATE FUNCTION [stat].[ObservationsSmoothed]
(
	@Sample					[stat].[SampleRandomVar]	READONLY,									--	Data points to examine
	@SmoothingType			CHAR (1)	= 'N',			--	Type of smoothing				
														--		M: Mean (Simple Average)
														--		L: Linear Regression
														--		N: Newton's Divided-Difference (Lagrange Interpolating Polynomials)
														--		S: Shepard's Method (Inverse Distance Weighted Interpolation)
	@SortOrder				SMALLINT	= 1				--	Sort order
)
RETURNS @ReturnTable TABLE 
(
	[xPlot]					FLOAT				NOT	NULL,

	[yPlot]					FLOAT					NULL,
	[yInterpolated]			FLOAT					NULL,

	[RowId]					INT						NULL,
	[yActual]				FLOAT					NULL,
	[yFunction]				FLOAT					NULL,

	PRIMARY KEY CLUSTERED ([xPlot] ASC)
)
WITH SCHEMABINDING
AS
BEGIN

	DECLARE	@Order					FLOAT	= CONVERT(FLOAT, ABS(@SortOrder) / @SortOrder);
	DECLARE @wTile					FLOAT	= CASE WHEN @Order = 1.0 THEN 0.0 ELSE 100.0 END;
	DECLARE	@SmoothingItems			INT		= 100;

	DECLARE	@EndPointMean			TINYINT;
	DECLARE	@InterpolationPoints	TINYINT;

	SET	@EndPointMean			= CASE @SmoothingType
									WHEN 'M' THEN 2
									WHEN 'L' THEN 2
									WHEN 'N' THEN 2
									WHEN 'S' THEN 2
								END;

	SET	@InterpolationPoints	= CASE @SmoothingType
									WHEN 'M' THEN 4
									WHEN 'L' THEN 4
									WHEN 'N' THEN 3
									WHEN 'S' THEN 4
								END;

	DECLARE @sysResults [stat].[sysResultsTempA];

	---------------------------------------------------------------------------
	--	Mean

	IF(@SmoothingType = 'M') BEGIN

		INSERT INTO @ReturnTable
		(
			[xPlot],
			[yPlot],
			[yInterpolated]
		)
		SELECT
			[xPlot]				= CONVERT(FLOAT, [t].[Id]),
			[yPlot]				= SUM([t].[y] * [t].[w]) / SUM([t].[w]),
			[yInterpolated]		= SUM([t].[y] * [t].[w]) / SUM([t].[w])
		FROM
			[stat].[sysInterpolationPoints](@Sample, @SortOrder, @EndPointMean, @InterpolationPoints) [t]
		GROUP BY
			[t].[Id];

	END;

	---------------------------------------------------------------------------
	--	Linear Regression

	IF(@SmoothingType = 'L') BEGIN

		INSERT INTO @ReturnTable
		(
			[xPlot],
			[yPlot],
			[yInterpolated]
		)
		SELECT
			[t].[Id],
				[yPlot]			= ([t].[yMean] - [t].[b1] * [t].[wMean]) + ([t].[b1] * [t].[Id]),
				[yInterpolated]	= ([t].[yMean] - [t].[b1] * [t].[wMean]) + ([t].[b1] * [t].[Id])
		FROM (
			SELECT
				[t].[Id],
					[yMean]	= AVG([t].[y]),
					[wMean]	= AVG([t].[wPercentile]),
					[b1]	= (CONVERT(FLOAT, COUNT(1)) * SUM([t].[wPercentile] * [t].[y]) - (SUM([t].[wPercentile]) * SUM([t].[y])))
								/
							((CONVERT(FLOAT, COUNT(1)) * SUM(SQUARE([t].[wPercentile]))) - SQUARE(SUM([t].[wPercentile])))
			FROM
				[stat].[sysInterpolationPoints](@Sample, @SortOrder, @EndPointMean, @InterpolationPoints) [t]
			GROUP BY
				[t].[Id]
			) [t]
		GROUP BY
			[t].[Id],
			[t].[b1],
			[t].[yMean],
			[t].[wMean];

	END;

	---------------------------------------------------------------------------
	--	Newton's Divided-Difference (Lagrange Interpolating Polynomials)

	IF(@SmoothingType = 'N') BEGIN

		DECLARE @LaGrangeA	TABLE
		(
			[xPlot]					INT					NOT	NULL,
			[i]						INT					NOT	NULL,
			[x]						FLOAT				NOT	NULL,
			[y]						FLOAT				NOT	NULL,

			PRIMARY KEY CLUSTERED([xPlot] ASC, [i] ASC)
		);

		DECLARE @LaGrange	TABLE
		(
			[xPlot]					INT					NOT	NULL,
			[i]						INT					NOT	NULL,
			[j]						INT					NOT	NULL,
			[k]						INT					NOT	NULL,
			[y]						FLOAT				NOT	NULL,
			[Num]					FLOAT				NOT	NULL,
			[Den]					FLOAT				NOT	NULL,

			PRIMARY KEY CLUSTERED([xPlot] ASC, [i] ASC, [j] ASC)
		);

		INSERT INTO @LaGrangeA
		(
			[xPlot],
			[i],
			[x],
			[y]
		)
		SELECT
				[xPlot]	= [t].[Id],
				[i]		= RANK() OVER(PARTITION BY [t].[Id] ORDER BY [t].[wPercentile] ASC) - 1,
				[x]		= [t].[wPercentile],
			[t].[y]
		FROM
			[stat].[sysInterpolationPoints](@Sample, @SortOrder, @EndPointMean, @InterpolationPoints) [t];

		INSERT INTO @LaGrange
		(
			[xPlot],
			[i],
			[j],
			[k],
			[y],
			[Num],
			[Den]
		)
		SELECT
			[a].[xPlot],
			[a].[i],
				[j]		= ROW_NUMBER() OVER(PARTITION BY [a].[xPlot], [a].[i] ORDER BY [a].[xPlot] ASC,	 [b].[x] ASC)  - 1,
				[k]		= ROW_NUMBER() OVER(PARTITION BY [a].[xPlot], [a].[i] ORDER BY [a].[xPlot] DESC, [b].[x] DESC) - 1,
			[a].[y],
				[Num]	= ([a].[xPlot] - [b].[x]),
				[Den]	= ([a].[x] - [b].[x])
		FROM
			@LaGrangeA	[a]
		INNER JOIN
			@LaGrangeA	[b]
				ON	[b].[xPlot]	=	[a].[xPlot]
				AND	[b].[i]		<>	[a].[i];

		WITH [Mul] AS
		(
			SELECT
				[l].[xPlot],
				[l].[i],
				[l].[j],
				[l].[k],
				[l].[y],
				[l].[Num],
				[l].[Den]
			FROM
				@LaGrange	[l]
			WHERE
				[l].[j]	= 0
			UNION ALL
			SELECT
				[l].[xPlot],
				[l].[i],
				[l].[j],
				[l].[k],
				[l].[y],
					[Num] = [m].[Num] * [l].[Num],
					[Den] = [m].[Den] * [l].[Den]
			FROM
				@LaGrange	[l]
			INNER JOIN
				[Mul]		[m]
					ON	[m].[xPlot]	= [l].[xPlot]
					AND	[m].[i]		= [l].[i]
					AND	[m].[j]		= [l].[j] - 1
			WHERE
				[l].[j] <> 0
		)
		INSERT INTO @sysResults
		(
			[xPlot],
			[yInterpolated]
		)
		SELECT
			[m].[xPlot],
				[yInterpolated]	= SUM([m].[y] * [m].[Num] / [m].[Den])
		FROM
			[Mul] [m]
		WHERE
			[m].[k]	= 0
		GROUP BY
			[m].[xPlot];

		INSERT INTO @ReturnTable
		(
			[xPlot],
			[yPlot],
			[yInterpolated]
		)
		SELECT
			[r].[xPlot],
			[r].[yPlot],
			[r].[yInterpolated]
		FROM [stat].[sysContinuousIncDec](@sysResults, @Order) [r];

	END;

	---------------------------------------------------------------------------
	--	Shepard's Method (Inverse Distance Weighted Interpolation)

	IF(@SmoothingType = 'S') BEGIN

		INSERT INTO @sysResults
		(
			[xPlot],
			[yInterpolated]
		)
		SELECT
			[t].[Id],
			SUM([t].[y] * SQUARE(([t].[s] - [t].[h]) / ([t].[s] * [t].[h])) / [t].[d])
		FROM (
			SELECT
				[t].[Id],
				[t].[y],
				[t].[h],
				[t].[s],
					[d]	= SUM(SQUARE(([t].[s] - [t].[h]) / ([t].[s] * [t].[h]))) OVER(PARTITION BY [t].[Id])
			FROM (
				SELECT
					[t].[Id],
					[t].[y],
						[h]	= ABS(CONVERT(FLOAT, [t].[Id]) - [t].[wPercentile]),
						[s]	= MAX(ABS(CONVERT(FLOAT, [t].[Id]) - [t].[wPercentile])) OVER(PARTITION BY [t].[Id])
				FROM
					[stat].[sysInterpolationPoints](@Sample, @SortOrder, @EndPointMean, @InterpolationPoints) [t]
				) [t]
			) [t]
		GROUP BY
			[t].[Id];

		INSERT INTO @ReturnTable
		(
			[xPlot],
			[yPlot],
			[yInterpolated]
		)
		SELECT
			[r].[xPlot],
			[r].[yPlot],
			[r].[yInterpolated]
		FROM [stat].[sysContinuousIncDec](@sysResults, @Order) [r];

	END;

	---------------------------------------------------------------------------
	--	Add Actual Points

	MERGE INTO @ReturnTable AS [t]
	USING
	(
		SELECT
				[xPlot]	= [r].[wPercentile],
			[r].[RowId],
			[r].[yActual],
				[yFunction] = [stat].[sysInterpolate]([r].[wPercentile], [p].[xPlot], [p].[yPlot], [n].[xPlot], [n].[yPlot], DEFAULT, DEFAULT)
		FROM
			[stat].[sysRaw](@Sample, @SortOrder, @EndPointMean) [r]
		LEFT OUTER JOIN
			@ReturnTable	[p]
				ON	[p].[xPlot]	= FLOOR([r].[wPercentile])
		LEFT OUTER JOIN
			@ReturnTable	[n]
				ON	[n].[xPlot]	= CEILING([r].[wPercentile])
	) AS [s]
	ON [s].[xPlot] = [t].[xPlot]
	WHEN MATCHED THEN
	UPDATE SET
		[t].[RowId]		= [s].[RowId],
		[t].[yActual]	= [s].[yActual],
		[t].[yFunction]	= [s].[yFunction]
	WHEN NOT MATCHED THEN
	INSERT(	   [xPlot],		[RowId],	 [yActual], [yFunction])
	VALUES([s].[xPlot], [s].[RowId], [s].[yActual], [s].[yFunction]);

	RETURN;

END