﻿CREATE FUNCTION [stat].[DescriptiveGeomHarm]
(
	--	http://en.wikipedia.org/wiki/Portal:Statistics
	--	http://en.wikipedia.org/wiki/Summary_statistic
	--	http://en.wikipedia.org/wiki/Descriptive_statistics
	
	@Sample					[stat].[SampleRandomVar]	READONLY									--	Data points to examine
)
RETURNS @ReturnTable TABLE 
(
	[xCount]					INT					NOT	NULL	CHECK([xCount]	>= 1),				--	Item Count
	[GeometricMean]				FLOAT				NOT	NULL,										--	Geometric Mean						http://en.wikipedia.org/wiki/Geometric_mean
	[GeometricMeanWeighted]		FLOAT					NULL,										--	Weighted Geometric Mean				http://en.wikipedia.org/wiki/Weighted_geometric_mean
	[GeometricStDevP]			FLOAT				NOT	NULL	CHECK([GeometricStDevP]	>= 0.0),	--	Geometirc Standard Deviation		http://en.wikipedia.org/wiki/Geometric_standard_deviation

	[HarmonicMean]				FLOAT				NOT	NULL,										--	Harmonic Mean						http://en.wikipedia.org/wiki/Harmonic_mean
	[HarmonicMeanWeighted]		FLOAT					NULL										--	Weighted Harmonic Mean				http://en.wikipedia.org/wiki/Harmonic_mean#Weighted_harmonic_mean
)
WITH SCHEMABINDING
AS
BEGIN

	DECLARE	@xCount			SMALLINT;
	DECLARE @gMean			FLOAT;
	DECLARE @wgMean			FLOAT;
	DECLARE @gStDevP		FLOAT;

	DECLARE @hMean			FLOAT;
	DECLARE @whMean			FLOAT;

	SELECT
		@xCount		= COUNT(1),
		@gMean		= EXP(AVG(LOG([s].[x]))),
		@hMean		= COUNT(1) / SUM(CASE WHEN [s].[x] <> 0.0 THEN 1.0/[s].[x] END),
		@wgMean		= EXP(SUM([s].[w] * LOG([s].[x])) / SUM([s].[w])),
		@whMean		= SUM([s].[w]) / SUM(CASE WHEN [s].[x] <> 0.0 THEN [s].[w]/[s].[x] END)
	FROM
		@Sample [s];

	IF (@xCount = 0) RETURN;

	SELECT
		@gStDevP	= EXP(SQRT(SUM(SQUARE(LOG([s].[x] / @gMean))) / @xCount))
	FROM
		@Sample [s];

	INSERT INTO @ReturnTable
	(
		[xCount],
		[GeometricMean],
		[GeometricMeanWeighted],
		[GeometricStDevP],
		[HarmonicMean],
		[HarmonicMeanWeighted]
	)
	SELECT
		@xCount,
		@gMean,
		@wgMean,
		@gStDevP,
		@hMean,
		@whMean;

	RETURN;

END;
GO