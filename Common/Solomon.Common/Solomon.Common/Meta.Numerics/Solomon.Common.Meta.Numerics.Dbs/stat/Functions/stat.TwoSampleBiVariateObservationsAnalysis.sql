﻿CREATE FUNCTION [stat].[TwoSampleBiVariateObservationsAnalysis]
(
	@SampleBasis			[stat].[SampleBiVariate]	READONLY,									--	Data points to examine
	@SampleAudit			[stat].[SampleBiVariate]	READONLY,									--	Data points to examine
	
	@Relationship			CHAR(1)				= 'L',
	@Confidence_Pcnt		FLOAT				= 0.90,			--	Prediction/confidence interval level	[0.0, 1.0]
	@PearsonThreshold		FLOAT				= 0.90,			--	Pearson threshold						[0.0, 1.0]
	@ResidualThreshold		FLOAT				= 2.00,			--	Residual identification threshold
	@xAsCeiling				INT					= 0
)
RETURNS @ReturnTable TABLE
(
	[Relationship]			CHAR(1)				NOT	NULL,

	[Id]					INT					NOT	NULL,
	[RowId]					INT					NOT	NULL,
	[x]						FLOAT				NOT	NULL,	--	Independant variable
	[y]						FLOAT				NOT	NULL,	--	Dependant variable

	[yQuartile]				TINYINT				NOT	NULL	CHECK([yQuartile] >= 1),				--	Quartile value						http://en.wikipedia.org/wiki/Quantile
	[yPercentile]			FLOAT				NOT	NULL,
	[yPercentRank]			FLOAT				NOT	NULL	CHECK(ROUND([yPercentRank], 5)	BETWEEN 0.0 AND 100.0),	--	Percentile			https://en.wikipedia.org/wiki/Percentile_rank
	[yCumulative]			FLOAT				NOT	NULL	CHECK(ROUND([yCumulative], 5)	BETWEEN 0.0 AND 100.0),	--	Cumulative			https://en.wikipedia.org/wiki/Cumulative_distribution_function
	[yRank]					INT					NOT	NULL	CHECK([yRank]		>= 1),				--	Value Ranking						http://msdn.microsoft.com/en-us/library/ms176102.aspx
	[yRankDense]			INT					NOT	NULL	CHECK([yRankDense]	>= 1),				--	Value Ranking (Dense)				http://msdn.microsoft.com/en-us/library/ms173825.aspx

	[yEstimate]				FLOAT				NOT	NULL,

	[zScore]				FLOAT				NOT	NULL,											--	Distance Away From Mean				http://en.wikipedia.org/wiki/Z-test
	[zQuartile]				TINYINT					NULL	CHECK([zQuartile] >= 1),				--	Quartile for Z-Test
	[zPercentile]			FLOAT				NOT	NULL	CHECK(ROUND([zPercentile], 5)	BETWEEN 0.0 AND 100.0),	--	Percentile			http://en.wikipedia.org/wiki/Percentile
	[zHypothesis]			INT					NOT	NULL,											--	Null Hypothesis 0 (Rejection), 1 (Acceptance)

	[ErrorAbsolute]			AS CONVERT(FLOAT, [yEstimate] - [y])									--	Absolute approximation error		http://en.wikipedia.org/wiki/Approximation_error
							PERSISTED			NOT	NULL,
	[ErrorRelative]			AS CONVERT(FLOAT, CASE WHEN ([yEstimate] <> 0.0) THEN					--	Relative approximation error		http://en.wikipedia.org/wiki/Approximation_error
								([yEstimate] - [y]) / [yEstimate]
								END)
							PERSISTED			NOT	NULL,

	[ResidualStandard]		FLOAT					NULL,											--	Standard residual error				http://en.wikipedia.org/wiki/Errors_and_residuals_in_statistics
	[ResidualAbsolute]		FLOAT				NOT	NULL,
	[ResidualRelative]		FLOAT				NOT	NULL,

	[PlotY1]				AS CONVERT(FLOAT, CASE WHEN [yQuartile] = 1 THEN [y] END)				--	Green
							PERSISTED					,
	[PlotY2]				AS CONVERT(FLOAT, CASE WHEN [yQuartile] = 2 THEN [y] END)				--	Blue
							PERSISTED					,
	[PlotY3]				AS CONVERT(FLOAT, CASE WHEN [yQuartile] = 3 THEN [y] END)				--	Yellow
							PERSISTED					,
	[PlotY4]				AS CONVERT(FLOAT, CASE WHEN [yQuartile] = 4 THEN [y] END)				--	Red
							PERSISTED					,

	[PlotZ1]				AS CONVERT(FLOAT, CASE WHEN [zQuartile] = 1 THEN [y] END)				--	Green
							PERSISTED					,
	[PlotZ2]				AS CONVERT(FLOAT, CASE WHEN [zQuartile] = 2 THEN [y] END)				--	Blue
							PERSISTED					,
	[PlotZ3]				AS CONVERT(FLOAT, CASE WHEN [zQuartile] = 3 THEN [y] END)				--	Yellow
							PERSISTED					,
	[PlotZ4]				AS CONVERT(FLOAT, CASE WHEN [zQuartile] = 4 THEN [y] END)				--	Red
							PERSISTED					,

	[ResidualThreshold]		FLOAT				NOT	NULL	CHECK([ResidualThreshold] > 0.0) ,
	[ResidualStdIn]			AS CONVERT(FLOAT, CASE WHEN ABS([ResidualStandard]) <  [ResidualThreshold] THEN [y] END)
							PERSISTED				,
	[ResidualStdOut]		AS CONVERT(FLOAT, CASE WHEN ABS([ResidualStandard]) >= [ResidualThreshold] THEN [y] END)
							PERSISTED				,

	[ConfidenceInterval]	FLOAT				NOT	NULL	CHECK([ConfidenceInterval]	>= 0.0),	--	Confidence Interval					http://en.wikipedia.org/wiki/Confidence_interval
	[PredictionInterval]	FLOAT				NOT	NULL	CHECK([PredictionInterval]	>= 0.0),	--	Prediction Interval					http://en.wikipedia.org/wiki/Prediction_interval

	[PredictionLower]		AS CONVERT(FLOAT, [yEstimate] - [PredictionInterval])						--	Lower Prediction Limit
							PERSISTED			NOT	NULL,
	[ConfidenceLower]		AS CONVERT(FLOAT, [yEstimate] - [ConfidenceInterval])						--	Lower Confidence Limit
							PERSISTED			NOT	NULL,
	[ConfidenceUpper]		AS CONVERT(FLOAT, [yEstimate] + [ConfidenceInterval])						--	Upper Confidence Limit
							PERSISTED			NOT	NULL,
	[PredictionUpper]		AS CONVERT(FLOAT, [yEstimate] + [PredictionInterval])						--	Upper Prediction Limit
							PERSISTED			NOT	NULL,

	[ConfidenceIn]			AS CONVERT(FLOAT, CASE WHEN ([y]	 BETWEEN [yEstimate] - [ConfidenceInterval] AND [yEstimate] + [ConfidenceInterval]) THEN [y] END)
							PERSISTED					,
	[ConfidenceOut]			AS CONVERT(FLOAT, CASE WHEN ([y] NOT BETWEEN [yEstimate] - [ConfidenceInterval] AND [yEstimate] + [ConfidenceInterval]) THEN [y] END)
							PERSISTED					,
	[PredictionIn]			AS CONVERT(FLOAT, CASE WHEN ([y]	 BETWEEN [yEstimate] - [PredictionInterval] AND [yEstimate] + [PredictionInterval]) THEN [y] END)
							PERSISTED					,
	[PredictionOut]			AS CONVERT(FLOAT, CASE WHEN ([y] NOT BETWEEN [yEstimate] - [PredictionInterval] AND [yEstimate] + [PredictionInterval]) THEN [y] END)
							PERSISTED					,

	[PearsonThreshold]		FLOAT				NOT	NULL	CHECK(ROUND([PearsonThreshold], 5) BETWEEN 0.0 AND 1.0),

	[PearsonIn]				AS CONVERT(FLOAT, CASE WHEN ([ResidualAbsolute] <= [PearsonThreshold]	AND [ResidualRelative] <= [PearsonThreshold]) THEN [y] END)
							PERSISTED					,	--	Ring (Black)
	[PearsonOut]			AS CONVERT(FLOAT, CASE WHEN ([ResidualAbsolute] > [PearsonThreshold]	OR	[ResidualRelative] > [PearsonThreshold]) THEN [y] END)
							PERSISTED					,	--	Ring (Black)

	[xAsCeiling]			INT						NULL,

	[CeilingBreach]			AS CONVERT(FLOAT, CASE WHEN [xAsCeiling] = 1 AND [y] > [x] THEN [y] END)
							PERSISTED					,

	--	Pearson Out, Residual In 
	[SuspectWarning]		AS CONVERT(FLOAT,
								CASE WHEN 
									([ResidualAbsolute] > [PearsonThreshold]	OR	[ResidualRelative] > [PearsonThreshold])	--	Pearson Out
								AND (ABS([ResidualStandard]) <  [ResidualThreshold])											--	Residual In
								THEN [y] END)
							PERSISTED					,

	--	Residual Out, Ceiling Breach
	[SuspectCritical]		AS CONVERT(FLOAT,
								CASE WHEN
									(ABS([ResidualStandard]) >= [ResidualThreshold])	--	Residual Out
								OR ([xAsCeiling] = 1 AND [y] > [x])						--	Ceiling Breach
								THEN [y] END)
							PERSISTED				,


	[b0]					FLOAT				NOT	NULL,
	[b1]					FLOAT				NOT	NULL,
	[Confidence_Pcnt]		FLOAT				NOT	NULL,

	PRIMARY KEY CLUSTERED([x] ASC, [y] ASC, [Id] ASC)
)
WITH SCHEMABINDING
AS
BEGIN

	DECLARE	@Count				FLOAT;						--	Number of observations in the sample

	SELECT
		@Count		= CONVERT(FLOAT, COUNT(1))
	FROM
		@SampleBasis	[s];

	IF (@Count <= 2.0) RETURN;

	DECLARE	@nTiles				TINYINT	= 4;

	DECLARE	@DFd				FLOAT	= @Count - 1.0;		--	Degrees of Freedom (Denominator)
	DECLARE	@DFn				FLOAT	= 1.0;				--	Degrees of Freedom (Number of Independent Variables)

	DECLARE @Alpha2Tail			FLOAT	= CASE WHEN @Confidence_Pcnt >= 1.0 THEN 0.999999999999999 ELSE @Confidence_Pcnt END;
	DECLARE	@StudentTStatistic	FLOAT	= [stat].[Student_Statistic](@DFd, @Alpha2Tail, 2);

	DECLARE	@b0					FLOAT;	--	First Coefficient or Term
	DECLARE	@b1					FLOAT;	--	Second Coefficient or Term

	DECLARE	@xMean				FLOAT;	--	Mean of linearlized x values (Indepentent)
	DECLARE	@yMean				FLOAT;	--	Mean of Y for @b1e, Mean, Pearson (Dependent)

	DECLARE	@xStDev				FLOAT;	--	Standard Deviation, x
	DECLARE	@yStDev				FLOAT;	--	Standard Deviation, y

	DECLARE	@Stdev				FLOAT;	--	Standard Deviation of the dependent values about the function
	DECLARE	@SumXiX 			FLOAT;	--	Sum of xi - xbar (Σ(xi-AVG(x))

	DECLARE @EstimateBasis		TABLE
	(
		[Id]					INT					NOT	NULL,
		[RowId]					INT					NOT	NULL,
		[x]						FLOAT				NOT	NULL,	--	Independant variable
		[y]						FLOAT				NOT	NULL,	--	Dependant variable
		[yEstimate]				FLOAT				NOT	NULL,

		[ErrorAbsolute]			AS CONVERT(FLOAT, [yEstimate] - [y])									--	Absolute approximation error		http://en.wikipedia.org/wiki/Approximation_error
								PERSISTED			NOT	NULL,

		[ErrorRelative]			AS CONVERT(FLOAT, CASE WHEN ([yEstimate] <> 0.0) THEN					--	Relative approximation error		http://en.wikipedia.org/wiki/Approximation_error
									([yEstimate] - [y]) / [yEstimate]
									END)
								PERSISTED			NOT	NULL,

		PRIMARY KEY CLUSTERED([Id] ASC),
		UNIQUE NONCLUSTERED([y] ASC, [RowId] ASC)
	);

	DECLARE @EstimateAudit		TABLE
	(
		[Id]					INT					NOT	NULL,
		[RowId]					INT					NOT	NULL,
		[x]						FLOAT				NOT	NULL,	--	Independant variable
		[y]						FLOAT				NOT	NULL,	--	Dependant variable
		[yEstimate]				FLOAT				NOT	NULL,

		[ErrorAbsolute]			AS CONVERT(FLOAT, [yEstimate] - [y])									--	Absolute approximation error		http://en.wikipedia.org/wiki/Approximation_error
								PERSISTED			NOT	NULL,

		[ErrorRelative]			AS CONVERT(FLOAT, CASE WHEN ([yEstimate] <> 0.0) THEN					--	Relative approximation error		http://en.wikipedia.org/wiki/Approximation_error
									([yEstimate] - [y]) / [yEstimate]
									END)
								PERSISTED			NOT	NULL,

		PRIMARY KEY CLUSTERED([Id] ASC),
		UNIQUE NONCLUSTERED([y] ASC, [RowId] ASC)
	);

	DECLARE @IntermediateAudit	TABLE
	(
		[Id]					INT					NOT	NULL,

		[zScore]				FLOAT				NOT	NULL,											--	Distance Away From Mean				http://en.wikipedia.org/wiki/Z-test
		[zPercentile]			FLOAT				NOT	NULL	CHECK(ROUND([zPercentile], 5)	BETWEEN 0.0 AND 100.0),	--	Percentile			http://en.wikipedia.org/wiki/Percentile
		[zHypothesis]			INT					NOT	NULL,											--	Null Hypothesis 0 (Rejection), 1 (Acceptance)

		[ResidualStandard]		FLOAT					NULL,											--	Standard residual error				http://en.wikipedia.org/wiki/Errors_and_residuals_in_statistics
		[ResidualAbsolute]		FLOAT				NOT	NULL,
		[ResidualRelative]		FLOAT				NOT	NULL,

		[ConfidenceInterval]	FLOAT				NOT	NULL	CHECK([ConfidenceInterval]	>= 0.0),	--	Confidence Interval					http://en.wikipedia.org/wiki/Confidence_interval
		[PredictionInterval]	FLOAT				NOT	NULL	CHECK([PredictionInterval]	>= 0.0),	--	Prediction Interval					http://en.wikipedia.org/wiki/Prediction_interval

		PRIMARY KEY CLUSTERED([Id] ASC)
	);

	---------------------------------------------------------------------------
	--	Mean: ŷ = avg(x)

	IF (@Relationship = 'M') BEGIN

		SELECT
			@b1		= 0.0,
			@b0		= AVG([s].[y]),

			@xMean	= AVG([s].[x]),
			@yMean	= AVG([s].[y]),

			@xStDev	= STDEVP([s].[x]),
			@yStDev	= STDEVP([s].[y])
		FROM
			@SampleBasis		[s];
			
		INSERT INTO @EstimateBasis
		(
			[Id],
			[RowId],
			[x],
			[y],
			[yEstimate]
		)
		SELECT
			[s].[Id],
			[s].[RowId],
				[x]			= 100.0 *	CONVERT(FLOAT, ROW_NUMBER()	OVER(ORDER BY [s].[y] ASC, [s].[RowId] ASC) - 0.5) / @Count,
			[s].[y],
				[yEstimate]	= [stat].[sysEstimateMean](@b0, @b1, [s].[x])
		FROM
			@SampleBasis		[s];

		INSERT INTO @EstimateAudit
		(
			[Id],
			[RowId],
			[x],
			[y],
			[yEstimate]
		)
		SELECT
			[s].[Id],
			[s].[RowId],
				[x]			= 100.0 *	CONVERT(FLOAT, ROW_NUMBER()	OVER(ORDER BY [s].[y] ASC, [s].[RowId] ASC) - 0.5) / @Count,
			[s].[y],
				[yEstimate]	= [stat].[sysEstimateMean](@b0, @b1, [s].[x])
		FROM
			@SampleAudit		[s];

	END;

	---------------------------------------------------------------------------
	--	Linear: ŷ = a + bx

	IF (@Relationship = 'L') BEGIN

		SELECT
			@b1		= CASE WHEN (SUM(SQUARE([s].[x])) - SQUARE(SUM([s].[x])) / CONVERT(FLOAt, COUNT(1))) <> 0
						THEN
							(CAST(COUNT(1) AS FLOAT) * SUM([s].[x] * [s].[y]) - SUM([s].[x]) * SUM([s].[y])) / (CONVERT(FLOAt, COUNT(1)) * SUM(SQUARE([s].[x])) - SQUARE(SUM([s].[x])))
						ELSE
							0.0
						END,

			@xMean	= AVG([s].[x]),
			@yMean	= AVG([s].[y]),

			@xStDev	= STDEVP([s].[x]),
			@yStDev	= STDEVP([s].[y])
		FROM
			@SampleBasis		[s];

		SELECT
			@b0 = AVG([s].[y]) - (@b1 * AVG([s].[x]))
		FROM
			@SampleBasis		[s];

		INSERT INTO @EstimateBasis
		(
			[Id],
			[RowId],
			[x],
			[y],
			[yEstimate]
		)
		SELECT
			[s].[Id],
			[s].[RowId],
			[s].[x],
			[s].[y],
				[yEstimate]	= [stat].[sysEstimateLinear](@b0, @b1, [s].[x])
		FROM
			@SampleBasis		[s];

		INSERT INTO @EstimateAudit
		(
			[Id],
			[RowId],
			[x],
			[y],
			[yEstimate]
		)
		SELECT
			[s].[Id],
			[s].[RowId],
			[s].[x],
			[s].[y],
				[yEstimate]	= [stat].[sysEstimateLinear](@b0, @b1, [s].[x])
		FROM
			@SampleAudit		[s];

	END;

	---------------------------------------------------------------------------
	--	Exponential: ŷ = a * EXP(b*x)

	IF (@Relationship = 'E') BEGIN

		SELECT
			@b1		= CASE WHEN (CAST(COUNT(1) AS FLOAT	) * SUM(SQUARE([s].[x])) - SQUARE(SUM([s].[x]))) <> 0
						THEN
							(CAST(COUNT(1) AS FLOAT	) * SUM([s].[x] * LOG([s].[y])) - (SUM([s].[x]) * SUM(LOG([s].[y])))) / (CAST(COUNT(1) AS FLOAT	) * SUM(SQUARE([s].[x])) - SQUARE(SUM([s].[x])))
						ELSE
							0.0
						END,

			@xMean	= AVG([s].[x]),
			@yMean	= AVG(LOG([s].[y])),

			@xStDev	= STDEVP([s].[x]),
			@yStDev	= STDEVP(LOG([s].[y]))
		FROM
			@SampleBasis		[s]
		WHERE
			[s].[y]		<>	 0.0;

		SELECT
			@b0 = EXP(AVG(LOG([s].[y])) - @b1 * AVG([s].[x]))
		FROM
			@SampleBasis		[s]
		WHERE
			[s].[y]		<>	 0.0;

		INSERT INTO @EstimateBasis
		(
			[Id],
			[RowId],
			[x],
			[y],
			[yEstimate]
		)
		SELECT
			[s].[Id],
			[s].[RowId],
			[s].[x],
			[s].[y],
				[yEstimate]	= [stat].[sysEstimateExponential](@b0, @b1, [s].[x])
		FROM
			@SampleBasis		[s]
		WHERE
			[s].[y]		<>	 0.0;

		INSERT INTO @EstimateAudit
		(
			[Id],
			[RowId],
			[x],
			[y],
			[yEstimate]
		)
		SELECT
			[s].[Id],
			[s].[RowId],
			[s].[x],
			[s].[y],
				[yEstimate]	= [stat].[sysEstimateExponential](@b0, @b1, [s].[x])
		FROM
			@SampleAudit		[s]
		WHERE
			[s].[y]		<>	 0.0;

	END;

	---------------------------------------------------------------------------
	--	Power: ŷ = a * x ^ b

	IF (@Relationship = 'P') BEGIN

		SELECT
			@b1		= CASE WHEN (CAST(COUNT(1) AS FLOAT	) * SUM(SQUARE(LOG10([s].[x]))) - SQUARE(SUM(LOG10([s].[x])))) <> 0
						THEN
							(CAST(COUNT(1) AS FLOAT	) * SUM(LOG10([s].[x]) * LOG10([s].[y])) - (SUM(LOG10([s].[x])) * SUM(LOG10([s].[y])))) / (CAST(COUNT(1) AS FLOAT	) * SUM(SQUARE(LOG10([s].[x]))) - SQUARE(SUM(LOG10([s].[x]))))
						ELSE
							0.0
						END,

			@xMean	= AVG(LOG10([s].[x])),
			@yMean	= AVG(LOG10([s].[y])),

			@xStdev	= STDEVP(LOG10([s].[x])),
			@yStdev	= STDEVP(LOG10([s].[y]))
		FROM
			@SampleBasis		[s]
		WHERE
				[s].[x]	<>	 0.0
			AND	[s].[y]	<>	 0.0;

		SELECT
			@b0 = POWER(10.0, (AVG(LOG10([s].[y])) - @b1 * AVG(LOG10([s].[x]))))
		FROM
			@SampleBasis		[s]
		WHERE
				[s].[x]	<>	 0.0
			AND	[s].[y]	<>	 0.0;

		INSERT INTO @EstimateBasis
		(
			[Id],
			[RowId],
			[x],
			[y],
			[yEstimate]
		)
		SELECT
			[s].[Id],
			[s].[RowId],
			[s].[x],
			[s].[y],
				[yEstimate]	= [stat].[sysEstimatePower](@b0, @b1, [s].[x])
		FROM
			@SampleBasis		[s]
		WHERE
				[s].[x]	<>	 0.0
			AND	[s].[y]	<>	 0.0;

		INSERT INTO @EstimateAudit
		(
			[Id],
			[RowId],
			[x],
			[y],
			[yEstimate]
		)
		SELECT
			[s].[Id],
			[s].[RowId],
			[s].[x],
			[s].[y],
				[yEstimate]	= [stat].[sysEstimatePower](@b0, @b1, [s].[x])
		FROM
			@SampleAudit		[s]
		WHERE
				[s].[x]	<>	 0.0
			AND	[s].[y]	<>	 0.0;

	END;

	---------------------------------------------------------------------------
	--	Saturation Growth Rate: ŷ = a * x / (b + x)

	IF (@Relationship = 'S') BEGIN

		SELECT
			@b1 = CASE WHEN (CAST(COUNT(1) AS FLOAT	) * SUM(SQUARE(1.0/[s].[x])) - SQUARE(SUM(1.0/[s].[x]))) <> 0
				THEN
					(CAST(COUNT(1) AS FLOAT	) * SUM(1.0/[s].[x] * 1.0/[s].[y]) - (SUM(1.0/[s].[x]) * SUM(1.0/[s].[y]))) / (CAST(COUNT(1) AS FLOAT	) * SUM(SQUARE(1.0/[s].[x])) - SQUARE(SUM(1.0/[s].[x])))
				ELSE
					0.0
				END,

			@xMean	= AVG(1.0/[s].[x]),
			@xMean	= AVG(1.0/[s].[y]),

			@xStDev	= STDEVP(1.0/[s].[x]),
			@yStDev	= STDEVP(1.0/[s].[y])
		FROM
			@SampleBasis		[s]
		WHERE
				[s].[x]	<>	 0.0
			AND	[s].[y]	<>	 0.0;

		SELECT
			@b0 = 1.0/((AVG(1.0/[s].[y]) - @b1 * AVG(1.0/[s].[x])))
		FROM
			@SampleBasis		[s]
		WHERE
				[s].[x]	<>	 0.0
			AND	[s].[y]	<>	 0.0
		HAVING
			((AVG(1.0/[s].[y]) - @b1 * AVG(1.0/[s].[x]))) <> 0.0;

		INSERT INTO @EstimateBasis
		(
			[Id],
			[RowId],
			[x],
			[y],
			[yEstimate]
		)
		SELECT
			[s].[Id],
			[s].[RowId],
			[s].[x],
			[s].[y],
				[yEstimate]	= [stat].[sysEstimateSaturation](@b0, @b1, [s].[x])
		FROM
			@SampleBasis		[s]
		WHERE
				[s].[x]	<>	 0.0
			AND	[s].[y]	<>	 0.0;

		INSERT INTO @EstimateAudit
		(
			[Id],
			[RowId],
			[x],
			[y],
			[yEstimate]
		)
		SELECT
			[s].[Id],
			[s].[RowId],
			[s].[x],
			[s].[y],
				[yEstimate]	= [stat].[sysEstimateSaturation](@b0, @b1, [s].[x])
		FROM
			@SampleAudit		[s]
		WHERE
				[s].[x]	<>	 0.0
			AND	[s].[y]	<>	 0.0;

	END;

	---------------------------------------------------------------------------
	--	Standard Calculations

	SELECT
		@SumXiX = SUM(SQUARE([e].[x] - @xMean)),
		@StDev	= SQRT(SUM(SQUARE([e].[y] - [e].[yEstimate])) / CONVERT(FLOAT, COUNT(1) - 2))
	FROM
		@EstimateBasis [e];

	INSERT INTO @IntermediateAudit
	(
		[Id],
		[zScore],
		[zPercentile],
		[zHypothesis],
		[ResidualStandard],
		[ResidualAbsolute],
		[ResidualRelative],
		[ConfidenceInterval],
		[PredictionInterval]
	)
	SELECT
		[e].[Id],

			[zScore]				= CASE WHEN @StDev <> 0.0 THEN
											([e].[y] - [e].[yEstimate]) / @StDev
										END,

			[zPercentile]			= [stat].[Student_Probability](@Count, CASE WHEN @StDev <> 0.0 THEN ([e].[y] - [e].[yEstimate]) / @StDev END, 1),

			[zHypothesis]			= CASE WHEN @StDev <> 0.0 THEN
											CASE WHEN ABS([e].[y] - [e].[yEstimate]) / @StDev < 0.0 THEN 1 ELSE 0 END
										END,
				
			[ResidualStandard]		= ([e].[y]	- [e].[yEstimate])
										/
										SQRT(SQUARE(@StDev) * (1.0 - (1.0 / @DFd) - SQUARE([e].[x] - @xMean) / @SumXiX)),

			[ResidualAbsolute]		=  (SELECT COUNT(1) FROM @EstimateBasis [b]
											WHERE	SQUARE([b].[y] - [b].[yEstimate])
												<=	SQUARE([e].[y] - [e].[yEstimate])
											)	 /	@Count,

			[ResidualRelative]		=	(SELECT COUNT(1) FROM @EstimateBasis [b]
											WHERE	CASE WHEN [b].[y] <> 0.0 THEN SQUARE(([b].[y] - [b].[yEstimate]) / [b].[y]) ELSE 1.0 END
												<=	CASE WHEN [e].[y] <> 0.0 THEN SQUARE(([e].[y] - [e].[yEstimate]) / [e].[y]) ELSE 1.0 END
											)	 /	@Count,

			[ConfidenceInterval]	= @StudentTStatistic * SQRT(SQUARE(@StDev) * (		(1.0 / @DFd) + SQUARE([e].[x] - @xMean) / @SumXiX)),		
			[PredictionInterval]	= @StudentTStatistic * SQRT(SQUARE(@StDev) * (1.0 + (1.0 / @DFd) + SQUARE([e].[x] - @xMean) / @SumXiX))
	FROM
		@EstimateAudit	[e];

	INSERT INTO @ReturnTable
	(
		[Relationship],

		[Id],
		[RowId],
		[x],
		[y],

		[yQuartile],
		[yPercentile],
		[yPercentRank],
		[yCumulative],
		[yRank],
		[yRankDense],

		[yEstimate],

		[zScore],
		[zQuartile],
		[zPercentile],
		[zHypothesis],

		[ResidualStandard],
		[ResidualAbsolute],
		[ResidualRelative],

		[ResidualThreshold],

		[ConfidenceInterval],
		[PredictionInterval],

		[PearsonThreshold],
		[xAsCeiling],

		[b0],
		[b1],
		[Confidence_Pcnt]
	)
	SELECT
			[Relationship]		= @Relationship,

		[e].[Id],
		[e].[RowId],
		[e].[x],
		[e].[y],

			[yQuartile]			=			NTILE(@nTiles)				OVER(ORDER BY [e].[y] ASC, [e].[RowId] ASC),
			[yPercentile]		= 100.0 *	CONVERT(FLOAT, ROW_NUMBER()	OVER(ORDER BY [e].[y] ASC, [e].[RowId] ASC) - 0.5) / @Count,
			[yPercentRank]		= 100.0 *	PERCENT_RANK()				OVER(ORDER BY [e].[y] ASC, [e].[RowId] ASC),
			[yCumulative]		= 100.0 *	CUME_DIST()					OVER(ORDER BY [e].[y] ASC, [e].[RowId] ASC),
			[yRank]				=			RANK()						OVER(ORDER BY [e].[y] ASC, [e].[RowId] ASC),
			[yRankDense]		=			DENSE_RANK()				OVER(ORDER BY [e].[y] ASC, [e].[RowId] ASC),	

		[e].[yEstimate],

		[i].[zScore],
			[zQuartile]			=			NTILE(@nTiles)				OVER(ORDER BY ABS([i].[zScore]) ASC),
		[i].[zPercentile],
		[i].[zHypothesis],

		[i].[ResidualStandard],
		[i].[ResidualAbsolute],
		[i].[ResidualRelative],

			[ResidualThreshold]	= @ResidualThreshold,

		[i].[ConfidenceInterval],
		[i].[PredictionInterval],

			[PearsonThreshold]	= @PearsonThreshold,
			[xAsCeiling]		= @xAsCeiling,

			[b0]				= @b0,
			[b1]				= @b1,
			[Confidence_Pcnt]	= @Confidence_Pcnt

	FROM
		@EstimateAudit			[e]
	INNER JOIN
		@IntermediateAudit		[i]
			ON	[i].[Id]	= [e].[Id];

	RETURN;

END;
