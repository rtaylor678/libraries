﻿CREATE FUNCTION [stat].[ObservationsAnalysis]
(
	--	http://en.wikipedia.org/wiki/Portal:Statistics
	--	http://en.wikipedia.org/wiki/Summary_statistic
	--	http://en.wikipedia.org/wiki/Descriptive_statistics

	@Sample					[stat].[SampleRandomVar]	READONLY,									--	Data points to examine


	@SortOrder				SMALLINT			= 1,												--	Sort order of data 1 (Ascending), -1 Descending
	@Tiles					TINYINT				= 4,												--	Number of nTiles
	@Confidence_Pcnt		FLOAT				= 0.90,												--	Prediction/confidence interval level	[0.0, 1.0]
	@PearsonThreshold		FLOAT				= 0.90,												--	Pearson threshold						[0.0, 1.0]
	@ResidualThreshold		FLOAT				= 2.00												--	Residual identification threshold
)
RETURNS @ReturnTable TABLE 
(
	[Id]					INT					NOT	NULL,
	[RowId]					INT					NOT	NULL,
	[x]						FLOAT				NOT	NULL,											--	Raw value
	[w]						FLOAT					NULL,											--	Raw value

	[xQuartile]				TINYINT				NOT	NULL	CHECK([xQuartile] >= 1),				--	Quartile value						http://en.wikipedia.org/wiki/Quantile
	[xPercentile]			FLOAT				NOT	NULL	CHECK(ROUND([xPercentile], 5)	BETWEEN 0.0 AND 100.0),	--	Item-tile			http://en.wikipedia.org/wiki/Percentile
	[xPercentRank]			FLOAT				NOT	NULL	CHECK(ROUND([xPercentRank], 5)	BETWEEN 0.0 AND 100.0),	--	Percentile			https://en.wikipedia.org/wiki/Percentile_rank
	[xCumulative]			FLOAT				NOT	NULL	CHECK(ROUND([xCumulative], 5)	BETWEEN 0.0 AND 100.0),	--	Cumulative			https://en.wikipedia.org/wiki/Cumulative_distribution_function
	[xRank]					INT					NOT	NULL	CHECK([xRank]		>= 1),				--	Value Ranking						http://msdn.microsoft.com/en-us/library/ms176102.aspx
	[xRankDense]			INT					NOT	NULL	CHECK([xRankDense]	>= 1),				--	Value Ranking (Dense)				http://msdn.microsoft.com/en-us/library/ms173825.aspx

	[wPercentile]			FLOAT					NULL	CHECK(ROUND([wPercentile], 5)	BETWEEN 0.0 AND 100.0),	--	Item-tile			http://en.wikipedia.org/wiki/Percentile

	[zScore]				FLOAT				NOT	NULL,											--	Distance Away From Mean				http://en.wikipedia.org/wiki/Z-test
	[zQuartile]				TINYINT					NULL	CHECK([zQuartile] >= 1),				--	Quartile for Z-Test
	[zPercentile]			FLOAT				NOT	NULL	CHECK(ROUND([zPercentile], 5)	BETWEEN 0.0 AND 100.0),	--	Percentile			http://en.wikipedia.org/wiki/Percentile
	[zHypothesis]			INT					NOT	NULL,											--	Null Hypothesis 0 (Rejection), 1 (Acceptance)

	[ErrorAbsolute]			AS CONVERT(FLOAT, [xMean] - [x])										--	Absolute approximation error		http://en.wikipedia.org/wiki/Approximation_error
							PERSISTED			NOT	NULL,
	[ErrorRelative]			AS CONVERT(FLOAT, CASE WHEN ([xMean] <> 0.0) THEN						--	Relative approximation error		http://en.wikipedia.org/wiki/Approximation_error
								([xMean] - [x]) / [xMean]
								END)
							PERSISTED			NOT	NULL,

	[ResidualStandard]		FLOAT					NULL,											--	Standard residual error				http://en.wikipedia.org/wiki/Errors_and_residuals_in_statistics
	[ResidualAbsolute]		FLOAT				NOT	NULL,
	[ResidualRelative]		FLOAT				NOT	NULL,

	[PlotQ1]				AS CONVERT(FLOAT, CASE WHEN [xQuartile] = 1 THEN [x] END)				--	Green
							PERSISTED					,
	[PlotQ2]				AS CONVERT(FLOAT, CASE WHEN [xQuartile] = 2 THEN [x] END)				--	Blue
							PERSISTED					,
	[PlotQ3]				AS CONVERT(FLOAT, CASE WHEN [xQuartile] = 3 THEN [x] END)				--	Yellow
							PERSISTED					,
	[PlotQ4]				AS CONVERT(FLOAT, CASE WHEN [xQuartile] = 4 THEN [x] END)				--	Red
							PERSISTED					,

	[PlotZ1]				AS CONVERT(FLOAT, CASE WHEN [zQuartile] = 1 THEN [x] END)				--	Green
							PERSISTED					,
	[PlotZ2]				AS CONVERT(FLOAT, CASE WHEN [zQuartile] = 2 THEN [x] END)				--	Blue
							PERSISTED					,
	[PlotZ3]				AS CONVERT(FLOAT, CASE WHEN [zQuartile] = 3 THEN [x] END)				--	Yellow
							PERSISTED					,
	[PlotZ4]				AS CONVERT(FLOAT, CASE WHEN [zQuartile] = 4 THEN [x] END)				--	Red
							PERSISTED					,

	[ResidualThreshold]		FLOAT				NOT	NULL	CHECK([ResidualThreshold] > 0.0) ,
	[ResidualStdIn]			AS CONVERT(FLOAT, CASE WHEN ABS([ResidualStandard]) <  [ResidualThreshold] THEN [ResidualStandard] END)
							PERSISTED				,
	[ResidualStdOut]		AS CONVERT(FLOAT, CASE WHEN ABS([ResidualStandard]) >= [ResidualThreshold] THEN [ResidualStandard] END)
							PERSISTED				,

	[xMean]					FLOAT				NOT	NULL,											--	Mean								http://en.wikipedia.org/wiki/Mean

	[ConfidenceInterval]	FLOAT				NOT	NULL	CHECK([ConfidenceInterval]	>= 0.0),	--	Confidence Interval					http://en.wikipedia.org/wiki/Confidence_interval
	[PredictionInterval]	FLOAT				NOT	NULL	CHECK([PredictionInterval]	>= 0.0),	--	Prediction Interval					http://en.wikipedia.org/wiki/Prediction_interval

	[PredictionLower]		AS CONVERT(FLOAT, [xMean] - [PredictionInterval])						--	Lower Prediction Limit
							PERSISTED			NOT	NULL,
	[ConfidenceLower]		AS CONVERT(FLOAT, [xMean] - [ConfidenceInterval])						--	Lower Confidence Limit
							PERSISTED			NOT	NULL,
	[ConfidenceUpper]		AS CONVERT(FLOAT, [xMean] + [ConfidenceInterval])						--	Upper Confidence Limit
							PERSISTED			NOT	NULL,
	[PredictionUpper]		AS CONVERT(FLOAT, [xMean] + [PredictionInterval])						--	Upper Prediction Limit
							PERSISTED			NOT	NULL,

	[ConfidenceIn]			AS CONVERT(FLOAT, CASE WHEN ([x]	 BETWEEN [xMean] - [ConfidenceInterval] AND [xMean] + [ConfidenceInterval]) THEN [x] END)
							PERSISTED					,
	[ConfidenceOut]			AS CONVERT(FLOAT, CASE WHEN ([x] NOT BETWEEN [xMean] - [ConfidenceInterval] AND [xMean] + [ConfidenceInterval]) THEN [x] END)
							PERSISTED					,
	[PredictionIn]			AS CONVERT(FLOAT, CASE WHEN ([x]	 BETWEEN [xMean] - [PredictionInterval] AND [xMean] + [PredictionInterval]) THEN [x] END)
							PERSISTED					,
	[PredictionOut]			AS CONVERT(FLOAT, CASE WHEN ([x] NOT BETWEEN [xMean] - [PredictionInterval] AND [xMean] + [PredictionInterval]) THEN [x] END)
							PERSISTED					,

	[PearsonThreshold]		FLOAT				NOT	NULL	CHECK(ROUND([PearsonThreshold], 5) BETWEEN 0.0 AND 1.0),

	[PearsonIn]				AS CONVERT(FLOAT, CASE WHEN ([ResidualAbsolute] <= [PearsonThreshold]	AND [ResidualRelative] <= [PearsonThreshold]) THEN [x] END)
							PERSISTED					,	--	Ring (Black)
	[PearsonOut]			AS CONVERT(FLOAT, CASE WHEN ([ResidualAbsolute] > [PearsonThreshold]	OR	[ResidualRelative] > [PearsonThreshold]) THEN [x] END)
							PERSISTED					,	--	Ring (Black)

	CHECK ([ConfidenceInterval] <= [PredictionInterval]),
	PRIMARY KEY CLUSTERED ([x] ASC, [Id] ASC)
)
WITH SCHEMABINDING
AS
BEGIN

	DECLARE	@xCount			FLOAT;
	DECLARE	@xMin			FLOAT;
	DECLARE	@xSum			FLOAT;
	DECLARE	@xMean			FLOAT;
	DECLARE	@xStDevP		FLOAT;
	DECLARE	@xSumXiX		FLOAT;

	DECLARE	@wSum			FLOAT;
	DECLARE	@xwMean			FLOAT;

	SELECT
		@xCount		= COUNT(1),
		@xMin		= MIN([s].[x]),
		@xSum		= SUM([s].[x]),
		@xMean		= AVG([s].[x]),
		@xStDevP	= STDEVP([s].[x]),

		@wSum		= SUM([s].[w]),
		@xwMean		= SUM([s].[x] * [s].[w]) / SUM([s].[w])
	FROM
		@Sample	[s];

	IF (@xCount <= 2) RETURN;

	SELECT
		@xSumXiX = SUM(SQUARE([s].[x] - @xMean))
	FROM
		@Sample	[s];

	DECLARE	@Order			FLOAT	= CONVERT(FLOAT, ABS(@SortOrder) / @SortOrder);
	DECLARE	@nTiles			TINYINT	= COALESCE(@Tiles, 4);
	DECLARE @tAlpha			FLOAT	= @Confidence_Pcnt;	
	DECLARE	@tStat			FLOAT	= [stat].[Student_Statistic](@xCount - 1.0, @tAlpha, 2);
	DECLARE	@Confidence		FLOAT	= @tStat * @xStDevP / SQRT(@xCount);
	DECLARE	@Prediction		FLOAT	= @tStat * @xStDevP * SQRT(1.0 + 1.0 / @xCount);
	DECLARE @wPTile			FLOAT	= CASE WHEN @Order = 1.0 THEN 0.0 ELSE 100.0 END;

	INSERT INTO @ReturnTable
	(
		[Id], [RowId],
		[x], [w],
		[xQuartile], [xPercentile], [xPercentRank], [xCumulative], [xRank], [xRankDense],
		[zScore], [zPercentile], [zHypothesis],
		[ResidualStandard], [ResidualAbsolute], [ResidualRelative], [ResidualThreshold],
		[xMean],
		[ConfidenceInterval], [PredictionInterval],
		[PearsonThreshold]
	)
	SELECT
		[s].[Id],
		[s].[RowId],
		[s].[x],
		[s].[w],

			[xQuartile]			=			NTILE(@nTiles)				OVER(ORDER BY [s].[x] * @Order ASC, [s].[RowId] ASC),

			[xPercentile]		= 100.0 *	CONVERT(FLOAT, ROW_NUMBER()	OVER(ORDER BY [s].[x] * @Order ASC, [s].[RowId] ASC) - 0.5) / @xCount,
			[xPercentRank]		= 100.0 *	PERCENT_RANK()				OVER(ORDER BY [s].[x] * @Order ASC, [s].[RowId] ASC),
			[xCumulative]		= 100.0 *	CUME_DIST()					OVER(ORDER BY [s].[x] * @Order ASC, [s].[RowId] ASC),

			[xRank]				=			RANK()						OVER(ORDER BY [s].[x] * @Order ASC, [s].[RowId] ASC),
			[xRankDense]		=			DENSE_RANK()				OVER(ORDER BY [s].[x] * @Order ASC, [s].[RowId] ASC),	

			[zScore]			= CASE WHEN @xStDevP <> 0.0 THEN ([s].[x] - @xMean) / @xStDevP ELSE 0.0 END,
			[zPercentile]		= [stat].[Student_Probability](@xCount, CASE WHEN @xStDevP <> 0.0 THEN ([s].[x] - @xMean) / @xStDevP ELSE 0.0 END, 1),
			[zHypothesis]		= CASE WHEN CASE WHEN @xStDevP <> 0.0 THEN ABS(([s].[x] - @xMean) / @xStDevP) ELSE 0.0 END < @tStat
									THEN 1
									ELSE 0
									END,

			[ResidualStandard]	= CASE WHEN @xSumXiX <> 0.0 AND SQRT(SQUARE(@xStDevP) * (1.0 - 1.0/@xCount - SQUARE([s].[x] - @xMean) / @xSumXiX)) <> 0.0
				THEN
					([s].[x] - @xMean) /
					SQRT(SQUARE(@xStDevP) * (1.0 - 1.0/@xCount - SQUARE([s].x - @xMean) / @xSumXiX))
				END,

			[ResidualAbsolute]	= (SELECT COUNT(1) FROM @Sample [b]
				WHERE	SQUARE([b].[x] - @xMean) <= SQUARE([s].[x] - @xMean)
					) / @xCount,

			[ResidualRelative]	= (SELECT COUNT(1) FROM @Sample [b]
				WHERE	CASE WHEN [b].[x] <> 0.0 THEN SQUARE(([b].[x] - @xMean) / [b].[x]) ELSE 1.0 END
					<=	CASE WHEN [s].[x] <> 0.0 THEN SQUARE(([s].[x] - @xMean) / [s].[x]) ELSE 1.0 END
					) / @xCount,

			[ResidualThreshold]		= @ResidualThreshold,

			[xMean]					= @xMean,
			[ConfidenceInterval]	= @Confidence,
			[PredictionInterval]	= @Prediction,
			[PearsonThreshold]		= @PearsonThreshold

	FROM
		@Sample	[s];

	UPDATE [r]
	SET
			@wPTile			= @wPTile + [r].[w] / @wSum * 100.0			* @Order,
		[r].[wPercentile]	= @wPTile - [r].[w] / @wSum * 100.0 / 2.0	* @Order,
		[r].[zQuartile]		= [u].[zQuartile]
	FROM
		@ReturnTable		[r]
	INNER JOIN (
		SELECT
			[x].[Id],
				[zQuartile]  = NTILE(@nTiles) OVER(ORDER BY ABS([x].[zScore]) ASC)
		FROM
			@ReturnTable	[x]
		) [u]
		ON	[u].[Id]	= [r].[Id];

	RETURN;

END;
GO