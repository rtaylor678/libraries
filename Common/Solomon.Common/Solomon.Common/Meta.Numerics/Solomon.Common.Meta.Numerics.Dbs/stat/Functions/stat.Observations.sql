﻿CREATE FUNCTION [stat].[Observations]
(
	@Sample			[stat].[SampleRandomVar]		READONLY							--	Data points to examine
)
RETURNS TABLE
WITH SCHEMABINDING
AS RETURN
(
SELECT
	[o].[Id],
	[o].[RowId],
	[o].[x],
	[o].[w],

	[o].[xQuartile],
	[o].[xPercentile],
	[o].[xPercentRank],
	[o].[xCumulative],
	[o].[xRank],
	[o].[xRankDense],

	[o].[wPercentile],

	[o].[zScore],
	[o].[zQuartile],
	[o].[zPercentile],
	[o].[zHypothesis],

	[o].[ErrorAbsolute],
	[o].[ErrorRelative],

	[o].[ResidualStandard],
	[o].[ResidualAbsolute],
	[o].[ResidualRelative],

	[o].[PlotQ1],
	[o].[PlotQ2],
	[o].[PlotQ3],
	[o].[PlotQ4],

	[o].[PlotZ1],
	[o].[PlotZ2],
	[o].[PlotZ3],
	[o].[PlotZ4],

	[o].[ResidualThreshold],
	[o].[ResidualStdIn],
	[o].[ResidualStdOut],

	[o].[xMean],

	[o].[ConfidenceInterval],
	[o].[PredictionInterval],

	[o].[PredictionLower],
	[o].[ConfidenceLower],
	[o].[ConfidenceUpper],
	[o].[PredictionUpper],

	[o].[ConfidenceIn],
	[o].[ConfidenceOut],
	[o].[PredictionIn],
	[o].[PredictionOut],

	[o].[PearsonThreshold],
	[o].[PearsonIn],
	[o].[PearsonOut]

FROM
	[stat].[ObservationsAnalysis](@Sample, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT)	[o]
);