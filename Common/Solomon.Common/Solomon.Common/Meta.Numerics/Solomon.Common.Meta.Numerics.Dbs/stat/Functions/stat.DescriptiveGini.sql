﻿CREATE FUNCTION [stat].[DescriptiveGini]
(
	--	http://en.wikipedia.org/wiki/Gini_coefficient

	@Sample					[stat].[SampleRandomVar]	READONLY										--	Data points to examine
)
RETURNS @ReturnTable TABLE 
(
	[Id]					INT				NOT	NULL,
	[RowId]					INT				NOT	NULL,
	[x]						FLOAT 			NOT	NULL,
	[w]						FLOAT			NOT	NULL,

	[xLorenz]				FLOAT			NOT	NULL	CHECK(ROUND([xLorenz], 5) BETWEEN 0.0 AND 100.0)
														DEFAULT(0),
	[yLorenz]				FLOAT			NOT	NULL	CHECK(ROUND([yLorenz], 5) BETWEEN 0.0 AND 100.0)
														DEFAULT(0),
	[_dLorenz]				AS  ROUND(CONVERT(FLOAT, [xLorenz] - [yLorenz]), 10)
							PERSISTED		NOT	NULL,

	[Gini]					FLOAT				NULL 	CHECK([Gini] BETWEEN 0.0 AND 100.0),

	PRIMARY KEY CLUSTERED ([x] ASC, [Id] ASC)
)
WITH SCHEMABINDING
AS
BEGIN

	DECLARE	@xCount			INT;
	DECLARE	@xSum			FLOAT;				--	Sum of X values; Value of interest on the Y-Axis
	DECLARE	@wSum			FLOAT;				--	Sum of W values

	DECLARE	@i				FLOAT	= 0.0;		--	Cumulative w
	DECLARE	@j				FLOAT	= 0.0;		--	Cumulative x
	DECLARE	@s				FLOAT	= 0.0;		--	Previous value of @i
	DECLARE	@t				FLOAT	= 0.0;		--	Previous value of @j

	DECLARE @Area			FLOAT	= 0.0;		--	Gini Coefficient

	SELECT
		@xCount	= COUNT([s].[x]),
		@xSum	= SUM(ABS([s].[x])),
		@wSum	= SUM(COALESCE(ABS([s].[w]), 1))
	FROM
		@Sample	[s];

	IF (@xCount = 0 OR @xSum = 0.0) RETURN;

	INSERT INTO @ReturnTable
	(
		[Id],
		[RowId],
		[x],
		[w]
	)
	VALUES
		(0, 0, 0, 0);

	INSERT INTO @ReturnTable
	(
		[Id],
		[RowId],
		[x],
		[w]
	)
	SELECT
		[s].[Id],
		[s].[RowId],
		ABS([s].[x]),
		COALESCE(ABS([s].[w]), 1)
	FROM
		@Sample	[s];

	UPDATE @ReturnTable
	SET
		@i = [xLorenz] = @i + [w] / @wSum * 100.0,
		@j = [yLorenz] = @j + [x] / @xSum * 100.0,
		@Area = @Area + (@i - @s) * (@j + @t) / 100.0,
		@s = @i,
		@t = @j;

	IF (@Area >= 100.0)
	BEGIN
		DELETE FROM @ReturnTable;
		RETURN;
	END;

	UPDATE @ReturnTable
	SET [Gini] = 100.0 - @Area;

	RETURN;

END;
GO