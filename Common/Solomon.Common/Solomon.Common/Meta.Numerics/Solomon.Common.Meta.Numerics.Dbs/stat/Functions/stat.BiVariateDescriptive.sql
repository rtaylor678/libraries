﻿CREATE FUNCTION [stat].[BiVariateDescriptive]
(
	@Results		[stat].[ResultsBiVariateObservations]	READONLY
)
RETURNS @ReturnTable TABLE
(
	[Relationship]			CHAR(1),
	[b0]					FLOAT,
	[b1]					FLOAT,

	[SSE]					FLOAT,
	[SSR]					FLOAT,
	[SST]					FLOAT,
	[MSE]					FLOAT,
	[MSR]					FLOAT,
	[COV]					FLOAT,
	[COD]					FLOAT,
	[COR]					FLOAT,

	[SumXiX]				FLOAT,
	[b1StdError]			FLOAT,
	[StdAway]				FLOAT,

	[StudentTStatistic]		FLOAT,
	[StudentTTest]			FLOAT,
	[StudentTConfidence]	FLOAT,
	[StudentTHypothesis]	FLOAT,
	[StudentTIsDependent]	INT,

	[FisherStatistic]		FLOAT,
	[FisherTest]			FLOAT,
	[FisherConfidence]		FLOAT,
	[FisherHypothesis]		FLOAT,
	[FisherIsDependent]		INT,

	[ChiStatistic]			FLOAT,
	[ChiTest]				FLOAT,
	[ChiConfidence]			FLOAT,
	[ChiHypothesis]			FLOAT,
	[ChiIsDependent]		INT,

	[Rho]					FLOAT,
	[Tau]					FLOAT
)
WITH SCHEMABINDING
AS
BEGIN

	DECLARE	@Count				FLOAT;		--	Number of observations in the sample
	DECLARE	@xMean				FLOAT;		--	Mean of linearlized x values (Indepentent)
	DECLARE	@yMean				FLOAT;		--	Mean of Y for @b1e, Mean, Pearson (Dependent)

	DECLARE	@xStDev				FLOAT;		--	Standard Deviation, x
	DECLARE	@yStDev				FLOAT;		--	Standard Deviation, x

	DECLARE	@SSE				FLOAT;		--	Sum of Squares due to Error (Error Sum of Squares)
	DECLARE	@SSR				FLOAT;		--	Sum of Squares due to Regression (Regression Sum of Squares)
	DECLARE	@SST				FLOAT;		--	Sum of Squares (Total: SST = SSE + SSR)
	DECLARE	@MSE				FLOAT;		--	Mean Square Error (Estimates Population Variance (σ²) or Sample Variance (s²)) 
	DECLARE	@MSR				FLOAT;		--	Mean Square Regression
	DECLARE	@COV				FLOAT;		--	Covariance
	DECLARE	@COD				FLOAT;		--	Coefficient of Determination (r²)
	DECLARE	@COR				FLOAT;		--	Correlation Coefficient (r)

	DECLARE	@SumXiX 			FLOAT;		--	Sum of xi - xbar (Σ(xi-AVG(x))
	DECLARE	@Stdev				FLOAT;		--	Standard Deviation of the dependent values about the function
	DECLARE	@StdAway			FLOAT;		--	Calculate the number of Standard Deviations to set the Min/Max validation range 
	DECLARE	@b1StdError			FLOAT;

	DECLARE	@b0					FLOAT;
	DECLARE	@b1					FLOAT;
	DECLARE	@Confidence_Pcnt	FLOAT;		--	Prediction/confidence interval level	[0.0, 1.0]
	DECLARE	@Relationship		CHAR(1);

	SELECT
		@xMean				= AVG([t].[x]),
		@yMean				= AVG([t].[y]),

		@xStDev				= STDEVP([t].[x]),
		@yStDev				= STDEVP([t].[y]),

		@Count				= CONVERT(FLOAT, COUNT(1)),
		@SST				= SUM([t].[SST]),
		@SSR				= SUM([t].[SSR]),
		@SSE				= SUM([t].[SSE]),
		@MSE				= SUM([t].[SSE]) / CONVERT(FLOAT, COUNT(1) - 2),
		@SumXiX				= SUM([t].[SumXiX]),
		@COV				= SUM([t].[COV]),
		@COD				= SUM([t].[SSE]) / SUM([t].[SST]),

		@b0					= MAX([t].[b0]),
		@b1					= MAX([t].[b1]),
		@Confidence_Pcnt	= MAX([t].[Confidence_Pcnt]),
		@Relationship		= MAX([t].[Relationship])
	FROM (
		SELECT
			[e].[x],
			[e].[y],
			[e].[b0],
			[e].[b1],
			[e].[Confidence_Pcnt],
			[e].[Relationship],

			[SSE]		= SQUARE([e].[y] -	[e].[yEstimate]),
			[SST]		= SQUARE([e].[y]								- AVG([e].[y]) OVER()),
			[SSR]		= SQUARE(			[e].[yEstimate]				- AVG([e].[y]) OVER()),
			[SumXiX]	= SQUARE([e].[x]								- AVG([e].[x]) OVER()),
			[COV]		= ([e].[x] - AVG([e].[x]) OVER()) * ([e].[y]	- AVG([e].[y]) OVER())	
		FROM
			@Results [e]
		) [t];
			
	DECLARE	@DFd				FLOAT	= @Count - 1.0;		--	Degrees of Freedom (Denominator)
	DECLARE	@DFn				FLOAT	= 1.0;				--	Degrees of Freedom (Number of Independent Variables)

	SET @StDev = SQRT(@MSE);
	SET	@b1StdError				= SQRT(@SSE / @Count);

	SET	@MSR					= @SSR / CONVERT(FLOAT, @DFd);
	SET	@COR					= CASE WHEN @b1 <> 0.0 THEN ABS(@b1) / @b1 * SQRT(@SSR / @SST) END;
	SET	@StdAway				= CASE WHEN @SSR <> 0.0 THEN @SST / @SSR END;
	
	DECLARE @Alpha2Tail			FLOAT	= CASE WHEN @Confidence_Pcnt >= 1.0 THEN 0.999999999999999 ELSE @Confidence_Pcnt END;

	---------------------------------------------------------------------------
	--	Student T-Test

	DECLARE	@StudentTStatistic		FLOAT	= [stat].[Student_Statistic](@DFd, @Alpha2Tail, 2);
	DECLARE	@StudentTTest			FLOAT	= CASE WHEN @B1StdError <> 0.0 THEN ABS(@b1 / @b1StdError) END;
	DECLARE	@StudentTConfidence		FLOAT	= [stat].[Student_Probability](@DFd, @StudentTTest, 2);
	DECLARE	@StudentTHypothesis		INT		= CASE WHEN @StudentTTest > @StudentTStatistic THEN 1 ELSE 0 END;  -- Assumption is independent (1 = Independent, 0 = Dependent)

	---------------------------------------------------------------------------
	--	Fisher Test (working)

	DECLARE	@FisherStatistic		FLOAT	= [stat].[Fisher_Statistic](@DFn, @DFd, @Alpha2Tail);
	DECLARE	@FisherTest				FLOAT	= CASE WHEN @MSE <> 0.0 THEN @MSR / @MSE ELSE 0.0 END;
	DECLARE	@FisherConfidence		FLOAT	= [stat].[Fisher_Probability](@DFn, @DFd, @FisherTest);
	DECLARE	@FisherHypothesis		INT		= CASE WHEN @FisherTest > @FisherStatistic THEN 1 ELSE 0 END;

	--------------------------------------------------------------------------------
	--	Spearman's rank correlation coefficient (Rho)
	--	If X and Y are independent, then the coefficient is approximately zero.
	--	Pearson product-moment correlation coefficient (PMCC)
	--	Chi-Squared Test

	DECLARE	@Rho					FLOAT;
	DECLARE	@PMCC					FLOAT;
	DECLARE	@ChiTest				FLOAT;

	SELECT
		@Rho		= SUM(([r].[x] - @xMean) * ([r].[y] - @yMean))
						/ SQRT(
							CASE WHEN SUM(SQUARE([r].[x] - @xMean)) * SUM(SQUARE([r].[y] - @yMean)) <> 0.0
							THEN SUM(SQUARE([r].[x] - @xMean)) * SUM(SQUARE([r].[y] - @yMean))
							END),
		@PMCC		= SUM(([r].[x] - @xMean) * ([r].[y] - @yMean)) / @DFd / CASE WHEN (@xStDev * @yStDev) <> 0.0 THEN (@xStDev * @yStDev) END,
		@ChiTest	= SUM(CASE WHEN [r].[yEstimate] <> 0.0 THEN SQUARE([r].[y] - [r].[yEstimate]) / [r].[yEstimate] END)
	FROM
		@Results	[r];

	DECLARE	@ChiStatistic			FLOAT	= [stat].[ChiSquared_Statistic](@DFd, @Confidence_Pcnt);
	DECLARE	@ChiConfidence			FLOAT	= [stat].[ChiSquared_Probability](@DFd, @ChiTest);
	DECLARE	@ChiHypothesis			FLOAT	= CASE WHEN @ChiTest > @ChiStatistic THEN 1 ELSE 0 END;

	--------------------------------------------------------------------------------
	--	Kendall rank correlation coefficient (tau)
	--	If the agreement between the two rankings is perfect (i.e., the two rankings are the same) the coefficient has value 1.
	--	If the disagreement between the two rankings is perfect (i.e., one ranking is the reverse of the other) the coefficient has value −1.
	--	If X and Y are independent, then the coefficient is approximately zero.
	
	DECLARE	@Tau					FLOAT;

	SELECT @Tau = (SUM(
				CASE WHEN ([r1].[x] > [r2].[x] AND [r1].[y] > [r2].[y]) OR     ([r1].[x] < [r2].[x] AND [r1].[y] < [r2].[y]) THEN 1.0 ELSE 0.0 END) -
					SUM(CASE WHEN NOT  (([r1].[x] > [r2].[x] AND [r1].[y] > [r2].[y]) OR  ([r1].[x] < [r2].[x] AND [r1].[y] < [r2].[y])) THEN 1.0 ELSE 0.0 END)) /
					CAST(COUNT(1) AS FLOAT)
	FROM
		@Results [r1]
	CROSS JOIN
		@Results [r2]
	WHERE
		[r1].[Id] <> [r2].[Id];
		
	INSERT INTO @ReturnTable
	(
		[Relationship],
		[b0],
		[b1],

		[SSE],
		[SSR],
		[SST],
		[MSE],
		[MSR],
		[COV],
		[COD],
		[COR],

		[SumXiX],
		[b1StdError],
		[StdAway],

		[StudentTStatistic],
		[StudentTTest],
		[StudentTConfidence],
		[StudentTHypothesis],
		[StudentTIsDependent],

		[FisherStatistic],
		[FisherTest],
		[FisherConfidence],
		[FisherHypothesis],
		[FisherIsDependent],

		[ChiStatistic],
		[ChiTest],
		[ChiConfidence],
		[ChiHypothesis],
		[ChiIsDependent],

		[Rho],
		[Tau]
	)
	SELECT
		[Relationship]			= @Relationship,
		[b0]					= @b0,
		[b1]					= @b1,

		[SSE]					= @SSE,
		[SSR]					= @SSR,
		[SST]					= @SST,
		[MSE]					= @MSE,
		[MSR]					= @MSR,
		[COV]					= @COV,
		[COD]					= @COD,
		[COR]					= @COR,

		[SumXiX]				= @SumXiX,
		[b1StdError]			= @b1StdError,
		[StdAway]				= @StdAway,

		[StudentTStatistic]		= @StudentTStatistic,
		[StudentTTest]			= @StudentTTest,
		[StudentTConfidence]	= @StudentTConfidence,
		[StudentTHypothesis]	= @StudentTHypothesis,
		[StudentTIsDependent]	= 1 - @StudentTHypothesis,

		[FisherStatistic]		= @FisherStatistic,
		[FisherTest]			= @FisherTest,
		[FisherConfidence]		= @FisherConfidence,
		[FisherHypothesis]		= @FisherHypothesis,
		[FisherIsDependent]		= 1 - @FisherHypothesis,

		[ChiStatistic]			= @ChiStatistic,
		[ChiTest]				= @ChiTest,
		[ChiConfidence]			= @ChiConfidence,
		[ChiHypothesis]			= @ChiHypothesis,
		[ChiIsDependent]		= 1 - @ChiHypothesis,

		[Rho]					= @Rho,
		[Tau]					= @Tau;

	RETURN;

END;