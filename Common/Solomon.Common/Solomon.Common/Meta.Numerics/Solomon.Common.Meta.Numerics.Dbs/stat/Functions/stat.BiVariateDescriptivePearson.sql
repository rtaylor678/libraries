﻿CREATE FUNCTION [stat].[BiVariateDescriptivePearson]
(
	@Results		[stat].[ResultsBiVariateObservations]	READONLY
)
RETURNS @ReturnTable TABLE
(
	[pTile]				FLOAT		NOT	NULL	CHECK([pTile]	>= 0.0 AND ROUND([pTile], 3)	<= 1.0),
	[Pearson]			FLOAT		NOT	NULL	CHECK([Pearson]	>= 0.0 AND ROUND([Pearson], 3)	<= 1.0),
	[items]				INT			NOT NULL	CHECK([items] >  0),

	PRIMARY KEY CLUSTERED ([pTile] ASC)
)
WITH SCHEMABINDING
AS
BEGIN

	DECLARE @pTile				FLOAT	 = 0.00;
	DECLARE @pMax				FLOAT	 = 1.00;
	DECLARE @pStep				FLOAT	 = 0.01;

	DECLARE	@xMean				FLOAT;	--	Mean of linearlized x values (Indepentent)
	DECLARE	@yMean				FLOAT;	--	Mean of Y for @b1e, Mean, Pearson (Dependent)

	WHILE @pTile <= @pMax + @pStep BEGIN

		SELECT
			@xMean = AVG([r].[x]),
			@yMean = AVG([r].[y]) 
		FROM
			@Results	[r]
		WHERE
				[r].[ResidualAbsolute] < @pTile
			AND [r].[ResidualRelative] < @pTile;

		INSERT INTO @ReturnTable
		(
			[pTile],
			[Pearson],
			[items]
		)
		SELECT
			@pTile,
			SUM(([r].[x] - @xMean) * ([r].[y] - @yMean)) / CONVERT(FLOAT, COUNT(1)) / STDEVP([r].[x]) / STDEVP([r].[y]),
			COUNT(1)
		FROM
			@Results	[r]
		WHERE
				[r].[ResidualAbsolute] < @pTile
			AND [r].[ResidualRelative] < @pTile
		HAVING	STDEVP([r].[x]) <> 0
			AND	STDEVP([r].[y]) <> 0;

		IF (@pTile >= @pMax) BREAK ELSE SET @pTile = @pTile + @pStep;

	END;

	RETURN;

END;