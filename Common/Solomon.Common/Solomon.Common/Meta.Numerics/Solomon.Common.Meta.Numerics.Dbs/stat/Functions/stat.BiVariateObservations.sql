﻿CREATE FUNCTION [stat].[BiVariateObservations]
(
	@Sample					[stat].[SampleBiVariate]	READONLY
)
RETURNS TABLE
WITH SCHEMABINDING
AS RETURN
(
SELECT
	[b].[Relationship],

	[b].[Id],
	[b].[RowId],
	[b].[x],
	[b].[y],

	[b].[yQuartile],
	[b].[yPercentile],
	[b].[yPercentRank],
	[b].[yCumulative],
	[b].[yRank],
	[b].[yRankDense],
	[b].[yEstimate],

	[b].[zScore],
	[b].[zQuartile],
	[b].[zPercentile],
	[b].[zHypothesis],

	[b].[ErrorAbsolute],
	[b].[ErrorRelative],

	[b].[ResidualStandard],
	[b].[ResidualAbsolute],
	[b].[ResidualRelative],

	[b].[PlotY1],
	[b].[PlotY2],
	[b].[PlotY3],
	[b].[PlotY4],

	[b].[PlotZ1],
	[b].[PlotZ2],
	[b].[PlotZ3],
	[b].[PlotZ4],

	[b].[ResidualThreshold],
	[b].[ResidualStdIn]	,
	[b].[ResidualStdOut],

	[b].[ConfidenceInterval],
	[b].[PredictionInterval],
	[b].[PredictionLower],
	[b].[ConfidenceLower],
	[b].[ConfidenceUpper],
	[b].[PredictionUpper],
	[b].[ConfidenceIn],
	[b].[ConfidenceOut],
	[b].[PredictionIn],
	[b].[PredictionOut],

	[b].[PearsonThreshold],
	[b].[PearsonIn],
	[b].[PearsonOut],

	[b].[xAsCeiling],
	[b].[CeilingBreach],

	[b].[SuspectWarning],
	[b].[SuspectCritical],

	[b].[b0],
	[b].[b1],
	[b].[Confidence_Pcnt]
FROM
	[stat].[BiVariateObservationsAnlaysis](@Sample, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT) [b]
);