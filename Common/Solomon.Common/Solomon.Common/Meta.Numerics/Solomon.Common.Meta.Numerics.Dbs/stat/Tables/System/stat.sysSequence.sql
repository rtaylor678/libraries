﻿CREATE TABLE [stat].[sysSequence]
(
	[Id]		INT					NOT NULL	IDENTITY(0,1),

	[Id_Pcnt]	AS CONVERT(FLOAT, [Id] / 100.0)
				PERSISTED			NOT	NULL,

	[RowGuid]	UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_sysSequence]			DEFAULT(NEWSEQUENTIALID()) ROWGUIDCOL,
												CONSTRAINT [UX_sysSequence_RoeGuid]	UNIQUE([RowGuid]),

	CONSTRAINT [PK_sysSequence]	PRIMARY KEY CLUSTERED([Id] ASC)
);
GO

CREATE STATISTICS [ST_sysSequence_Id]
ON [stat].[sysSequence]
(
	[Id]
)
WITH SAMPLE 100 PERCENT;
GO

CREATE STATISTICS [ST_sysSequence_Id_Pcnt]
ON [stat].[sysSequence]
(
	[Id_Pcnt]
)
WITH SAMPLE 100 PERCENT;
GO

CREATE UNIQUE INDEX [IX_sysSequence_Id]
ON [stat].[sysSequence]
(
	[Id] ASC
)
WHERE
	[Id] <= 100;
GO