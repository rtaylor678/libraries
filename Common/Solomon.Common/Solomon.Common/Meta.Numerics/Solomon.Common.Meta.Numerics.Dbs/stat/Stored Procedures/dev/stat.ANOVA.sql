﻿--CREATE PROCEDURE [stat].[ANOVA]
--(
--	@Population			[stat].[Sample_RandomVar]		READONLY,			--	Data points to examine
--	@Confidence_Pcnt	FLOAT				= 90.0,							--	Prediction/confidence interval level
--	@SortOrder			SMALLINT			= 1,							--	Sort order
--	@Tiles				TINYINT				= NULL,							--	Number of nTiles
--	@CapMin				FLOAT				= NULL,							--	Minimum reporting values to average
--	@CapMax				FLOAT				= NULL,							--	Maximum reporting values to average
	
--	@PearsonThreshold	FLOAT				= 0.90,							--	Pearson threshold					[0.0, 1.0]
--	@ResidualThreshold	FLOAT				= 2.00,							--	Residual identification threshold
--	@zWidth				FLOAT				= 0.5,							--	Histogram bucket width

--	@SmoothingType		CHAR (1)			= 'M',							--	Type of smoothing				
--	@EndPointMean		TINYINT				= 2,							--	Number of endpoints to average
--	@iPoints			TINYINT				= 4								--	Number of observations used during interpolation or regression
--)
--AS
--BEGIN

--	SET NOCOUNT ON;
	
--	----------------------------------------------------------------------------------
--	----	Population Statistics

--	--DECLARE @Arithmetic				[stat].[Results_Descriptive];

--	--INSERT INTO @Arithmetic(xCount,
--	--	xMin, xMax, xMean, xStDevP, xVarP, xSum, xSumXiX,
--	--	wMean,
--	--	xLimitLower, xLimitUpper,
--	--	tTest, tConf, tAlpha, tStat, iConfidence, iPrediction,
--	--	xKurtosis, xSkewness)
--	--SELECT
--	--	a.xCount,
--	--	a.xMin,
--	--	a.xMax,
--	--	a.xMean,
--	--	a.xStDevP,
--	--	a.xVarP,
--	--	a.xSum,
--	--	a.xSumXiX,
--	--	a.wMean,
--	--	a.xLimitLower,
--	--	a.xLimitUpper,
--	--	a.tTest,
--	--	a.tConf,
--	--	a.tAlpha,
--	--	a.tStat,
--	--	a.iConfidence,
--	--	a.iPrediction,
--	--	a.xKurtosis,
--	--	a.xSkewness
--	--FROM stat.DescriptiveArithmetic(@Population, @Confidence_Pcnt, @CapMin, @CapMax) a;

--	--SELECT * FROM @Arithmetic;

--	----------------------------------------------------------------------------------
--	----	Quartile Statistics

--	--DECLARE @Tile				[stat].[Results_Descriptive];

--	--INSERT INTO @Tile(qTile, xCount, xMin, xMax, xMean, xStDevS, xVarS, xSum, xSumXiX, wMean, xLimitLower, xLimitUpper, TileBreakLower, TileBreakUpper)
--	--SELECT
--	--	t.qTile,
--	--	t.xCount,
--	--	t.xMin,
--	--	t.xMax,
--	--	t.xMean,
--	--	t.xStDevS,
--	--	t.xVarS,
--	--	t.xSum,
--	--	t.xSumXiX,
--	--	t.wMean,
--	--	t.xLimitLower,
--	--	t.xLimitUpper,
--	--	t.TileBreakLower,
--	--	t.TileBreakUpper
--	--FROM stat.DescriptiveNTile(@Population, @SortOrder, @Tiles, @CapMin, @CapMax)	t;

--	--SELECT * FROM @Tile;

--	----------------------------------------------------------------------------------
--	----	Box Plots

--	--DECLARE @Box					stat.DescriptiveBoxPlotResults;

--	--INSERT INTO @Box(xCount, BoxLower, BoxMedian, BoxUpper)
--	--SELECT
--	--	o.xCount,
--	--	o.BoxLower,
--	--	o.BoxMedian,
--	--	o.BoxUpper
--	--FROM stat.DescriptiveBoxPlot(@Population) o;

--	--SELECT * FROM @Box;

--	----------------------------------------------------------------------------------
--	----	Geometric and Harmonic Means

--	--DECLARE @Geometric				stat.DescriptiveGeomHarmResults;

--	--INSERT INTO @Geometric(xCount, gMean, wgMean, gStDevP, hMean, whMean)
--	--SELECT
--	--	gh.xCount,
--	--	gh.gMean,
--	--	gh.wgMean,
--	--	gh.gStDevP,
--	--	gh.hMean,
--	--	gh.whMean
--	--FROM stat.DescriptiveGeomHarm(@Population) gh

--	--SELECT * FROM @Geometric;

--	----------------------------------------------------------------------------------
--	----	Record Statistics

--	--DECLARE @ObservationResults		stat.ObservationsRandomVarResults;

--	--INSERT INTO @ObservationResults(FactorSetID, Refnum, x,
--	--	iPcntTile, zPcntTile, xQuartile,
--	--	xRow, xRank, xRankDense,
--	--	zScore, zQuartile, zHypothesis,
--	--	ErrorAbsolute, ErrorRelative,
--	--	ResidualStandard, ResidualAbsolute, ResidualRelative, ResidualThreshold,
--	--	xMean, iConfidence, iPrediction,
--	--	Basis, Audit,
--	--	PearsonThreshold
--	--)
--	--SELECT
--	--	r.FactorSetID,
--	--	r.Refnum,
--	--	r.x,

--	--	r.iPcntTile,
--	--	r.zPcntTile,
--	--	r.xQuartile,

--	--	r.xRow,
--	--	r.xRank,
--	--	r.xRankDense,

--	--	r.zScore,
--	--	r.zQuartile,
--	--	r.zHypothesis,

--	--	r.ErrorAbsolute,
--	--	r.ErrorRelative,
--	--	r.ResidualStandard,
--	--	r.ResidualAbsolute,
--	--	r.ResidualRelative,
--	--	r.ResidualThreshold,
--	--	r.xMean,
--	--	r.iConfidence,
--	--	r.iPrediction,
--	--	r.Basis,
--	--	r.Audit,
--	--	r.PearsonThreshold
--	--FROM stat.ObservationsRandomVar(@Population, @SortOrder, @Tiles, @Confidence_Pcnt, @PearsonThreshold, @ResidualThreshold) r;

--	--SELECT * FROM @ObservationResults;

--	----------------------------------------------------------------------------------
--	----	Z-Test Histogram

--	--DECLARE @Histogram				stat.DescriptiveHistogramResults;

--	--INSERT INTO @Histogram(zTestLower, zTestUpper, LimitLower, LimitUpper,
--	--	xCount, xMin, xMax, xMean)
--	--SELECT
--	--	h.zTestLower,
--	--	h.zTestUpper,
--	--	h.LimitLower,
--	--	h.LimitUpper,

--	--	h.xCount,
--	--	h.xMin,
--	--	h.xMax,
--	--	h.xMean
--	--FROM stat.DescriptiveHistogram(@ObservationResults, @zWidth) h;

--	--SELECT * FROM @Histogram;

--	----------------------------------------------------------------------------------
--	----	Gini Coefficient and Lorenz Curve

--	--DECLARE @Lorenz					stat.DescriptiveGiniResults;

--	--INSERT INTO @Lorenz(FactorSetID, Refnum, x, w, xLorenz, yLorenz, Gini)
--	--SELECT
--	--	g.FactorSetID,
--	--	g.Refnum,
--	--	g.x,
--	--	g.w,
--	--	g.xLorenz,
--	--	g.yLorenz,
--	--	g.Gini
--	--FROM stat.DescriptiveGini(@Population) g;

--	--SELECT * FROM @Lorenz;

--	----------------------------------------------------------------------------------
--	----	Smoothed

--	----DECLARE @Smoothed					stat.ObservationsSmoothedResults;

--	----INSERT INTO @Smoothed(xSmoothed, ySmoothed, FactorSetID, Refnum, rActual, rSmoothed)
--	----SELECT
--	----	s.xSmoothed,
--	----	s.ySmoothed,
--	----	s.FactorSetID,
--	----	s.Refnum,
--	----	s.rActual,
--	----	s.rSmoothed
--	----FROM stat.ObservationsSmoothed(@Population, @SmoothingType, @SortOrder, @EndPointMean, @iPoints) s;

--	----SELECT * FROM @Smoothed;

--END