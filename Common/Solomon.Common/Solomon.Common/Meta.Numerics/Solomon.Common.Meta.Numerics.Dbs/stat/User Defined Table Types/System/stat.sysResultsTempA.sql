﻿CREATE TYPE [stat].[sysResultsTempA] AS TABLE
(
	[xPlot]					FLOAT				NOT	NULL,
	[yInterpolated]			FLOAT				NOT	NULL,

	[yPlot]					FLOAT					NULL,

	PRIMARY KEY CLUSTERED([xPlot] ASC)
);