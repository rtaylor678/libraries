﻿CREATE TYPE [stat].[ResultsDescriptiveHistogram] AS TABLE
(
	[zTestLower]			FLOAT				NOT NULL,									--	Minimum Z-Score of bucket
	[zTestUpper]			FLOAT				NOT NULL,									--	Maximum Z-Score of bucket
	[zRange]				VARCHAR(14)			NOT	NULL,

	[LimitLower]			FLOAT				NOT	NULL,									--	Minimum value of bucket
	[LimitUpper]			FLOAT				NOT	NULL,									--	Maximum value of bucket
	[LimitRange]			VARCHAR(32)			NOT	NULL,

	[xCount]				INT					NOT	NULL	CHECK([xCount]	>= 0),			--	Item Count

	[xHistogramObs]			NVARCHAR(MAX)		NOT	NULL,

	[Observation]			FLOAT					NULL,

	[xMin]					FLOAT					NULL,									--	Minimum
	[xMax]					FLOAT					NULL,									--	Maximum
	[xRange]				FLOAT					NULL,									--	Range
	[xMean]					FLOAT					NULL,									--	Mean

	CHECK([zTestLower] <= [zTestUpper]),
	CHECK([LimitLower] <= [LimitUpper]),

	CHECK([xMin]	<= [xMax]),
	CHECK([xMin]	BETWEEN [LimitLower] AND [LimitUpper]),
	CHECK([xMax]	BETWEEN [LimitLower] AND [LimitUpper]),
	CHECK([xMean]	BETWEEN [xMin] AND [xMax]),

	PRIMARY KEY CLUSTERED([zTestLower] ASC)
);