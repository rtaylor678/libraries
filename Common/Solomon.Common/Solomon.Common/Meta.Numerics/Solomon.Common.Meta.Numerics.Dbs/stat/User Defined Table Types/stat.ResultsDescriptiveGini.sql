﻿CREATE TYPE [stat].[ResultsDescriptiveGini] AS TABLE
(
	[Id]					INT				NOT	NULL,
	[RowId]					INT				NOT	NULL,
	[x]						FLOAT 			NOT	NULL,
	[w]						FLOAT			NOT	NULL,

	[xLorenz]				FLOAT			NOT	NULL	CHECK(ROUND([xLorenz], 5) BETWEEN 0.0 AND 100.0)
														DEFAULT(0),
	[yLorenz]				FLOAT			NOT	NULL	CHECK(ROUND([yLorenz], 5) BETWEEN 0.0 AND 100.0)
														DEFAULT(0),
	[dLorenz]				FLOAT			NOT	NULL,

	[Gini]					FLOAT				NULL 	CHECK([Gini] BETWEEN 0.0 AND 100.0),

	PRIMARY KEY CLUSTERED ([x] ASC, [Id] ASC)
);