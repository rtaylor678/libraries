﻿CREATE TYPE [stat].[ResultsDescriptive] AS TABLE
(
	[qTile]					TINYINT				NOT	NULL	CHECK([qTile]	>= 1),					--	Quartile value
	[SampleType]			VARCHAR(10)			NOT	NULL,

	[xCount]				INT					NOT	NULL	CHECK([xCount]	>= 1),					--	Item Count
	[xMin]					FLOAT				NOT	NULL,											--	Minimum
	[xMax]					FLOAT				NOT	NULL,											--	Maximum
	[xRange]				FLOAT				NOT	NULL,

	[BreakPrevious]			FLOAT					NULL,
	[BreakNext]				FLOAT					NULL,

	[xMean]					FLOAT				NOT	NULL,											--	Mean								http://en.wikipedia.org/wiki/Mean
	[xStDev]				FLOAT				NOT	NULL	CHECK([xStDev]	>= 0.0),				--	Standard Deviation			σ		http://en.wikipedia.org/wiki/Standard_deviation
	[xVar]					FLOAT				NOT	NULL	CHECK([xVar]	>= 0.0),				--	Variance					σ²		http://en.wikipedia.org/wiki/Variance

	[ErrorStandard]			FLOAT				NOT	NULL,											--	Standard Error						http://en.wikipedia.org/wiki/Z-test
	[VariationCoefficient]	FLOAT					NULL,											--  Coefficient of Variation			http://en.wikipedia.org/wiki/Coefficient_of_variation
	[DispersionIndex]		FLOAT					NULL,											--  Index of Dispersion					http://en.wikipedia.org/wiki/Variance-to-mean_ratio

	[xSum]					FLOAT				NOT	NULL,											--	Sum	of x					Σ(x)
	[SSE]					FLOAT				NOT	NULL,											--	Sum Square Error			Σ(xi-AVG(x))
	[MSE]					FLOAT				NOT	NULL,											--	Mean Square Error			Σ(xi-AVG(x)) / (n-2)

	[xwMean]				FLOAT					NULL,											--	Weighted Mean
	[xwDiv]					FLOAT					NULL,											--	SUM(x) / SUM(w)

	[LimitLower]			FLOAT				NOT	NULL,											--	Minimum (Mean of smallest two items, >= @CapMin)
	[LimitUpper]			FLOAT				NOT	NULL,											--	Minimum (Mean of largest two items, <= @CapMax)
	[LimitRange]			FLOAT				NOT	NULL,

	[StudentTTest]			FLOAT				NOT	NULL	CHECK([StudentTTest]		>= 0.0),	--	Calcualted T Test
	[StudentTConfidence]	FLOAT				NOT	NULL	CHECK([StudentTConfidence]	>= 0.0),	--	Calculated Confidence
	[StudentTAlpha]			FLOAT				NOT	NULL	CHECK([StudentTAlpha]		>= 0.0),	--	Alpha' value						http://en.wikipedia.org/wiki/Student's_t-test
	[StudentTStatistic]		FLOAT				NOT	NULL	CHECK([StudentTStatistic]	>= 0.0),	--	Student T Statistic
	[StudentTHypothesis]	TINYINT				NOT	NULL,

	[IsNormalStatistic]		FLOAT				NOT	NULL,
	[IsNormalProbability]	FLOAT				NOT	NULL,
	[IsUniformStatistic]	FLOAT				NOT	NULL,
	[IsUniformProbability]	FLOAT				NOT	NULL,

	[ConfidenceInterval]	FLOAT				NOT	NULL	CHECK([ConfidenceInterval]	>= 0.0),	--	Confidence Interval					http://en.wikipedia.org/wiki/Confidence_interval
	[PredictionInterval]	FLOAT				NOT	NULL	CHECK([PredictionInterval]	>= 0.0),	--	Prediction Interval					http://en.wikipedia.org/wiki/Prediction_interval
	
	[PredictionLower]		FLOAT				NOT	NULL,
	[ConfidenceLower]		FLOAT				NOT	NULL,
	[ConfidenceUpper]		FLOAT				NOT	NULL,
	[PredictionUpper]		FLOAT				NOT	NULL,

	[BoxLowerExtreme]		FLOAT				NOT	NULL,
	[BoxLowerWhisker]		FLOAT				NOT	NULL,
	[BoxLower]				FLOAT				NOT	NULL,
	[BoxMedian]				FLOAT				NOT	NULL,
	[BoxUpper]				FLOAT				NOT	NULL,
	[BoxUpperWhisker]		FLOAT				NOT	NULL,
	[BoxUpperExtreme]		FLOAT				NOT	NULL,
	[BoxIMR]				FLOAT				NOT	NULL,
	[BoxWidth]				FLOAT				NOT	NULL,
	[BoxNotch]				FLOAT				NOT	NULL,

	[Kurtosis]				FLOAT					NULL,											--	Pearson's sample excess Kurtosis	http://en.wikipedia.org/wiki/Kurtosis
	[Skewness]				FLOAT					NULL,											--	Skewness (negative leans right)		http://en.wikipedia.org/wiki/Skewness

	CHECK([xMin]				<= [xMean]),
	CHECK([xMean]				<= [xMax]),
	CHECK([xMin]				<= [LimitLower]),
	CHECK([xMax]				>= [LimitUpper]),

	CHECK([ConfidenceInterval]	<= [PredictionInterval]),

	CHECK([BoxLowerExtreme]		<= [BoxLowerWhisker]),
	CHECK([BoxLowerWhisker]		<= [BoxLower]),
	CHECK([BoxLower]			<= [BoxMedian]),
	CHECK([BoxMedian]			<= [BoxUpper]),
	CHECK([BoxUpper]			<= [BoxUpperWhisker]),
	CHECK([BoxUpperWhisker]		<= [BoxUpperExtreme]),

	PRIMARY KEY CLUSTERED([qTile] ASC)
);