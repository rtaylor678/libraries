﻿CREATE TYPE [stat].[ResultsObservationsSmoothed] AS TABLE
(
	[xPlot]					FLOAT				NOT	NULL,

	[yPlot]					FLOAT					NULL,
	[yInterpolated]			FLOAT					NULL,

	[RowId]					INT						NULL,
	[yActual]				FLOAT					NULL,
	[yFunction]				FLOAT					NULL,

	PRIMARY KEY CLUSTERED ([xPlot] ASC)
);