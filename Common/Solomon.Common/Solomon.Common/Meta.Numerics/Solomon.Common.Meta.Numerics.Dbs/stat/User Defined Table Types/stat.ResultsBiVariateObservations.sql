﻿CREATE TYPE [stat].[ResultsBiVariateObservations] AS TABLE
(
	[Relationship]			CHAR(1)				NOT	NULL,

	[Id]					INT					NOT	NULL,
	[RowId]					INT					NOT	NULL,
	[x]						FLOAT				NOT	NULL,	--	Independant variable
	[y]						FLOAT				NOT	NULL,	--	Dependant variable

	[yQuartile]				TINYINT				NOT	NULL	CHECK([yQuartile] >= 1),				--	Quartile value						http://en.wikipedia.org/wiki/Quantile
	[yPercentile]			FLOAT				NOT	NULL,
	[yPercentRank]			FLOAT				NOT	NULL	CHECK(ROUND([yPercentRank], 5)	BETWEEN 0.0 AND 100.0),	--	Percentile			https://en.wikipedia.org/wiki/Percentile_rank
	[yCumulative]			FLOAT				NOT	NULL	CHECK(ROUND([yCumulative], 5)	BETWEEN 0.0 AND 100.0),	--	Cumulative			https://en.wikipedia.org/wiki/Cumulative_distribution_function
	[yRank]					INT					NOT	NULL	CHECK([yRank]		>= 1),				--	Value Ranking						http://msdn.microsoft.com/en-us/library/ms176102.aspx
	[yRankDense]			INT					NOT	NULL	CHECK([yRankDense]	>= 1),				--	Value Ranking (Dense)				http://msdn.microsoft.com/en-us/library/ms173825.aspx

	[yEstimate]				FLOAT				NOT	NULL,

	[zScore]				FLOAT				NOT	NULL,											--	Distance Away From Mean				http://en.wikipedia.org/wiki/Z-test
	[zQuartile]				TINYINT					NULL	CHECK([zQuartile] >= 1),				--	Quartile for Z-Test
	[zPercentile]			FLOAT				NOT	NULL	CHECK(ROUND([zPercentile], 5)	BETWEEN 0.0 AND 100.0),	--	Percentile			http://en.wikipedia.org/wiki/Percentile
	[zHypothesis]			INT					NOT	NULL,											--	Null Hypothesis 0 (Rejection), 1 (Acceptance)

	[ErrorAbsolute]			FLOAT				NOT	NULL,											--	Absolute approximation error		http://en.wikipedia.org/wiki/Approximation_error
	[ErrorRelative]			FLOAT				NOT	NULL,											--	Relative approximation error		http://en.wikipedia.org/wiki/Approximation_error

	[ResidualStandard]		FLOAT					NULL,											--	Standard residual error				http://en.wikipedia.org/wiki/Errors_and_residuals_in_statistics
	[ResidualAbsolute]		FLOAT				NOT	NULL,
	[ResidualRelative]		FLOAT				NOT	NULL,

	[PlotY1]				FLOAT					NULL,
	[PlotY2]				FLOAT					NULL,
	[PlotY3]				FLOAT					NULL,
	[PlotY4]				FLOAT					NULL,

	[PlotZ1]				FLOAT					NULL,
	[PlotZ2]				FLOAT					NULL,
	[PlotZ3]				FLOAT					NULL,
	[PlotZ4]				FLOAT					NULL,

	[ResidualThreshold]		FLOAT				NOT	NULL	CHECK([ResidualThreshold] > 0.0) ,
	[ResidualStdIn]			FLOAT					NULL,
	[ResidualStdOut]		FLOAT					NULL,

	[ConfidenceInterval]	FLOAT				NOT	NULL	CHECK([ConfidenceInterval]	>= 0.0),	--	Confidence Interval					http://en.wikipedia.org/wiki/Confidence_interval
	[PredictionInterval]	FLOAT				NOT	NULL	CHECK([PredictionInterval]	>= 0.0),	--	Prediction Interval					http://en.wikipedia.org/wiki/Prediction_interval

	[PredictionLower]		FLOAT				NOT	NULL,
	[ConfidenceLower]		FLOAT				NOT	NULL,
	[ConfidenceUpper]		FLOAT				NOT	NULL,
	[PredictionUpper]		FLOAT				NOT	NULL,

	[ConfidenceIn]			FLOAT					NULL,
	[ConfidenceOut]			FLOAT					NULL,
	[PredictionIn]			FLOAT					NULL,
	[PredictionOut]			FLOAT					NULL,

	[PearsonThreshold]		FLOAT				NOT	NULL	CHECK(ROUND([PearsonThreshold], 5) BETWEEN 0.0 AND 1.0),
	[PearsonIn]				FLOAT					NULL,
	[PearsonOut]			FLOAT					NULL,

	[xAsCeiling]			INT						NULL,
	[CeilingBreach]			FLOAT					NULL,

	[SuspectWarning]		FLOAT					NULL,
	[SuspectCritical]		FLOAT					NULL,

	[b0]					FLOAT				NOT	NULL,
	[b1]					FLOAT				NOT	NULL,
	[Confidence_Pcnt]		FLOAT				NOT	NULL,

	PRIMARY KEY CLUSTERED([x] ASC, [y] ASC, [Id] ASC)
);