﻿CREATE TYPE [stat].[ResultsDescriptiveGeomHarm] AS TABLE
(
	[xCount]					INT					NOT	NULL	CHECK([xCount]	>= 1),				--	Item Count
	[GeometricMean]				FLOAT				NOT	NULL,										--	Geometric Mean						http://en.wikipedia.org/wiki/Geometric_mean
	[GeometricMeanWeighted]		FLOAT					NULL,										--	Weighted Geometric Mean				http://en.wikipedia.org/wiki/Weighted_geometric_mean
	[GeometricStDevP]			FLOAT				NOT	NULL	CHECK([GeometricStDevP]	>= 0.0),	--	Geometirc Standard Deviation		http://en.wikipedia.org/wiki/Geometric_standard_deviation

	[HarmonicMean]				FLOAT				NOT	NULL,										--	Harmonic Mean						http://en.wikipedia.org/wiki/Harmonic_mean
	[HarmonicMeanWeighted]		FLOAT					NULL										--	Weighted Harmonic Mean				http://en.wikipedia.org/wiki/Harmonic_mean#Weighted_harmonic_mean
);