﻿CREATE TYPE [stat].[ResultsBiVariateDescriptive] AS TABLE
(
	[Relationship]			CHAR(1),
	[b0]					FLOAT,
	[b1]					FLOAT,

	[SSE]					FLOAT,
	[SSR]					FLOAT,
	[SST]					FLOAT,
	[MSE]					FLOAT,
	[MSR]					FLOAT,
	[COV]					FLOAT,
	[COD]					FLOAT,
	[COR]					FLOAT,

	[SumXiX]				FLOAT,
	[b1StdError]			FLOAT,
	[StdAway]				FLOAT,

	[StudentTStatistic]		FLOAT,
	[StudentTTest]			FLOAT,
	[StudentTConfidence]	FLOAT,
	[StudentTHypothesis]	FLOAT,
	[StudentTIsDependent]	INT,

	[FisherStatistic]		FLOAT,
	[FisherTest]			FLOAT,
	[FisherConfidence]		FLOAT,
	[FisherHypothesis]		FLOAT,
	[FisherIsDependent]		INT,

	[ChiStatistic]			FLOAT,
	[ChiTest]				FLOAT,
	[ChiConfidence]			FLOAT,
	[ChiHypothesis]			FLOAT,
	[ChiIsDependent]		INT,

	[Rho]					FLOAT,
	[Tau]					FLOAT
);