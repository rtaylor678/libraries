﻿CREATE TYPE [stat].[SampleRandomVar] AS TABLE
(
	[Id]				INT					NOT	NULL	IDENTITY(1, 1),
	[RowId]				INT					NOT	NULL,

	[x]					FLOAT				NOT	NULL,	--	Independant variable
	[w]					FLOAT					NULL,	--	Weighting factor

	PRIMARY KEY CLUSTERED([x] ASC, [Id] ASC),
	UNIQUE NONCLUSTERED([x] DESC, [Id] ASC)
);