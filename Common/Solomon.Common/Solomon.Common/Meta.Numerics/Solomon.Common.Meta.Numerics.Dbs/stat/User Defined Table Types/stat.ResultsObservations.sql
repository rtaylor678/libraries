﻿CREATE TYPE [stat].[ResultsObservations] AS TABLE
(
	[Id]					INT					NOT	NULL,
	[RowId]					INT					NOT	NULL,
	[x]						FLOAT				NOT	NULL,											--	Raw value
	[w]						FLOAT					NULL,

	[xQuartile]				TINYINT				NOT	NULL	CHECK([xQuartile] >= 1),				--	Quartile value						http://en.wikipedia.org/wiki/Quantile
	[xPercentile]			FLOAT				NOT	NULL	CHECK(ROUND([xPercentile], 5)	BETWEEN 0.0 AND 100.0),	--	Item-tile			http://en.wikipedia.org/wiki/Percentile
	[xPercentRank]			FLOAT				NOT	NULL	CHECK(ROUND([xPercentRank], 5)	BETWEEN 0.0 AND 100.0),	--	Percentile			https://en.wikipedia.org/wiki/Percentile_rank
	[xCumulative]			FLOAT				NOT	NULL	CHECK(ROUND([xCumulative], 5)	BETWEEN 0.0 AND 100.0),	--	Cumulative			https://en.wikipedia.org/wiki/Cumulative_distribution_function
	[xRank]					INT					NOT	NULL	CHECK([xRank]		>= 1),				--	Value Ranking						http://msdn.microsoft.com/en-us/library/ms176102.aspx
	[xRankDense]			INT					NOT	NULL	CHECK([xRankDense]	>= 1),				--	Value Ranking (Dense)				http://msdn.microsoft.com/en-us/library/ms173825.aspx

	[wPercentile]			FLOAT					NULL	CHECK(ROUND([wPercentile], 5)	BETWEEN 0.0 AND 100.0),	--	Item-tile			http://en.wikipedia.org/wiki/Percentile

	[zScore]				FLOAT				NOT	NULL,											--	Distance Away From Mean				http://en.wikipedia.org/wiki/Z-test
	[zQuartile]				TINYINT					NULL	CHECK([zQuartile] >= 1),				--	Quartile for Z-Test
	[zPercentile]			FLOAT				NOT	NULL	CHECK(ROUND([zPercentile], 5)	BETWEEN 0.0 AND 100.0),	--	Percentile			http://en.wikipedia.org/wiki/Percentile
	[zHypothesis]			INT					NOT	NULL,											--	Null Hypothesis 0 (Rejection), 1 (Acceptance)

	[ErrorAbsolute]			FLOAT				NOT	NULL,											--	Absolute approximation error		http://en.wikipedia.org/wiki/Approximation_error
	[ErrorRelative]			FLOAT					NULL,											--	Realitve approximation error		http://en.wikipedia.org/wiki/Approximation_error

	[ResidualStandard]		FLOAT					NULL,											--	Standard residual error				http://en.wikipedia.org/wiki/Errors_and_residuals_in_statistics
	[ResidualAbsolute]		FLOAT				NOT	NULL,
	[ResidualRelative]		FLOAT				NOT	NULL,

	[PlotQ1]				FLOAT					NULL,
	[PlotQ2]				FLOAT					NULL,
	[PlotQ3]				FLOAT					NULL,
	[PlotQ4]				FLOAT					NULL,

	[PlotZ1]				FLOAT					NULL,
	[PlotZ2]				FLOAT					NULL,
	[PlotZ3]				FLOAT					NULL,
	[PlotZ4]				FLOAT					NULL,

	[ResidualThreshold]		FLOAT				NOT	NULL	CHECK([ResidualThreshold] > 0.0) ,
	[ResidualStdIn]			FLOAT					NULL,
	[ResidualStdOut]		FLOAT					NULL,

	[xMean]					FLOAT				NOT	NULL,											--	Mean								http://en.wikipedia.org/wiki/Mean

	[ConfidenceInterval]	FLOAT				NOT	NULL	CHECK([ConfidenceInterval]	>= 0.0),	--	Confidence Interval					http://en.wikipedia.org/wiki/Confidence_interval
	[PredictionInterval]	FLOAT				NOT	NULL	CHECK([PredictionInterval]	>= 0.0),	--	Prediction Interval					http://en.wikipedia.org/wiki/Prediction_interval

	[PredictionLower]		FLOAT				NOT	NULL,
	[ConfidenceLower]		FLOAT				NOT	NULL,
	[ConfidenceUpper]		FLOAT				NOT	NULL,
	[PredictionUpper]		FLOAT				NOT	NULL,

	[ConfidenceIn]			FLOAT					NULL,
	[ConfidenceOut]			FLOAT					NULL,
	[PredictionIn]			FLOAT					NULL,
	[PredictionOut]			FLOAT					NULL,

	[PearsonThreshold]		FLOAT				NOT	NULL	CHECK(ROUND([PearsonThreshold], 5) BETWEEN 0.0 AND 1.0),
	[PearsonIn]				FLOAT					NULL,
	[PearsonOut]			FLOAT					NULL,

	CHECK ([ConfidenceInterval] <= [PredictionInterval]),
	PRIMARY KEY CLUSTERED ([x] ASC, [Id] ASC)
);