﻿CREATE TYPE [stat].[SampleBiVariate] AS TABLE
(
	[Id]				INT					NOT	NULL	IDENTITY(1, 1),
	[RowId]				INT					NOT	NULL,

	[x]					FLOAT				NOT	NULL,	--	Independant variable
	[y]					FLOAT				NOT	NULL,	--	Dependant variable

	PRIMARY KEY CLUSTERED([x] ASC, [y] ASC, [Id] ASC)
);