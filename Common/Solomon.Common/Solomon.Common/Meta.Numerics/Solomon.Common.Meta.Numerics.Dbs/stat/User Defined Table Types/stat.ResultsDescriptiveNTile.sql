﻿CREATE TYPE [stat].[ResultsDescriptiveNTile] AS TABLE
(
	[qTile]					TINYINT				NOT	NULL	CHECK([qTile]	>= 1),					--	Quartile value
	[SampleType]			VARCHAR(10)			NOT	NULL,

	[xCount]				INT					NOT	NULL	CHECK([xCount]	>= 1),					--	Item Count
	[xMin]					FLOAT				NOT	NULL,											--	Minimum
	[xMax]					FLOAT				NOT	NULL,											--	Maximum
	[xRange]				FLOAT				NOT	NULL,

	[BreakPrevious]			FLOAT					NULL,
	[BreakNext]				FLOAT					NULL,

	[xMean]					FLOAT				NOT	NULL,											--	Mean								http://en.wikipedia.org/wiki/Mean
	[xStDev]				FLOAT				NOT	NULL	CHECK([xStDev]	>= 0.0),				--	Standard Deviation			σ		http://en.wikipedia.org/wiki/Standard_deviation
	[xVar]					FLOAT				NOT	NULL	CHECK([xVar]	>= 0.0),				--	Variance					σ²		http://en.wikipedia.org/wiki/Variance

	[LimitLower]			FLOAT				NOT	NULL,											--	Minimum (Mean of smallest two items, >= @CapMin)
	[LimitUpper]			FLOAT				NOT	NULL,											--	Minimum (Mean of largest two items, <= @CapMax)
	[LimitRange]			FLOAT				NOT	NULL,

	CHECK([xMin]				<= [xMean]),
	CHECK([xMean]				<= [xMax]),
	CHECK([xMin]				<= [LimitLower]),
	CHECK([xMax]				>= [LimitUpper]),

	PRIMARY KEY CLUSTERED([qTile] ASC)
);