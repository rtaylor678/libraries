﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;
using System.Configuration;

namespace Solomon.Common.DocumentManagement.MFiles
{
    //this is optional, used for debugging locally, does not need to go to Production.
    public static class DevConfigHelper
    {        
        public static string ReadConfig(string key)
        {
            string result = string.Empty;
            try
            {
                string helperPath = Environment.SpecialFolder.UserProfile.ToString() + @"\DevConfigHelper.txt";
                StreamReader streamReader = new StreamReader(helperPath);
                List<string> valueslist = new List<string>();
                while(!streamReader.EndOfStream)
                {
                    valueslist.Add(streamReader.ReadLine());
                }
                streamReader.Close();
                foreach (string item in valueslist)
                {
                    string[] keyValuePair = item.Split('^'); //delimiter);
                    if(keyValuePair[0].ToUpper() == key.ToUpper())
                    {
                        result = keyValuePair[1];
                        break;
                    }
                }
                return result;
            }
            catch (Exception exRead)
            {
                try
                {
                    result = ConfigurationManager.AppSettings[key].ToString();
                }
                catch (Exception configException)
                {
                    return string.Empty;
                }
                return result;
            }

        }

        private static string TrimDirectories(string directories, int countToRemove)
        {
            string[] startPath = directories.Split('\\');
            string endPath = string.Empty;
            for(int count = 0;count < (startPath.Length- countToRemove);count++)
            {
                endPath +=startPath[count] + "\\";
            }
            return endPath.Substring(0,endPath.Length-1);
        }
    }
}
