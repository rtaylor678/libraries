﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Solomon.Common.DocumentManagement.MFiles.WebAccess;
using System.Configuration;
using System.IO;
using Solomon.Common.Utility;

namespace Solomon.Common.DocumentManagement.MFiles
{
    public class MFilesManager : IDisposable
    {
        private Api _api { get; set; }
        private string _id = string.Empty;
        private string _password = string.Empty;
        private string _vaultGuid = null;
        private string _url = string.Empty;
        
        private bool _searchDeletedFiles = false;
        private bool _disposed = false;
        private bool _loggedIn = false;
        private static Solomon.Common.Utility.Log _logger 
            = Solomon.Common.Utility.Log.Instance();

        //classes
        private const int _classDoc = 0;
        private static int _benchmarkingParticipantClassId = 52;
        private static int _benchmarkingGeneralClassValue = 99;
        
        //ValueListIds
        private static int _generalRefineryValueListId = 198;
        private static int _idrValueListId = 12;

        //PropertyDefs
        private static int _nameOrTitlePropertyDef = 0;
        private static int _benchmarkingParticipantPropertyDef = 1144;
        private static int _documentNamePropertyDef = 1178;
        private static int _documentDatePropertyDef = 1179;        
        private static int _deliverableTypePropertyDef = 1211;
        private static int _generalRefineryPropertyDefinitionId = 1327;
        private static int _drawingOrArchivePropertyDef = 1331;
        private static int _projectPropertyDef = 1142;
        private static int _refNumPropertyDef = 1172;


        public MFilesManager(string url, string vaultGuid, string id, string password,
            bool searchDeletedFiles = false)
        {
            try
            {
                //NOTE: Calling app must have reference to Log4Net in order to create _logger else will error.
                _vaultGuid = vaultGuid;
                _password = password;
                _url = url;
                _id = id;
                _searchDeletedFiles = searchDeletedFiles;
                _api = Api.Login(_url, id, password, _vaultGuid);
                _loggedIn = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void LogIn(string url, string vaultGuid, string id, string password,
            bool searchDeletedFiles = false)
        {
            try
            {
                if (!_loggedIn)
                {
                    _vaultGuid = vaultGuid;
                    _password = password;
                    _url = url;
                    _id = id;
                    _searchDeletedFiles = searchDeletedFiles;
                    _api = Api.Login(_url, id, password, _vaultGuid);
                    _loggedIn = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        

        /// <summary>
        /// This is an Unfiltered Search For all Objects in the Vault. Default max return count is 500
        /// </summary>
        /// <returns>Results<ObjectVersion></returns>
        public Results<ObjectVersion> GetObjectsUnfiltered()
        {
            Results<ObjectVersion> objects = _api.Get.Objects();
            return OnlyNondeletedObjects(objects);
        }

        /// <summary>
        /// Gets values from lookup table
        /// </summary>
        /// <param name="lookupId">lookupId is the ID of the set of lookup values you want.</param>
        /// <returns>Results<ValueListItem></returns>
        public Results<ValueListItem> GetSomeValueListItems(int lookupId)
        {
            Results<ValueListItem> valueListItems = _api.Structure.ValueListItems(lookupId);
            return valueListItems;
        }

        public Results<ValueListItem> GetSomeValueListItems(int lookupId, int? propertydef = null, int? pN = null,
            int? parentItem = null, int? ownerItem = null, string name = null, int? page = null, int? pagesize = null)
        {
            Results<ValueListItem> valueListItems = _api.Structure.ValueListItems(lookupId, propertydef, pN, parentItem, 
                ownerItem, name, page, pagesize);
            return valueListItems;
        }

        public ObjType[] GetAllObjTypeItems()
        {
            ObjType[] objyTYpeItems = _api.Structure.ObjectTypes();
            return objyTYpeItems;
        }

        /// <summary>
        /// Gets ID of Benchmarking Participant using refnum
        /// </summary>
        /// <param name="longRefNum">Sample: 2015PCH998</param>
        /// <returns>int ID</returns>
        public int GetBenchmarkingParticipantId(string longRefNum)
        {
            int id = -1;
            Results<ObjectVersion> benchmarkingParticipants = GetObjectsByPropertyValue(_refNumPropertyDef, "=", longRefNum);
            if (benchmarkingParticipants.Items.Count() == 1)
            {
                id = benchmarkingParticipants.Items[0].ObjVer.ID;
            }

            /* old way which no longer works
            Results<ValueListItem> valueListItems = _api.Structure.ValueListItems(163);
            //Results<ValueListItem> valueListItems = _api.Structure.ValueListItems(163,null,null,null,null,"PCH134");
            int count = 1;
            foreach (ValueListItem listItem in valueListItems.Items)
            {
                if (longRefNum.ToUpper() == listItem.DisplayID.ToUpper())
                {
                    id = listItem.ID;
                    break;
                }
            }
            */
            return id;
        }

        /// <summary>
        /// Gets ID of Benchmarking Participant using UNIQUE name or title
        /// </summary>
        /// <param name="title">Sample: "Power10Co-NATURKRAFT - NATURKRAFT (Company)"</param>
        /// <returns>int ID</returns>
        public int GetBenchmarkingParticipantIdByUniqueNameOrTitle(string title)
        {
            int id = -1;
            Results<ObjectVersion> benchmarkingParticipants = GetObjectsByPropertyValue(_nameOrTitlePropertyDef, "=", title);
            if (benchmarkingParticipants.Items.Count() == 1)
            {
                id = benchmarkingParticipants.Items[0].ObjVer.ID;
            }
            return id;
        }

        /// <summary>
        /// This returns a Specific Object from the Vault... Excluding the Properties
        /// </summary>
        /// <param name="docId">The id of the doc</param>
        /// <returns>ExtendedObjectVersion</returns>
        public ExtendedObjectVersion GetSpecificObjectExclude(int docId)
        {
            ExtendedObjectVersion result = _api.Get.ObjectVersion(_classDoc, docId, -1, false);
            return result;
        }

        public ExtendedObjectVersion GetSpecificObjectInclude(int docId, bool includeExtendedProperties)
        {
            ExtendedObjectVersion result = _api.Get.ObjectVersion(_classDoc, docId, -1, includeExtendedProperties);
            return result;
        }

        public ObjectVersion GetObjectById(int docId)
        {
            ObjectVersion result = _api.Get.Object(_classDoc, docId, -1);
            return result.Deleted == false ? result : null;
        }

        /// <summary>
        /// Returns the History of an Object as an ObjectVersion []
        /// </summary>
        /// <param name="docId">The id of the doc</</param>
        /// <returns>ObjectVersion[]</returns>
        public ObjectVersion[] GetObjectHistory(int docId)
        {
            ObjectVersion[] result = _api.Get.ObjectHistory(_classDoc, docId);
            return OnlyNondeletedObjects(result);
        }

        /// <summary>
        /// This test gets the Checked Out Status of an Object
        /// </summary>
        /// <param name="docId">The id of the doc</param>
        /// <param name="docVersion">The version number of the doc</param>
        /// <returns>MFCheckOutStatus</returns>
        public MFCheckOutStatus GetCheckedOutStatus(int docId, int docVersion)
        {
            MFCheckOutStatus result = _api.Get.CheckedOutStatus(_classDoc, docId, docVersion);
            return result;
        }

        /// <summary>
        /// Returns the Title of an Object
        /// </summary>
        /// <param name="docId">The id of the doc</param>
        /// <param name="docVersion"> version number of the doc</param>
        /// <returns>string</returns>
        public string GetObjectTitle(int docId, int docVersion)
        {
            return _api.Get.ObjectTitle(_classDoc, docId, docVersion);
        }

        /// <summary>
        /// Get the Properties of an Object as a PropertyValue []
        /// </summary>
        /// <param name="docId">The id of the doc</param>
        /// <returns>IList<PropertyValue></returns>
        public IList<PropertyValue> GetObjectProperties(int docId)
        {
            PropertyValue[] result = _api.Get.Properties(_api.CreateInstance.ObjVer(_classDoc, docId));
            //result.ToList().ForEach(p => Console.WriteLine("{0} => {1}", p.PropertyDef, p.TypedValue.DisplayValue));
            return result.ToList();
        }

        /// <summary>
        /// Downloads a file to the C:\Temp folder
        /// </summary>
        /// <param name="propertyDefinitionId">ID of the desired property definition</param>
        /// <param name="searchOperator">i.e. "=" (equals), "*=" (contains), etc.</param>
        /// <param name="propertyValue">Value to match in the search</param>
        /// <returns>bool</returns>
        public bool DownloadFileToTempFolder(int propertyDefinitionId, string searchOperator, string propertyValue)
        {
            try
            {
                // Define search parameters. (Ex: Search for objects whose name is "test".)
                //propertyDefinitionId = 0; // ID of the desired property definition ('Name or title' in this case.)
                //searchOperator = "="; // where Name EQUALS property value. Refer to http://www.m-files.com/mfws/syntax.html#sect:search-encoding for more operators.
                //propertyValue = "Issues with Live M-Files for Accounting"; // Search for "test" as the property value.

                Results<ObjectVersion> objects = _api.Client.Get<Results<ObjectVersion>>("objects.aspx?p" +
                    propertyDefinitionId.ToString() + searchOperator + propertyValue);
                objects = OnlyNondeletedObjects(objects);
                if (objects == null || objects.Items == null || objects.Items.Count() < 1)
                    return true;

                ObjectVersion objectVersion = objects.Items[0];

                ObjectFile file = _api.Get.Files(objectVersion.ObjVer).SingleOrDefault();

                Stream objectFilestream = _api.Client.Get<Stream>("objects/" + objectVersion.ObjVer.Type + "/"
                    + objectVersion.ObjVer.ID + "/" + objectVersion.ObjVer.Version + "/files/" + file.ID
                    + "/content?extensions=mfwa&active-vault=" + _vaultGuid);

                using (var fileStream = File.Create("C:\\temp\\" + file.Name + "." + file.Extension))
                {
                    objectFilestream.CopyTo(fileStream);
                }
                return true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return false;
            }
        }

        public bool DownloadFile(string folderPath, int docId, bool deleteExisting = true)
        {
            try
            {
                ObjectVersion objectVersion = GetObjectById(docId);
                if (objectVersion.Deleted == true)
                    return true;

                ObjectFile file = _api.Get.Files(objectVersion.ObjVer).SingleOrDefault();
                Stream objectFilestream = _api.Client.Get<Stream>("objects/" + objectVersion.ObjVer.Type + "/"
                    + objectVersion.ObjVer.ID + "/" + objectVersion.ObjVer.Version + "/files/" + file.ID
                    + "/content?extensions=mfwa&active-vault=" + _vaultGuid);
                if (!folderPath.EndsWith(@"\"))
                {
                    folderPath += @"\";
                }
                string fullPath = folderPath + file.Name + "." + file.Extension;
                if (File.Exists(fullPath) && deleteExisting)
                {
                    File.Delete(fullPath);
                }
                using (var fileStream = File.Create(fullPath))
                {

                    objectFilestream.CopyTo(fileStream);
                }
                return true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return false;
            }
        }

        public bool DownloadFileToTempFolder(int docId)
        {
            try
            {
                // Define search parameters. (Ex: Search for objects whose name is "test".)
                //propertyDefinitionId = 0; // ID of the desired property definition ('Name or title' in this case.)
                //searchOperator = "="; // where Name EQUALS property value. Refer to http://www.m-files.com/mfws/syntax.html#sect:search-encoding for more operators.
                //propertyValue = "Issues with Live M-Files for Accounting"; // Search for "test" as the property value.
                ObjectVersion objectVersion = GetObjectById(docId);
                if (objectVersion.Deleted == true)
                    return true;

                ObjectFile file = _api.Get.Files(objectVersion.ObjVer).SingleOrDefault();
                Stream objectFilestream = _api.Client.Get<Stream>("objects/" + objectVersion.ObjVer.Type + "/"
                    + objectVersion.ObjVer.ID + "/" + objectVersion.ObjVer.Version + "/files/" + file.ID
                    + "/content?extensions=mfwa&active-vault=" + _vaultGuid);

                using (var fileStream = File.Create("C:\\temp\\" + file.Name + "." + file.Extension))
                {
                    objectFilestream.CopyTo(fileStream);
                }
                return true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return false;
            }
        }

        public bool DownloadFileToThisFolder(int docId, string pathToFolder)
        {
            try
            {
                // Define search parameters. (Ex: Search for objects whose name is "test".)
                //propertyDefinitionId = 0; // ID of the desired property definition ('Name or title' in this case.)
                //searchOperator = "="; // where Name EQUALS property value. Refer to http://www.m-files.com/mfws/syntax.html#sect:search-encoding for more operators.
                //propertyValue = "Issues with Live M-Files for Accounting"; // Search for "test" as the property value.
                ObjectVersion objectVersion = GetObjectById(docId);
                if (objectVersion.Deleted == true)
                    return true;

                ObjectFile file = _api.Get.Files(objectVersion.ObjVer).SingleOrDefault();
                Stream objectFilestream = _api.Client.Get<Stream>("objects/" + objectVersion.ObjVer.Type + "/"
                    + objectVersion.ObjVer.ID + "/" + objectVersion.ObjVer.Version + "/files/" + file.ID
                    + "/content?extensions=mfwa&active-vault=" + _vaultGuid);

                using (var fileStream = File.Create(pathToFolder = "\\" + file.Name + "." + file.Extension))
                {
                    objectFilestream.CopyTo(fileStream);
                }
                return true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return false;
            }
        }

        /// <summary>
        /// Uploads an already-existing file into MFiles
        /// </summary>
        /// <param name="fullPath">Path including filename and extension of file to upload.</param>
        /// <returns>ObjectVersion</returns>
        public ObjectVersion UploadNewVersionOfDoc(string fullPath, int docId)
        {
            ObjectVersion objectVersion = _api.Get.Object(_classDoc, docId); //, -1);
            ObjectFile file = _api.Get.Files(objectVersion.ObjVer).SingleOrDefault();
            string requestURL = "objects/" + objectVersion.ObjVer.Type.ToString() + "/" + objectVersion.ObjVer.ID.ToString() + "/" + "latest" + "/files/" + file.ID.ToString() + "/content";
            string filepath = fullPath;

            //   FileStream filestream = new FileStream(filepath, FileMode.Open);
            using (FileStream filestream = new FileStream(filepath, FileMode.Open))
            {             
                _api.CheckOut(objectVersion.ObjVer);
                _api.Client.Put<ObjectVersion>(requestURL, filestream);
                _api.CheckIn(objectVersion.ObjVer);
            }            
            return objectVersion;
        }

        /// <summary>
        /// Upload a new doc of type Benchmarking Participant, and set its object class
        /// </summary>
        /// <param name="fullPath">Path to file including filename</param>
        /// <param name="docClass">0 is unclassified, 1 is other, corresp is 40</param>
        /// <param name="deliverableType">Deliverable Type property ID</param>
        /// <param name="benchmarkingParticipant">Benchmarking Participant property ID</param>
        /// <param name="documentDate">Date of the document</param>
        /// <param name="fileName">Optional - use this to show a different filename in MFiles than the name of the file in Windows</param>
        /// <returns></returns>
        //DEPRECATED
        //public ObjectVersion UploadNewDoc(string fullPath, int docClass, int deliverableType,
        //    int benchmarkingParticipant, DateTime? documentDate, string fileName = "")
        //{
        //    try
        //    {
        //        string[] pathArray = fullPath.Split('\\');
        //        string fileNameFromFullPath = pathArray[pathArray.Length - 1];
        //        string[] fileArray = fileNameFromFullPath.Split('.');
        //        string fileExt = fileArray[fileArray.Length - 1];
        //        string fileNameNoExt = string.Empty;
        //        for (int count = 0; count < fileArray.Length - 1; count++)
        //        {
        //            fileNameNoExt += fileArray[count] + ".";
        //        }
        //        fileNameNoExt=fileNameNoExt.Remove(fileNameNoExt.Length - 1);
        //        /*
        //        if (fileName != null)
        //            fileNameFromFullPath = fileName.ToString();
        //        */
        //        DateTime newDocumentDate = DateTime.Today;
        //        if (documentDate != null)
        //            newDocumentDate = (DateTime)documentDate;

        //        string fileNameToUse = fileName != "" ? fileName : fileNameNoExt;

        //        PropertyValue[] PropertyValues = new[]
        //        {
        //            //new PropertyValue   //Name-or-Title
        //            //{
        //            //    PropertyDef = 0,
        //            //    TypedValue = new TypedValue { DataType = MFDataType.Text, Value = fileNameToUse }
        //            //},
        //            new PropertyValue   //Document Name 
        //            {
        //                PropertyDef = _documentNamePropertyDef ,
        //                TypedValue = new TypedValue { DataType = MFDataType.Text, Value = fileNameToUse }
        //            },
        //            new PropertyValue
        //            {
        //                PropertyDef = _deliverableTypePropertyDef , //deliverabletype
        //                TypedValue = new TypedValue
        //                {
        //                    DataType = MFDataType.Lookup,
        //                    Lookup = new Lookup { Item = deliverableType , Version = -1 }
        //                }
        //            },
        //            new PropertyValue   //benchmarking participant
        //            {
        //                PropertyDef = _benchmarkingParticipantPropertyDef ,
        //                TypedValue = new TypedValue
        //                {
        //                    DataType =MFDataType.MultiSelectLookup, 
        //                    Lookups = new Lookup[]{ new Lookup { Item = benchmarkingParticipant , Version = -1 }}
        //                }
        //            },
        //            new PropertyValue  
        //            {
        //                PropertyDef = _documentDatePropertyDef ,
        //                TypedValue = new TypedValue
        //                {
        //                    DataType = MFDataType.Date,
        //                    Value= newDocumentDate  
        //                }
        //            }
        //        };
        //        //Orig: return _api.Create.CreateSingleFileObject(0, docClass, fullPath, PropertyValues);
        //        return _api.Create.CreateSingleFileObjectClassVersionMinus1(_classDoc, docClass, fullPath, PropertyValues);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        /// <summary>
        /// Upload a new doc of type Benchmarking Participant, and set its object class
        /// </summary>
        /// <param name="fullPath">Path to file including filename</param>
        /// <param name="docClass">0 is unclassified, 1 is other, corresp is 40</param>
        /// <param name="deliverableType">Deliverable Type property ID</param>
        /// <param name="benchmarkingParticipant">Benchmarking Participant property ID</param>
        /// <param name="documentDate">Date of the document</param>
        /// <param name="project">Id of project to use</param>
        /// <param name="fileName">Optional - use this to show a different filename in MFiles than the name of the file in Windows</param>
        /// <returns></returns>
        public ObjectVersion UploadNewDoc(string fullPath, int docClass, int deliverableType,
            int benchmarkingParticipant, DateTime? documentDate, int project, string fileName = "")
        {
            try
            {
                string[] pathArray = fullPath.Split('\\');
                string fileNameFromFullPath = pathArray[pathArray.Length - 1];
                string[] fileArray = fileNameFromFullPath.Split('.');
                string fileExt = fileArray[fileArray.Length - 1];
                string fileNameNoExt = string.Empty;
                for (int count = 0; count < fileArray.Length - 1; count++)
                {
                    fileNameNoExt += fileArray[count] + ".";
                }
                fileNameNoExt = fileNameNoExt.Remove(fileNameNoExt.Length - 1);
                /*
                if (fileName != null)
                    fileNameFromFullPath = fileName.ToString();
                */
                DateTime newDocumentDate = DateTime.Today;
                if (documentDate != null)
                    newDocumentDate = (DateTime)documentDate;

                string fileNameToUse = fileName != "" ? fileName : fileNameNoExt;

                PropertyValue[] PropertyValues = new[]
                {
                    //new PropertyValue   //Name-or-Title
                    //{
                    //    PropertyDef = 0,
                    //    TypedValue = new TypedValue { DataType = MFDataType.Text, Value = fileNameToUse }
                    //},
                    new PropertyValue   //Document Name 
                    {
                        PropertyDef = _documentNamePropertyDef ,
                        TypedValue = new TypedValue { DataType = MFDataType.Text, Value = fileNameToUse }
                    },
                    new PropertyValue
                    {
                        PropertyDef = _deliverableTypePropertyDef , //deliverabletype
                        TypedValue = new TypedValue
                        {
                            DataType = MFDataType.Lookup,
                            Lookup = new Lookup { Item = deliverableType , Version = -1 }
                        }
                    },
                    new PropertyValue   //benchmarking participant
                    {
                        PropertyDef = _benchmarkingParticipantPropertyDef ,
                        TypedValue = new TypedValue
                        {
                            DataType =MFDataType.MultiSelectLookup, 
                            Lookups = new Lookup[]{ new Lookup { Item = benchmarkingParticipant , Version = -1 }}
                        }
                    },
                    new PropertyValue   //project
                    {
                        PropertyDef = _projectPropertyDef,
                        TypedValue = new TypedValue
                        {
                            DataType =MFDataType.MultiSelectLookup, 
                            Lookups = new Lookup[]{ new Lookup { Item = project , Version = -1 }}
                        }
                    },
                    new PropertyValue  
                    {
                        PropertyDef = _documentDatePropertyDef ,
                        TypedValue = new TypedValue
                        {
                            DataType = MFDataType.Date,
                            Value= newDocumentDate  
                        }
                    }
                };
                //Orig: return _api.Create.CreateSingleFileObject(0, docClass, fullPath, PropertyValues);
                return _api.Create.CreateSingleFileObjectClassVersionMinus1(_classDoc, docClass, fullPath, PropertyValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ObjectVersion UploadNewIDR(string fullPath, string longRefNum,
            DateTime? documentDate, int project, string fileNameWithoutExt = "")
        {
            try
            {
                string[] pathArray = fullPath.Split('\\');
                string fileNameAndExtFromFullPath = pathArray[pathArray.Length - 1];
                string[] fileNameAndExtFromFullPathParts = fileNameAndExtFromFullPath.Split('.');
                string fileNameWithoutExtFromFullPath = string.Empty;
                for(int i=0; i<fileNameAndExtFromFullPathParts.Length-1;i++)
                {
                    fileNameWithoutExtFromFullPath += fileNameAndExtFromFullPathParts[i] + ".";
                }
                fileNameWithoutExtFromFullPath = fileNameWithoutExtFromFullPath.Remove(fileNameWithoutExtFromFullPath.Length - 1); //trim trailing '.'
                string[] fileArray = fileNameAndExtFromFullPath.Split('.');
                string fileExt = fileArray[fileArray.Length - 1];
                if (fileNameWithoutExt != null && fileNameWithoutExt.Length > 0)
                    fileNameWithoutExtFromFullPath = fileNameWithoutExt.ToString();

                int participant = GetBenchmarkingParticipantId(longRefNum);
                DateTime newDocumentDate = DateTime.Today;
                if (documentDate != null)
                    newDocumentDate = (DateTime)documentDate;

                PropertyValue[] PropertyValues = new[]
                {
                    new PropertyValue   //name 
                    {
                        PropertyDef = _documentNamePropertyDef ,
                        TypedValue = new TypedValue { DataType = MFDataType.Text, Value = fileNameWithoutExtFromFullPath }
                    },
                    new PropertyValue
                    {
                        PropertyDef = _deliverableTypePropertyDef , //deliverabletype
                        TypedValue = new TypedValue
                        {
                            DataType = MFDataType.Lookup,
                            Lookup = new Lookup { Item = _idrValueListId, Version=-1 }
                        }
                    },
                    new PropertyValue   //benchmarking participant
                    {
                        PropertyDef = _benchmarkingParticipantPropertyDef ,
                        TypedValue = new TypedValue
                        {
                            DataType =MFDataType.MultiSelectLookup, 
                            Lookups = new Lookup[]{ new Lookup { Item = participant, Version=-1 }}
                        }
                    },
                    new PropertyValue   //project
                    {
                        PropertyDef = _projectPropertyDef,
                        TypedValue = new TypedValue
                        {
                            DataType =MFDataType.MultiSelectLookup, 
                            Lookups = new Lookup[]{ new Lookup { Item = project , Version = -1 }}
                        }
                    },
                    new PropertyValue  
                    {
                        PropertyDef = _documentDatePropertyDef ,
                        TypedValue = new TypedValue
                        {
                            DataType = MFDataType.Date,
                            Value= newDocumentDate  
                        }
                    }
                };
                //return _api.Create.CreateSingleFileObject(0, 52, fullPath, PropertyValues);
                return _api.Create.CreateSingleFileObjectClassVersionMinus1(_classDoc, _benchmarkingParticipantClassId, fullPath, PropertyValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ObjectVersion UploadNewGeneralRefineryItem(string fullPath, string fileNameWithoutExt, 
            string generalRefineryTextId, DateTime? documentDate, int drawingArchive, int project ) 
        {
            //if drawingArchive = 0, then put at GeneralRefinery level. If 1, put at 
            //  acrhive subfolder. If 2, put at Drawings subfolder
            try
            {
                Results<ValueListItem>  valueListItemResults
                    = GetSomeValueListItems(_generalRefineryValueListId); //198 is for all GeneralRefinery
                int generalIdPropertyValue=-1;
                foreach(ValueListItem  itm  in valueListItemResults.Items)
                {
                    if( itm.DisplayID == generalRefineryTextId)
                    {
                        generalIdPropertyValue = itm.ID;
                        break;
                    }
                }
                DateTime newDocumentDate = DateTime.Today;
                if (documentDate != null)
                    newDocumentDate = (DateTime)documentDate;

                PropertyValue[] PropertyValues = new[]
                {
                    new PropertyValue   //name 
                    {
                        PropertyDef =  _documentNamePropertyDef ,
                        TypedValue = new TypedValue { DataType = MFDataType.Text, Value = fileNameWithoutExt }
                    },
                    
                    new PropertyValue
                    {
                        PropertyDef = _generalRefineryPropertyDefinitionId , //1327, NOT  198, //GeneralRefinery
                        TypedValue = new TypedValue
                        {
                            DataType = MFDataType.MultiSelectLookup,
                            Lookups = new Lookup[]{ new Lookup { Item = generalIdPropertyValue, Version=-1 }}
                        }
                    }
                    ,new PropertyValue  
                    {
                        PropertyDef = _documentDatePropertyDef ,
                        TypedValue = new TypedValue
                        {
                            DataType = MFDataType.Date,
                            Value= newDocumentDate  
                        }
                    },
                    new PropertyValue   //project
                    {
                        PropertyDef = _projectPropertyDef,
                        TypedValue = new TypedValue
                        {
                            DataType =MFDataType.MultiSelectLookup, 
                            Lookups = new Lookup[]{ new Lookup { Item = project , Version = -1 }}
                        }
                    }                    
                };
                if (drawingArchive > 0)
                {
                    PropertyValue drawingOrArchive = new PropertyValue
                    {
                        PropertyDef = _drawingOrArchivePropertyDef,  //not 199
                        TypedValue = new TypedValue
                        {
                            DataType = MFDataType.MultiSelectLookup,
                            Lookups = new Lookup[] { new Lookup { Item = drawingArchive, Version = -1 } }
                        }
                    };
                    Array.Resize(ref PropertyValues,PropertyValues.Length+1);
                    PropertyValues[PropertyValues.Length-1] = drawingOrArchive;
                }

                return _api.Create.CreateSingleFileObjectClassVersionMinus1(_classDoc, _benchmarkingGeneralClassValue, fullPath, PropertyValues);                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Tries to log the user out of MFiles.
        /// This always fails anyway. 
        /// </summary>
        public void Logout()
        {
            _api.Logout();
            try
            {
                string result = _api.Get.Filename(_classDoc, 1, 122, 1);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }

        /// <summary>
        /// Returns the Structure - Properties (all)
        /// </summary>
        /// <returns>IList<PropertyDef></returns>
        public IList<PropertyDef> GetPropertyDefs()
        {
            PropertyDef[] result = _api.Structure.PropertyDefs();
            return result.ToList();
        }

        ///// <summary>
        ///// Gets a list of PropertyDefs for a specific doc
        ///// </summary>
        ///// <param name="docId">The id of the doc</param>
        ///// <returns>IList<PropertyDef></returns>
        //public IList<PropertyDef> GetPropertyDefs(int docId)
        //{
        //    ExtendedObjectClass ec = new ExtendedObjectClass();
        //    List<AssociatedPropertyDef> ad = ec.AssociatedPropertyDefs.ToList();
        //    foreach (AssociatedPropertyDef associatedPropertyDef in ec.AssociatedPropertyDefs)
        //    {
        //        int pd = associatedPropertyDef.PropertyDef;
        //    }
        //    return null;
        //}

        /// <summary>
        /// This is a search for objects by property value.
        /// </summary>
        /// <param name="propertyDefinitionId">ID of the desired property definition</param>
        /// <param name="searchOperator">i.e. "=" (equals), "*=" (contains), etc.</param>
        /// <param name="propertyValue">Value to match in the search</param>
        /// <returns>Results<ObjectVersion></returns>
        public Results<ObjectVersion> GetObjectsByPropertyValue(int propertyDefinitionId, string searchOperator, string propertyValue)
        {
            // Define search parameters. (Ex: Search for objects whose name is "test".)
            //propertyDefinitionId = 0; // ID of the desired property definition ('Name or title' in this case.)
            //searchOperator = "="; // where Name EQUALS property value. Refer to http://www.m-files.com/mfws/syntax.html#sect:search-encoding for more operators.
            ////string propertyValue = "test with Mike"; // Search for "test" as the property value.
            //propertyValue = "test"; // Search for "test" as the property value.

            //searchOperator = "*=";  //contains

            Results<ObjectVersion> objects =
                _api.Client.Get<Results<ObjectVersion>>("objects.aspx?p" +
                propertyDefinitionId.ToString() + searchOperator + propertyValue);
            return OnlyNondeletedObjects(objects);
        }

        public MFCheckOutStatus GetCheckedoutStatus(int docId)
        {
            return _api.Get.CheckedOutStatus(_classDoc, docId, -1);
        }

        public ObjectVersion SetCheckedoutStatus(int docId, MFCheckOutStatus status)
        {
            return _api.Set.CheckOutStatus(_classDoc, docId, -1, status);
        }

        /* THis doesn't work. Setting to Private so no-one else unwittingly tries to make it work
        private ObjectVersion SetCheckedoutStatus(ObjVer objectVersion, MFCheckOutStatus status)
        {
            return _api.Set.CheckOutStatus(objectVersion, MFCheckOutStatus.CheckedOutToMe);
        }
        */

        /// <summary>
        /// This seems to be the only way to 'Delete' a file. It appears to me that it does not really delete it, but show the file's status as deleted.
        /// </summary>
        /// <param name="fileId">ID number of the file</param>
        /// <returns></returns>
        public bool DeleteFile(int fileId)
        {
            try
            {
                // First we get the Status, for display
                bool status = _api.Get.DeletedStatus(_classDoc, 1);

                // Then we set the status
                ObjectVersion result = _api.Set.DeletedStatus(_classDoc, fileId, !status);

                // Reset Deleted Status
                //_api.Set.DeletedStatus(0, 1, status);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private Results<ObjectVersion> OnlyNondeletedObjects(Results<ObjectVersion> objectVersionList)
        {
            if ((objectVersionList == null) || (objectVersionList.Items == null) || _searchDeletedFiles)
                return objectVersionList;

            objectVersionList.Items = objectVersionList.Items.Where(x => x.Deleted != true).ToArray();
            return objectVersionList;
        }

        private ObjectVersion[] OnlyNondeletedObjects(ObjectVersion[] objectVersions)
        {
            if ((objectVersions == null) || _searchDeletedFiles)
                return objectVersions;

            objectVersions = objectVersions.Where(x => x.Deleted != true).ToArray();
            return objectVersions;
        }

        public Results<ObjectVersion> GetObjectsByMultiplePropertyValues(char delim, List<string> filters)
        {
            string combinedFilter = string.Empty;
            foreach (string filter in filters)
            {
                string[] args = filter.Split(delim);
                if(combinedFilter.Length <1)
                {
                    combinedFilter+="p";
                }
                else
                {
                        combinedFilter+="&p";
                }
                
                for(int count = 0; count < 3; count++)
                {
                    combinedFilter += args[count];
                }
            }
            //"objects.aspx?p" + "0" + "*=" + "test" + "&p" + "1144" + "=" + "5322");
                //..Results<ObjectVersion> results3 = _library..Api.Client.Get<Results<ObjectVersion>>("objects.aspx?p" + "0" + "*=" + "test" + "&p" + "1144" + "=" + "5322");
            return _api.Client.Get<Results<ObjectVersion>>("objects.aspx?" + combinedFilter);
        }

        public int GetUserId()
        {
            SessionInfo sessionInfo = _api.Get.SessionInfo;
            return sessionInfo.UserID;
        }

        void IDisposable.Dispose()
        {
            try
            {
                if (!_disposed)
                {
                    _api.Logout();                    
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            finally
            {
                _disposed = true;
            }
        }
    }
}
