﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;

namespace Solomon.Common.DocumentManagement.MFiles.WebAccess.Tests
{
    public static class DevConfigHelper
    {
        public static string ReadConfig(string key)
        {
            string result = string.Empty;
            try
            {
                //Assembly me = Assembly.GetEntryAssembly();
                Assembly me = Assembly.GetExecutingAssembly();
                string testDirectories = TrimDirectories(me.Location, 3) ;
                StreamReader streamReader = new StreamReader(@"C:\Users\sfb.DC1\DevConfigHelper.txt");
                List<string> valueslist = new List<string>();
                while(!streamReader.EndOfStream)
                {
                    valueslist.Add(streamReader.ReadLine());
                }
                streamReader.Close();
                foreach (string item in valueslist)
                {
                    char[] delim = new char[] { Convert.ToChar(14) };
                    string[] keyValuePair = item.Split(delim); //delimiter);
                    if(keyValuePair[0].ToUpper() == key.ToUpper())
                    {
                        result = keyValuePair[1];
                        break;
                    }
                }
                return result;
            }
            catch (Exception exRead)
            {
                return string.Empty;
            }

        }

        private static string TrimDirectories(string directories, int countToRemove)
        {
            string[] startPath = directories.Split('\\');
            string endPath = string.Empty;
            for(int count = 0;count < (startPath.Length- countToRemove);count++)
            {
                endPath +=startPath[count] + "\\";
            }
            return endPath.Substring(0,endPath.Length-1);
        }
    }
}
