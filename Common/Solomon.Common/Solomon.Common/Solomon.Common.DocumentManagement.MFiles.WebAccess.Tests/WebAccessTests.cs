﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Authentication = Solomon.Common.DocumentManagement.MFiles.WebAccess.Structs.Authentication;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;
using Newtonsoft.Json;
using System.Drawing;
using System.Diagnostics;
using Solomon.Common.DocumentManagement.MFiles.WebAccess;
using Solomon.Common.DocumentManagement.MFiles.WebAccess.Mfws;

namespace Solomon.Common.DocumentManagement.MFiles.WebAccess.Tests
{
    [TestClass]
    public class WebAccessTests
    {
        /// <summary>
        /// Public API Wrapper Object
        /// </summary>
        private Api Api { get; set; }

        /// <summary>
        /// Private Login to Vault Method - Update Credential Info
        /// </summary>
        private void Login()
        {
            if (Api == null)
            {
                string password = DevConfigHelper.ReadConfig("MFilesPw");
                Api = Api.Login(@"http://dallas-dms.solomononline.com/REST", "DC1\\sbard", password, "{05B803B2-09F6-4C38-8EC8-BC4659936E25}");
                //Api = Api.Login(@"http://localhost/REST", "domainName\\userName", "password", "{VAULT-GUID}" );
            }
        }

        /// <summary>
        /// This is a search for objects by property value.
        /// </summary>
        [TestMethod]
        public void _Custom_GetObjectsByPropertyValue()
        {
            Login();

            // Define search parameters. (Ex: Search for objects whose name is "test".)
            int propertyDefinitionId = 0; // ID of the desired property definition ('Name or title' in this case.)
            string searchOperator = "="; // where Name EQUALS property value. Refer to http://www.m-files.com/mfws/syntax.html#sect:search-encoding for more operators.
            string propertyValue = "test"; // Search for "test" as the property value.

            Results<ObjectVersion> objects = Api.Client.Get<Results<ObjectVersion>>("objects.aspx?p" + propertyDefinitionId.ToString() + "=" + propertyValue);

            Console.WriteLine("Result Count: " + objects.Items.Count());
        }

        /// <summary>
        /// The Initial Test, Login to the Vault
        /// </summary>
        [TestMethod]
        public void _00_LoginToVault()
        {
            Login();
            Assert.IsNotNull(Api.Client.AuthToken);
            Console.WriteLine("Authentication Key: {0}", Api.Client.AuthToken);
        }

        /// <summary>
        /// This is an Unfiltered Search For all Objects in the Vault
        /// </summary>
        [TestMethod]
        public void _01_GetObjectsUnfiltered()
        {
            Login();
            Results<ObjectVersion> objects = Api.Get.Objects();

            Console.WriteLine("Result Count: " + objects.Items.Count());
        }

        /// <summary>
        /// This is a Filtered Search by the Object Type ID
        /// </summary>
        [TestMethod]
        public void _02_GetObjectsOfType()
        {
            // Filter Type ID
            const int typeId = 0;

            Login();
            Results<ObjectVersion> objects = Api.Get.Objects(typeId);
            Assert.IsTrue(objects.Items.ToList().TrueForAll(item => item.ObjVer.Type == typeId));

            Console.WriteLine("Result Count: " + objects.Items.Count());
        }

        /// <summary>
        /// This returns a Specific Object from the Vault... Excluding the Properties
        /// </summary>
        [TestMethod]
        public void _03_GetSpecificObjectExclude()
        {
            Login();
            ExtendedObjectVersion result = Api.Get.ObjectVersion(0, 4, -1, false);
            Assert.IsNotNull(result);

            Console.WriteLine("An Object was found, but properties were not included in the request.");
        }

        /// <summary>
        /// This returns a Specific Object from the Vault... Including the Properties
        /// </summary>
        [TestMethod]
        public void _04_GetSpecificObjectInclude()
        {
            Login();
            ExtendedObjectVersion result = Api.Get.ObjectVersion(0, 1);

            Console.WriteLine("Resulting Object: " + result.Properties[0].TypedValue.DisplayValue);
        }

        /// <summary>
        /// This test checks the Deleted Status of an Object: Is Deleted = True | False
        /// </summary>
        [TestMethod]
        public void _05_GetDeletedStatus()
        {
            Login();
            bool result = Api.Get.DeletedStatus(0, 1);

            Console.WriteLine("Deleted Status => {0}", result);
        }

        /// <summary>
        /// This test Sets the Deleted Status of an Object: True | False
        /// </summary>
        [TestMethod]
        public void _06_SetDeletedStatus()
        {
            Login();
            // First we get the Status, for display
            bool status = Api.Get.DeletedStatus(0, 1);

            // Then we set the status
            ObjectVersion result = Api.Set.DeletedStatus(0, 1, !status);

            // Print the Original Status
            Console.WriteLine("{0} :: Original Deleted Status => {1}", result.Title, status);

            // Print the Current Status
            Console.WriteLine("{0} :: Updated Deleted Status => {1}", result.Title, result.Deleted);

            // Reset Deleted Status
            Api.Set.DeletedStatus(0, 1, status);
        }

        /// <summary>
        /// Returns the History of an Object as an ObjectVersion []
        /// </summary>
        [TestMethod]
        public void _07_GetObjectHistory()
        {
            Login();
            ObjectVersion[] result = Api.Get.ObjectHistory(0, 1);

            Console.WriteLine("{0} has {1} items in the history array", result.Last().Title, result.Length);
        }

        /// <summary>
        /// This test returns a Comment [] for an Object
        /// </summary>
        [TestMethod]
        public void _08_GetObjectComments()
        {
            Login();
            VersionComment[] result = Api.Get.ObjectComments(0, 1, -1);

            if (result.Length != 0)
                Console.WriteLine("Latest Comment = {0}", result.Last().Comment.TypedValue.DisplayValue);
        }

        /// <summary>
        /// This test Adds a Comment to an Object
        /// </summary>
        [TestMethod]
        public void _09_SetObjectComment()
        {
            Login();
            ExtendedObjectVersion result = Api.Set.ObjectComment(0, 1, -1, "Comment Two");

            Console.WriteLine("Latest Comment = {0}",
                result.Properties.Single(p => p.PropertyDef == 33).TypedValue.DisplayValue);
        }

        /// <summary>
        /// This test gets the Checked Out Status of an Object
        /// </summary>
        [TestMethod]
        public void _10_GetCheckedOutStatus()
        {
            Login();
            MFCheckOutStatus result = Api.Get.CheckedOutStatus(0, 1, -1);

            Console.WriteLine("Checked Out Status = {0}", result);
        }

        /// <summary>
        /// This test Sets the Checked Out Status of an Object
        /// </summary>
        [TestMethod]
        public void _11_SetCheckedOutStatus()
        {
            Login();
            // We effectively Checkout the object by setting its MFCheckOutStatus = CheckedOutToMe
            ObjectVersion result = Api.Set.CheckOutStatus(0, 1, -1, MFCheckOutStatus.CheckedOutToMe);

            Console.WriteLine("Object Updated: {0}", result.Title);
            MFCheckOutStatus currentStatus = Api.Get.CheckedOutStatus(0, 1, -1);
            Console.WriteLine("Checked Out Status = " + currentStatus);
        }

        /// <summary>
        /// This test performs an Undo-Checkout operation
        /// </summary>
        [TestMethod]
        public void _12_UndoCheckout()
        {
            Login();
            MFCheckOutStatus result = Api.Get.CheckedOutStatus(0, 1, -1);

            Console.WriteLine("Initial Checked Out Status = {0}", result);

            Console.WriteLine("Performing An Undo-Checkout...");
            Api.UndoCheckout(0, 1, -1);

            // Get the Updated Status
            result = Api.Get.CheckedOutStatus(0, 1, -1);

            Console.WriteLine("Current Checked Out Status = {0}", result);
        }

        /// <summary>
        /// This test simply returns the Title of an Object
        /// </summary>
        [TestMethod]
        public void _13_GetObjectTitle()
        {
            Login();
            string result = Api.Get.ObjectTitle(0, 1, -1);

            Console.WriteLine("Object Title = {0}", result);
        }

        /// <summary>
        /// This test simply Sets the Title of an Object
        /// </summary>
        [TestMethod]
        public void _14_SetObjectTitle()
        {
            Login();
            // Get the Title
            string result = Api.Get.ObjectTitle(0, 1, -1);
            Console.WriteLine("Initial Object Title = {0}", result);

            // Set the Title
            ObjectVersion updatedOv = Api.Set.ObjectTitle(0, 1, -1, "New " + DateTime.Now.ToLongTimeString());

            // Print the updated Title
            Console.WriteLine("Updated Title = {0}", updatedOv.Title);
        }

        /// <summary>
        /// This test simply returns the Workflow State of an Object
        /// </summary>
        [TestMethod]
        public void _15_GetObjectWorkflowState()
        {
            Login();
            ObjectWorkflowState result = Api.Get.ObjectWorkflowState(0, 1, -1);

            // For readability
            string asJson = JsonConvert.SerializeObject(result);
            Console.WriteLine(asJson);

            Console.WriteLine("Object Title = {0}", result.StateName);
        }

        /// <summary>
        /// This test Sets the Workflow State of an Object
        /// </summary>
        [TestMethod]
        public void _16_SetObjectWorkflowState()
        {
            Login();
            ObjectWorkflowState result = Api.Get.ObjectWorkflowState(0, 1, -1);
            Console.WriteLine("Object State = {0}", result.StateName);

            ExtendedObjectVersion updatedResult = Api.Set.ObjectWorkflowState(0, 1, -1, Api.CreateInstance.ObjectWorkflowState(101, 102));

            Console.WriteLine("Updated State = {0}",
                updatedResult.Properties.Single(p => p.PropertyDef == 39).TypedValue.DisplayValue);
        }

        /// <summary>
        /// Gets The Objects Thumbnail as a Stream
        /// </summary>
        [TestMethod]
        public void _17_GetObjectThumbnailStream()
        {
            const string filePath = "C:\\test.jpg";

            Login();
            Stream result = Api.Get.ObjectThumbnail(0, 1, -1);
            Image img = Image.FromStream(result);
            img.Save(filePath);

            Assert.IsTrue(File.Exists(filePath));
        }

        /// <summary>
        /// Gets the Relationships of an Object
        /// </summary>
        [TestMethod]
        public void _18_GetObjectRelationships()
        {
            Login();
            ObjectVersion[] relatedResult = Api.Get.ObjectRelationships(10, 1, -1, RelationshipType.both);

            foreach (ObjectVersion objectVersion in relatedResult)
            {
                Console.WriteLine(objectVersion.Title);
            }
        }

        /// <summary>
        /// Gets the Relationship Count of an Object
        /// </summary>
        [TestMethod]
        public void _19_GetObjectRelationshipCount()
        {
            Login();
            int relatedResultCount = Api.Get.ObjectRelationshipCount(10, 1, -1, RelationshipType.both);

            Console.WriteLine("Related Result Count: " + relatedResultCount);
        }

        /// <summary>
        /// Gets the Sub-Objects of an Object
        /// </summary>
        [TestMethod]
        public void _20_GetSubObjects()
        {
            Login();
            ObjectVersion[] result = Api.Get.SubObjects(Api.CreateInstance.ObjVer(101, 1));

            Console.WriteLine("Sub-Objects:");
            foreach (ObjectVersion objectVersion in result)
            {
                Console.WriteLine("\t- " + objectVersion.Title);
            }
        }

        /// <summary>
        /// Gets the Sub-Objects of an Object Using IDs
        /// </summary>
        [TestMethod]
        public void _21_GetSubObjectsById()
        {
            Login();
            ObjectVersion[] result = Api.Get.SubObjects(101, 1);

            Console.WriteLine("Sub-Objects:");
            foreach (ObjectVersion objectVersion in result)
            {
                Console.WriteLine("\t- " + objectVersion.Title);
            }
        }

        /// <summary>
        /// Gets the Sub-Object Count of an Object
        /// </summary>
        [TestMethod]
        public void _22_GetSubObjectCount()
        {
            Login();
            int result = Api.Get.SubObjectCount(Api.CreateInstance.ObjVer(101, 1));

            Console.WriteLine("Sub-Object Count: " + result);
        }

        /// <summary>
        /// Gets the Sub-Object Count of an Object Using IDs
        /// </summary>
        [TestMethod]
        public void _23_GetSubObjectCountById()
        {
            Login();
            var result = Api.Get.SubObjectCount(101, 1);

            Console.WriteLine("Sub-Object Count: " + result);
        }

        /// <summary>
        /// Get the Properties of an Object as a PropertyValue []
        /// </summary>
        [TestMethod]
        public void _24_GetObjectProperties()
        {
            Login();
            PropertyValue[] result = Api.Get.Properties(Api.CreateInstance.ObjVer(101, 1));

            Console.WriteLine("Prop Count: " + result.Length);

            result.ToList().ForEach(p => Console.WriteLine("{0} => {1}", p.PropertyDef, p.TypedValue.DisplayValue));
        }

        /// <summary>
        /// Get the Properties of an Object as a PropertyValue [] - Using IDs
        /// </summary>
        [TestMethod]
        public void _25_GetObjectPropertiesById()
        {
            Login();
            PropertyValue[] result = Api.Get.Properties(101, 1);

            Console.WriteLine("Prop Count: " + result.Length);

            result.ToList().ForEach(p => Console.WriteLine("{0} => {1}", p.PropertyDef, p.TypedValue.DisplayValue));
        }

        /// <summary>
        /// Get the Properties For Display (excludes some system props) of an Object as a PropertyValue []
        /// </summary>
        [TestMethod]
        public void _26_GetObjectPropertiesForDisplay()
        {
            Login();
            PropertyValue[] result = Api.Get.Properties(Api.CreateInstance.ObjVer(101, 1), true);

            Console.WriteLine("Prop Count: " + result.Length);

            result.ToList().ForEach(p => Console.WriteLine("{0} => {1}", p.PropertyDef, p.TypedValue.DisplayValue));
        }

        /// <summary>
        /// Get the Properties For Display (excludes some system props) of an Object as a PropertyValue [] - Using IDs
        /// </summary>
        [TestMethod]
        public void _27_GetObjectPropertiesByIdForDisplay()
        {
            Login();
            PropertyValue[] result = Api.Get.Properties(101, 1, -1, true);

            Console.WriteLine("Prop Count: " + result.Length);

            result.ToList().ForEach(p => Console.WriteLine("{0} => {1}", p.PropertyDef, p.TypedValue.DisplayValue));
        }

        /// <summary>
        /// Sets properties on the object. If the object already has a value for the passed properties this value will be overridden.
        /// </summary>
        [TestMethod]
        public void _28_SetObjectProperties()
        {
            Login();
            List<PropertyValue> props = new List<PropertyValue> { Api.CreateInstance.PropertyValue(0, "Title For Testing #2") };

            ExtendedObjectVersion result = Api.Set.Properties(0, 1, -1, props.ToArray());

            Console.WriteLine(result.Properties.Single(p => p.PropertyDef == 0).TypedValue.DisplayValue);
        }

        /// <summary>
        /// Sets the object properties. Properties not included in the request are removed from the object.
        /// </summary>
        [TestMethod]
        public void _29_SetObjectPropertiesEx()
        {
            Login();

            // REQUIREMENT - This Property Must Exist
            const int pid = 1023;

            // Get the Current PropertyValue[]
            PropertyValue[] props = Api.Get.Properties(0, 1);

            // Set the Props
            Api.Set.PropertiesEx(0, 1, -1, new[] { Api.CreateInstance.PropertyValue(pid, "Some Text Prop Value") });

            // Get the New PropertyValue[]
            PropertyValue[] newProps = Api.Get.Properties(0, 1);

            // Show that the additional prop is Present.
            Console.WriteLine("Has Additional Property => " + newProps.Any(p => p.PropertyDef == pid));

            // write the original properties using the Ex method, so properties not in the array will be removed.
            ExtendedObjectVersion result = Api.Set.PropertiesEx(0, 1, -1, props);

            // Show that the additional prop is Not Present.
            Console.WriteLine("Has Additional Property => " + result.Properties.Any(p => p.PropertyDef == pid));
        }

        /// <summary>
        /// Gets a single Property on an Object
        /// </summary>
        [TestMethod]
        public void _30_GetObjectProperty()
        {
            Login();
            PropertyValue result = Api.Get.Property(0, 0, 1);

            Console.WriteLine("Property {0} => {1}", result.PropertyDef, result.TypedValue.DisplayValue);
        }

        /// <summary>
        /// Sets a single Property on an Object
        /// </summary>
        [TestMethod]
        public void _31_SetObjectProperty()
        {
            Login();
            const int pid = 1023;

            ExtendedObjectVersion result;
            using (Eco eco = new Eco(Api, 0, 1))
            { // Checkout
                result = Api.Set.Property(pid, "hi there", eco.Info.ObjVer);
            } // Checkin

            PropertyValue prop = result.Properties.Single(p => p.PropertyDef == pid);
            Console.WriteLine("Property {0} => {1}", prop.PropertyDef, prop.TypedValue.DisplayValue);
        }

        /// <summary>
        /// Removes a single Property from an Object
        /// </summary>
        [TestMethod]
        public void _32_RemoveObjectProperty()
        {
            Login();
            const int pid = 1023;

            ExtendedObjectVersion result;
            using (Eco eco = new Eco(Api, 0, 1))
            {
                result = Api.Delete.Property(eco.Info.ObjVer, pid);
            }

            Console.WriteLine("Has Property {0} => {1}", pid, result.Properties.Any(p => p.PropertyDef == pid));
        }

        /// <summary>
        /// Gets the Property Collections Of Multiple Objects
        /// </summary>
        [TestMethod]
        public void _33_GetPropertiesOfMultipleObjects()
        {
            Login();

            List<PropertyValue[]> result = Api.Get.PropertiesOfMultipleObjects(
                new[]
				{
					Api.CreateInstance.ObjVer( 0, 1 ),
					Api.CreateInstance.ObjVer( 0, 2 )
				}
            );

            result.ForEach(
                propVals =>
                    propVals.ToList()
                        .ForEach(p => Console.WriteLine("Property {0} => {1}", p.PropertyDef, p.TypedValue.DisplayValue)));
        }

        /// <summary>
        /// Returns the Object File [] for an Object
        /// </summary>
        [TestMethod]
        public void _34_GetObjectFiles()
        {
            Login();

            ObjectFile[] result = Api.Get.Files(Api.CreateInstance.ObjVer(0, 1));

            result.ToList().ForEach(f => Console.WriteLine(f.Name));
        }

        /// <summary>
        /// Single-File Objects Only: Sets the ObjectFile for the given Object Instance - Filepath
        /// </summary>
        [TestMethod]
        public void _35_SetObjectFileByPath()
        {
            Login();

            ObjectVersion result = Api.Set.File(@"C:\Users\travis.payne\Pictures\green_road.jpg", Api.CreateInstance.ObjVer(0, 1));

            Console.WriteLine(result.Title);
        }

        /// <summary>
        /// Single-File Objects Only: Sets the ObjectFile for the given Object Instance - Stream
        /// </summary>
        [TestMethod]
        public void _36_SetObjectFileByStream()
        {
            Login();

            ObjectVersion result;
            using (
                Stream stream = new FileStream(@"C:\Users\travis.payne\Pictures\green_road.jpg", FileMode.Open, FileAccess.Read))
            {
                result = Api.Set.File(stream, Api.CreateInstance.ObjVer(0, 1));
            }

            Console.WriteLine(result.Title);
        }

        /// <summary>
        /// Single-File Objects Only: Gets the ObjectFile for the existing FileID on the given Object Instance.
        /// Downloads the file and returns the filepath.
        /// </summary>
        [TestMethod]
        public void _37_DownloadFile()
        {
            Login();

            string result = Api.Get.FileContentAsPath(0, 1, 122, 1);

            Console.WriteLine(result);
            Process.Start(result);
        }

        /// <summary>
        /// Downloads the file thumbnail and returns the filepath.
        /// </summary>
        [TestMethod]
        public void _38_DownloadFileThumbnail()
        {
            Login();

            string result = Api.Get.FileThumbnailAsPath(0, 1, 122, 1);

            Assert.IsTrue(File.Exists(result));
        }

        /// <summary>
        /// Returns the filename of an ObjectFile
        /// </summary>
        [TestMethod]
        public void _39_GetObjectFileName()
        {
            Login();

            string result = Api.Get.Filename(0, 1, 122, 1);

            Console.WriteLine(result);
        }

        /// <summary>
        /// Create a temporary file on the server that can be assigned to a newly created object.
        /// </summary>
        [TestMethod]
        public void _40_CreateUploadInfo()
        {
            Login();

            UploadInfo result = Api.Create.UploadInfo(@"C:\Users\travis.payne\AppData\Local\Temp\hi there.PNG");

            Console.WriteLine(result.UploadID);
        }

        /// <summary>
        /// Create a temporary file [] on the server that can be assigned to a newly created object.
        /// </summary>
        [TestMethod]
        public void _41_CreateUploadInfoCollection()
        {
            Login();

            UploadInfo[] result = Api.Create.UploadInfoCollection(new[] { @"C:\Users\travis.payne\AppData\Local\Temp\hi there.PNG" });

            result.ToList().ForEach(r => Console.WriteLine(r.UploadID));
        }

        /// <summary>
        /// Returns the Session Info for the Currently Signed on User
        /// </summary>
        [TestMethod]
        public void _42_GetUserSession()
        {
            Login();

            SessionInfo result = Api.Get.SessionInfo;

            Console.WriteLine(result.AccountName);
        }

        /// <summary>
        /// Returns the vaults that are available to a User
        /// </summary>
        [TestMethod]
        public void _43_GetAvailableVaults()
        {
            Solomon.Common.DocumentManagement.MFiles.WebAccess.Mfws.MfwsClient client
            = new Solomon.Common.DocumentManagement.MFiles.WebAccess.Mfws.MfwsClient(@"http://localhost/REST");

            Authentication auth = client.Authentication("motivesys\\travis.payne", "", "{F5C81026-6D68-4014-8924-498C8EA1FA3C}");

            Vault[] result = Set.SessionAuthentication(client, auth);

            result.ToList().ForEach(r => Console.WriteLine(r.Name));
        }

        /// <summary>
        /// TODO - Await API Update as there is a bug here now
        /// </summary>
        [TestMethod]
        public void _44_Logout()
        {
            Login();
            Api.Logout();

            try
            {
                string result = Api.Get.Filename(0, 1, 122, 1);
                Console.WriteLine(result);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        /// <summary>
        /// Locally Stored Vault Object
        /// </summary>
        [TestMethod]
        public void _45_GetVault()
        {
            Login();
            Vault result = Api.Vault;

            Console.WriteLine(result.Name);
        }

        /// <summary>
        /// TODO - Await API Update as there is a bug here now
        /// </summary>
        [TestMethod]
        public void _46_SetVault()
        {
            Login();
            Vault result = Api.Vault;
            string jsonVault = JsonConvert.SerializeObject(result);

            Vault fromString = JsonConvert.DeserializeObject<Vault>(jsonVault);

            result = Api.Set.SessionVault(fromString);
            Console.WriteLine(result.Name);
        }

        /// <summary>
        /// The Current Authentication Token Stored Locally
        /// </summary>
        [TestMethod]
        public void _47_GetSessionTokenLocal()
        {
            Login();
            string result = Api.Get.CurrentAuthenticationTokenLocal;

            Console.WriteLine(result);
        }

        /// <summary>
        /// The Current Authentication Token Stored on the Server
        /// </summary>
        [TestMethod]
        public void _48_GetSessionTokenFromServer()
        {
            Login();
            string result = Api.Get.CurrentAuthenticationToken;

            Console.WriteLine(result);
        }

        /// <summary>
        /// Returns the Server Status Response
        /// </summary>
        [TestMethod]
        public void _49_GetServerStatus()
        {
            Login();
            StatusResponse result = Api.Get.ServerStatus;

            Console.WriteLine(result.Message);
        }

        /// <summary>
        /// Gets the Server's Public Key
        /// </summary>
        [TestMethod]
        public void _50_GetServerPublicKey()
        {
            Login();
            PublicKey result = Api.Get.PublicKey;

            Console.WriteLine(result.Exponent);
        }

        /// <summary>
        /// Returns a Vault[] of the Vaults on the Server
        /// </summary>
        [TestMethod]
        public void _51_GetVaultsOnServer()
        {
            Login();
            Vault[] result = Api.Get.Vaults();

            Console.WriteLine("Online Only:");
            result.ToList().ForEach(v => Console.WriteLine(v.Name));

            result = Api.Get.Vaults(false);

            Console.WriteLine("\n\nAll Vaults:");
            result.ToList().ForEach(v => Console.WriteLine(v.Name));
        }

        /// <summary>
        /// Creates a new Authentication Token
        /// </summary>
        [TestMethod]
        public void _52_CreateAuthToken()
        {
            Login();
            string result = Api.Create.AuthenticationToken(Api.Client.Auth);

            Console.WriteLine(result);
        }

        /// <summary>
        /// Collection of object type information.
        /// Internally M-Files considers both valuelists and the real object types as object types.
        /// By default this resource will return both kinds of types. If you want only one kind use the type-parameter.
        /// </summary>
        [TestMethod]
        public void _53_GetObjectTypes()
        {
            Login();
            ObjType[] result = Api.Structure.ObjectTypes();

            result.ToList().ForEach(r => Console.WriteLine(r.Name));
        }

        /// <summary>
        /// Information on a single object type.
        /// </summary>
        [TestMethod]
        public void _54_GetObjectTypeById()
        {
            Login();
            ObjType result = Api.Structure.ObjectType(0);

            Console.WriteLine(result.Name);
        }

        /// <summary>
        /// Icon for a single object type - Stream
        /// </summary>
        [TestMethod]
        public void _55_GetObjectTypeIcon()
        {
            Login();
            Stream result = Api.Structure.ObjectTypeIcon(0);

            Console.WriteLine(result.GetType());
        }

        /// <summary>
        /// Icon for a single object type - Filepath
        /// </summary>
        [TestMethod]
        public void _56_GetObjectTypeIconAsPath()
        {
            Login();
            string result = Api.Structure.ObjectTypeIconAsPath(0, true);

            Console.WriteLine(result);
            Assert.IsTrue(File.Exists(result));
        }

        /// <summary>
        /// Triggers a Refresh of an External Connection
        /// </summary>
        [TestMethod]
        public void _57_ObjectTypeExternalSourceRefresh()
        {
            Login();
            MFRefreshStatus result = Api.Structure.ObjectTypeExternalSourceRefresh(0);

            Console.WriteLine(result.ToString());
        }

        [TestMethod]
        public void _58_DEPRECATED()
        {
        }

        /// <summary>
        /// Returns the Structure - Classes
        /// </summary>
        [TestMethod]
        public void _59_GetClasses()
        {
            Login();
            ObjectClass[] result = Api.Structure.ObjectClasses();

            result.ToList().ForEach(x => Console.WriteLine(x.Name));
        }

        /// <summary>
        /// Returns the Structure - Class - By ID
        /// </summary>
        [TestMethod]
        public void _60_GetClassByID()
        {
            Login();
            ObjectClass result = Api.Structure.ObjectClass(0);

            Console.WriteLine(result.Name);
        }

        /// <summary>
        /// Returns the Structure - Class Icon - Filepath
        /// </summary>
        [TestMethod]
        public void _61_GetClassIconAsPath()
        {
            Login();
            string result = Api.Structure.ObjectClassIconAsPath(0);

            Console.WriteLine(result);
            Assert.IsTrue(File.Exists(result));
        }

        /// <summary>
        /// Returns the Structure - Properties
        /// </summary>
        [TestMethod]
        public void _62_GetPropertyDefs()
        {
            Login();
            PropertyDef[] result = Api.Structure.PropertyDefs();

            result.ToList().ForEach(x => Console.WriteLine(x.Name));
        }

        /// <summary>
        /// Returns the Structure - Properties - By ID
        /// </summary>
        [TestMethod]
        public void _63_GetPropertyDefById()
        {
            Login();
            PropertyDef result = Api.Structure.PropertyDef(100);

            Console.WriteLine(result.Name);
        }

        /// <summary>
        /// Returns the Structure - Workflows
        /// </summary>
        [TestMethod]
        public void _64_GetWorkflows()
        {
            Login();
            Workflow[] result = Api.Structure.Workflows();

            result.ToList().ForEach(x => Console.WriteLine(x.Name + " - " + x.ID));
        }

        /// <summary>
        /// Returns the Structure - Workflows - By ID
        /// </summary>
        [TestMethod]
        public void _65_GetWorkflowById()
        {
            Login();
            Workflow result = Api.Structure.Workflow(101);

            Console.WriteLine(result.Name);
        }

        /// <summary>
        /// Returns the Structure - Workflow State [] - By Workflow ID
        /// </summary>
        [TestMethod]
        public void _66_GetWorkflowState()
        {
            Login();
            WorkflowState[] result = Api.Structure.States(101);

            result.ToList().ForEach(x => Console.WriteLine(x.Name + " - " + x.ID));
        }

        /// <summary>
        /// Returns the Structure - Workflow State - By Workflow ID / State ID
        /// </summary>
        [TestMethod]
        public void _67_GetWorkflowStateById()
        {
            Login();
            WorkflowState result = Api.Structure.State(101, 102);

            Console.WriteLine(result.Name);
        }

        /// <summary>
        /// Returns Value List Items By Filtered Search
        /// </summary>
        [TestMethod]
        public void _68_GetValueListItems()
        {
            Login();
            Results<ValueListItem> result = Api.Structure.ValueListItems(1, null, null, null, null, "Unclass*");

            result.Items.ToList().ForEach(i => Console.WriteLine(i.Name));
        }

        /// <summary>
        /// Creates a new Value List Item Object Instance
        /// </summary>
        [TestMethod]
        public void _69_CreateValueListItem()
        {
            Login();
            ValueListItem result = Api.Create.ValueListItem(103, "Test Data Item 1");

            Console.WriteLine(result.Name);
        }

        /// <summary>
        /// Returns the specified Value List Item
        /// </summary>
        [TestMethod]
        public void _70_GetValueListItem()
        {
            Login();
            ValueListItem result = Api.Get.ValueListItem(103, 1);

            Console.WriteLine(result.Name);
        }

        /// <summary>
        /// Deletes a Value List Item from a Value List.
        /// </summary>
        [TestMethod]
        public void _71_DeleteValueListItem()
        {
            Login();
            Api.Delete.ValueListItem(103, 2);
        }

        /// <summary>
        /// Returns Value List Item Title.
        /// </summary>
        [TestMethod]
        public void _72_GetValueListItemTitle()
        {
            Login();
            Api.Create.ValueListItem(103, "Title Test");

            Results<ValueListItem> result = Api.Structure.ValueListItems(103);

            Console.WriteLine(Api.Get.ValueListItemTitle(103, result.Items.Last().ID));
        }

        /// <summary>
        /// Sets a Value List Item Name
        /// </summary>
        [TestMethod]
        public void _73_SetValueListItemTitle()
        {
            Login();
            Results<ValueListItem> result = Api.Structure.ValueListItems(103);
            Console.WriteLine(result.Items.Last().Name);

            Console.WriteLine(Api.Set.ValueListItemName(103, result.Items.Last().ID, "Updated Name").Name);
        }

        /// <summary>
        /// Returns Folder Content Items of a View
        /// </summary>
        [TestMethod]
        public void _74_GetViewItems()
        {
            Login();

            // By Class / Assignment
            FolderContentItems result = Api.Get.ViewFolderContentItems("V2/L-100");

            foreach (FolderContentItem item in result.Items)
            {
                Console.WriteLine(item.ObjectVersion.Title);
            }
        }

        /// <summary>
        /// Returns a View's Item Count.
        /// </summary>
        [TestMethod]
        public void _75_GetViewItemCount()
        {
            Login();

            // By Class / Assignment
            int result = Api.Get.ViewItemCount("V2/L-100");

            Console.WriteLine("Count: " + result);
        }

        /// <summary>
        /// Returns the ObjectVersion's contained within a View.
        /// </summary>
        [TestMethod]
        public void _76_GetViewObjectVersions()
        {
            Login();

            // By Class / Assignment
            Results<ObjectVersion> result = Api.Get.ViewObjectVersions("V2/L-100");

            result.Items.ToList().ForEach(x => Console.WriteLine(x.Title));
        }

        /// <summary>
        /// Returns ObjectVersion's from the Favorites View.
        /// </summary>
        [TestMethod]
        public void _77_GetFavorites()
        {
            Login();

            ObjectVersion[] result = Api.Get.Favorites;

            result.ToList().ForEach(x => Console.WriteLine(x.Title));
        }

        /// <summary>
        /// Returns a specified Object Version from a Users Favorites.
        /// </summary>
        [TestMethod]
        public void _78_GetFavorite()
        {
            Login();

            ObjectVersion result = Api.Get.Favorite(0, 1);

            Console.WriteLine(result.Title);
        }

        /// <summary>
        /// Removes an Object from Favorites.
        /// </summary>
        [TestMethod]
        public void _79_RemoveFavorite()
        {
            Login();

            Api.Delete.Favorite(0, 1);
        }

        /// <summary>
        /// Returns ObjectVersion's from the Recently Accessed By Me View.
        /// </summary>
        [TestMethod]
        public void _80_RecentlyAccessedByMe()
        {
            Login();

            ObjectVersion[] result = Api.Get.RecentlyAccessedByMe;

            result.ToList().ForEach(x => Console.WriteLine(x.Title));
        }

        /// <summary>
        /// Adds an Object to the Recently Accessed By me View.
        /// </summary>
        [TestMethod]
        public void _81_AddItemToRecentlyAccessedByMe()
        {
            Login();

            ObjID objid = new ObjID { Type = 0, ID = 1 };

            ExtendedObjectVersion result = Api.Set.RecentlyAccessedByMe(objid);

            Console.WriteLine(result.Properties[0].TypedValue.DisplayValue);
        }

        /// <summary>
        /// Creates a new Single File Object in the Vault
        /// </summary>
        [TestMethod]
        public void _82_CreateSingleFileObject()
        {
            Login();

            //ObjectVersion result = Api.Create.CreateSingleFileObject(0, 0, @"C:\MFWS Documentation.pdf");
            ObjectVersion result = Api.Create.CreateSingleFileObject(true,0, 0, @"C:\MFWS Documentation.pdf");

            Console.WriteLine(result.Title);
        }

        /// <summary>
        /// Creates a new Multi File object in the vault
        /// </summary>
        [TestMethod]
        public void _83_CreateMultiFileObject()
        {
            Login();

            string[] files = new[]
			{
				@"C:\MFWS Documentation.pdf",
				@"C:\Test PDF.pdf"
			};

            ObjectVersion result = Api.Create.CreateMultiFileObject(0, 0, files, "Test Multi File Doc");

            Console.WriteLine(result.Title);
        }
    }
}
