﻿using System;

namespace Solomon.Common.Utility
{
    /// <summary>
    /// A singleton Log4Net wrapper
    /// </summary>
    public class Log
    {
        private static log4net.ILog _log = null;
        private static object _lock = new object();
        private static Log _instance = null; 
        private Log()
        {
            if (_log == null)
            {
                _log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            }
        }

        public static Log Instance()
        {
            if (_instance == null)
            {
                lock (_lock)
                {
                    if (_instance == null)
                    {
                        _instance = new Log();
                    }
                }
            }

            return _instance;
        }

        public void Error(Exception ex)
        {
            lock (_lock)
            {
                Error(ex.Message, ex);
            }
        }

        public void Error(string message, Exception ex)
        {
            lock (_lock)
            {
                _log.Error(message, ex);
            }
        }

        public void Info(Exception ex)
        {
            lock (_lock)
            {
                Info(ex.Message, ex);
            }
        }
        
        public void Info(string message, Exception ex)
        {
            lock (_lock)
            {
                _log.Info(message, ex);
            }
        }

        public void Info(string message, string origin)
        {
            lock (_lock)
            {
                System.Exception oex = new Exception(origin);
                _log.Info(message, oex);
            }
        }
        
        public void Debug(Exception ex)
        {
            lock (_lock)
            {
                Debug(ex.Message, ex);
            }
        }
        
        public void Debug(string message, Exception ex)
        {
            lock (_lock)
            {
                _log.Debug(message, ex);
            }
        }

        public void Fatal(Exception ex)
        {
            lock (_lock)
            {
                Fatal(ex.Message, ex);
            }
        }
        
        public void Fatal(string message, Exception ex)
        {
            lock (_lock)
            {
                _log.Fatal(message, ex);
            }
        }        
    }
}
