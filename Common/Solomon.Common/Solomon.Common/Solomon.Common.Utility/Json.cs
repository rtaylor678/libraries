﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solomon.Common.Utility.Serialization
{
    /// <summary>
    /// JSON Serialization utilities 
    /// </summary>
    public class Json
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj">Input object for JSON serialization</param>
        /// <returns>JSON serialized string</returns>
        public static string Serialize(object obj)
        {
            return JsonConvert.SerializeObject(obj, Formatting.Indented);
        }

        /// <summary>
        /// Serializes JSON to the specified .Net Type
        /// </summary>
        /// <typeparam name="T">Specified .Net type JSON to be converted </typeparam>
        /// <param name="json">The JSON to desrialize</param>
        /// <returns></returns>
        public static T Deserialize<T>(string json)
        {
            return JsonConvert.DeserializeObject<T>(json);
        }

    }
}
