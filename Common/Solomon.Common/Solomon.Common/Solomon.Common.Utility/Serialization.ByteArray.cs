﻿using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;

namespace Solomon.Common.Utility.Serialization
{
	public static class ByteArray
    {
		/// <summary>
		/// Converts a byte array to the defined type.
		/// </summary>
		/// <typeparam name="T">Type to return</typeparam>
		/// <param name="byteArray"></param>
		/// <returns>type defined in returnType</returns>
		/// <example>string s = Deserialize<string>(new byte[0]);</example>
		public static T ToObject<T>(byte[] byteArray)
		{
			using (MemoryStream stream = new MemoryStream(byteArray))
			{
				BinaryFormatter formatter = new BinaryFormatter();
				return (T)formatter.Deserialize(stream);
			}
		}

		/// <summary>
		/// Converts an object to byte array.
		/// </summary>
		/// <param name="obj">The object to serialize to bytes</param>
		/// <returns>byte[]</returns>
		public static byte[] ToArray(object obj)
		{
			using (MemoryStream stream = new MemoryStream())
			{
				BinaryFormatter formatter = new BinaryFormatter();
				formatter.Serialize(stream, obj);
				return stream.ToArray();
			}
		}

		public static byte[] Left(byte[] raw, int length, out byte[] right)
		{
			byte[] l;
			Split(raw, length, out l, out right);
			return l;
		}

		public static bool Split(byte[] raw, int length, out byte[] left, out byte[] right)
		{
			left = raw.Take(length).ToArray();
			right = raw.Skip(length).Take(raw.Length - length).ToArray();
			return true;
		}
	}
}