﻿using System.IO;
using System.IO.Compression;

namespace Solomon.Common.Utility.Compression
{
	/// <summary>
	/// Compresses and expands byte[] using GZipStream
	/// </summary>
	public static class GZip
	{
		/// <summary>
		/// Creates a compressed a byte array; on failure creates a zero byte array (byte[0])
		/// </summary>
		/// <param name="raw">input array</param>
		/// <param name="compressed">compressed output</param>
		/// <returns></returns>
		public static bool Compress(byte[] raw, out byte[] compressed)
		{
			try
			{
				using (MemoryStream ms = new MemoryStream())
				{
					using (GZipStream gz = new GZipStream(ms, CompressionLevel.Optimal, false))
					{
						gz.Write(raw, 0, raw.Length);
						gz.Flush();
						gz.Close();
					}
					compressed = ms.ToArray();
					ms.Flush();
					ms.Close();
				}
				return true;
			}
			catch
			{
				compressed = new byte[0];
				return false;
			}
		}

		/// <summary>
		/// Decompresses a byte array; on failure creates a zero byte array (byte[0])
		/// </summary>
		/// <param name="raw">compressed input</param>
		/// <param name="expanded">expanded output</param>
		/// <returns></returns>
		public static bool Expand(byte[] raw, out byte[] expanded)
		{
			try
			{
				using (MemoryStream ms = new MemoryStream())
				{
					using (MemoryStream cs = new MemoryStream(raw, 0, raw.Length, false, false))
					{
						using (GZipStream gz = new GZipStream(cs, CompressionMode.Decompress, false))
						{
							gz.CopyTo(ms);
							gz.Flush();
							gz.Close();
						}
						cs.Flush();
						cs.Close();
					}
					expanded = ms.ToArray();
					ms.Flush();
					ms.Close();
				}
				return true;
			}
			catch
			{
				expanded = new byte[0];
				return false;
			}
		}
	}
}