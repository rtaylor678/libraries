﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.IO;
using System.Threading.Tasks;

namespace Solomon.Common.Email
{
    [Serializable]
    public class Manager
    {

        Config _config = new Config();
 
        #region "Constructors"

        public Manager()
        {

        }

        public Manager(Config config)
        {
            _config.EmailFrom = config.EmailFrom;
            _config.EmailTo = config.EmailTo;
            _config.Subject = config.Subject;
            _config.Body = config.Body;
            _config.IsBodyHtml = config.IsBodyHtml;
            _config.SmtpServer = config.SmtpServer;
            _config.Port = config.Port;
            _config.UserName = config.UserName;
            _config.UserPassword = config.UserPassword;
            _config.Attachments = config.Attachments;
            _config.DeleteFilesAfterSend = config.DeleteFilesAfterSend;
        }

        #endregion "Constructors"

        #region "Public Members"

        //TO DO:
        //public void SendEmail()
        //{
        //    SendEmailsUsingConfig();
        //}

        /// <summary>
        /// Sends emails using simple Smtp protocol without user name and password credentials. 
        /// </summary>
        /// <param name="emailFrom">Required</param>
        /// <param name="emailTo">Required - Can send to multiple recepients using comma, or semicolon as delimiter</param>
        /// <param name="subject">Required</param>
        /// <param name="body">Required</param>
        /// <param name="smtpServer">Required</param>
        /// <returns></returns>
        public void SendEmail(string emailFrom, string emailTo, string subject, string body, string smtpServer)
        {
            SendEmailsSimpleSmtpClient(emailFrom, emailTo, string.Empty, subject, body, smtpServer);
        }

        public void SendEmail(string emailFrom, string emailTo, string subject, string body, bool isBodyHtml, string smtpServer)
        {
            SendEmailsSimpleSmtpClientAsHtmlContent(emailFrom, emailTo, subject, body, isBodyHtml, smtpServer);
        }

        public void SendEmail(string emailFrom, string emailTo, string subject, string body, string smtpServer, int port, string userName, string userPassword)
        {
            SendEmailsSmtpClientWithCredentials(emailFrom, emailTo, subject, body, smtpServer, port, userName, userPassword);
        }

        public void SendEmail(string emailFrom, string emailTo, string subject, string body, string smtpServer, string[] attachments, bool deleteFilesAfterSend)
        {
            SendEmailsPlusAttachmentSimpleSmtpClient(emailFrom, emailTo, subject, body, smtpServer, attachments, deleteFilesAfterSend);
        }

        public void SendEmail(string emailFrom, string emailTo, string subject, string body, string smtpServer, string[] attachments, int port, string userName, string userPassword, bool deleteFilesAfterSend)
        {
            SendEmailsPlusAttachmentSmtpClientWithCredentials(emailFrom, emailTo, subject, body, smtpServer, attachments, port, userName, userPassword, deleteFilesAfterSend);
        }

        #endregion "Public Members"

        #region "Private Members"

        private void SendEmailsUsingConfig()
        {
            //TO DO:
        }

        private void SendEmailsSimpleSmtpClient(string emailFrom, string emailTo, string emailBCC, string subject, string body, string smtpServer)
        {

            try
            {

                if (emailTo != null)
                {
                    if (emailFrom != null)
                    {
                        string[] toAddresses = Regex.Split(emailTo, "[,;] *");
                        MailMessage mailMessage = new MailMessage();

                        mailMessage.Subject = subject;
                        mailMessage.Body = body;
                        if (emailBCC != string.Empty)
                        {
                            MailAddress oAddrFrom = new MailAddress(emailBCC);
                            mailMessage.Bcc.Add(oAddrFrom);
                        }

                        try
                        {
                            foreach (string mailTo in toAddresses)
                            {
                                mailMessage.To.Add(mailTo);
                            }

                            MailAddress mailFromAddress = new MailAddress(emailFrom);
                            mailMessage.From = mailFromAddress;

                            //Email the recipient
                            SmtpClient client = new SmtpClient(smtpServer);
                            client.Send(mailMessage);

                        }
                        catch (Exception exMail)
                        {
                            throw exMail;
                        }
                    }
                    else
                    {
                        throw new Exception("Solomon.Common.Email - No recipients in, 'From:' List. Cannot send email. Must have at least one sender");
                    }
                }
                else
                {
                    throw new Exception("Solomon.Common.Email - No recipients in, 'To:' List. Cannot send email. Must have at least one recipient");
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private void SendEmailsSimpleSmtpClientAsHtmlContent(string emailFrom, string emailTo, string subject, string body, bool isBodyHtml, string smtpServer)
        {
            /// <param name="args[0]">From</param>
            /// <param name="args[1]">To</param>
            /// <param name="args[2]">Subject</param>
            /// <param name="args[3]">Body</param>
            /// <param name="args[4]">Host</param>

            try
            {

                if (emailTo != null)
                {
                    if (emailFrom != null)
                    {
                        string[] toAddresses = Regex.Split(emailTo, "[,;] *");
                        MailMessage mailMessage = new MailMessage();

                        mailMessage.IsBodyHtml = isBodyHtml;
                        mailMessage.Subject = subject;
                        mailMessage.Body = body;

                        try
                        {
                            foreach (string mailTo in toAddresses)
                            {
                                mailMessage.To.Add(mailTo);
                            }

                            MailAddress mailFromAddress = new MailAddress(emailFrom);
                            mailMessage.From = mailFromAddress;

                            //Email the recipient
                            SmtpClient client = new SmtpClient(smtpServer);
                            client.Send(mailMessage);

                        }
                        catch (Exception exMail)
                        {
                            throw exMail;
                        }
                    }
                    else
                    {
                        throw new Exception("Solomon.Common.Email - No recipients in, 'From:' List. Cannot send email. Must have at least one sender");
                    }
                }
                else
                {
                    throw new Exception("Solomon.Common.Email - No recipients in, 'To:' List. Cannot send email. Must have at least one recipient");
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private void SendEmailsSmtpClientWithCredentials(string emailFrom, string emailTo, string subject, string body, string smtpServer, int port, string userName, string userPassword)
        {
            /// <param name="args[0]">From</param>
            /// <param name="args[1]">To</param>
            /// <param name="args[2]">Subject</param>
            /// <param name="args[3]">Body</param>
            /// <param name="args[4]">Host</param>

            try
            {

                if (emailTo != null)
                {
                    if (emailFrom != null)
                    {
                        string[] toAddresses = Regex.Split(emailTo, "[,;] *");
                        MailMessage mailMessage = new MailMessage();

                        mailMessage.Subject = subject;
                        mailMessage.Body = body;

                        try
                        {
                            foreach (string mailTo in toAddresses)
                            {
                                mailMessage.To.Add(mailTo);
                            }

                            MailAddress mailFromAddress = new MailAddress(emailFrom);
                            mailMessage.From = mailFromAddress;

                            //Email the recipient
                            SmtpClient smtpClient = new SmtpClient(smtpServer, port);
                            System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(userName, userPassword);
                            smtpClient.Credentials = credentials;

                            smtpClient.Send(mailMessage);

                        }
                        catch (Exception exMail)
                        {
                            throw exMail;
                        }
                    }
                    else
                    {
                        throw new Exception("Solomon.Common.Email - No recipients in, 'From:' List. Cannot send email. Must have at least one sender");
                    }
                }
                else
                {
                    throw new Exception("Solomon.Common.Email - No recipients in, 'To:' List. Cannot send email. Must have at least one recipient");
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private void SendEmailsPlusAttachmentSimpleSmtpClient(string emailFrom, string emailTo, string subject, string body, string smtpServer, string[] attachments, bool deleteFilesAfterSend)
        {
            /// <param name="args[0]">From</param>
            /// <param name="args[1]">To</param>
            /// <param name="args[2]">Subject</param>
            /// <param name="args[3]">Body</param>
            /// <param name="args[4]">Host</param>
            /// <param name="args[5]">Attachments</param>
            /// <param name="args[6]">DeleteFilesAfterSend</param>

            try
            {

                if (emailTo != null)
                {
                    if (emailFrom != null)
                    {
                        string[] toAddresses = Regex.Split(emailTo, "[,;] *");
                        MailMessage mailMessage = new MailMessage();

                        mailMessage.Subject = subject;
                        mailMessage.Body = body;

                        try
                        {
                            foreach (string mailTo in toAddresses)
                            {
                                mailMessage.To.Add(mailTo);
                            }

                            //Add attachments
                            foreach (string attachment in attachments)
                            {

                                if (attachment != string.Empty)
                                {
                                    Attachment attachFile = new Attachment(attachment);
                                    mailMessage.Attachments.Add(attachFile);
                                }
                            }

                            MailAddress mailFromAddress = new MailAddress(emailFrom);
                            mailMessage.From = mailFromAddress;

                            //Email the recipient
                            SmtpClient client = new SmtpClient(smtpServer);
                            client.Send(mailMessage);

                        }
                        catch (Exception exMail)
                        {
                            throw exMail;
                        }
                    }
                    else
                    {
                        throw new Exception("Solomon.Common.Email - No recipients in, 'From:' List. Cannot send email. Must have at least one sender");
                    }
                }
                else
                {
                    throw new Exception("Solomon.Common.Email - No recipients in, 'To:' List. Cannot send email. Must have at least one recipient");
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private void SendEmailsPlusAttachmentSmtpClientWithCredentials(string emailFrom, string emailTo, string subject, string body, string smtpServer, string[] attachments, int port, string userName, string userPassword, bool deleteFilesAfterSend)
        {
            /// <param name="args[0]">From</param>
            /// <param name="args[1]">To</param>
            /// <param name="args[2]">Subject</param>
            /// <param name="args[3]">Body</param>
            /// <param name="args[4]">Host</param>
            /// <param name="args[5]">Attachments</param>
            /// <param name="args[6]">DeleteFilesAfterSend</param>

            try
            {

                if (emailTo != null)
                {
                    if (emailFrom != null)
                    {
                        string[] toAddresses = Regex.Split(emailTo, "[,;] *");
                        MailMessage mailMessage = new MailMessage();

                        mailMessage.Subject = subject;
                        mailMessage.Body = body;

                        try
                        {
                            foreach (string mailTo in toAddresses)
                            {
                                mailMessage.To.Add(mailTo);
                            }

                            //Add attachments
                            foreach (string attachment in attachments)
                            {
                                Attachment attachFile = new Attachment(attachment);
                                mailMessage.Attachments.Add(attachFile);
                            }

                            MailAddress mailFromAddress = new MailAddress(emailFrom);
                            mailMessage.From = mailFromAddress;

                            //Email the recipient
                            SmtpClient smtpClient = new SmtpClient(smtpServer, port);
                            System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(userName, userPassword);
                            smtpClient.Credentials = credentials;

                            smtpClient.Send(mailMessage);

                            //Delete File after send if true
                            if (deleteFilesAfterSend == true)
                            {
                                foreach (string attachment in attachments)
                                {
                                    if (File.Exists(attachment) == true)
                                    {
                                        try
                                        {
                                            File.Delete(attachment);
                                        }
                                        catch (Exception fileEx)
                                        {
                                            throw fileEx;
                                        }
                                    }
                                }
                            }

                        }
                        catch (Exception exMail)
                        {
                            throw exMail;
                        }
                    }
                    else
                    {
                        throw new Exception("Solomon.Common.Email - No recipients in, 'From:' List. Cannot send email. Must have at least one sender");
                    }
                }
                else
                {
                    throw new Exception("Solomon.Common.Email - No recipients in, 'To:' List. Cannot send email. Must have at least one recipient");
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

    }

    #endregion "Private Members"
}
