﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Reflection;


namespace Solomon.Common.Email
{
    [Serializable] 
    public class Config
    {       
        [Required]
        public string EmailFrom { get; set; }

        [Required]
        public string EmailTo { get; set; }

        [Required]
        public string Subject { get; set; }

        public string Body { get; set; }

        public bool IsBodyHtml { get; set; }

        [Required]
        public string SmtpServer { get; set; }
        
        public string Port { get; set; }
        public string UserName { get; set; }
        public string UserPassword { get; set; }
        public string[] Attachments { get; set; }
        public bool DeleteFilesAfterSend { get; set; }

    }
}
