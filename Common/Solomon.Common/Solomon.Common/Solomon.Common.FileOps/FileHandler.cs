﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data;
using System.Collections;

namespace Solomon.Common.FileOps
{
    [Serializable]
    public class FileHandler
    {


        public System.Data.DataTable ReadWholeFileToDataTable(ref StreamReader file, string delimiter)
        {
            System.Data.DataTable dataTableInputFile = new System.Data.DataTable();

            try
            {
                //First row should be header, let's take the data of the first row and parse it. 
                char[] delimiterChar = { Convert.ToChar(delimiter) };

                do
                {
                    string row = file.ReadLine();
                    string[] columnArray = row.Split(delimiterChar);

                    //Add new row to datatable
                    dataTableInputFile.NewRow();

                    foreach (string colValue in columnArray)
                    {
                        DataColumn dcColValue = new DataColumn(colValue);

                        dcColValue.DataType = System.Type.GetType("System.String");
                        dataTableInputFile.Columns.Add(dcColValue);
                    }

                } while (file.Peek() >= 0);

                return dataTableInputFile;
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public void DeleteFile(string file)
        {
            try
            {
                if (File.Exists(file) == true)
                {
                    if (IsFileOpenOrReadOnly(ref file) == true)
                    {
                        //Will do special handling here
                    }
                    else
                    {
                        File.Delete(file);
                    }

                }
            }
            catch (IOException ex)
            {
                throw ex;
            }
        }

        public static string MapPathInAssembly(string VirtualPath)
        {
            string strPath = System.Reflection.Assembly.GetExecutingAssembly().Location;
            strPath = strPath.Substring(0, strPath.IndexOf("\\bin")) + "\\" + VirtualPath;
            return strPath;
        }

        public void MoveFile(string file, string fileName, string fileDirectory)
        {
            try
            {
                if (File.Exists(file) == true)
                {
                    if (IsFileOpenOrReadOnly(ref file) == true)
                    {
                        throw new Exception("File is currently opened by process or is read-only");
                    }
                    else
                    {
                        File.Move(file, fileDirectory + fileName);
                    }

                }
            }
            catch (IOException ex)
            {

                throw ex;
            }
        }

        public bool FileExists(string file)
        {
            bool exists = false;

            try
            {
                if (File.Exists(file) == true)
                {
                    exists = true;
                }

                return exists;
            }
            catch (Exception)
            {

                throw;
            }
        }

        //private void ReleaseExcelObject(object obj)
        //{
        //    try
        //    {
        //        //System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
        //        System.Runtime.InteropServices.Marshal.FinalReleaseComObject(obj);
        //        obj = null;
        //    }
        //    catch (Exception ex)
        //    {
        //        string error = ex.Message;
        //        obj = null;
        //        // MessageBox.Show("Unable to release the Object " + ex.ToString());
        //    }
        //    finally
        //    {
        //        GC.Collect();
        //    }
        //}

        private int CountTabDelimiters(string line, char delimiter)
        {
            int count = 0;
            char delim = Convert.ToChar(delimiter);
            foreach (char character in line)
            {

                if (character == delimiter)
                {
                    count++;
                }
            }

            return count + 1;
        }

        public int CountDelimiters(string line, string delimiter)
        {

            int count = 0;

            if (delimiter == "TAB")
            {
                char[] tabDelim = new char[] { '\t' };

                foreach (char character in line)
                {
                    if (character == tabDelim[0])
                    {
                        count++;
                    }
                }
            }
            else
            {
                char delim = Convert.ToChar(delimiter);

                foreach (char character in line)
                {
                    if (character == delim)
                    {
                        count++;
                    }
                }
            }

            return count + 1;
        }

        private bool IsFileOpenOrReadOnly(ref string file)
        {
            try
            {
                //first make sure it's not a read only file
                if ((File.GetAttributes(file) & FileAttributes.ReadOnly) != FileAttributes.ReadOnly)
                {
                    //first we open the file with a FileStream
                    using (FileStream stream = new FileStream(file, FileMode.OpenOrCreate, FileAccess.Read, FileShare.None))
                    {
                        try
                        {
                            stream.ReadByte();
                            return false;
                        }
                        catch (IOException)
                        {
                            return true;
                        }

                    }
                }
                else
                {
                    return true;
                }
            }
            catch (IOException)
            {
                return true;
            }
        }

        public bool SaveFile(string file)
        {
            bool save = false;

            try
            {
                if (File.Exists(file) == true)
                {
                    DeleteFile(file);

                    using (FileStream fs = File.Create(file))
                    {
                        fs.Close();
                    }

                    save = true;

                }
                else
                {
                    using (FileStream fs = File.Create(file))
                    {
                        fs.Close();
                    }

                    save = true;
                }

                return save;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool CreateDirectory(string dirPath)
        {

            try
            {
                if (System.IO.Directory.Exists(dirPath) != true)
                {
                    Directory.CreateDirectory(dirPath);
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool DeleteDirectory(string dirPath)
        {

            try
            {
                if (System.IO.Directory.Exists(dirPath) == true)
                {
                    Directory.Delete(dirPath);
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool DirectoryExists(string dirPath)
        {

            try
            {
                if (System.IO.Directory.Exists(dirPath) != true)
                {
                    return false;
                }
                else
                {
                    return true;
                }

            }
            catch (Exception)
            {
                return false;
            }
        }


        //public string[] GetDirectories(string dirPath)
        //{
        //    if (DirectoryExists(dirPath) == true)
        //    {

        //    }
        //    else
        //    {

        //    }

        //}

     
    }
}
