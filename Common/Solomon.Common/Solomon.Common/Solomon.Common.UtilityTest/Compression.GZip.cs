﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Solomon.Common.Utility.Compression
{
	[TestClass]
	public class GZipTest
	{
		[TestMethod]
		public void GZipCompression()
		{
			byte[] regBytes = new byte[100];

			byte[] cmpBytes;
			GZip.Compress(regBytes, out cmpBytes);

			byte[] expBytes;
			GZip.Expand(cmpBytes, out expBytes);

			Assert.IsTrue(regBytes.SequenceEqual(expBytes));
			Assert.IsTrue(regBytes.Length >= cmpBytes.Length);
			Assert.IsTrue(expBytes.Length >= cmpBytes.Length);
		}
	}
}