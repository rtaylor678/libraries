﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Solomon.Common.Utility.Serialization
{
	[TestClass]
	public class ByteArrayTest
	{
		//	https://www.random.org/cgi-bin/randbyte?nbytes=10&format=h

		static readonly byte[] left0 = new byte[7] { 0x87, 0xb1, 0x4b, 0x0e, 0xb2, 0x72, 0xfb };
		static readonly byte[] right0 = new byte[10] { 0x9c, 0x9a, 0x3a, 0xc3, 0x43, 0x48, 0x2a, 0xe3, 0x02, 0x08 };
		byte[] t = left0.Concat(right0).ToArray();
		int len = left0.Length;

		[TestMethod]
		public void BytesLeft()
		{
			byte[] right1;
			byte[] left1 = ByteArray.Left(t, len, out right1);

			Assert.IsTrue(left0.SequenceEqual(left1));
			Assert.IsTrue(right0.SequenceEqual(right1));
		}

		[TestMethod]
		public void BytesSplit()
		{
			byte[] left2;
			byte[] right2;

			bool s = ByteArray.Split(t, len, out left2, out right2);

			Assert.IsTrue(left0.SequenceEqual(left2));
			Assert.IsTrue(right0.SequenceEqual(right2));
			Assert.IsTrue(s);
		}

		[TestMethod]
		public void BytesVerify()
		{
			byte[] right1;
			byte[] left1 = ByteArray.Left(t, len, out right1);

			byte[] left2;
			byte[] right2;

			bool s = ByteArray.Split(t, len, out left2, out right2);

			Assert.IsTrue(left1.SequenceEqual(left2));
			Assert.IsTrue(right1.SequenceEqual(right2));
			Assert.IsTrue(s);
		}
	}
}