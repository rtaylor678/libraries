﻿
using Solomon.Common.Utility;
using System;
using System.Data.Entity;
using System.Data.Entity.Validation;

namespace Solomon.Common.Repository
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private DbContext context;

        public UnitOfWork(DbContext context)
        {
            if (context == null)
                throw new ArgumentNullException("context");

            this.context = context;
            this.IsDisposed = false;
        }

        public IRepository<TEntity> GetRepository<TEntity>() where TEntity : class
        {
            return new Repository<TEntity>(this.context);
        }

        public bool SaveChanges()
        {
            try
            {
                return this.context.SaveChanges() > 0;
            }
            catch (DbEntityValidationException ex)
            {
                foreach (var error in ex.EntityValidationErrors)
                {
                    Log.Instance().Error(string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        error.Entry.Entity.GetType().Name, error.Entry.State), ex);

                    foreach (var validationError in error.ValidationErrors)
                    {
                        Log.Instance().Error(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                            validationError.PropertyName, validationError.ErrorMessage), ex);
                    }
                }
                return false;
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (!IsDisposed && context != null)
            {
                IsDisposed = true;
                context.Dispose();
            }
        }

        public bool IsDisposed
        {
            get;
            private set;
        }

        #endregion
    }
}
