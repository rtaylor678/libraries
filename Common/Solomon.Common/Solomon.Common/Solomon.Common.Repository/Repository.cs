﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq.Expressions;
using System.Collections;
using System.Reflection;
using System.Data.SqlClient;



namespace Solomon.Common.Repository
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        #region Private members
        
        private DbContext context;
        private DbSet<TEntity> entityInstance;        

        #endregion

        #region Constructors

        public Repository(DbContext context)
        {
            this.context = context;
        }

        #endregion

        #region Properties

        public int Count
        {
            get
            {
                return this.GetCount();
            }
        }

        public DbContext Context
        {
            get
            {
                return context;
            }
        }

        public DbSet<TEntity> EntityInstance
        {
            get
            {
                if (this.entityInstance == null)
                    this.entityInstance = context.Set<TEntity>();

                return this.entityInstance;
            }
        }

        #endregion 
       
        #region Public Methods

        public IEnumerable<TEntity> FindBy(Expression<Func<TEntity, bool>> predicate)
        {
            return EntityInstance.Where(predicate);
        }

        public IQueryable<TEntity> GetAll(Expression<Func<TEntity, bool>> where)
        {
            return EntityInstance.Where(where);
        }

        public void Add(TEntity entity)
        {
            EntityInstance.Add(entity); 
            
        }

        public int Add(IEnumerable<TEntity> entities)
        {
            foreach (TEntity entity in entities)
            {
                EntityInstance.Add(entity);
            }

            return entities.Count();
        }

        public void Update(TEntity entity)
        {
            EntityInstance.Attach(entity);
            context.Entry<TEntity>(entity).State = EntityState.Modified;
        }

        public void Delete(TEntity entity)
        {
            EntityInstance.Remove(entity);
        }

        public IQueryable<TEntity> GetAll()
        {
            return EntityInstance;
        }

        public TEntity Get(Expression<Func<TEntity, Boolean>> where)
        {
            return EntityInstance.FirstOrDefault(where);
        }

        public int GetCount()
        {
            return EntityInstance.Count();
        }

        public int GetCount(Expression<Func<TEntity, bool>> expression)
        {
            return EntityInstance.Where(expression).Count();
        }

        public int ReplaceAll(IEnumerable<TEntity> entities)
        {
            this.DeleteAll();

            int addCount = this.Add(entities);

            return addCount;
        }

        private int DeleteAll()
        {
            int deleteCount = this.GetCount();

            IQueryable<TEntity> entities = this.GetAll();

            foreach (TEntity entity in entities)
            {
                this.Delete(entity);
            }

            return deleteCount;
        }

        public IEnumerator<TEntity> GetEnumerator()
        {
            return EntityInstance.AsEnumerable().GetEnumerator();
        }

        IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return EntityInstance.AsEnumerable().GetEnumerator();
        }        

        #endregion                       

        #region Private Methods
        public int ExecuteSqlCommand(string storedProcedure, object parameters = null)
        {
            if (string.IsNullOrEmpty(storedProcedure))
                throw new ArgumentException("storedProcedure");

            var arguments = PrepareArguments(storedProcedure, parameters);
            return this.context.Database.ExecuteSqlCommand(arguments.Item1, arguments.Item2);
        }

        public IEnumerable<TElement> SqlQuery<TElement>(string storedProcedure, object parameters = null)
        {
            if (string.IsNullOrEmpty(storedProcedure))
                throw new ArgumentException("storedProcedure");

            var arguments = PrepareArguments(storedProcedure, parameters);
            return this.context.Database.SqlQuery<TElement>(arguments.Item1, arguments.Item2);
        }

        public IEnumerable SqlQuery(Type returnType, string storedProcedure, object parameters = null)
        {
            if (returnType == null)
                throw new ArgumentNullException("returnType");

            if (string.IsNullOrEmpty(storedProcedure))
                throw new ArgumentException("storedProcedure");

            var arguments = PrepareArguments(storedProcedure, parameters);
            return this.context.Database.SqlQuery(returnType, arguments.Item1, arguments.Item2);
        }

        private Tuple<string, object[]> PrepareArguments(string storedProcedure, object parameters)
        {
            var parameterNames = new List<string>();
            var parameterParameters = new List<object>();

            if (parameters != null)
            {
                foreach (PropertyInfo propertyInfo in parameters.GetType().GetProperties())
                {
                    string name = "@" + propertyInfo.Name;
                    object value = propertyInfo.GetValue(parameters, null);

                    parameterNames.Add(name);
                    parameterParameters.Add(new SqlParameter(name, value ?? DBNull.Value));
                }
            }

            if (parameterNames.Count > 0)
                storedProcedure += " " + string.Join(", ", parameterNames);

            return new Tuple<string, object[]>(storedProcedure, parameterParameters.ToArray());
        }
        #endregion
    }
}
