﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Solomon.Common.Repository
{
    public interface IRepository<E> : IEnumerable<E> where E : class
    {
        int Count { get; }
        void Add(E entity);
        int Add(IEnumerable<E> entities);
        void Update(E entity);
        void Delete(E entity);

        E Get(Expression<Func<E, Boolean>> expression);
        int GetCount();
        int GetCount(Expression<Func<E, Boolean>> expression);
        int ReplaceAll(IEnumerable<E> entities);
        
        IEnumerable<E> FindBy(Expression<Func<E, bool>> predicate);
        IQueryable<E> GetAll(Expression<Func<E, bool>> where);
        IQueryable<E> GetAll();
    }
}
