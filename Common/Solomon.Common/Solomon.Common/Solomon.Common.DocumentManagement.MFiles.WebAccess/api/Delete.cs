﻿using Solomon.Common.DocumentManagement.MFiles.WebAccess.Mfws;

namespace Solomon.Common.DocumentManagement.MFiles.WebAccess
{
	public class Delete
	{
		/// <summary>
		/// Self Populating Constructor
		/// </summary>
		/// <param name="client"> </param>
		/// <param name="api"> Parent Api </param>
        public Delete(Mfws.MfwsClient client, Api api)
		{
			Api = api;
			Client = client;
		}

		/// <summary>
		/// Performs an Undo Checkout
		/// </summary>
		/// <param name="type"> ObjectType </param>
		/// <param name="id"> Object ID </param>
		/// <param name="version"> Object Version </param>
		public void UndoCheckout( int type, int id, int version )
		{
			Client.Delete( _Delete.Object( type, id, version, true, false ) );
		}

		/// <summary>
		/// Performs an Undo Checkout
		/// </summary>
		/// <param name="ov"> Object Version </param>
		public void UndoCheckout( ObjectVersion ov )
		{
			Client.Delete( _Delete.Object( ov.ObjVer.Type, ov.ObjVer.ID, ov.ObjVer.Version, true, false ) );
		}

		/// <summary>
		/// Deletes a Property from an Object
		/// </summary>
		/// <param name="propID"> Property ID to Delete </param>
		/// <param name="type"> Object Type </param>
		/// <param name="id"> Object ID </param>
		/// <param name="version"> Object Version </param>
		/// <returns> </returns>
		public ExtendedObjectVersion Property( int propID, int type, int id, int version = -1 )
		{
			Client.Delete( _Get.Property( type, id, version, propID ) );

			// return the updated object version
			return Api.Get.ObjectVersion( type, id );
		}

		/// <summary>
		/// Deletes a Property from an Object
		/// </summary>
		/// <param name="objVer"> Target ObjVer </param>
		/// <param name="propID"> Property ID to Remove </param>
		/// <returns>
		/// <see cref="RestTools.ExtendedObjectVersion" />
		/// </returns>
		public ExtendedObjectVersion Property( ObjVer objVer, int propID )
		{
			Client.Delete( _Get.Property( objVer.Type, objVer.ID, objVer.Version, propID ) );

			// return the updated object version
			return Api.Get.ObjectVersion( objVer.Type, objVer.ID );
		}

		/// <summary>
		/// Performs a logout for the session.
		/// </summary>
		public void Session()
		{
			Client.Delete( _Get.SessionInfo );
		}

		/// <summary>
		/// Deletes a Value List Item from a Value List.
		/// </summary>
		public void ValueListItem( int listId, int itemId )
		{
			Client.Delete( _Get.StructureValueListItem( listId, itemId ) );
		}

		/// <summary>
		/// Removes an Object from Favorites.
		/// </summary>
		public void Favorite( int type, int id )
		{
			Api.Client.Delete( _Get.Favorite( type, id ) );
		}

		/// <summary>
		/// Removed an Object from Favorites.
		/// </summary>
		public void Favorite( ObjVer objVer )
		{
			Api.Client.Delete( _Get.Favorite( objVer.Type, objVer.ID ) );
		}

		/// <summary>
		/// Removed an Object from Favorites.
		/// </summary>
		public void Favorite( ObjectVersion ov )
		{
			Api.Client.Delete( _Get.Favorite( ov.ObjVer.Type, ov.ObjVer.ID ) );
		}

		#region Private Items

		/// <summary>
		/// M-Files Rest API Client Helper Object.  Used privately for this class.
		/// </summary>
		private MfwsClient Client { get; set; }

		/// <summary>
		/// Parent Api instance for internal use.
		/// </summary>
		private Api Api { get; set; }

		#endregion
	}
}