﻿using Solomon.Common.DocumentManagement.MFiles.WebAccess.Mfws;

namespace Solomon.Common.DocumentManagement.MFiles.WebAccess
{
	/// <summary>
	/// M-Files REST API - Wrapper Class
	/// </summary>
	public class Api : IApi
	{
		/// <summary>
		/// Self Populating Constructor
		/// </summary>
		/// <param name="baseUrl"> Base URL - REST Endpoint </param>
		/// <param name="username"> Username Credential </param>
		/// <param name="password"> Password Credential </param>
		/// <param name="vaultGuid"> GUID of the target Vault </param>
		public Api( string baseUrl, string username, string password, string vaultGuid )
		{
			Client = new Solomon.Common.DocumentManagement.MFiles.WebAccess.Mfws.MfwsClient( baseUrl );
			Client.Connect( username, password, vaultGuid );
			Get = new Get( Client, this );
			Set = new Set( Client, this );
			Delete = new Delete( Client, this );
			Create = new Create( Client, this );
			CreateInstance = new CreateInstance( Client, this );
			Structure = new Structure( Client, this );
		}

		/// <summary>
		/// Returns the Session Info Based on the User Logged into the Vault.
		/// </summary>
		/// <returns> SessionInfo </returns>
		public Vault Vault
		{
			get { return Client.Get<Vault>( _Get.Vault ); }
		}

		/// <summary>
		/// M-Files Rest API Client Helper Object.  Used privately for this class.
		/// </summary>
		public MfwsClient Client { get; set; }

		/// <summary>
		/// Vault Retrieval Operations
		/// </summary>
		public Get Get { get; set; }

		/// <summary>
		/// Vault Deletion Operations
		/// </summary>
		public Delete Delete { get; set; }

		/// <summary>
		/// Vault Set or Post Operations
		/// </summary>
		public Set Set { get; set; }

		/// <summary>
		/// Creation of Objects in the Vault
		/// </summary>
		public Create Create { get; set; }

		/// <summary>
		/// Object Instance Creation
		/// </summary>
		public CreateInstance CreateInstance { get; set; }

		/// <summary>
		/// Vault Structure Operations
		/// </summary>
		public Structure Structure { get; set; }

		public static Api Login( string baseUrl, string username, string password, string vaultGuid )
		{
			return new Api( baseUrl, username, password, vaultGuid );
		}

		/// <summary>
		/// Aliased Session() -- Logout
		/// </summary>
		public void Logout()
		{
			Delete.Session();
		}

		/// <summary>
		/// Performs an Undo Checkout
		/// </summary>
		public void UndoCheckout( ObjectVersion ov )
		{
			Delete.UndoCheckout( ov );
		}

		/// <summary>
		/// Performs an Undo Checkout
		/// </summary>
		public void UndoCheckout( int type, int id, int version )
		{
			Delete.UndoCheckout( type, id, version );
		}

		/// <summary>
		/// Performs a Checkout
		/// </summary>
		/// <param name="objVer">
		/// <see cref="ObjVer" />
		/// </param>
		/// <returns>
		/// <see cref="RestTools.ObjectVersion" />
		/// </returns>
		public ObjectVersion CheckOut( ObjVer objVer )
		{
			return Set.CheckOutStatus( objVer, MFCheckOutStatus.CheckedOutToMe );
		}

		/// <summary>
		/// Performs a Checkout
		/// </summary>
		/// <param name="type"> Object Type </param>
		/// <param name="id"> Object ID </param>
		/// <param name="version"> Object Version </param>
		/// <returns>
		/// <see cref="RestTools.ObjectVersion" />
		/// </returns>
		public ObjectVersion CheckOut( int type, int id, int version = -1 )
		{
			return Set.CheckOutStatus( type, id, version, MFCheckOutStatus.CheckedOutToMe );
		}

		/// <summary>
		/// Performs a Check in
		/// </summary>
		/// <param name="objVer"> ObjVer of Target </param>
		/// <returns> </returns>
		public ObjectVersion CheckIn( ObjVer objVer )
		{
			return Set.CheckOutStatus( objVer, MFCheckOutStatus.CheckedIn );
		}

		/// <summary>
		/// Performs a Check in
		/// </summary>
		/// <param name="type"> Object Instance Type </param>
		/// <param name="id"> Object Instance ID </param>
		/// <param name="version"> Object Instance Version </param>
		/// <returns> </returns>
		public ObjectVersion CheckIn( int type, int id, int version = -1 )
		{
			return Set.CheckOutStatus( type, id, version, MFCheckOutStatus.CheckedIn );
		}
	}
}