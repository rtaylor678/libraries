﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Solomon.Common.DocumentManagement.MFiles.WebAccess.Mfws;

namespace Solomon.Common.DocumentManagement.MFiles.WebAccess
{
	/// <summary>
	/// Helper Class for Easily Creating a New Items in the Vault
	/// </summary>
	public class Create
	{
		/// <summary>
		/// Self Populating Constructor
		/// </summary>
		/// <param name="client"> Pre-Instantiated M-Files Rest Client Object </param>
		/// <param name="api"> Parent Api </param>
		public Create( MfwsClient client, Api api )
		{
			Api = api;
			Client = client;
		}

		/// <summary>
		/// Transient resource used in calculating new authentication tokens.
		/// </summary>
		/// <returns> string </returns>
        public string AuthenticationToken(Authentication auth)
		{
			return Client.Post<PrimitiveType<string>>( @"server/authenticationtokens", auth ).Value;
		}

		/// <summary>
		/// Upload Info Object for use in Object Creation via CreationInfo.
		/// </summary>
		/// <returns>
		/// <see cref="UploadInfo" />
		/// </returns>
		public UploadInfo UploadInfo( string filepath )
		{
			using( var fileStream = File.OpenRead( filepath ) )
			{
				var memStream = new MemoryStream();
				memStream.SetLength( fileStream.Length );
				fileStream.Read( memStream.GetBuffer(), 0, ( int ) fileStream.Length );

				return Client.Post<UploadInfo>( _Set.TemporaryFiles, fileStream );
			}
		}

		/// <summary>
		/// Upload Info Object for use in Object Creation via CreationInfo.
		/// </summary>
		/// <returns> <see cref="UploadInfo" />[] </returns>
		public UploadInfo [] UploadInfoCollection( string [] filepaths )
		{
			// create a temp list
			var results = new List<UploadInfo>();

			// loop over the file path array
			foreach( var path in filepaths )
			{
				// Add Each as a new UploadInfo to the List
				results.Add( UploadInfo( path ) );
			}

			// Convert to an Array and return the results
			return results.ToArray();
		}

		/// <summary>
		/// Creates a new Value List Item Object Instance
		/// </summary>
		/// <param name="listId"> Value List ID </param>
		/// <param name="value"> Value to Set on Item </param>
		/// <returns> </returns>
		public ValueListItem ValueListItem( int listId, string value )
		{
			var item = new ValueListItem { Name = value };

			return Client.Post<ValueListItem>( _Get.StructureValueListItems( listId ), item );
		}

		/// <summary>
		/// This method will create a new single file object in the vault.
		/// </summary>
		/// <param name="type">Object ID</param>
		/// <param name="classId">Class ID</param>
		/// <param name="filePath">Filepath</param>
		/// <param name="props">Additional Properties to add to the object</param>
		/// <returns></returns>
		public ObjectVersion CreateSingleFileObject(bool newDoc, int type, int classId, string filePath, PropertyValue[] props = null )
		{
			// Use file from the disk.
			FileInfo file = new FileInfo( filePath );

			// Open file for reading.
			FileStream fileStream = File.OpenRead(file.FullName);

			// Upload the file to M-Files as temporary file upload.
			UploadInfo uploadInfo = FileOnServer( fileStream );

			// Add the title and the extension without dot to the upload info.
			uploadInfo.Title = Path.GetFileNameWithoutExtension( filePath );
			uploadInfo.Extension = file.Extension.Replace(".", "");  // Remove dot.

			// Create a new ObjectCreationInfo object

            ObjectCreationInfo oci = null; // Api.CreateInstance.ObjectCreationInfo(classId, uploadInfo.Title, true);
            if (newDoc)
            {
                oci = Api.CreateInstance.ObjectCreationInfoClassVersionMinus1(classId, uploadInfo.Title, true);
            }
            else
            {
                 oci = Api.CreateInstance.ObjectCreationInfoNewVersion(classId, uploadInfo.Title, true);
            }
			List<PropertyValue> additionalProps = oci.PropertyValues.ToList();

			// add any additional props
			if( props != null )
			{
				additionalProps.AddRange(props);
				oci.PropertyValues = additionalProps.ToArray();
			}

			// populate the files
			oci.Files = new [] { uploadInfo };

            try
            {
                fileStream.Close();
            }
            catch { }

			// Make the request. Document has the object type id of zero.
			return Client.Post<ObjectVersion>(_Set.CreateObject(type), oci );
		}

        /// <summary>
        /// This method will create a new single file object (avoiding 'version 0' problem) in the vault.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="classId"></param>
        /// <param name="filePath"></param>
        /// <param name="props"></param>
        /// <returns></returns>
        public ObjectVersion CreateSingleFileObjectClassVersionMinus1(int type, int classId, string filePath, PropertyValue[] props = null)
        {
            // Use file from the disk.
            FileInfo file = new FileInfo(filePath);

            // Open file for reading.
            FileStream fileStream = File.OpenRead(file.FullName);

            // Upload the file to M-Files as temporary file upload.
            UploadInfo uploadInfo = FileOnServer(fileStream);

            // Add the title and the extension without dot to the upload info.
            uploadInfo.Title = Path.GetFileNameWithoutExtension(filePath);
            uploadInfo.Extension = file.Extension.Replace(".", "");  // Remove dot.

            // Create a new ObjectCreationInfo object
            //ObjectCreationInfo oci = Api.CreateInstance.ObjectCreationInfo(classId, uploadInfo.Title, true);
            ObjectCreationInfo oci = Api.CreateInstance.ObjectCreationInfoClassVersionMinus1(classId, uploadInfo.Title, true);
            List<PropertyValue> additionalProps = oci.PropertyValues.ToList();

            // add any additional props
            if (props != null)
            {
                additionalProps.AddRange(props);
                oci.PropertyValues = additionalProps.ToArray();
            }

            // populate the files
            oci.Files = new[] { uploadInfo };
            try
            {
                fileStream.Close();
            }
            catch { }
            // Make the request. Document has the object type id of zero.
            return Client.Post<ObjectVersion>(_Set.CreateObject(type), oci);
        }

        /// <summary>
        /// This method will create a new multi file object in the vault.
        /// </summary>
        /// <param name="type">Object ID</param>
        /// <param name="classId">Class ID</param>
        /// <param name="filePaths">Collection of File paths</param>
        /// <param name="title">Object Title</param>
        /// <param name="props">Additional Properties to add to the object</param>
        /// <returns></returns>
        public ObjectVersion CreateMultiFileObject( int type, int classId, string[] filePaths, string title, PropertyValue[] props = null )
		{
            throw new System.NotImplementedException();
            /*
			List<UploadInfo> files = new List<UploadInfo>();

			// Use files from the disk.
			foreach( string filePath in filePaths )
			{
				FileInfo file = new FileInfo( filePath );				
				// Open file for reading.
				FileStream fileStream = File.OpenRead(file.FullName);

				// Upload the file to M-Files as temporary file upload.
				UploadInfo uploadInfo = FileOnServer( fileStream );

				// Add the title and the extension without dot to the upload info.
				uploadInfo.Title = Path.GetFileNameWithoutExtension( filePath );
				uploadInfo.Extension = file.Extension.Replace(".", "");  // Remove dot.

				// add to the collection
				files.Add( uploadInfo );
			}

			// Create a new ObjectCreationInfo object
			ObjectCreationInfo oci = Api.CreateInstance.ObjectCreationInfo( classId, title, false );
			List<PropertyValue> additionalProps = oci.PropertyValues.ToList();

			// add any additional props
			if( props != null )
			{
				additionalProps.AddRange(props);
				oci.PropertyValues = additionalProps.ToArray();
			}

			// populate the files
			oci.Files = files.ToArray();

			// Make the request. Document has the object type id of zero.
			return Client.Post<ObjectVersion>(_Set.CreateObject(type), oci );
            */
		}

		/// <summary>
		/// Uploads a file to M-Files as a temporary upload.
		/// </summary>
		/// <param name="stream">The file stream to upload.</param>
		/// <returns>The upload info of the temporary upload.</returns>
		private UploadInfo FileOnServer(Stream stream)
		{
			// Upload the file.
			return Client.Post<UploadInfo>(_Set.TemporaryFiles, stream);
		}

		#region Private Items

		/// <summary>
		/// M-Files Rest API Client Helper Object.  Used privately for this class.
		/// </summary>
		private MfwsClient Client { get; set; }

		/// <summary>
		/// Parent Api instance for private use.
		/// </summary>
		private Api Api { get; set; }

		#endregion
	}
}