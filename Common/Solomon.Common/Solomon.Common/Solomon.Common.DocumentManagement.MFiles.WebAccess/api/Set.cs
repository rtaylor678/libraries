﻿using System.IO;
using Solomon.Common.DocumentManagement.MFiles.WebAccess.Mfws;

namespace Solomon.Common.DocumentManagement.MFiles.WebAccess
{
	/// <summary>
	/// Wrapper Class for the M-Files API. Returns Pre-Cast Objects from a M-Files Web Service Request.
	/// </summary>
	public class Set
	{
		/// <summary>
		/// Self Populating Constructor
		/// </summary>
		/// <param name="client"> </param>
		/// <param name="api"> Parent Api </param>
		public Set( MfwsClient client, Api api )
		{
			Api = api;
			Client = client;
		}

		/// <summary>
		/// Sets the Deleted Status of an Object.
		/// <param name="type"> Object Type </param>
		/// <param name="id"> Object ID </param>
		/// <param name="isDeleted"> Value to Set </param>
		/// </summary>
		/// <returns>
		/// <see cref="RestTools.ObjectVersion" />
		/// </returns>
		public ObjectVersion DeletedStatus( int type, int id, bool isDeleted )
		{
			// We set the newStatus.Value = isDeleted
			var newStatus = new PrimitiveType<bool> { Value = isDeleted };

			// Now we use the isDeleted value of to PUT (aka Set), the value on the Object
			var ov = Client.Put<ObjectVersion>( _Set.DeletedStatus( type, id ), newStatus );

			// Return the updated Object Version
			return ov;
		}

		/// <summary>
		/// Sets a Comment on an Object.
		/// <param name="type"> Object Type </param>
		/// <param name="id"> Object ID </param>
		/// <param name="version"> Object Version </param>
		/// <param name="comment"> Comment Text </param>
		/// </summary>
		/// <returns>
		/// <see cref="RestTools.ExtendedObjectVersion" />
		/// </returns>
		public ExtendedObjectVersion ObjectComment( int type, int id, int version, string comment )
		{
			return Client.Put<ExtendedObjectVersion>( _Get.ObjectComments( type, id, version ),
				new PrimitiveType<string> { Value = comment } );
		}

		/// <summary>
		/// Sets the Objects Title
		/// </summary>
		/// <param name="type"> Object Type </param>
		/// <param name="id"> Object ID </param>
		/// <param name="version"> Object Version </param>
		/// <param name="title"> Value to Set </param>
		/// <returns>
		/// <see cref="RestTools.ObjectVersion" />
		/// </returns>
		public ObjectVersion ObjectTitle( int type, int id, int version, string title )
		{
			return Client.Put<ObjectVersion>( _Get.ObjectTitle( type, id, version ), title );
		}

		/// <summary>
		/// Sets the Objects Workflow State
		/// </summary>
		/// <param name="type"> Object Type </param>
		/// <param name="id"> Object ID </param>
		/// <param name="version"> Object Version </param>
		/// <param name="wfState"> The ObjectWorkflowState to Set </param>
		/// <returns> </returns>
		public ExtendedObjectVersion ObjectWorkflowState( int type, int id, int version, ObjectWorkflowState wfState )
		{
			return Client.Put<ExtendedObjectVersion>( _Get.ObjectWorkflowState( type, id, version ), wfState );
		}

		/// <summary>
		/// Sets properties on the object. If the object already has a value for the passed properties this value will be overridden.
		/// </summary>
		/// <param name="type"> Object Type </param>
		/// <param name="id"> Object ID </param>
		/// <param name="version"> Object Version </param>
		/// <param name="propertyValues"> <see cref="PropertyValue" />[] to Set </param>
		/// <returns>
		/// <see cref="RestTools.ExtendedObjectVersion" />
		/// </returns>
		public ExtendedObjectVersion Properties( int type, int id, int version, PropertyValue [] propertyValues )
		{
			return Client.Post<ExtendedObjectVersion>( _Get.Properties( type, id, version, false ), propertyValues );
		}

		/// <summary>
		/// Sets the object properties. Properties not included in the request are removed from the object.
		/// </summary>
		/// <param name="type"> Object Type </param>
		/// <param name="id"> Object ID </param>
		/// <param name="version"> Object Version </param>
		/// <param name="propertyValues"> <see cref="PropertyValue" />[] to Set </param>
		/// <returns>
		/// <see cref="RestTools.ExtendedObjectVersion" />
		/// </returns>
		public ExtendedObjectVersion PropertiesEx( int type, int id, int version, PropertyValue [] propertyValues )
		{
			return Client.Put<ExtendedObjectVersion>( _Get.Properties( type, id, version, false ), propertyValues );
		}

		/// <summary>
		/// Sets a Property on an Object.
		/// </summary>
		/// <param name="type"> Object Type </param>
		/// <param name="id"> Object ID </param>
		/// <param name="version"> Object Version </param>
		/// <param name="propertyValue"> <see cref="PropertyValue" /> to Set </param>
		/// <returns>
		/// <see cref="RestTools.ExtendedObjectVersion" />
		/// </returns>
		public ExtendedObjectVersion Property( int type, int id, int version, PropertyValue propertyValue )
		{
			return Client.Put<ExtendedObjectVersion>( _Get.Property( type, id, version, propertyValue.PropertyDef ),
				propertyValue );
		}

		/// <summary>
		/// Sets the Property on an Object.
		/// </summary>
		/// <param name="value"> Un-cast Property Value </param>
		/// <param name="type"> Object Type </param>
		/// <param name="id"> Object ID </param>
		/// <param name="version"> Object Version </param>
		/// <param name="propID"> Property ID </param>
		/// <returns>
		/// <see cref="RestTools.ExtendedObjectVersion" />
		/// </returns>
		public ExtendedObjectVersion Property( int propID, dynamic value, int type, int id, int version = -1 )
		{
			PropertyValue prop = Api.CreateInstance.PropertyValue( propID, value );
			return Client.Put<ExtendedObjectVersion>( _Get.Property( type, id, version, propID ), prop );
		}

		/// <summary>
		/// Sets the Property on an Object.
		/// </summary>
		/// <param name="objVer"> Target ObjVer </param>
		/// <param name="propertyValue"> PropertyValues to Set </param>
		/// <returns>
		/// <see cref="RestTools.ExtendedObjectVersion" />
		/// </returns>
		public ExtendedObjectVersion Property( ObjVer objVer, PropertyValue propertyValue )
		{
			return
				Client.Put<ExtendedObjectVersion>(
					_Get.Property( objVer.Type, objVer.ID, objVer.Version, propertyValue.PropertyDef ), propertyValue );
		}

		/// <summary>
		/// Sets the Property on an Object.
		/// </summary>
		/// <param name="propID"> Property ID </param>
		/// <param name="value"> Un-cast Property Value </param>
		/// <param name="objVer"> Target ObjVer </param>
		/// <returns>
		/// <see cref="RestTools.ExtendedObjectVersion" />
		/// </returns>
		public ExtendedObjectVersion Property( int propID, dynamic value, ObjVer objVer )
		{
			PropertyValue prop = Api.CreateInstance.PropertyValue( propID, value );
			return Client.Put<ExtendedObjectVersion>( _Get.Property( objVer.Type, objVer.ID, objVer.Version, propID ), prop );
		}

		/// <summary>
		/// Sets the Checked Out Status of an Object
		/// </summary>
		/// <param name="type"> Object Type </param>
		/// <param name="id"> Object ID </param>
		/// <param name="version"> Object Version </param>
		/// <param name="status">
		/// <see cref="MFCheckOutStatus" />
		/// </param>
		/// <returns>
		/// <see cref="RestTools.ObjectVersion" />
		/// </returns>
		public ObjectVersion CheckOutStatus( int type, int id, int version, MFCheckOutStatus status )
		{
			return Client.Put<ObjectVersion>( _Get.ObjectCheckoutStatus( type, id, version ),
				new PrimitiveType<MFCheckOutStatus> { Value = status } );
		}

		/// <summary>
		/// Sets the Checked Out Status of an Object
		/// </summary>
		/// <param name="objVer"> ObjVer of Target </param>
		/// <param name="status">
		/// <see cref="MFCheckOutStatus" />
		/// </param>
		/// <returns>
		/// <see cref="RestTools.ObjectVersion" />
		/// </returns>
		public ObjectVersion CheckOutStatus( ObjVer objVer, MFCheckOutStatus status )
		{
			return Client.Put<ObjectVersion>( _Get.ObjectCheckoutStatus( objVer.Type, objVer.ID, objVer.Version ),
				new PrimitiveType<MFCheckOutStatus> { Value = status } );
		}

		/// <summary>
		/// Multi-File Objects Only: Sets the ObjectFile for the given Object Instance.
		/// </summary>
		/// <param name="stream"> Stream of File to Set </param>
		/// <param name="type"> Object Type </param>
		/// <param name="id"> Object ID </param>
		/// <param name="version"> Object Version </param>
		/// <returns>
		/// <see cref="RestTools.ObjectVersion" />
		/// </returns>
		public ObjectVersion Files( Stream stream, int type, int id, int version = -1 )
		{
			return Api.Client.Post<ObjectVersion>( _Get.Files( type, id, version ), stream );
		}

		/// <summary>
		/// Multi-File Objects Only: Sets the ObjectFile for the given Object Instance.
		/// </summary>
		/// <param name="filePath"> Local filepath </param>
		/// <param name="type"> Object Type </param>
		/// <param name="id"> Object ID </param>
		/// <param name="version"> Object Version </param>
		/// <returns>
		/// <see cref="RestTools.ObjectVersion" />
		/// </returns>
		public ObjectVersion File( string filePath, int type, int id, int version = -1 )
		{
			ObjectVersion result = null;
			using( Stream stream = System.IO.File.OpenRead( filePath ) )
			{
				result = Api.Client.Post<ObjectVersion>( _Get.Files( type, id, version ), stream );
			}

			return result;
		}

		/// <summary>
		/// Multi-File Objects Only: Sets the ObjectFile for the given Object Instance.
		/// </summary>
		/// <param name="filePath"> local filepath </param>
		/// <param name="objVer"> Target ObjVer </param>
		/// <param name="macformatting"> Should this be formatted for a Mac? </param>
		/// <returns> </returns>
		public ObjectVersion Files( string filePath, int fileID, ObjVer objVer, bool macformatting = false )
		{
			ObjectVersion result = null;
			using( Stream stream = System.IO.File.OpenRead( filePath ) )
			{
				result = Api.Client.Post<ObjectVersion>( _Get.Files( objVer.Type, objVer.ID, objVer.Version ), stream );
			}

			return result;
		}

		/// <summary>
		/// Sets the ObjectFile for the existing FileID on the given Object Instance.
		/// </summary>
		/// <param name="filePath"> local filepath </param>
		/// <param name="fileID"> FileID of the File to Modify </param>
		/// <param name="type"> Object Instance Type </param>
		/// <param name="id"> Object Instance ID </param>
		/// <param name="version"> Object Instance Version </param>
		/// <returns>
		/// <see cref="RestTools.ObjectVersion" />
		/// </returns>
		public ObjectVersion File( string filePath, int fileID, int type, int id, int version )
		{
			ObjectVersion result = null;
			using( Stream stream = System.IO.File.OpenRead( filePath ) )
			{
				result = Api.Client.Post<ObjectVersion>( _Get.ObjectFileContent( type, id, version, fileID, false ), stream );
			}

			return result;
		}

		/// <summary>
		/// Single-File Objects Only: Sets the ObjectFile for the given Object Instance.
		/// </summary>
		/// <param name="filePath"> local filepath </param>
		/// <param name="objVer"> Target ObjVer </param>
		/// <returns>
		/// <see cref="RestTools.ObjectVersion" />
		/// </returns>
		public ObjectVersion File( string filePath, ObjVer objVer )
		{
			ObjectVersion result = null;
			using( Stream stream = System.IO.File.OpenRead( filePath ) )
			{
				result = File( stream, objVer );
			}

			return result;
		}

		/// <summary>
		/// Single-File Objects Only: Sets the ObjectFile for the given Object Instance.
		/// </summary>
		/// <param name="stream"> local filepath </param>
		/// <param name="objVer"> Target ObjVer </param>
		/// <returns>
		/// <see cref="RestTools.ObjectVersion" />
		/// </returns>
		public ObjectVersion File( Stream stream, ObjVer objVer )
		{
			var result = Api.Client.Post<ObjectVersion>( _Get.Files( objVer.Type, objVer.ID, objVer.Version ), stream );
			return result;
		}

        /// <summary>
		/// Performs a Login
		/// </summary>
		/// <param name="client"> MfwsClient </param>
		/// <param name="auth"> Authentication Object </param>
        /// <returns> <see cref="RestTools.Vault" />[] </returns> //Solomon.Common.DocumentManagement.MFiles.WebAccess.Structs.Authentication auth)
        public static Vault[] SessionAuthentication(MfwsClient client, Authentication auth)
		{
			return client.Put<Vault []>( _Set.SessionInfo, auth );
		}

		/// <summary>
		/// Sets the current vault.
		/// </summary>
		/// <param name="vault"> </param>
		/// <returns>
		/// <see cref="RestTools.Vault" />
		/// </returns>
		public Vault SessionVault( Vault vault )
		{
			MfwsClient newSessionClient = Client.Clone();
			return newSessionClient.Put<Vault>( _Set.Vault, vault );
		}

		/// <summary>
		/// Sets a Value List Item Name
		/// </summary>
		/// <param name="listId"> Value List ID </param>
		/// <param name="itemId"> Value List Item ID </param>
		/// <param name="name"> Title to Set </param>
		/// <returns>
		/// <see cref="RestTools.ValueListItem" />
		/// </returns>
		public ValueListItem ValueListItemName( int listId, int itemId, string name )
		{
			return Client.Put<ValueListItem>( _Get.StructureValueListItemTitle( listId, itemId ), name );
		}

		/// <summary>
		/// Adds an Object to the Recently Accessed By me View.
		/// </summary>
		/// <param name="objid">
		/// <see cref="ObjID" />
		/// </param>
		/// <returns> </returns>
		public ExtendedObjectVersion RecentlyAccessedByMe( ObjID objid )
		{
			return Client.Post<ExtendedObjectVersion>( _Get.RecentlyAccessedByMe, objid );
		}

		/// <summary>
		/// Adds an Object to the Recently Accessed By me View.
		/// </summary>
		/// <param name="type"> Object Type </param>
		/// <param name="id"> Object ID </param>
		/// <returns>
		/// <see cref="RestTools.ExtendedObjectVersion" />
		/// </returns>
		public ExtendedObjectVersion RecentlyAccessedByMe( int type, int id )
		{
			var objid = new ObjID { ID = id, Type = type };
			return RecentlyAccessedByMe( objid );
		}

		#region Private Items

		/// <summary>
		/// M-Files Rest API Client Helper Object.  Used privately for this class.
		/// </summary>
		private MfwsClient Client { get; set; }

		/// <summary>
		/// Parent Api instance for internal use.
		/// </summary>
		private Api Api { get; set; }

		#endregion
	}
}