﻿using System.IO;
using Solomon.Common.DocumentManagement.MFiles.WebAccess.Mfws;

namespace Solomon.Common.DocumentManagement.MFiles.WebAccess
{
	public class Structure
	{
		/// <summary>
		/// Object Type Filter Enum
		/// </summary>
		public enum ObjectTypeFilter
		{
			none,
			real,
			valuelist,
			both
		}

		/// <summary>
		/// Self Populating Constructor
		/// </summary>
		/// <param name="client"> </param>
		/// <param name="api"> Parent Api </param>
        public Structure(Mfws.MfwsClient client, Api api)
		{
			Api = api;
			Client = client;
		}

		/// <summary>
		/// M-Files Rest API Client Helper Object.  Used privately for this class.
		/// </summary>
		private MfwsClient Client { get; set; }

		/// <summary>
		/// Parent Api instance for internal use.
		/// </summary>
		private Api Api { get; set; }

		/// <summary>
		/// Collection of object type information.
		/// Internally M-Files considers both valuelists and the real object types as object types.
		/// By default this resource will return both kinds of types. If you want only one kind use the type-parameter.
		/// </summary>
		/// <param name="filter"> real returns only real object types. valuelist returns only non-object type valuelists. Both is the default which returns all object types </param>
		/// <returns> </returns>
		public ObjType [] ObjectTypes( ObjectTypeFilter filter = ObjectTypeFilter.none )
		{
			return Client.Get<ObjType []>( _Get.StructureObjectTypes( filter.ToString() ) );
		}

		/// <summary>
		/// Information on a single object type.
		/// </summary>
		/// <param name="id"> Object Type ID </param>
		/// <returns> </returns>
		public ObjType ObjectType( int id )
		{
			return Client.Get<ObjType>( _Get.StructureObjectType( id ) );
		}

		/// <summary>
		/// Icon for a single object type.
		/// </summary>
		/// <param name="id"> Object Type ID </param>
		/// <returns> </returns>
		public Stream ObjectTypeIcon( int id )
		{
			return Client.Get<Stream>( _Get.StructureObjectTypeIcon( id ) );
		}

		/// <summary>
		/// Icon for a single object type.
		/// </summary>
		/// <param name="id"> Object Type ID </param>
		/// <param name="overwriteExisting"> Should the cached version be used </param>
		/// <returns> string </returns>
		public string ObjectTypeIconAsPath( int id, bool overwriteExisting = false )
		{
			var result = Path.Combine( Path.GetTempPath(), string.Format( "OT_ICON_{0}.ico", id ) );

			if( File.Exists( result ) )
			{
				if( overwriteExisting )
				{
					File.Delete( result );
				}
				else
				{
					return result;
				}
			}

			using( var stream = Client.Get<Stream>( _Get.StructureObjectTypeIcon( id ) ) )
			{
				using( var output = new FileStream( result, FileMode.Create ) )
				{
					stream.CopyTo( output );
				}
			}

			return result;
		}

		/// <summary>
		/// Triggers a Refresh of an External Connection
		/// </summary>
		/// <param name="id">Object Type ID</param>
		/// <param name="status">Refresh Type</param>
		/// <returns></returns>
		public MFRefreshStatus ObjectTypeExternalSourceRefresh( int id, MFRefreshStatus status = MFRefreshStatus.Quick )
		{
			return Client.Put<MFRefreshStatus>( _Set.ExternalObjectTypeRefreshStatus( id ), status );
		}

		/// <summary>
		/// Returns the Structure - Classes
		/// </summary>
		/// <param name="objType">Classes of Type</param>
		/// <param name="byGroup">Group the results</param>
		/// <returns></returns>
		public ObjectClass [] ObjectClasses( int? objType = null, bool? byGroup = null )
		{
			return Client.Get<ObjectClass []>( _Get.StructureClasses( objType, byGroup ) );
		}

		public ObjectClass ObjectClass( int id, bool include = false )
		{
			return Client.Get<ObjectClass>( _Get.StructureClass( id, include ) );
		}

		public Stream ObjectClassIcon( int id, int size = 16 )
		{
			return Client.Get<Stream>( _Get.StructureClassIcon( id, size ) );
		}

		/// <summary>
		/// Icon for a single object class.
		/// </summary>
		/// <param name="id"> Object Class ID </param>
		/// <param name="size">
		/// Icon dimension. Default is 16.
		/// Notes: The icons supports only certain sizes and the real size on the received icon in the response might differ from the specified size.
		/// </param>
		/// <param name="overwriteExisting"> Should the cached version be used </param>
		/// <returns> string </returns>
		public string ObjectClassIconAsPath( int id, int size = 16, bool overwriteExisting = false )
		{
			var result = Path.Combine( Path.GetTempPath(), string.Format( "OC_ICON_{0}.ico", id ) );

			if( File.Exists( result ) )
			{
				if( overwriteExisting )
				{
					File.Delete( result );
				}
				else
				{
					return result;
				}
			}

			using( var stream = ObjectClassIcon( id, size ) )
			{
				using( var output = new FileStream( result, FileMode.Create ) )
				{
					stream.CopyTo( output );
				}
			}

			return result;
		}

		public PropertyDef [] PropertyDefs()
		{
			return Client.Get<PropertyDef []>( _Get.StructureProperties );
		}

		public PropertyDef PropertyDef( int id )
		{
			return Client.Get<PropertyDef>( _Get.StructureProperty( id ) );
		}

		public Workflow [] Workflows()
		{
			return Client.Get<Workflow []>( _Get.StructureWorkflows );
		}

		public Workflow Workflow( int id )
		{
			return Client.Get<Workflow>( _Get.StructureWorkflow( id ) );
		}

		public WorkflowState [] States( int workflowId )
		{
			return Client.Get<WorkflowState []>( _Get.StructureWorkflowStates( workflowId ) );
		}

		public WorkflowState State( int workflowId, int stateId )
		{
			return Client.Get<WorkflowState>( _Get.StructureWorkflowState( workflowId, stateId ) );
		}

		public Results<ValueListItem> ValueListItems( int listId, int? propertydef = null, int? pN = null,
			int? parentItem = null, int? ownerItem = null, string name = null, int? page = null, int? pagesize = null )
		{
			return
				Client.Get<Results<ValueListItem>>( _Get.StructureValueListItems( listId, propertydef, pN, parentItem, ownerItem,
					name, page, pagesize ) );
		}
	}
}