﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Solomon.Common.DocumentManagement.MFiles.WebAccess.Mfws;

namespace Solomon.Common.DocumentManagement.MFiles.WebAccess
{
	/// <summary>
	/// Wrapper Class for the M-Files API. Returns Pre-Cast Objects from a M-Files Web Service Request.
	/// </summary>
	public class Get
	{
		/// <summary>
		/// Self Populating Constructor
		/// </summary>
		/// <param name="client"> </param>
		/// <param name="api"> Parent Api </param>
		public Get( MfwsClient client, Api api )
		{
			Api = api;
			Client = client;
		}

		/// <summary>
		/// Returns the Session Info Based on the User Logged into the Vault.
		/// </summary>
		public SessionInfo SessionInfo
		{
			get { return Api.Client.Get<SessionInfo>( _Get.SessionInfo ); }
		}

		/// <summary>
		/// Returns the Authentication Token from the Server.
		/// </summary>
		public string CurrentAuthenticationToken
		{
			get { return Api.Client.Get<PrimitiveType<string>>( _Get.AuthenticationToken ).Value; }
		}

		/// <summary>
		/// Returns the Authentication Token from the Locally Stored Variable.
		/// </summary>
		public string CurrentAuthenticationTokenLocal
		{
			get { return Api.Client.AuthToken; }
		}

		/// <summary>
		/// Returns the status of the Server.
		/// </summary>
		public StatusResponse ServerStatus
		{
			get { return Api.Client.Get<StatusResponse>( _Get.ServerStatus ); }
		}

		/// <summary>
		/// Returns the Public Key of the Server.
		/// </summary>
		public PublicKey PublicKey
		{
			get { return Api.Client.Get<PublicKey>( _Get.ServerPublicKey ); }
		}

		/// <summary>
		/// Returns ObjectVersion's from the Favorites View.
		/// </summary>
		/// <returns> <see cref="RestTools.ObjectVersion" />[] </returns>
		public ObjectVersion [] Favorites
		{
			get { return Api.Client.Get<ObjectVersion []>( _Get.Favorites ); }
		}

		/// <summary>
		/// Returns ObjectVersion's from the Recently Accessed By Me View.
		/// </summary>
		/// <returns> <see cref="RestTools.ObjectVersion" />[] </returns>
		public ObjectVersion [] RecentlyAccessedByMe
		{
			get { return Api.Client.Get<ObjectVersion []>( _Get.RecentlyAccessedByMe ); }
		}

		/// <summary>
		/// Retrieves a collection of objects in the vault. The amount of returned objects is limited by the server.
		/// </summary>
		public Results<ObjectVersion> Objects()
		{
			return Client.Get<Results<ObjectVersion>>( _Get.Objects );
		}

		/// <summary>
		/// Retrieves objects of a specific type from the vault. The amount of returned objects is limited by the server.
		/// </summary>
		/// <param name="type"> Object Type </param>
		/// <returns> Results(<see cref="RestTools.ObjectVersion" />) </returns>
		public Results<ObjectVersion> Objects( int type )
		{
			return Client.Get<Results<ObjectVersion>>( _Get.ObjectType( type ) );
		}

		/// <summary>
		/// Retrieves a specific object from the vault.
		/// <param name="type"> Object Type </param>
		/// <param name="id"> Object ID </param>
		/// <param name="version"> Specific Version of the Object to return </param>
		/// </summary>
		/// <returns>
		/// <see cref="RestTools.ObjectVersion" />
		/// </returns>
		public ObjectVersion Object( int type, int id, int version = -1 )
		{
			return Client.Get<ObjectVersion>( _Get.ObjectVersion( type, id, version ) );
		}

		/// <summary>
		/// Retrieves a specific object from the vault.
		/// </summary>
		/// <param name="type"> Object Type </param>
		/// <param name="id"> Object ID </param>
		/// <param name="version"> Specific Version of the Object to return </param>
		/// <param name="includeProperties"> When true => includes Properties ... Presently useless without properties included </param>
		/// <returns>
		/// <see cref="RestTools.ExtendedObjectVersion" />
		/// </returns>
		public ExtendedObjectVersion ObjectVersion( int type, int id, int version = -1, bool includeProperties = true )
		{
			return Client.Get<ExtendedObjectVersion>( _Get.ObjectVersion( type, id, version, includeProperties ) );
		}

		/// <summary>
		/// Retrieves a objects Deleted Status.
		/// <param name="type"> Object Type </param>
		/// <param name="id"> Object ID </param>
		/// </summary>
		/// <returns> True or False </returns>
		public bool DeletedStatus( int type, int id )
		{
			return Client.Get<PrimitiveType<bool>>( _Get.ObjectDeletedStatus( type, id ) ).Value;
		}

		/// <summary>
		/// Retrieves a specific object's History.
		/// <param name="type"> Object Type </param>
		/// <param name="id"> Object ID </param>
		/// </summary>
		/// <returns> <see cref="RestTools.ObjectVersion" />[] </returns>
		public ObjectVersion [] ObjectHistory( int type, int id )
		{
			return Client.Get<ObjectVersion []>( _Get.ObjectHistory( type, id ) );
		}

		/// <summary>
		/// Retrieves a specific object version's comment array.
		/// <param name="type"> Object Type </param>
		/// <param name="id"> Object ID </param>
		/// <param name="version"> Object Version </param>
		/// </summary>
		/// <returns> <see cref="RestTools.VersionComment" />[] </returns>
		public VersionComment [] ObjectComments( int type, int id, int version )
		{
			return Client.Get<VersionComment []>( _Get.ObjectComments( type, id, version ) );
		}

		/// <summary>
		/// Retrieves a specific object versions checked out status.
		/// <param name="type"> Object Type </param>
		/// <param name="id"> Object ID </param>
		/// <param name="version"> Object Version </param>
		/// </summary>
		/// <returns>
		/// <see cref="RestTools.MFCheckOutStatus" />
		/// </returns>
		public MFCheckOutStatus CheckedOutStatus( int type, int id, int version )
		{
			return Client.Get<PrimitiveType<MFCheckOutStatus>>( _Get.ObjectCheckoutStatus( type, id, version ) ).Value;
		}

		/// <summary>
		/// Returns the Objects Title
		/// </summary>
		/// <param name="type"> Object Type </param>
		/// <param name="id"> Object ID </param>
		/// <param name="version"> Object Instance Version </param>
		/// <returns> String Title </returns>
		public string ObjectTitle( int type, int id, int version )
		{
			return Client.Get<string>( _Get.ObjectTitle( type, id, version ) );
		}

		/// <summary>
		/// Returns the Objects Workflow State
		/// </summary>
		/// <param name="type"> Object Type </param>
		/// <param name="id"> Object ID </param>
		/// <param name="version"> Object Version </param>
		/// <returns>
		/// <see cref="RestTools.ObjectWorkflowState" />
		/// </returns>
		public ObjectWorkflowState ObjectWorkflowState( int type, int id, int version )
		{
			return Client.Get<ObjectWorkflowState>( _Get.ObjectWorkflowState( type, id, version ) );
		}

		/// <summary>
		/// Object Image Thumbnail Stream
		/// </summary>
		/// <param name="type"> Object Instance Type </param>
		/// <param name="id"> Object Instance ID </param>
		/// <param name="version"> Object Instance Version </param>
		/// <returns>
		/// <see cref="Stream" />
		/// </returns>
		public Stream ObjectThumbnail( int type, int id, int version )
		{
			return Client.Get<Stream>( _Get.ObjectThumbnail( type, id, version ) );
		}

		/// <summary>
		/// Object Relationships
		/// </summary>
		/// <param name="type"> Object Type </param>
		/// <param name="id"> Object ID </param>
		/// <param name="version"> Object Version </param>
		/// <param name="direction"> Relationship Direction as <see cref="RestTools.RelationshipType" /> </param>
		/// <param name="filterType"> Return only Related Objects of this Type </param>
		/// <returns> <see cref="RestTools.ObjectVersion" />[] </returns>
		public ObjectVersion [] ObjectRelationships( int type, int id, int version, RelationshipType direction,
			int filterType = -1 )
		{
			return Client.Get<ObjectVersion []>( _Get.ObjectRelationships( type, id, version, direction, filterType ) );
		}

		/// <summary>
		/// Object Relationships as a <see cref="RestTools.Lookup" />[]
		/// </summary>
		/// <param name="type"> Object Type </param>
		/// <param name="id"> Object ID </param>
		/// <param name="version"> Object Version </param>
		/// <param name="direction"> Relationship Direction as <see cref="RestTools.RelationshipType" /> </param>
		/// <param name="filterType"> Return only Related Objects of this Type </param>
		/// <returns> <see cref="RestTools.Lookup" />[] </returns>
		public Lookup [] ObjectRelationshipsAsLookups( int type, int id, int version, RelationshipType direction,
			int filterType = -1 )
		{
			return Client.Get<Lookup []>( _Get.ObjectRelationships( type, id, version, direction, filterType, true ) );
		}

		/// <summary>
		/// Returns the Objects Relationships Count
		/// </summary>
		/// <param name="type"> Object Type </param>
		/// <param name="id"> Object ID </param>
		/// <param name="version"> Object Version </param>
		/// <param name="direction"> Relationship Direction as <see cref="RestTools.RelationshipType" /> </param>
		/// <param name="filterType"> Return only Related Objects of this Type </param>
		/// <returns> <see cref="int" /> - Count </returns>
		public int ObjectRelationshipCount( int type, int id, int version, RelationshipType direction, int filterType = -1 )
		{
			return
				Client.Get<PrimitiveType<int>>( _Get.ObjectRelationshipCount( type, id, version, direction, filterType ) ).Value;
		}

		/// <summary>
		/// Sub-Objects as a <see cref="RestTools.ObjectVersion" />[]
		/// </summary>
		/// <param name="objVer"> ObjVer of the Parent Object </param>
		/// <returns> <see cref="RestTools.ObjectVersion" />[] </returns>
		public ObjectVersion [] SubObjects( ObjVer objVer )
		{
			return Client.Get<ObjectVersion []>( _Get.SubObjects( objVer.Type, objVer.ID, objVer.Version ) );
		}

		/// <summary>
		/// Sub-Objects as a <see cref="RestTools.ObjectVersion" />[]
		/// </summary>
		/// <param name="type"> Object Type </param>
		/// <param name="id"> Object ID </param>
		/// <param name="version"> Object Version </param>
		/// <returns> <see cref="RestTools.ObjectVersion" />[] </returns>
		public ObjectVersion [] SubObjects( int type, int id, int version = -1 )
		{
			return Client.Get<ObjectVersion []>( _Get.SubObjects( type, id, version ) );
		}

		/// <summary>
		/// Sub-Objects Count
		/// </summary>
		/// <param name="type"> Object Type </param>
		/// <param name="id"> Object ID </param>
		/// <param name="version"> Object Version </param>
		/// <returns> <see cref="int" /> - Count </returns>
		public int SubObjectCount( int type, int id, int version = -1 )
		{
			return Client.Get<PrimitiveType<int>>( _Get.SubObjectCount( type, id, version ) ).Value;
		}

		/// <summary>
		/// Sub-Objects Count
		/// </summary>
		/// <param name="objVer"> <see cref="RestTools.ObjVer" />of the Parent Object </param>
		/// <returns> <see cref="int" /> - Count </returns>
		public int SubObjectCount( ObjVer objVer )
		{
			return Client.Get<PrimitiveType<int>>( _Get.SubObjectCount( objVer.Type, objVer.ID, objVer.Version ) ).Value;
		}

		/// <summary>
		/// Properties of Multiple Objects
		/// </summary>
		/// <param name="objVers"> <see cref="ObjVer" />[] of the Target Objects </param>
		/// <returns> List(<see cref="PropertyValue" />[]) </returns>
		public List<PropertyValue []> PropertiesOfMultipleObjects( ObjVer [] objVers )
		{
			return Client.Get<PropertyValue [] []>( _Get.PropertiesOfMultipleObjects( objVers ) ).ToList();
		}

		/// <summary>
		/// Objects Properties in a PropertyValue[]
		/// </summary>
		/// <param name="objVer"> <see cref="ObjVer" /> of the Parent Object </param>
		/// <param name="forDisplay"> If true, the response will be filtered and sorted for display. Each object has several built-in properties which usually aren’t shown to the user. This parameter can be used to leave those out. </param>
		/// <returns> <see cref="RestTools.PropertyValue" />[] </returns>
		public PropertyValue [] Properties( ObjVer objVer, bool forDisplay = false )
		{
			return Client.Get<PropertyValue []>( _Get.Properties( objVer.Type, objVer.ID, objVer.Version, forDisplay ) );
		}

		/// <summary>
		/// Objects Properties in a PropertyValue[]
		/// </summary>
		/// <param name="type"> Object Type </param>
		/// <param name="id"> Object ID </param>
		/// <param name="version"> Object Version </param>
		/// <param name="forDisplay"> If true, the response will be filtered and sorted for display. Each object has several built-in properties which usually aren’t shown to the user. This parameter can be used to leave those out. </param>
		/// <returns> <see cref="RestTools.PropertyValue" />[] </returns>
		public PropertyValue [] Properties( int type, int id, int version = -1, bool forDisplay = false )
		{
			return Client.Get<PropertyValue []>( _Get.Properties( type, id, version, forDisplay ) );
		}

		/// <summary>
		/// Gets a Property Value
		/// </summary>
		/// <param name="propID"> Property ID </param>
		/// <param name="type"> Object Type </param>
		/// <param name="id"> Object ID </param>
		/// <param name="version"> Object Version </param>
		/// <returns>
		/// <see cref="RestTools.PropertyValue" />
		/// </returns>
		public PropertyValue Property( int propID, int type, int id, int version = -1 )
		{
			return Client.Get<PropertyValue>( _Get.Property( type, id, version, propID ) );
		}

		/// <summary>
		/// Gets a Property Value
		/// </summary>
		/// <param name="propID"> Property ID </param>
		/// <param name="objVer"> ObjVer of Target Object </param>
		/// <returns>
		/// <see cref="RestTools.PropertyValue" />
		/// </returns>
		public PropertyValue Property( int propID, ObjVer objVer )
		{
			return Client.Get<PropertyValue>( _Get.Property( objVer.Type, objVer.ID, objVer.Version, propID ) );
		}

		/// <summary>
		/// <see cref="ObjectFile" />[] for the given Object.
		/// </summary>
		/// <param name="objVer"> <see cref="ObjVer" /> of Target </param>
		/// <returns> <see cref="RestTools.ObjectFile" />[] </returns>
		public ObjectFile [] Files( ObjVer objVer )
		{
			return Files( objVer.Type, objVer.ID, objVer.Version );
		}

		/// <summary>
		/// <see cref="ObjectFile" />[] for the given Object.
		/// </summary>
		/// <param name="type"> Object Type </param>
		/// <param name="id"> Object ID </param>
		/// <param name="version"> Object Version </param>
		/// <returns> <see cref="RestTools.ObjectFile" />[] </returns>
		public ObjectFile [] Files( int type, int id, int version = -1 )
		{
			return Api.Client.Get<ObjectFile []>( _Get.Files( type, id, version ) );
		}

		/// <summary>
		/// Single-File Objects Only: Gets the ObjectFile for the existing FileID on the given Object Instance.
		/// </summary>
		/// <param name="type"> Object Version Instance Type </param>
		/// <param name="id"> Object Version Instance ID </param>
		/// <param name="version"> Object Version </param>
		/// <param name="fileID"> File ID </param>
		/// <param name="macFormatting"> If true, appends a SHA-512 message authentication code at the end of the stream </param>
		/// <returns>
		/// <see cref="Stream" />
		/// </returns>
		public Stream FileContent( int type, int id, int version, int fileID, bool macFormatting = false )
		{
			using( var stream = Api.Client.Get<Stream>( _Get.ObjectFileContent( type, id, version, fileID, macFormatting ) ) )
			{
				return stream;
			}
		}

		/// <summary>
		/// Single-File Objects Only: Gets the ObjectFile for the existing FileID on the given Object Instance... 
		/// Downloads the file and returns the filepath.
		/// </summary>
		/// <param name="type"> Object Version Instance Type </param>
		/// <param name="id"> Object Version Instance ID </param>
		/// <param name="version"> Object Version </param>
		/// <param name="fileID"> File ID </param>
		/// <param name="macFormatting"> If true, appends a SHA-512 message authentication code at the end of the stream </param>
		/// <param name="overwriteExisting"> When True => Overwrites Existing files of the same name in the users temp directory </param>
		/// <returns> String Path </returns>
		public string FileContentAsPath( int type, int id, int version, int fileID, bool macFormatting = false,
			bool overwriteExisting = true )
		{
			ObjectFile file = Api.Get.Files( type, id, version ).SingleOrDefault( f => f.ID == fileID );
			if( file == null ) return string.Empty;

			// Create the filepath
			string result = Path.Combine( Path.GetTempPath(), string.Format( "{0}.{1}", file.Name, file.Extension ) );

			if( File.Exists( result ) )
			{
				if( overwriteExisting )
				{
					File.Delete( result );
				}
				else
				{
					return result;
				}
			}

			using( Stream stream = Api.Client.Get<Stream>( _Get.ObjectFileContent( type, id, version, fileID, macFormatting ) ) )
			{
				using( FileStream output = new FileStream( result, FileMode.Create ) )
				{
					stream.CopyTo( output );
				}
			}

			return result;
		}

		/// <summary>
		/// Get the preview thumbnail for an Object
		/// </summary>
		/// <param name="type"> Object Version Instance Type </param>
		/// <param name="id"> Object Version Instance ID </param>
		/// <param name="version"> Object Version </param>
		/// <param name="fileID"> File ID </param>
		/// <param name="force"> If true, the server streams an empty image instead of giving a HTTP 404 response </param>
		/// <param name="size"> Width and Height square pixel dimensions </param>
		/// <param name="width"> Optional - Overrides thumbnail width </param>
		/// <param name="height"> Optional - Overrides thumbnail height </param>
		public Stream FileThumbnail( int type, int id, int version, int fileID, bool force = true, int size = 32,
			int? width = null, int? height = null )
		{
			using(
				var stream =
					Api.Client.Get<Stream>( _Get.ObjectFileThumbnail( type, id, version, fileID, force, size, width, height ) ) )
			{
				return stream;
			}
		}

		/// <summary>
		/// Get the preview thumbnail for an Object
		/// </summary>
		/// <param name="type"> Object Version Instance Type </param>
		/// <param name="id"> Object Version Instance ID </param>
		/// <param name="version"> Object Version </param>
		/// <param name="fileID"> File ID </param>
		/// <param name="overwriteExisting"> When True => Overwrites Existing files of the same name in the users temp directory </param>
		/// <param name="force"> If true, the server streams an empty image instead of giving a HTTP 404 response </param>
		/// <param name="size"> Width and Height square pixel dimensions </param>
		/// <param name="width"> Optional - Overrides thumbnail width </param>
		/// <param name="height"> Optional - Overrides thumbnail height </param>
		public string FileThumbnailAsPath( int type, int id, int version, int fileID, bool overwriteExisting = true,
			bool force = true, int size = 32, int? width = null, int? height = null )
		{
			var file = Api.Get.Files( type, id, version ).SingleOrDefault( f => f.ID == fileID );
			if( file == null ) return string.Empty;

			var result = Path.Combine( Path.GetTempPath(), string.Format( "{0}.{1}", file.Name, file.Extension ) );

			if( File.Exists( result ) )
			{
				if( overwriteExisting )
				{
					File.Delete( result );
				}
				else
				{
					return result;
				}
			}

			using(
				var stream =
					Api.Client.Get<Stream>( _Get.ObjectFileThumbnail( type, id, version, fileID, force, size, width, height ) ) )
			{
				using( var output = new FileStream( result, FileMode.Create ) )
				{
					stream.CopyTo( output );
				}
			}

			return result;
		}

		/// <summary>
		/// Returns the Title for the Object and File specified.
		/// </summary>
		/// <param name="type"> Object Type </param>
		/// <param name="id"> Object ID </param>
		/// <param name="version"> Object Version </param>
		/// <param name="fileID"> Specific File ID </param>
		/// <returns> String Filename </returns>
		public string Filename( int type, int id, int version, int fileID )
		{
			return Api.Client.Get<string>( _Get.ObjectFileTitle( type, id, version, fileID ) );
		}

		/// <summary>
		/// Returns the Vaults on the Server.
		/// </summary>
		/// <param name="onlineOnly"> Should Off-line Vaults be excluded from results </param>
		/// <returns> <see cref="RestTools.Vault" />[] </returns>
		public Vault [] Vaults( bool onlineOnly = true )
		{
			return Api.Client.Get<Vault []>( _Get.ServerVaults( onlineOnly ) );
		}

		/// <summary>
		/// Returns the specified Value List Item
		/// </summary>
		/// <returns>
		/// <see cref="RestTools.ValueListItem" />
		/// </returns>
		public ValueListItem ValueListItem( int listId, int itemId )
		{
			return Api.Client.Get<ValueListItem>( _Get.StructureValueListItem( listId, itemId ) );
		}

		/// <summary>
		/// Returns Value List Item Title.
		/// </summary>
		/// <returns> String Title </returns>
		public string ValueListItemTitle( int listId, int itemId )
		{
			return Api.Client.Get<PrimitiveType<string>>( _Get.StructureValueListItemTitle( listId, itemId ) ).Value;
		}

		/// <summary>
		/// Returns Folder Content Items of a View
		/// </summary>
		/// <returns>
		/// <see cref="RestTools.FolderContentItems" />
		/// </returns>
		public FolderContentItems ViewFolderContentItems( string path )
		{
			return Api.Client.Get<FolderContentItems>( _Get.FolderContentItems( path ) );
		}

		/// <summary>
		/// Returns the ObjectVersion's contained within a View.
		/// </summary>
		/// <returns> Results(<see cref="ObjectVersion" />) </returns>
		public Results<ObjectVersion> ViewObjectVersions( string path )
		{
			return Api.Client.Get<Results<ObjectVersion>>( _Get.ViewContent( path ) );
		}

		/// <summary>
		/// Returns a View's Item Count.
		/// </summary>
		/// <returns> <see cref="int" /> - Count </returns>
		public int ViewItemCount( string path )
		{
			return Api.Client.Get<PrimitiveType<int>>( _Get.ViewItemCount( path ) ).Value;
		}

		/// <summary>
		/// Returns a specified Object Version from a Users Favorites.
		/// </summary>
		/// <returns>
		/// <see cref="RestTools.ObjectVersion" />
		/// </returns>
		public ObjectVersion Favorite( int type, int id )
		{
			return Api.Client.Get<ObjectVersion>( _Get.Favorite( type, id ) );
		}

		/// <summary>
		/// Returns the Datatype of a Property
		/// </summary>
		/// <param name="propID"> Property ID </param>
		/// <returns>
		/// <see cref="RestTools.MFDataType" />
		/// </returns>
		public MFDataType Datatype( int propID )
		{
			try
			{
				return Client.Get<PropertyDef>( _Get.StructureProperty( propID ) ).DataType;
			}
			catch( Exception )
			{
				return MFDataType.Uninitialized;
			}
		}

		#region Private Items

		/// <summary>
		/// M-Files Rest API Client Helper Object.  Used privately for this class.
		/// </summary>
		private MfwsClient Client { get; set; }

		/// <summary>
		/// Parent Api instance for internal use.
		/// </summary>
		private Api Api { get; set; }

		#endregion
	}
}