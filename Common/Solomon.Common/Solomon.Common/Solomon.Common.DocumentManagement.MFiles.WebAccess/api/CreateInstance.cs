﻿using System;
using System.Linq;
using System.Web.Helpers;
using Solomon.Common.DocumentManagement.MFiles.WebAccess.Mfws;

namespace Solomon.Common.DocumentManagement.MFiles.WebAccess
{
	/// <summary>
	/// Helper Class for Easily Creating a New Instance of Commonly Used Objects
	/// </summary>
	public class CreateInstance
	{
		/// <summary>
		/// Self Populating Constructor
		/// </summary>
		/// <param name="client"> Pre-Instantiated M-Files Rest Client Object </param>
		/// <param name="api"> Parent Api </param>
		public CreateInstance( MfwsClient client, Api api )
		{
			Api = api;
			Client = client;
		}

		/// <summary>
		/// Creates a new instance of a <see cref="ObjectWorkflowState" /> Object from the Workflow and State ID
		/// </summary>
		/// <param name="workflowID"> Workflow ID </param>
		/// <param name="stateID"> </param>
		/// <param name="comment"> </param>
		/// <returns> </returns>
		public ObjectWorkflowState ObjectWorkflowState( int workflowID, int stateID, string comment = "" )
		{
			var workflow = Client.Get<Workflow>( _Get.StructureWorkflow( workflowID ) );
			var state = Client.Get<WorkflowState>( _Get.StructureWorkflowState( workflowID, stateID ) );

			var ows =
				Json.Decode<ObjectWorkflowState>(
					"{\"State\":{\"PropertyDef\":39,\"TypedValue\":{\"DataType\":9,\"HasValue\":true,\"Value\":null,\"Lookup\":{\"Deleted\":false,\"DisplayValue\":\"Some State\",\"Hidden\":false,\"Item\":101,\"Version\":-1},\"Lookups\":null,\"DisplayValue\":\"Some State\",\"SortingKey\":\"\",\"SerializedValue\":\"101\"}},\"StateID\":101,\"StateName\":\"Some State\",\"Workflow\":{\"PropertyDef\":38,\"TypedValue\":{\"DataType\":9,\"HasValue\":true,\"Value\":null,\"Lookup\":{\"Deleted\":false,\"DisplayValue\":\"Some Workflow\",\"Hidden\":false,\"Item\":101,\"Version\":-1},\"Lookups\":null,\"DisplayValue\":\"Some Workflow\",\"SortingKey\":\"\",\"SerializedValue\":\"101\"}},\"WorkflowID\":101,\"WorkflowName\":\"Some Workflow\",\"VersionComment\":\"\"}" );

			// Update Comment Value
			ows.VersionComment = comment;

			// Update State Values
			ows.State.TypedValue.DisplayValue = state.Name;
			ows.State.TypedValue.SerializedValue = state.ID.ToString();
			ows.State.TypedValue.Lookup.DisplayValue = state.Name;
			ows.State.TypedValue.Lookup.Item = state.ID;

			// Update Workflow Values
			ows.Workflow.TypedValue.DisplayValue = workflow.Name;
			ows.Workflow.TypedValue.SerializedValue = workflow.ID.ToString();
			ows.Workflow.TypedValue.Lookup.DisplayValue = workflow.Name;
			ows.Workflow.TypedValue.Lookup.Item = workflow.ID;

			// return generated object
			return ows;
		}

		/// <summary>
		/// Creates an ObjVer
		/// </summary>
		/// <returns> </returns>
		public ObjVer ObjVer( int type, int id, int version = -1 )
		{
			return new ObjVer
			{
				ID = id,
				Type = type,
				Version = version
			};
		}

		/// <summary>
		/// Creates a Default ObjectCreationInfo Object for you.
		/// Pre-Populated PropertyValues { 0 => Title, 22 => Single File, 100 => Class ID }
		/// </summary>
		/// <param name="classID"> Class ID of the object to create </param>
		/// <param name="title"> String title for the value of Property Zero ( 0 ) </param>
		/// <param name="singleFile"> Is this a Single File Object </param>
		/// <returns>
		/// <see cref="ObjectCreationInfo" />
		/// </returns>
		public ObjectCreationInfo ObjectCreationInfoNewVersion( int classID, string title, bool singleFile)
		{
			// Create the object creation info.
			var creationInfo = new ObjectCreationInfo
			{
				PropertyValues = new []
				{
					new PropertyValue
					{
						PropertyDef = 0,
						TypedValue = new TypedValue { DataType = MFDataType.Text, Value = title }
					},
					new PropertyValue
					{
						PropertyDef = 22,
						TypedValue = new TypedValue { DataType = MFDataType.Boolean, Value = singleFile }
					},
					new PropertyValue
					{
						PropertyDef = 100,
						TypedValue = new TypedValue
						{
							DataType = MFDataType.Lookup,
							Lookup = new Lookup { Item = classID }
						}
					}
				}
			};

			return creationInfo;
		}

        /// <summary>
		/// Creates a Default ObjectCreationInfo Object for you, but makes sure you don't get the 'Version 0' problem.
		/// Pre-Populated PropertyValues { 0 => Title, 22 => Single File, 100 => Class ID }
        /// </summary>
        /// <param name="classID"></param>
        /// <param name="title"></param>
        /// <param name="singleFile"></param>
        /// <returns></returns>
        public ObjectCreationInfo ObjectCreationInfoClassVersionMinus1(int classID, string title, bool singleFile)
        {
            // Create the object creation info.
            var creationInfo = new ObjectCreationInfo
            {
                PropertyValues = new[]
				{
					new PropertyValue
					{
						PropertyDef = 0,
						TypedValue = new TypedValue { DataType = MFDataType.Text, Value = title }
					},
					new PropertyValue
					{
						PropertyDef = 22,
						TypedValue = new TypedValue { DataType = MFDataType.Boolean, Value = singleFile }
					},
					new PropertyValue
					{
						PropertyDef = 100,
						TypedValue = new TypedValue
						{
							DataType = MFDataType.Lookup,
							Lookup = new Lookup { Item = classID, Version=-1 }
						}
					}
				}
            };

            return creationInfo;
        }

		/// <summary>
		/// Overload PropertyValue Creation Function
		/// </summary>
		/// <param name="propID"> Property ID </param>
		/// <param name="value"> Value as Lookups[] </param>
		/// <returns>
		/// <see cref="RestTools.PropertyValue" />
		/// </returns>
		public PropertyValue PropertyValue( int propID, Lookup [] value )
		{
			return PropertyValue( propID, value, value.GetType() );
		}

		/// <summary>
		/// Overload PropertyValue Creation Function
		/// </summary>
		/// <param name="propID"> Property ID </param>
		/// <param name="value"> Value as Lookup </param>
		/// <returns>
		/// <see cref="RestTools.PropertyValue" />
		/// </returns>
		public PropertyValue PropertyValue( int propID, Lookup value )
		{
			return PropertyValue( propID, value, value.GetType() );
		}

		/// <summary>
		/// Overload PropertyValue Creation Function
		/// </summary>
		/// <param name="propID"> Property ID </param>
		/// <param name="value"> Value as ( object | Lookup | Lookups[] ) </param>
		/// <returns>
		/// <see cref="RestTools.PropertyValue" />
		/// </returns>
		public PropertyValue PropertyValue( int propID, dynamic value )
		{
			return PropertyValue( propID, value, value.GetType() );
		}

		/// <summary>
		/// Creates a new Instance of a TypedValue Object
		/// </summary>
		/// <param name="propID"> Property ID </param>
		/// <param name="value"> Value ( Lookups | Lookup | generic value ) </param>
		/// <returns>
		/// <see cref="RestTools.TypedValue" />
		/// </returns>
		public TypedValue TypedValue( int propID, dynamic value )
		{
			var type = Api.Get.Datatype( propID );
			MFDataType [] lookupTypes = { MFDataType.MultiSelectLookup, MFDataType.Lookup };

			return new TypedValue
			{
				DataType = type,
				Lookup = type == MFDataType.Lookup ? value ?? new Lookup() : new Lookup(),
				Lookups = type == MFDataType.MultiSelectLookup ? value ?? new Lookup [] { } : new Lookup [] { },
				HasValue = value != null,
				Value = lookupTypes.Contains( type ) ? null : value
			};
		}

		/// <summary>
		/// Overload PropertyValue Creation Function
		/// </summary>
		/// <param name="propID"> Property ID </param>
		/// <param name="value"> Value as ( object | Lookup | Lookups[] ) </param>
		/// <param name="T"> Allows for passing the Type of the 'value' object </param>
		/// <returns>
		/// <see cref="RestTools.PropertyValue" />
		/// </returns>
		private PropertyValue PropertyValue( int propID, object value, Type T = null )
		{
			// Create a new TypedValue 
			var tv = TypedValue( propID, null );

			if( value == null )
			{
				tv.Value = null;
				tv.DisplayValue = "";
			}
			else if( T == typeof( Lookup [] ) )
			{
				tv.Lookups = value as Lookup [];
				tv.DisplayValue = "";
				tv.Lookups.ToList().ForEach( l => tv.DisplayValue += l.DisplayValue + "; " );
			}
			else if( T == typeof( Lookup ) )
			{
				tv.Lookup = value as Lookup;
				tv.DisplayValue = tv.Lookup.DisplayValue;
			}
			else
			{
				tv.Value = value;
				tv.DisplayValue = Util.CastToString( value, tv.DataType );
			}

			return PropertyValue( propID, tv );
		}

		/// <summary>
		/// Overload PropertyValue Creation Function
		/// </summary>
		/// <param name="propID"> Property ID </param>
		/// <param name="value"> Value as <see cref="RestTools.TypedValue" /> </param>
		/// <returns>
		/// <see cref="RestTools.PropertyValue" />
		/// </returns>
		public PropertyValue PropertyValue( int propID, TypedValue value )
		{
			var propertyValue = new PropertyValue
			{
				PropertyDef = propID,
				TypedValue = value
			};

			return propertyValue;
		}

		#region Private Items

		/// <summary>
		/// M-Files Rest API Client Helper Object.  Used privately for this class.
		/// </summary>
		private MfwsClient Client { get; set; }

		/// <summary>
		/// Parent Api instance for private use.
		/// </summary>
		private Api Api { get; set; }

		#endregion
	}
}