﻿namespace Solomon.Common.DocumentManagement.MFiles.WebAccess
{
	/// <summary>
	/// Formatted PUT Resource URIs
	/// </summary>
	internal static class _Set
	{
		/// <summary>
		/// Stores a temporary file on the server and assigns an ID for it. Once uploaded the file can be used in object creation
		/// </summary>
		public static string TemporaryFiles
		{
			get { return string.Format( "files" ); }
		}

		/// <summary>
		/// Set the current session.
		/// </summary>
		public static string SessionInfo
		{
			get { return string.Format( "session" ); }
		}

		/// <summary>
		/// Set the current session.
		/// </summary>
		public static string Vault
		{
			get { return string.Format( "session/vault" ); }
		}

		/// <summary>
		/// Transient resource used in calculating new authentication tokens.
		/// </summary>
		public static string AuthenticationTokens()
		{
			return string.Format( "server/authenticationtokens" );
		}

		/// <summary>
		/// Transient resource used in calculating new authentication tokens.
		/// </summary>
		/// <param name="typeID"> Object Type ID </param>
		public static string ExternalObjectTypeRefreshStatus( int typeID )
		{
			return string.Format( "/structure/objecttypes/{0}/refreshstatus", typeID );
		}

		/// <summary>
		/// Object Creation URI
		/// </summary>
		/// <param name="type"> </param>
		/// <returns> </returns>
		public static string CreateObject( int type )
		{
			return string.Format( "objects/{0}", type );
		}

		/// <summary>
		/// Sets the Deleted Status of an Object in the Vault
		/// </summary>
		/// <param name="type"> Object Type ID </param>
		/// <param name="id"> Object ID </param>
		/// <returns> </returns>
		public static string DeletedStatus( int type, int id )
		{
			return string.Format( "objects/{0}/{1}/deleted", type, id );
		}
	}
}