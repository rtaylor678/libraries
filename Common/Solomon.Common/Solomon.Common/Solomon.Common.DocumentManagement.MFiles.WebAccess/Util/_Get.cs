﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Solomon.Common.DocumentManagement.MFiles.WebAccess
{
	/// <summary>
	/// Formatted GET Resource URIs
	/// </summary>
	internal static class _Get
	{
		/// <summary>
		/// Get All Objects in the Vault
		/// </summary>
		public static string Objects
		{
			get { return "objects"; }
		}

		/// <summary>
		/// Get the Current user session information.
		/// </summary>
		public static string SessionInfo
		{
			get { return "session"; }
		}

		/// <summary>
		/// Get the Current session's Vault object.
		/// </summary>
		public static string Vault
		{
			get { return "session/vault"; }
		}

		/// <summary>
		/// Get the Current session's Authentication Token.
		/// </summary>
		public static string AuthenticationToken
		{
			get { return @"session/authenticationtoken"; }
		}

		/// <summary>
		/// Get the Public Key for the M-Files Server.
		/// </summary>
		public static string ServerPublicKey
		{
			get { return "server/publickey"; }
		}

		/// <summary>
		/// Get the M-Files Server Status.
		/// </summary>
		public static string ServerStatus
		{
			get { return "server/status"; }
		}

		/// <summary>
		/// Get the Structure.Properties
		/// </summary>
		public static string StructureProperties
		{
			get { return "structure/properties"; }
		}

		/// <summary>
		/// Get the Structure.Workflows
		/// </summary>
		public static string StructureWorkflows
		{
			get { return "structure/workflows"; }
		}

		/// <summary>
		/// Get Favorites for the User from this Session
		/// </summary>
		public static string Favorites
		{
			get { return string.Format( "favorites" ); }
		}

		/// <summary>
		/// Get Recently Accessed By Me Items
		/// </summary>
		public static string RecentlyAccessedByMe
		{
			get { return "recentlyaccessedbyme"; }
		}

		/// <summary>
		/// Get all Objects in the Vault of a Specific Object Type
		/// </summary>
		/// <param name="type"> </param>
		public static string ObjectType( int type )
		{
			return string.Format( "objects/{0}", type );
		}

		/// <summary>
		/// Get a Specific Object Instance
		/// </summary>
		/// <param name="type"> Object Instance Type </param>
		/// <param name="id"> Object Instance ID </param>
		/// <param name="version"> Specific Version of the Object to return </param>
		/// <param name="includeProperties"> When true => includes Properties </param>
		public static string ObjectVersion( int type, int id, int version, bool includeProperties = false )
		{
			var versionStr = version == -1 ? "latest" : version.ToString();

			if( includeProperties )
			{
				return string.Format( "objects/{0}/{1}/{2}?include=properties", type, id, versionStr );
			}
			return string.Format( "objects/{0}/{1}/{2}", type, id, versionStr );
		}

		/// <summary>
		/// Get a the Object Deleted Status
		/// </summary>
		/// <param name="type"> Object Version Instance Type </param>
		/// <param name="id"> Object Version Instance ID </param>
		public static string ObjectDeletedStatus( int type, int id )
		{
			return string.Format( "objects/{0}/{1}/deleted", type, id );
		}

		/// <summary>
		/// Get the History of an Object Instance
		/// </summary>
		/// <param name="type"> Object Version Instance Type </param>
		/// <param name="id"> Object Version Instance ID </param>
		public static string ObjectHistory( int type, int id )
		{
			return string.Format( "objects/{0}/{1}/history", type, id );
		}

		/// <summary>
		/// Get the Comments for a Version Specific Object Instance
		/// </summary>
		/// <param name="type"> Object Version Instance Type </param>
		/// <param name="id"> Object Version Instance ID </param>
		/// <param name="version"> Object Version </param>
		public static string ObjectComments( int type, int id, int version )
		{
			var versionStr = version == -1 ? "latest" : version.ToString();

			return string.Format( "objects/{0}/{1}/{2}/comments", type, id, versionStr );
		}

		/// <summary>
		/// Get the Check-Out Stats for a Version Specific Object Instance
		/// </summary>
		/// <param name="type"> Object Version Instance Type </param>
		/// <param name="id"> Object Version Instance ID </param>
		/// <param name="version"> Object Version </param>
		public static string ObjectCheckoutStatus( int type, int id, int version )
		{
			var versionStr = version == -1 ? "latest" : version.ToString();

			return string.Format( "objects/{0}/{1}/{2}/checkedout", type, id, versionStr );
		}

		/// <summary>
		/// Get the Title for a Version Specific Object Instance
		/// </summary>
		/// <param name="type"> Object Version Instance Type </param>
		/// <param name="id"> Object Version Instance ID </param>
		/// <param name="version"> Object Version </param>
		public static string ObjectTitle( int type, int id, int version )
		{
			var versionStr = version == -1 ? "latest" : version.ToString();

			return string.Format( "objects/{0}/{1}/{2}/title", type, id, versionStr );
		}

		/// <summary>
		/// Get the Workflow State for a Version Specific Object Instance
		/// </summary>
		/// <param name="type"> Object Version Instance Type </param>
		/// <param name="id"> Object Version Instance ID </param>
		/// <param name="version"> Object Version </param>
		public static string ObjectWorkflowState( int type, int id, int version )
		{
			var versionStr = version == -1 ? "latest" : version.ToString();

			return string.Format( "objects/{0}/{1}/{2}/workflowstate", type, id, versionStr );
		}

		/// <summary>
		/// Get the preview thumbnail for a Version Specific Object Instance
		/// </summary>
		/// <param name="type"> Object Version Instance Type </param>
		/// <param name="id"> Object Version Instance ID </param>
		/// <param name="version"> Object Version </param>
		/// <param name="force"> If true, the server streams an empty image instead of giving a HTTP 404 response </param>
		/// <param name="size"> Width and Height square pixel dimensions </param>
		/// <param name="width"> Optional - Overrides thumbnail width </param>
		/// <param name="height"> Optional - Overrides thumbnail height </param>
		public static string ObjectThumbnail( int type, int id, int version, bool force = true, int size = 32,
			int? width = null, int? height = null )
		{
			var @params = new Dictionary<string, string>
			{
				{ "force", force.ToString() },
				{ "size", size.ToString() }
			};

			if( width != null ) @params.Add( "width", width.ToString() );
			if( height != null ) @params.Add( "height", height.ToString() );

			var versionStr = version == -1 ? "latest" : version.ToString();

			var url = string.Format( "objects/{0}/{1}/{2}/preview", type, id, versionStr );
			var paramStr = "?";

			foreach( var param in @params )
				paramStr += string.Format( "&{0}={1}", param.Key, param.Value );

			// return the result
			return paramStr == "?" ? url : url + paramStr;
		}

		/// <summary>
		/// Get the Relationships as specified by the function params for a Version Specific Object Instance
		/// </summary>
		/// <param name="type"> Object Version Instance Type </param>
		/// <param name="id"> Object Version Instance ID </param>
		/// <param name="version"> Object Version </param>
		/// <param name="direction"> Relationship Direction(s) </param>
		/// <param name="objtype"> Filter by a specific object type </param>
		/// <param name="returnAsLookup"> When True => Return Type is Lookup[] ... Default is ObjectVersion[] </param>
		public static string ObjectRelationships( int type, int id, int version, RelationshipType direction, int objtype = -1,
			bool returnAsLookup = false )
		{
			var versionStr = version == -1 ? "latest" : version.ToString();

			var url = string.Format( "objects/{0}/{1}/{2}/relationships", type, id, versionStr );
			url += string.Format( "?{0}={1}", "direction", direction );

			if( objtype != -1 )
				url += string.Format( "?{0}={1}", "objtype", objtype );

			if( returnAsLookup )
				url += string.Format( "?{0}={1}", "type", "lookup" );

			// return the result
			return url;
		}

		/// <summary>
		/// Get the Relationship Count as specified by the params for a Version Specific Object Instance
		/// </summary>
		/// <param name="type"> Object Version Instance Type </param>
		/// <param name="id"> Object Version Instance ID </param>
		/// <param name="version"> Object Version </param>
		/// <param name="direction"> Relationship Direction(s) </param>
		/// <param name="objType"> Object Type Result Filter </param>
		/// <summary> </summary>
		public static string ObjectRelationshipCount( int type, int id, int version, RelationshipType direction,
			int objType = -1 )
		{
			var versionStr = version == -1 ? "latest" : version.ToString();

			var url = string.Format( "objects/{0}/{1}/{2}/relationships/count", type, id, versionStr );
			url += string.Format( "?{0}={1}", "direction", direction );

			// filter by specific type
			if( objType != -1 )
				url += string.Format( "&{0}={1}", "objType", objType );

			// return the result
			return url;
		}

		/// <summary>
		/// Get the Sub-Objects for a specific Object Version
		/// </summary>
		/// <param name="type"> Object Version Instance Type </param>
		/// <param name="id"> Object Version Instance ID </param>
		/// <param name="version"> Object Version </param>
		public static string SubObjects( int type, int id, int version )
		{
			var versionStr = version == -1 ? "latest" : version.ToString();

			return string.Format( "objects/{0}/{1}/{2}/subobjects", type, id, versionStr );
		}

		/// <summary>
		/// Get the Sub-Object Count for a specific Object Version
		/// </summary>
		/// <param name="type"> Object Version Instance Type </param>
		/// <param name="id"> Object Version Instance ID </param>
		/// <param name="version"> Object Version </param>
		public static string SubObjectCount( int type, int id, int version )
		{
			var versionStr = version == -1 ? "latest" : version.ToString();

			return string.Format( "objects/{0}/{1}/{2}/subobjects/count", type, id, versionStr );
		}

		/// <summary>
		/// Get the Properties of an Object for a specific Object Version
		/// </summary>
		/// <param name="type"> Object Version Instance Type </param>
		/// <param name="id"> Object Version Instance ID </param>
		/// <param name="version"> Object Version </param>
		/// <param name="forDisplay"> If true, the response will be filtered and sorted for display. Each object has several built-in properties which usually aren’t shown to the user. This parameter can be used to leave those out. </param>
		public static string Properties( int type, int id, int version, bool forDisplay )
		{
			var versionStr = version == -1 ? "latest" : version.ToString();

			var url = string.Format( "objects/{0}/{1}/{2}/properties", type, id, versionStr );

			if( forDisplay )
				url += string.Format( "?{0}={1}", "forDisplay", "True" );

			// return the result
			return url;
		}

		/// <summary>
		/// Get a specific Property on an a specific Object Version
		/// </summary>
		/// <param name="type"> Object Version Instance Type </param>
		/// <param name="id"> Object Version Instance ID </param>
		/// <param name="version"> Object Version </param>
		/// <param name="propID"> Property ID to return </param>
		public static string Property( int type, int id, int version, int propID )
		{
			var versionStr = version == -1 ? "latest" : version.ToString();

			return string.Format( "objects/{0}/{1}/{2}/properties/{3}", type, id, versionStr, propID );
		}

		/// <summary>
		/// Get the Properties of Multiple Objects
		/// </summary>
		public static string PropertiesOfMultipleObjects( ObjVer [] objects )
		{
			var url = string.Format( "objects/properties" );

			foreach( var objVer in objects )
			{
				var versionStr = objVer.Version == -1 ? "latest" : objVer.Version.ToString();

				url += string.Format( ";{0}/{1}/{2}", objVer.Type, objVer.ID, versionStr );
			}

			// return the result
			return url;
		}

		/// <summary>
		/// Get the Files for a specific Object Version
		/// </summary>
		/// <param name="type"> Object Version Instance Type </param>
		/// <param name="id"> Object Version Instance ID </param>
		/// <param name="version"> Object Version </param>
		public static string Files( int type, int id, int version )
		{
			var versionStr = version == -1 ? "latest" : version.ToString();

			return string.Format( "objects/{0}/{1}/{2}/files", type, id, versionStr );
		}

		/// <summary>
		/// Get a specific File on an specific Object Version
		/// </summary>
		/// <param name="type"> Object Version Instance Type </param>
		/// <param name="id"> Object Version Instance ID </param>
		/// <param name="version"> Object Version </param>
		/// <param name="fileID"> File ID </param>
		public static string File( int type, int id, int version, int fileID )
		{
			var versionStr = version == -1 ? "latest" : version.ToString();

			return string.Format( "objects/{0}/{1}/{2}/files/{3}", type, id, versionStr, fileID );
		}

		/// <summary>
		/// Get a specific File on an specific Object Version
		/// </summary>
		/// <param name="type"> Object Version Instance Type </param>
		/// <param name="id"> Object Version Instance ID </param>
		/// <param name="version"> Object Version </param>
		/// <param name="fileID"> File ID </param>
		/// <param name="macFormatting"> If true, appends a SHA-512 message authentication code at the end of the stream </param>
		public static string ObjectFileContent( int type, int id, int version, int fileID, bool macFormatting )
		{
			var versionStr = version == -1 ? "latest" : version.ToString();
			var url = string.Format( "objects/{0}/{1}/{2}/files/{3}/content", type, id, versionStr, fileID );

			if( macFormatting )
				url += string.Format( "?{0}={1}", "mac", macFormatting );

			// return result
			return url;
		}

		/// <summary>
		/// Get the preview thumbnail for a Version Specific Object Instance
		/// </summary>
		/// <param name="type"> Object Version Instance Type </param>
		/// <param name="id"> Object Version Instance ID </param>
		/// <param name="version"> Object Version </param>
		/// <param name="fileID"> File ID </param>
		/// <param name="force"> If true, the server streams an empty image instead of giving a HTTP 404 response </param>
		/// <param name="size"> Width and Height square pixel dimensions </param>
		/// <param name="width"> Optional - Overrides thumbnail width </param>
		/// <param name="height"> Optional - Overrides thumbnail height </param>
		public static string ObjectFileThumbnail( int type, int id, int version, int fileID, bool force = true, int size = 32,
			int? width = null, int? height = null )
		{
			var versionStr = version == -1 ? "latest" : version.ToString();

			object [] @params = { force, size, width, height };
			var url = string.Format( "objects/{0}/{1}/{2}/files/{3}/preview", type, id, versionStr, fileID );

			foreach( var param in @params )
			{
				// skip nulls
				if( param == null ) continue;

				Expression<Func<object>> expression = () => param;
				var name = ( expression.Body as MemberExpression ).Member.Name;

				url += string.Format( "?{0}={1}", name, param );
			}

			// return the result
			return url;
		}

		/// <summary>
		/// Get the Title for a Version Specific Object Instance
		/// </summary>
		/// <param name="type"> Object Version Instance Type </param>
		/// <param name="id"> Object Version Instance ID </param>
		/// <param name="version"> Object Version </param>
		/// <param name="fileID"> File ID </param>
		public static string ObjectFileTitle( int type, int id, int version, int fileID )
		{
			var versionStr = version == -1 ? "latest" : version.ToString();

			return string.Format( "objects/{0}/{1}/{2}/files/{3}/title", type, id, versionStr, fileID );
		}

		/// <summary>
		/// Performs an Object Search
		/// </summary>
		public static string SearchResults( string searchConditions )
		{
			return string.Format( "objects{0}", searchConditions );
		}

		/// <summary>
		/// Get the available Vaults on the server.
		/// </summary>
		public static string ServerVaults( bool onlineOnly )
		{
			return string.Format( "server/vaults?online={0}", onlineOnly );
		}

		/// <summary>
		/// Get the Structure.Objects
		/// </summary>
		public static string StructureObjectTypes( string filterType = "" )
		{
			if( string.IsNullOrWhiteSpace( filterType ) ) return string.Format( "structure/objecttypes" + filterType );

			filterType = filterType != "none" ? "?type=" + filterType : "";

			return string.Format( "structure/objecttypes" + filterType );
		}

		/// <summary>
		/// Get Structure.Object Where ID = typeID
		/// </summary>
		/// <param name="typeID"> Object Type ID </param>
		public static string StructureObjectType( int typeID )
		{
			return string.Format( "structure/objecttypes/{0}", typeID );
		}

		/// <summary>
		/// Get the icon for an Object Type
		/// </summary>
		/// <param name="typeID"> Object Type ID </param>
		public static string StructureObjectTypeIcon( int typeID )
		{
			return string.Format( "structure/objecttypes/{0}/icon", typeID );
		}

		/// <summary>
		/// Get the Structure.Classes
		/// </summary>
		public static string StructureClasses( int? objType, bool? byGroup )
		{
			if( objType == null && byGroup == null )
			{
				return string.Format( "structure/classes" );
			}
			if( objType != null && byGroup != null )
			{
				return string.Format( "structure/classes?objType={0}&byGroup={1}", objType, byGroup );
			}
			if( objType != null )
			{
				return string.Format( "structure/classes?objType={0}", objType );
			}
			return string.Format( "structure/classes?byGroup={0}", byGroup );
		}

		/// <summary>
		/// Get Structure.Class Where ID = classID
		/// </summary>
		/// <param name="classID"> Class ID </param>
		/// <param name="include"> Comma separated list of additional data sets to return. Currently only 'templates' are supported which will cause the response to include a list of available templates available to the class. </param>
		public static string StructureClass( int classID, bool include = false )
		{
			var param = include ? "?include=true" : "";
			return string.Format( "structure/classes/{0}{1}", classID, param );
		}

		/// <summary>
		/// Get the icon for a Class
		/// </summary>
		/// <param name="classID"> Class ID </param>
		/// <param name="size"> The icons supports only certain sizes and the real size on the received icon in the response might differ from the specified size. </param>
		public static string StructureClassIcon( int classID, int size = 16 )
		{
			var param = string.Format( "?size={0}", size );

			return string.Format( "structure/classes/{0}/icon{1}", classID, param );
		}

		/// <summary>
		/// Get Structure.Properties Where ID = propID
		/// </summary>
		/// <param name="propID"> Property ID </param>
		public static string StructureProperty( int propID )
		{
			return string.Format( "structure/properties/{0}", propID );
		}

		/// <summary>
		/// Get Structure.Workflows Where ID = workflowID
		/// </summary>
		/// <param name="workflowID"> Workflow ID </param>
		public static string StructureWorkflow( int workflowID )
		{
			return string.Format( "structure/workflows/{0}", workflowID );
		}

		/// <summary>
		/// Get the Structure.Workflows[ workflowID ].States
		/// </summary>
		/// <param name="workflowID"> Workflow ID </param>
		public static string StructureWorkflowStates( int workflowID )
		{
			return string.Format( "structure/workflows/{0}/states", workflowID );
		}

		/// <summary>
		/// Get the Structure.Workflows[ workflowID ].State[ stateID ]
		/// </summary>
		/// <param name="workflowID"> Workflow ID </param>
		/// <param name="stateID"> Workflow State ID </param>
		public static string StructureWorkflowState( int workflowID, int stateID )
		{
			return string.Format( "structure/workflows/{0}/states/{1}", workflowID, stateID );
		}

		/// <summary>
		/// Get the Structure.ValueLists[ listID ]
		/// </summary>
		/// <param name="listId"> Value List ID </param>
		/// <param name="propertydef"> Filters the items using the filter defined on the property definition </param>
		/// <param name="pN"> Filter items using defined property ID and its value. See search conditions. </param>
		/// <param name="parentItem"> Filters items by parent item. </param>
		/// <param name="ownerItem"> Filters items by owner item. </param>
		/// <param name="name"> Filter using name. Supports wildcards </param>
		/// <param name="page"> Retrieves only one page. Default page size is 100. </param>
		/// <param name="pagesize"> Defines page size. If page size is defined, retrieve only one page, default to first. </param>
		public static string StructureValueListItems( int listId, int? propertydef = null, int? pN = null,
			int? parentItem = null, int? ownerItem = null, string name = null, int? page = null, int? pagesize = null )
		{
			var param = "?";
			param += propertydef != null ? "propertydef=" + propertydef : "";
			param += pN != null ? "pN=" + pN : "";
			param += parentItem != null ? "parentItem=" + parentItem : "";
			param += ownerItem != null ? "ownerItem=" + ownerItem : "";
			param += name != null ? "name=" + name : "";
			param += page != null ? "page=" + page : "";
			param += pagesize != null ? "pagesize=" + pagesize : "";

			// TODO - Add Optional Params to this method
			return string.Format( "valuelists/{0}/items{1}", listId, param == "?" ? "" : param );
		}

		/// <summary>
		/// Get the Structure.ValueLists[ listID ].Items[ itemID ]
		/// </summary>
		/// <param name="listID"> Value List ID </param>
		/// <param name="itemID"> Value List Item ID </param>
		public static string StructureValueListItem( int listID, int itemID )
		{
			return string.Format( "valuelists/{0}/items/{1}", listID, itemID );
		}

		/// <summary>
		/// Get the Structure.ValueLists[ listID ].Items[ itemID ].Title
		/// </summary>
		/// <param name="listID"> Value List ID </param>
		/// <param name="itemID"> Value List Item ID </param>
		public static string StructureValueListItemTitle( int listID, int itemID )
		{
			return string.Format( "valuelists/{0}/items/{1}/title", listID, itemID );
		}

		/// <summary>
		/// Get Favorites for the User from this Session
		/// </summary>
		public static string Favorite( int type, int id )
		{
			var param = string.Format( "/{0}/{1}", type, id );
			return string.Format( "favorites{0}", param );
		}

		/// <summary>
		/// Get the Contents of a View
		/// </summary>
		public static string FolderContentItems( string path )
		{
			return string.Format( "views/{0}/items", path );
		}

		/// <summary>
		/// Get the ObjectVersions within a View
		/// </summary>
		public static string ViewContent( string path )
		{
			return string.Format( "views/{0}/objects", path );
		}

		/// <summary>
		/// Get the Content Count of a View
		/// </summary>
		public static string ViewItemCount( string path )
		{
			return string.Format( "views/{0}/items/count", path );
		}
	}
}