﻿using System;

namespace Solomon.Common.DocumentManagement.MFiles.WebAccess
{
	public class MfwsException : Exception
	{
		public MfwsException( ExceptionInfo error )
			: base(
				error.Message,
				error.InnerException != null
					? new MfwsException( error.InnerException )
					: null )
		{
		}
	}
}