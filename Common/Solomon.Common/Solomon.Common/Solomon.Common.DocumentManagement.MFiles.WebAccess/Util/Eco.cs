﻿using System;

namespace Solomon.Common.DocumentManagement.MFiles.WebAccess
{
	/// <summary>
	/// ECO - The Effortless Checkout Object. Disposable.
	/// </summary>
	public class Eco : IDisposable
	{
		private readonly bool checkedOutRequired;

		/// <summary>
		/// Cascading Constructor Overload
		/// </summary>
		/// <param name="api"> Parent Api </param>
		/// <param name="versionInfo"> </param>
		public Eco( Api api, ObjectVersion versionInfo ) : this( api, versionInfo.ObjVer )
		{
		}

		/// <summary>
		/// Cascading Constructor Overload
		/// </summary>
		/// <param name="api"> Parent Api </param>
		/// <param name="objVer"> Target ObjVer </param>
		public Eco( Api api, ObjVer objVer ) : this( api, objVer.Type, objVer.ID, objVer.Version )
		{
		}

		/// <summary>
		/// Constructor Base
		/// </summary>
		/// <param name="api"> Parent Api </param>
		/// <param name="type"> Object Instance Type </param>
		/// <param name="id"> Object Instance ID </param>
		/// <param name="version"> Object Instance Version </param>
		public Eco( Api api, int type, int id, int version = -1 )
		{
			Api = api;
			Info = Api.Get.Object( type, id, version );
			checkedOutRequired = !Info.ObjectCheckedOut;

			if( checkedOutRequired )
				Info = Api.CheckOut( Info.ObjVer );
		}

		private Api Api { get; set; }
		public ObjectVersion Info { get; set; }

		/// <summary>
		/// Self Cleanup
		/// </summary>
		public void Dispose()
		{
			if( checkedOutRequired )
				Info = Api.CheckIn( Info.ObjVer );
		}
	}
}