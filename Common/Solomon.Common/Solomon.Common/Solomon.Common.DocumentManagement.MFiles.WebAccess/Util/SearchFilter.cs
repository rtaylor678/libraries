﻿namespace Solomon.Common.DocumentManagement.MFiles.WebAccess
{
	public enum SearchFilter
	{
		QuickSearch,
		PropertyValue,
		ValueListItem,
		ObjectType,
		Deleted
	}
}