﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;

namespace Solomon.Common.DocumentManagement.MFiles.WebAccess
{
	public static class Util
	{
		public static string CastToString( object value, MFDataType type )
		{
			switch( type )
			{
				case MFDataType.MultiSelectLookup:
					var displayValue = "";
					( value as Lookup [] ).ToList().ForEach( l => displayValue += l.DisplayValue + "; " );

					return displayValue;
				case MFDataType.Lookup:
					return ( value as Lookup ).DisplayValue;
				case MFDataType.Boolean:
					return ( ( bool ) value ).ToString();
				case MFDataType.Date:
					return ( ( DateTime ) value ).ToShortDateString();
				case MFDataType.FILETIME:
					return ( ( DateTime ) value ).ToLongTimeString();
				case MFDataType.Floating:
					return ( ( float ) value ).ToString( CultureInfo.CurrentCulture );
				case MFDataType.Integer:
					return ( ( int ) value ).ToString();
				case MFDataType.Integer64:
					return ( ( Int64 ) value ).ToString();
				case MFDataType.Time:
					return ( ( DateTime ) value ).ToShortTimeString();
				case MFDataType.Timestamp:
					return ( ( DateTime ) value ).ToLongTimeString();
				default:
					return ( ( string ) value );
			}
		}

        //public static Stream PathToStream(string filepath)
        //{
        //    using (var fileStream = File.OpenRead(filepath))
        //    {
        //        var memStream = new MemoryStream();
        //        memStream.SetLength(fileStream.Length);
        //        fileStream.Read(memStream.GetBuffer(), 0, (int)fileStream.Length);

        //        return fileStream;
        //    }
        //}
	}
}