﻿namespace Solomon.Common.DocumentManagement.MFiles.WebAccess
{
	/// <summary>
	/// Formatted DELETE Resource URIs
	/// </summary>
	internal static class _Delete
	{
		/// <summary>
		/// Deletes an Object from in the Vault
		/// </summary>
		public static string Object( int type, int id, int version = -1, bool force = true, bool allVersions = true )
		{
			var versionStr = version == -1 ? "latest" : version.ToString();

			return string.Format( "objects/{0}/{1}/{2}?force={3}&allVersions={4}", type, id, versionStr, force, allVersions );
		}
	}
}