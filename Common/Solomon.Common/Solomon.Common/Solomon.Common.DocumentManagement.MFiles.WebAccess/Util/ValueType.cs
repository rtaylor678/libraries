﻿namespace Solomon.Common.DocumentManagement.MFiles.WebAccess
{
	public enum ValueType
	{
		Null,
		ID,
		TypedValue,
		IncludeDeleted
	}
}