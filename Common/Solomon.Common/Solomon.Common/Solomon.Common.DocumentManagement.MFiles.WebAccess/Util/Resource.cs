﻿using Solomon.Common.DocumentManagement.MFiles.WebAccess.Mfws;

namespace Solomon.Common.DocumentManagement.MFiles.WebAccess
{
	/// <summary>
	/// M-Files Web Service resource.
	/// </summary>
	public class Resource
	{
		private readonly MfwsClient client;

		public Resource( MfwsClient client, string path )
		{
			this.client = client;
			Path = path;
		}

		public string Path { get; protected set; }

		public TResult Get<TResult>()
		{
			return client.Get<TResult>( Path );
		}

		public TResult Post<TResult>( object data )
		{
			return client.Post<TResult>( Path, data );
		}

		public TResult Put<TResult>( object data )
		{
			return client.Put<TResult>( Path, data );
		}

		public void Delete()
		{
			client.Delete( Path );
		}
	}
}