﻿using System;
using System.Collections.Generic;
using System.Web.Helpers;

namespace Solomon.Common.DocumentManagement.MFiles.WebAccess
{
	/// <summary>
	/// Search Conditions Class
	/// </summary>
	public class SearchConditions
	{
		/// <summary>
		/// Search Conditions
		/// </summary>
		public List<SearchCondition> Conditions = new List<SearchCondition>();

		/// <summary>
		/// URI Search String
		/// </summary>
		public string SearchString
		{
			get { return "?" + string.Join( "&", Conditions ); }
		}

		/// <summary>
		/// Filter By Deleted = value
		/// </summary>
		/// <returns> </returns>
		public void Deleted( bool value )
		{
			Conditions.Add(
				new SearchCondition(
					SearchFilter.Deleted,
					ConditionType.Equal,
					ValueType.TypedValue,
					Json.Encode( new TypedValue { DataType = MFDataType.Boolean, Value = value } )
					)
				);
		}

		/// <summary>
		/// Filter Object Type or Value List Type by ID
		/// </summary>
		/// <returns> </returns>
		public void ObjType( int id, ConditionType cType = ConditionType.Equal )
		{
			Conditions.Add(
				new SearchCondition(
					SearchFilter.ObjectType,
					cType,
					ValueType.TypedValue,
					Json.Encode( new TypedValue { DataType = MFDataType.Boolean, Value = id } )
					)
				);
		}

		/// <summary>
		/// Filter Value List Item Search by ID
		/// </summary>
		/// <returns> </returns>
		public void ValueListItem( int id, ConditionType cType = ConditionType.Equal )
		{
			Conditions.Add(
				new SearchCondition(
					SearchFilter.ObjectType,
					cType,
					ValueType.TypedValue,
					Json.Encode( new TypedValue { DataType = MFDataType.Boolean, Value = id } )
					)
				);
		}

		/// <summary>
		/// Generic Search Filter
		/// </summary>
		/// <returns> </returns>
		public void Add( SearchFilter filterType, ValueType valueType, ConditionType cType, string value )
		{
			Add(
				new SearchCondition(
					filterType,
					cType,
					valueType,
					value
					)
				);
		}

		public void Add( SearchCondition sc )
		{
			Conditions.Add( sc );
		}
	}

	/// <summary>
	/// Search Condition Class
	/// </summary>
	public class SearchCondition
	{
		/// <summary>
		/// Used to Translate the Condition Type to a URI String
		/// </summary>
		private static readonly Dictionary<ConditionType, string> ConditionTypeEncode = new Dictionary<ConditionType, string>
		{
			{ ConditionType.Contains, "*=" },
			{ ConditionType.Equal, "=" },
			{ ConditionType.GreaterThan, ">>=" },
			{ ConditionType.GreaterThanEqualTo, ">=" },
			{ ConditionType.LessThan, "<<=" },
			{ ConditionType.LessThanEqualTo, "<=" },
			{ ConditionType.MatchesWildcard, "**=" },
			{ ConditionType.NotEqual, "!" },
			{ ConditionType.StartsWith, "^=" }
		};

		/// <summary>
		/// Used to Translate the Search Filter to a URI String
		/// </summary>
		private static readonly Dictionary<SearchFilter, string> SearchFilterEncode = new Dictionary<SearchFilter, string>
		{
			{ SearchFilter.Deleted, "d" },
			{ SearchFilter.ObjectType, "o" },
			{ SearchFilter.PropertyValue, "p" },
			{ SearchFilter.QuickSearch, "q" },
			{ SearchFilter.ValueListItem, "vl" }
		};

		/// <summary>
		/// Used to Translate the Value Type to a URI String
		/// </summary>
		private static readonly Dictionary<ValueType, string> ValueTypeEncode = new Dictionary<ValueType, string>
		{
			{ ValueType.ID, "e" },
			{ ValueType.Null, "%00" },
			{ ValueType.IncludeDeleted, "d=include" },
			{ ValueType.TypedValue, null }
		};

		/// <summary>
		/// Self Populating Constructor
		/// </summary>
		/// <param name="searchFilter"> </param>
		/// <param name="conditionType"> </param>
		/// <param name="valueType"> </param>
		/// <param name="value"> Stringified Value </param>
		/// <param name="id"> Passed when Search Filter = Property or Value List </param>
		public SearchCondition( SearchFilter searchFilter, ConditionType conditionType, ValueType valueType, string value, int id = -1 )
		{
			SearchFilter = searchFilter;
			if( id == -1 && SearchFilter == SearchFilter.PropertyValue || SearchFilter == SearchFilter.ValueListItem )
				throw new Exception(
					"For Search Filters ( Property or Value List ), you must pass a valid Property ID or Value List ID." );

			ID = id;
			ConditionType = conditionType;
			ValueType = valueType;
			Value = value;
		}

		/// <summary>
		/// Default Empty Constructor
		/// </summary>
		public SearchCondition()
		{
		}

		/// <summary>
		/// Query Value
		/// </summary>
		public string Value { get; set; }

		/// <summary>
		/// Value List ID or Property ID
		/// </summary>
		public int ID { get; set; }

		/// <summary>
		/// Value Type
		/// </summary>
		public ValueType ValueType { get; set; }

		/// <summary>
		/// Search Filter
		/// </summary>
		public SearchFilter SearchFilter { get; set; }

		/// <summary>
		/// How the Filter is Applied
		/// </summary>
		public ConditionType ConditionType { get; set; }

		/// <summary>
		/// ToString() implementation for use with string.Join on SearchCondition[]'s
		/// </summary>
		/// <returns> </returns>
		public override string ToString()
		{
			string typedValueString = null;

			if( ValueTypeEncode[ ValueType ] == null )
			{
				TypedValue typedValue = Json.Decode<TypedValue>( Value );

				if( typedValue.Value != null )
					typedValueString = typedValue.Value.ToString();
				else if( typedValue.Lookup != null )
					typedValueString = typedValue.Lookup.Item.ToString();
			}

			return string.Format(
				"{0}{1}{2}{3}",
				SearchFilterEncode[ SearchFilter ],
				ID == -1 ? "" : ID.ToString(),
				ConditionTypeEncode[ ConditionType ],
				typedValueString );
		}
	}
}