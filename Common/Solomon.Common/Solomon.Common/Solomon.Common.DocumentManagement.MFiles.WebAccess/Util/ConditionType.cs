﻿namespace Solomon.Common.DocumentManagement.MFiles.WebAccess
{
	public enum ConditionType
	{
		Equal,
		NotEqual,
		LessThan,
		LessThanEqualTo,
		GreaterThan,
		GreaterThanEqualTo,
		MatchesWildcard,
		Contains,
		StartsWith
	}
}