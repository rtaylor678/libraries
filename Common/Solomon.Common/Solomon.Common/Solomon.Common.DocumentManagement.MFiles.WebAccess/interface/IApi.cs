﻿using Solomon.Common.DocumentManagement.MFiles.WebAccess.Mfws;

namespace Solomon.Common.DocumentManagement.MFiles.WebAccess
{
	internal interface IApi
	{
		/// <summary>
		/// M-Files Rest API Client Helper Object.  Used privately for this class.
		/// </summary>
		MfwsClient Client { get; set; }

		/// <summary>
		/// Vault Retrieval Operations
		/// </summary>
		Get Get { get; set; }

		/// <summary>
		/// Vault Set or Post Operations
		/// </summary>
		Set Set { get; set; }

		/// <summary>
		/// Vault Deletion Operations
		/// </summary>
		Delete Delete { get; set; }

		/// <summary>
		/// Vault Object Creation Operations
		/// </summary>
		Create Create { get; set; }

		/// <summary>
		/// Common Object Instance Creation
		/// </summary>
		CreateInstance CreateInstance { get; set; }

		/// <summary>
		/// Vault Structure Modification and Retrieval Operations
		/// </summary>
		Structure Structure { get; set; }
	}
}